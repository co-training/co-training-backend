# Company training backend

## Description

As the name says, the company training project is a tool that i've created to ensure the development of technical skills of employees within a company.

Each employee will have a budget each year which can be spent on training. Employees will also be able to earn money by giving training. 
The budget can turn into real money at the end of the year.

This tool will help employees to organize trainings. An employee can plan a training, define its price and receive feedbacks from other employees.                                                               

The tool offers the possibility to an administrator to create user accounts of the application, to add a budget to each account and to add teams.

The frontend part of the app has been build using the awesome framework Vue.js

The backend , built with my favorite framework Spring, part has been split to multiple modules so we can have the
flexibility to deploy the app on different cloud providers. For now the project supports two solutions :

- Kubernetes , We will use the google kubernetes engine for deployment
- AWS lambdas , as you guessed we will use amazon lambdas for deployment

Of course, I will provide an explanation of how you can install the tool on your local machine for development. I will
also show how run it in a docker environment so you can just test it.

The project components :

* UI project: https://gitlab.com/co-training/co-training-gui
* Backend project: https://gitlab.com/co-training/co-training-backend
* Ops project : https://gitlab.com/co-training/co-training-deployment

## Live demo

Check the frontend project for a live demo :

- https://gitlab.com/co-training/co-training-gui#live-demo

To know more about how to deploy the app demo on heroku check this link :

- https://gitlab.com/co-training/co-training-backend/-/blob/feature/heroku-deployment/docs/heroku.md

## Functionality overview

As this is project has been developed using
the [Api-First Development Approach](https://dzone.com/articles/an-api-first-development-approach-1) and
the [Contract First](https://swagger.io/blog/api-design/design-first-or-code-first-api-development/) development, I
think that this contract will explain the app functionalities better than any other documentation :

- https://app.swaggerhub.com/apis-docs/mouhamedali/co-training/1.0.0

## Code structure

Here's the code structure.

```
co-training-backend
│
└─── co-training-common                : This module represents the business layer of our app
│
└─── co-training-dao                   : This module represents the persistence layer of our app (jpa entities)
│
└─── co-training-rest                  : This module is one of the controllers of our app and it uses the rest api to expose the business layer.
│
└─── co-training-rest-karate           : This module contains a bunch of karate tests, it represents the functionnal tests of the rest module.
```

You can find further explanations in the source directory of each module.

## Prerequisites

To set up your development environment, you will need to :

- install java 8 (or above)
- install maven
- install git to clone the project
- you favorite IDE (i'm using intellij)
- [docker](https://docs.docker.com/get-docker/)

As i'm using lombok, you have to add it your ide to not face compilation errors. Check these links :

- https://projectlombok.org/setup/intellij (I had an issue with the 2020.3 version. This is the
  solution : https://youtrack.jetbrains.com/issue/IDEA-250718#focus=Comments-27-4418347.0-0)
- https://www.baeldung.com/lombok-ide

To add sonarlint to intellij (as i'm using it), try this link :

- https://www.sonarlint.org/intellij

## Installing

To clone the project, run this command :

```shell script
$ git clone https://gitlab.com/co-training/co-training-backend.git
$ cd co-training-backend
```

And run this command to package the project and run test :

```shell script
$ mvn clean install
```

If everything turns out alright, you should end up with this result :

```
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Summary:
[INFO] 
[INFO] co-training-backend 1.0.0-SNAPSHOT ................. SUCCESS [  1.509 s]
[INFO] co-training-dao 1.0.0 .............................. SUCCESS [ 12.123 s]
[INFO] co-training-common 1.0.0 ........................... SUCCESS [ 11.031 s]
[INFO] co-training-rest 1.0.0.DEV-SNAPSHOT ................ SUCCESS [ 28.880 s]
[INFO] co-training-rest-karate 1.0.0 ...................... SUCCESS [ 20.540 s]
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  01:14 min
[INFO] Finished at: 2021-01-22T20:58:58+01:00
[INFO] ------------------------------------------------------------------------
```

## Run sonarqube

To ensure the quality of my code, i have used sonarqube to detect the several issues in the code. Before running sonar we have to start its server.
I've used sonar as a docker container. You can start it using this command :

```shell script
$ docker-compose start sonarqube
```

and wait for it to finish loading :

```shell script
$ docker-compose log -f sonarqube
```

To scan the code, you can use this command :

```shell script
$ mvn clean verify sonar:sonar
```

Now you can access sonar via this link :

- http://localhost:9000

![sonar-home](/uploads/2f32e0fa3e515569bdf47ca793b63de6/sonar-home.png)

the default username/password for sonar is admin/admin.

To stop the server you can use this command :

```shell script
$ docker-compose stop sonarqube
```

## Built With

* Java - oracle jdk "1.8.0_101"
* [Maven](https://maven.apache.org/) - Dependency Management (version 3.6.0)
* [Intellij](https://www.jetbrains.com/) - IDE (version 2020.2)
* [Spring Boot](https://spring.io/projects/spring-boot) - (v2.3.1.RELEASE)
* [Docker](https://www.docker.com/) - (version 19.03.5, build 633a0ea838)

## Contribution

Welcome to any contributor. You can contact me to give you write access to the repo.

This project is totally open source, you can fork it, change it, distribute it. Well the licence is MIT.

You can also use this link to check my trello board, the backlog and features to be added in a future version :

- https://trello.com/invite/b/ASRrmSDR/ee92d6988253bf27b321e6751d21bcb7/co-training

![trello](/uploads/cc6480e7b0b3fb17d5eaa716d61d728f/trello.png)

## Authors

* **Amdouni Mohamed Ali** [[github](https://github.com/mouhamed-ali)]

![co-training](/uploads/546371ffbbb00b6e85b31d7db79196f8/co-training.png)