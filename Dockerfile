FROM openjdk:8-jdk-alpine as builder
# copy the app bundle
ARG JAR_FILE=co-training-rest/target/co-training-rest*.jar
COPY ${JAR_FILE} application.jar
# extract layers
RUN java -Djarmode=layertools -jar application.jar extract


FROM openjdk:8-jdk-alpine
# User a custom user to run the web app. Do not use the root, it's not a good practice.
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
# Expose the 8090 port so the app can hadle external connections
EXPOSE 8090
# define a workdir
WORKDIR /usr/app
# make the owner of the work directory spring:spring
USER root
RUN chown spring:spring /usr/app
# return to spring user
USER spring:spring
# Use the layers mecanism, this will prevent the download of the project depedencies each time we build a new image
COPY --chown=spring:spring --from=builder dependencies/ ./
COPY --chown=spring:spring --from=builder spring-boot-loader/ ./
COPY --chown=spring:spring --from=builder snapshot-dependencies/ ./
# snapshot dependencies contains co-training-common and co-training-dao in my case
COPY --chown=spring:spring --from=builder application/ ./
# run the spring loader as a simple java main class
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]

