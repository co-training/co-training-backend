package cotraining.levels;

import com.intuit.karate.junit5.Karate;

public class LevelsRunner {

    @Karate.Test
    Karate testAll() {
        return Karate.run().relativeTo(getClass());
    }
}
