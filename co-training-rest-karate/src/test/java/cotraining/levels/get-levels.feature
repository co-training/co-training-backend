Feature: Get all the app levels

  Background:
    * url APP_URL
    * def levelsBase = 'levels'
    * def signIn = call read('classpath:signin-user.feature')
    * def userAuthToken = signIn.userAuthToken

  Scenario: Get levels

    Given path levelsBase
    And header Authorization = userAuthToken
    When method GET
    Then status 200
    And match $ == '#[]'
    # response is an array
    And match $ contains any ["BEGINNER","INTERMEDIATE","ADVANCED"]