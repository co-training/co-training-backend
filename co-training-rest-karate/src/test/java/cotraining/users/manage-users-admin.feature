Feature: Read users data using an admin token

  Background:
    * url APP_URL
    * def usersBase = 'users'
    * def adminID = 99
    * def userID = 88
    * def signInAdmin = call read('classpath:signin-admin.feature')
    * def adminAuthToken = signInAdmin.adminAuthToken


  Scenario: Get all the users

    Given path usersBase
    And header Content-Type = 'application/json'
    And header Authorization = adminAuthToken
    And param page = 0
    And param size = 50
    When method GET
    Then status 200
    And assert response.length <= 50
    And match response[0].firstName == '#notnull'
    And match response[0].lastName == '#notnull'
    And match response[0].job == '#notnull'
    And match response[0].id == '#notnull'
    And match response[0].team == '#object'


  Scenario: Get an admin user details

    Given path usersBase, adminID
    And header Authorization = adminAuthToken
    When method GET
    Then status 200
    And match $ == '#notnull'
    And match $.email == '#notnull'
    And match $.isEmailEnabled == false
    And match $.dockerHub == '#notnull'
    And match $.isDockerHubEnabled == true
    And match $.birthday == '#notnull'
    And match $.id == adminID


  Scenario: Get a non admin user details

    Given path usersBase, userID
    And header Authorization = adminAuthToken
    When method GET
    Then status 200
    And match $ == '#notnull'
    And match $.facebook == '#notnull'
    And match $.isFacebookEnabled == true
    And match $.phoneNumber == '#notnull'
    And match $.isPhoneNumberEnabled == false
    And match $.birthday == '#notnull'
    And match $.id == userID


  Scenario: Get an admin user description

    Given path usersBase, adminID, 'describe'
    And header Authorization = adminAuthToken
    When method GET
    Then status 200
    And match $ == '#notnull'
    And match $.email == '#null'
    And match $.isEmailEnabled == false
    And match $.dockerHub == '#notnull'
    And match $.isDockerHubEnabled == true
    And match $.birthday == '#null'
    And match $.id == adminID


  Scenario: Get a non admin user description

    Given path usersBase, userID, 'describe'
    And header Authorization = adminAuthToken
    When method GET
    Then status 200
    And match $ == '#notnull'
    And match $.facebook == '#notnull'
    And match $.isFacebookEnabled == true
    And match $.phoneNumber == '#null'
    And match $.isPhoneNumberEnabled == false
    And match $.birthday == '#null'
    And match $.id == userID


  Scenario: Get an admin resource

    Given path usersBase, adminID, 'resources'
    And header Authorization = adminAuthToken
    When method GET
    Then status 200
    And match $.id == adminID
    And match $.budget == '#notnull'
    And match $.nbDays == '#notnull'
    And match $.jackpot == '#notnull'


  Scenario: Get a user resource

    Given path usersBase, userID, 'resources'
    And header Authorization = adminAuthToken
    When method GET
    Then status 200
    And match $.id == userID
    And match $.budget == '#notnull'
    And match $.nbDays == '#notnull'
    And match $.jackpot == '#notnull'