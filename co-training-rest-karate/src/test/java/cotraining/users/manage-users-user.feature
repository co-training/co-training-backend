Feature: Read users data using a user token

  Background:
    * url APP_URL
    * def usersBase = 'users'
    * def adminID = 99
    * def userID = 88
    * def signIn = call read('classpath:signin-user.feature')
    * def userAuthToken = signIn.userAuthToken


  Scenario: Get all the users

    Given path usersBase
    And header Content-Type = 'application/json'
    And header Authorization = userAuthToken
    And param page = 0
    And param size = 50
    When method GET
    Then status 403
    And match response == '#present'
    And match response.error_description == 'Access is denied'


  Scenario: Get the user details

    Given path usersBase, userID
    And header Authorization = userAuthToken
    When method GET
    Then status 200
    And match $ == '#notnull'
    And match $.facebook == '#notnull'
    And match $.isFacebookEnabled == true
    And match $.phoneNumber == '#notnull'
    And match $.isPhoneNumberEnabled == false
    And match $.birthday == '#notnull'
    And match $.id == userID


  Scenario: Get another user details

    Given path usersBase, adminID
    And header Authorization = userAuthToken
    When method GET
    Then status 405
    And match $.error == 'Method Not Allowed'


  Scenario: Get the user description

    Given path usersBase, userID, 'describe'
    And header Authorization = userAuthToken
    When method GET
    Then status 200
    And match $ == '#notnull'
    And match $.facebook == '#notnull'
    And match $.isFacebookEnabled == true
    And match $.phoneNumber == '#null'
    And match $.isPhoneNumberEnabled == false
    And match $.birthday == '#null'
    And match $.id == userID


  Scenario: Get another user description

    Given path usersBase, adminID, 'describe'
    And header Authorization = userAuthToken
    When method GET
    Then status 200
    And match $ == '#notnull'
    And match $.email == '#null'
    And match $.isEmailEnabled == false
    And match $.dockerHub == '#notnull'
    And match $.isDockerHubEnabled == true
    And match $.birthday == '#null'
    And match $.id == adminID


  Scenario: Get the user resource

    Given path usersBase, userID, 'resources'
    And header Authorization = userAuthToken
    When method GET
    Then status 200
    And match $.id == userID
    And match $.budget == '#notnull'
    And match $.nbDays == '#notnull'
    And match $.jackpot == '#notnull'


  Scenario: Get another user resource

    Given path usersBase, adminID, 'resources'
    And header Authorization = userAuthToken
    When method GET
    Then status 405
    And match $.error == 'Method Not Allowed'