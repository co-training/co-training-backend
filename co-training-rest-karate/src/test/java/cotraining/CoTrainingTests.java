package cotraining;

import com.intuit.karate.junit5.Karate;

class CoTrainingTests {


    @Karate.Test
    Karate testAll() {
        return Karate.run().relativeTo(getClass());
    }

    // https://github.com/intuit/karate#junit-5 ; to get more advanced config

}