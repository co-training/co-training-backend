package cotraining.trainings;

import com.intuit.karate.junit5.Karate;

public class TrainingsRunner {

    @Karate.Test
    Karate testAll() {
        return Karate.run().relativeTo(getClass());
    }
}
