Feature: CRUD on the training resource

  Background:
    * url APP_URL
    * def trainingsBase = 'trainings'
    * def usersBase = 'users'
    * def reviewsBase = 'reviews'
    * def adminID = 99
    * def userID = 88
    * def signIn = call read('classpath:signin-user.feature')
    * def userAuthToken = signIn.userAuthToken


  Scenario: Create a new training

    Given path trainingsBase
    And header Content-Type = 'application/json'
    And header Authorization = userAuthToken
    And request read('new-training.json')
    When method POST
    Then status 201
    And match response == ''


  Scenario: Manage a training resource

    * def requirementSchema = { id: '#number', description: '#string'}
    * def criteriaSchema = { id: '#number', description: '#string'}
    * def tagSchema = { id: '#number', value: '#string'}
    * def interestsSkillsSchema = { id: '#number', description: '#string'}
    * def isValidTime = read('time-validator.js')
    # Get all trainings
    Given path trainingsBase
    And header Content-Type = 'application/json'
    And header Authorization = userAuthToken
    And param page = 0
    And param size = 50
    And param start = '2220-02-17'
    And param end = '2220-09-05'
    When method GET
    Then status 200
    And assert response.length <= 50
    And match each response ==
    # validate the training response against a json schema validation , TODO move this schema in a standalone file
    """
    {
      "id": "#number",
      "name": "#string",
      "description": "#string",
      "requirements": '##[] (requirementSchema)',
      "criterias": '##[] (criteriaSchema)',
      "level": "#string",
      "tags": '##[] (tagSchema)',
      "startingDate": "#? isValidTime(_)",
      "duration": "#number",
      "minParticipants": "#number",
      "maxParticipants": "#number",
      "image": "##string",
      "price": "#number",
      "author": {
        "firstName": "#string",
        "lastName": "#string",
        "job": "#string",
        "team": {
          "id": "#number",
          "name": "#string"
        },
        "photo": "##string",
        "about": "##string",
        "interestsSkills": '##[] (interestsSkillsSchema)',
        "email": "##string",
        "isEmailEnabled": "##boolean",
        "site": "##string",
        "isSiteEnabled": "##boolean",
        "linkedin": "##string",
        "isLinkedinEnabled": "##boolean",
        "youtube": "##string",
        "isYoutubeEnabled": "##boolean",
        "github": "##string",
        "isGithubEnabled": "##boolean",
        "gitlab": "##string",
        "isGitlabEnabled": "##boolean",
        "dockerHub": "##string",
        "isDockerHubEnabled": "##boolean",
        "twitter": "##string",
        "isTwitterEnabled": "##boolean",
        "facebook": "##string",
        "isFacebookEnabled": "##boolean",
        "phoneNumber": "##string",
        "isPhoneNumberEnabled": "##boolean",
        "birthday": "##? isValidTime(_)",
        "id": "#number"
      }
    }
    """
    And def trainingID = response[0].id

    # Get the last created training
    Given path trainingsBase, trainingID
    And header Content-Type = 'application/json'
    And header Authorization = userAuthToken
    When method GET
    Then status 200
    And match response contains {"id": #(trainingID),  "name": "Spring boot tutorial",  "description": "An introduction to the spring boot framework", "level": "BEGINNER", "startingDate": "2220-08-26", "duration": 5, "minParticipants": 5, "maxParticipants": 25, "price": 9.99}
    And match response.requirements == '#[2]'
    # requirements is an array of 2 elements
    And match response.criterias == '#[1] criteriaSchema'
    # criterias is an array of one element of type criteriaSchema
    And match response.tags == '##[_ > 1]'
    # tags optionally present (or null) and should be an array of size greater than 1
    And match response.author contains {"firstName": "Gerard", "lastName": "Lavoine", "id": #(userID)}

    # Get the given trainings
    Given path usersBase, userID, trainingsBase, 'given'
    And header Content-Type = 'application/json'
    And header Authorization = userAuthToken
    When method GET
    Then status 200
    And match response contains deep {"id": #(trainingID),  "name": "Spring boot tutorial",  "description": "An introduction to the spring boot framework", "level": "BEGINNER", "startingDate": "2220-08-26", "duration": 5, "minParticipants": 5, "maxParticipants": 25, "price": 9.99, "author": {"firstName": "Gerard", "lastName": "Lavoine", "id": #(userID)}}

    # Participate last created training, the owner of the training cannot participate to its training TODO uncomment the test below TODO FRONT-TEST
    #Given path trainingsBase, trainingID, 'participate'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #When method GET
    #Then status 400
    #And match response.error == "Bad Request"
    #And match response.message == "Sorry, you cannot participate to this training"

    # TODO this file should be dedicated to only training tests and we have to add common tests to test the whishlist, resources, participants ...
    # Get the user followed trainings, it should be empty as the last test fails
    #Given path usersBase, userID, trainingsBase, 'followed'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #When method GET
    #Then status 200
    #And match $ == '#[0]'

    # Get training's participants, for now we haven't any participant TODO check why this tests does not work
    #Given path trainingsBase, trainingID, 'participants'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #And param page = 0
    #And param size = 50
    #When method GET
    #Then status 200
    #And match $ == '#[0]'

    # Leave a training, the owner of the training cannot leave to its training
    #Given path trainingsBase, trainingID, 'leave'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #When method GET
    #Then status 400
    #And match response.error == "Bad Request"
    #And match response.message == "Sorry, you are not one of the participants of this training"

    ## Add the last training to the whishlist, a user can add its training to the whishlist
    #Given path trainingsBase, trainingID, 'whishlist', 'add'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #When method GET
    #Then status 200
    #And match $ == ''

    # Get the user whishlist
    #Given path usersBase, userID, trainingsBase, 'whishlist'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #When method GET
    #Then status 200
    #And match response contains deep {"id": #(trainingID),  "name": "Spring boot tutorial",  "description": "An introduction to the spring boot framework", "level": "BEGINNER", "startingDate": "2220-08-26", "duration": 5, "minParticipants": 5, "maxParticipants": 25, "price": 9.99, "author": {"firstName": "Gerard", "lastName": "Lavoine", "id": #(userID)}}

    ## Remove the last training from the whishlist
    #Given path trainingsBase, trainingID, 'whishlist', 'remove'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #When method GET
    #Then status 200
    #And match $ == ''

    # Get the user whishlist
    #Given path usersBase, userID, trainingsBase, 'whishlist'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #When method GET
    #Then status 200
    #And match response !contains {"id": #(trainingID)}

    # Add a new review to the last created training - the author of a training cannot give feedbacks
    #Given path reviewsBase
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #And request read('new-review.json')
    # the review will be attached to the current user whatever the user defined in the request body
    #When method POST
    #Then status 201
    #And match response == ''

    # Get the user review
    #Given path reviewsBase, 'search'
    #And header Content-Type = 'application/json'
    #And header Authorization = userAuthToken
    #And param page = 0
    #And param size = 50
    #And param trainingId = trainingID
    #And param uid = userID
    #When method GET
    #Then status 200
    #And def isToday =
    #  """
    #  function(inputDate) {
    #    var SimpleDateFormat = Java.type('java.text.SimpleDateFormat');
    #    var sdf = new SimpleDateFormat("yyyy-MM-dd");
    #    var Date = Java.type('java.util.Date');
    #    var date = new Date();
    #    karate.log('The current date is ',sdf.format(date));
    #    return sdf.format(date) === inputDate;
    #  }
    #  """
    #And match response contains deep { "stars": "3",  "comment": "This is a fabulous training.",  "isAnonymous": false, "author": {"firstName": "Gerard", "lastName": "Lavoine", "id": #(userID)}, "date": "#? isToday(_)"}
