Feature: Get all teams

  Background:
    * url APP_URL

  Scenario: Get all teams of the app

    Given path 'teams'
    When method GET
    Then status 200
    And match $ == '#[8]'
    And match $ contains read('teams.json')
  # we are using the h2 spring profile of our rest app which will load the h2 database, you can find data in data-h2
  # script file of the project
  # we will assert in this test that the number of returned teams is 8

# To know more about script structure check this url : https://github.com/intuit/karate#script-structure