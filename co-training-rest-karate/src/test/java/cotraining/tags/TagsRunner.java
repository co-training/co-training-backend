package cotraining.tags;

import com.intuit.karate.junit5.Karate;

public class TagsRunner {

    @Karate.Test
    Karate testAll() {
        return Karate.run().relativeTo(getClass());
    }
}
