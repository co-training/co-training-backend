Feature: Get all the app tags

  Background:
    * url APP_URL
    * def tagsBase = 'tags'
    * def signIn = call read('classpath:signin-user.feature')
    * def userAuthToken = signIn.userAuthToken

  Scenario: Get tags

    Given path tagsBase, 'search'
    And header Authorization = userAuthToken
    And param page = 0
    And param size = 50
    And param startOfTag = ''
    When method GET
    Then status 200
    And match $ == '#[]'
    # response is an array