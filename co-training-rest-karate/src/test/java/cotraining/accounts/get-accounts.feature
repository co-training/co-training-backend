Feature: Get all accounts of the app

  Background:
    * url APP_URL
    * def accountsBase = 'accounts'

  Scenario: Get all accounts by an admin

    * def signInAdmin = call read('classpath:signin-admin.feature')
    * def adminAuthToken = signInAdmin.adminAuthToken
    Given path accountsBase
    And header Content-Type = 'application/json'
    And header Authorization = adminAuthToken
    And param page = 0
    And param size = 50
    When method GET
    Then status 200
    And assert response.length <= 50
    And match response[0].id == '#notnull'
    And match response[0].email == '#notnull'
    And match response[0].creationDate == '#notnull'
    And match response[0].isActive == '#boolean'
    And match response[0].user == '#present'
    And match response[0].user == '#object'
    And match response[0].user.id == '#notnull'
    And match response[0].user.team == '#object'
    # The admin and the user accounts are created automatically when we use the spring h2 profile


  Scenario: Get all accounts by a user

    * def signInUser = call read('classpath:signin-user.feature')
    * def userAuthToken = signInUser.userAuthToken
    Given path accountsBase
    And header Content-Type = 'application/json'
    And header Authorization = userAuthToken
    And param page = 0
    And param size = 50
    When method GET
    Then status 403
    And match response == '#present'
    And match response.error_description == 'Access is denied'
