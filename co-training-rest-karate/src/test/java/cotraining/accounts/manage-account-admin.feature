Feature: Manage an account using an admin token

  Background:
    * url APP_URL
    * def accountsBase = 'accounts'
    * def adminID = 99
    * def userID = 88
    * def signInAdmin = call read('classpath:signin-admin.feature')
    * def adminAuthToken = signInAdmin.adminAuthToken

  Scenario: Get the admin account

    Given path accountsBase, adminID
    And header Authorization = adminAuthToken
    When method GET
    Then status 200
    And match $.id == '#notnull'
    And match $.email == '#notnull'
    And match $.isActive == true
    And match $.user == '#notnull'
    And match $.user == '#object'
    And match $.user.id == '#notnull'

  Scenario: Get the admin account's user object

    Given path accountsBase, adminID, 'user'
    And header Authorization = adminAuthToken
    When method GET
    Then status 200
    And match $ == '#notnull'
    And match $.id == '#notnull'
    And match $.firstName == '#notnull'
    And match $.lastName == '#notnull'
    And match $.team == '#present'


  Scenario: Delete an admin account

    # Admin accounts are not updatable
    Given path accountsBase, adminID
    And header Authorization = adminAuthToken
    When method DELETE
    # Method not allowed
    Then status 405
    And match $.message == 'Admin accounts are not updatable'


  Scenario: Cancel the admin account deletion

    # Admin accounts are not updatable
    Given path accountsBase, adminID, 'cancel'
    And header Authorization = adminAuthToken
    When method GET
    Then status 400


  Scenario: Cancel the user account deletion

    # cancel account deletion method is not yet implemented
    Given path accountsBase, userID, 'cancel'
    And header Authorization = adminAuthToken
    When method GET
    Then status 400