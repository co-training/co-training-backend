Feature: Create a new user

  Background:
    * url APP_URL

  Scenario: Create a new user

    Given path 'accounts'
    And header Content-Type = 'application/json'
    And param email = 'newemail@gmail.com'
    And param password = 'p@ssWord'
    And request read('new-user-account.json')
    When method POST
    Then status 201
    And match $ == ''

  Scenario: Created user account should be disabled

    Given url BASE_URL + '/oauth/token'
    # try to get a token for the created user
    And header Content-Type = 'application/x-www-form-urlencoded'
    And header Authorization = call read('classpath:basic-auth.js') { username: '#(APP_ID)', password: '#(APP_SECRET)' }
    And form field grant_type = 'password'
    And form field username = 'newemail@gmail.com'
    And form field password = 'p@ssWord'
    When method POST
    Then status 400
    And match response.error_description == 'User is disabled'
