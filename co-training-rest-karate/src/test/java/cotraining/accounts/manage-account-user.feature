Feature: Manage an account using a user token

  Background:
    * url APP_URL
    * def accountsBase = 'accounts'
    * def adminID = 99
    * def userID = 88
    * def signIn = call read('classpath:signin-user.feature')
    * def userAuthToken = signIn.userAuthToken

  Scenario: Get the user account

    Given path accountsBase, userID
    And header Authorization = userAuthToken
    When method GET
    Then status 200
    And match $.id == '#notnull'
    And match $.email == '#notnull'
    And match $.isActive == true
    And match $.user == '#notnull'
    And match $.user == '#object'
    And match $.user.id == '#notnull'

  Scenario: Get the user account's user object

    Given path accountsBase, userID, 'user'
    And header Authorization = userAuthToken
    When method GET
    Then status 403

  Scenario: Get the admin account's user object

    Given path accountsBase, adminID
    And header Authorization = userAuthToken
    When method GET
    Then status 405
    And match $.error == 'Method Not Allowed'

  Scenario: Delete an admin account

    Given path accountsBase, adminID
    And header Authorization = userAuthToken
    When method DELETE
    Then status 405
    And match $.error == 'Method Not Allowed'