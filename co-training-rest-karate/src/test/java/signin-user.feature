Feature: Get user token

  Background:
    * url BASE_URL

  Scenario: Get user token

    Given path 'oauth', 'token'
    And header Content-Type = 'application/x-www-form-urlencoded'
    And header Authorization = call read('classpath:basic-auth.js') { username: '#(APP_ID)', password: '#(APP_SECRET)' }
    And form field grant_type = 'password'
    And form field username = 'user@mail.com'
    And form field password = 'password'
    When method POST
    Then status 200
    And match response.access_token == '#notnull'


    * def userAuthToken = 'Bearer ' + response.access_token
    * print 'The user bearer header is : ', userAuthToken
