Feature: Get admin token

  Background:
    * url BASE_URL

  Scenario: Get admin token

    Given path 'oauth', 'token'
    And header Content-Type = 'application/x-www-form-urlencoded'
    And header Authorization = call read('classpath:basic-auth.js') { username: '#(APP_ID)', password: '#(APP_SECRET)' }
    And form field grant_type = 'password'
    And form field username = 'admin@mail.com'
    And form field password = 'password'
    When method POST
    Then status 200
    And match response.access_token == '#notnull'


    * def adminAuthToken = 'Bearer ' + response.access_token
    * print 'The admin bearer header is : ', adminAuthToken
