# Company training rest karate

## Description

This module represents the integration tests of the `co-training-rest` module. We will use the karate framework to build these tests, you can find more
details in the github project :
- https://github.com/intuit/karate

You don't have to be a java developer to make these tests because it is based on the `Gherkin` language. Here's a simple test :

![example_gherkin](/uploads/a32529631d317747c52dbfcc59f6d32b/example_gherkin.png)

## Run tests

### From intellij

You can run tests from you IDE, you have to run the server (using the h2 profile) as a first step (check
the `co-training-rest` module) then you can run tests as below.

![karate-tests](/uploads/7329e25d418699acabfe6c292d0fea42/karate-tests.png)

run tests from the `CoTrainingTests` class.

### From command line

To run the tests from the command line you have to shut down the server first if it is already started. Make sure you are using the h2 profile because when
you run the tests the first time you'll add new data to the database without cleaning it and this will prevent the tests the second time.

Why you have to turn off the server first ?

Well, we will run the karate as tests as integration tests and not unit tests as mentioned in the most of the examples in the net. If you'll use the
unit tests approach you have to run the server manually, so you can run tests. This approach is not the good one if you
need to run the server automatically. In my case i need this approach because i'll build the project and run tests in
gitlab pipelines before deploying the app.

To do so, these are the steps that i've followed (check the maven plugins configuration in the project `pom.xml`) :

1. maven-surefire-plugin : i've skip tests, so the karate tests will not be executed in the test phase,
2. maven-dependency-plugin : i've used this plugin to copy the source files of the `co-training-rest` module, so i can
   run the server (as a jar) from next plugin,
3. maven-failsafe-plugin : this plugin is used to make it possible to run junit tests as integration tests (in
   integration-test phase),
4. process-exec-maven-plugin : this plugin is used to run the spring server before running the tests and stop it after
   executing these tests.

From project root directory, run this command :

```shell script
$ mvn clean verify
```

```log
---------------------------------------------------------
feature: classpath:cotraining/levels/get-levels.feature
scenarios:  1 | passed:  1 | failed:  0 | time: 0.8960
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/trainings/manage-trainings.feature
scenarios:  2 | passed:  2 | failed:  0 | time: 0.9290
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/accounts/create-account.feature
scenarios:  2 | passed:  2 | failed:  0 | time: 0.3331
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/accounts/manage-account-admin.feature
scenarios:  5 | passed:  5 | failed:  0 | time: 0.9365
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/accounts/get-accounts.feature
scenarios:  2 | passed:  2 | failed:  0 | time: 0.3322
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/accounts/manage-new-account.feature
scenarios:  5 | passed:  5 | failed:  0 | time: 1.0614
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/accounts/manage-account-user.feature
scenarios:  4 | passed:  4 | failed:  0 | time: 0.5690
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/users/manage-users-user.feature
scenarios:  7 | passed:  7 | failed:  0 | time: 1.0369
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/users/manage-users-admin.feature
scenarios:  7 | passed:  7 | failed:  0 | time: 1.0977
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/tags/get-tags.feature
scenarios:  1 | passed:  1 | failed:  0 | time: 0.1714
---------------------------------------------------------
---------------------------------------------------------
feature: classpath:cotraining/teams/get-teams.feature
scenarios:  1 | passed:  1 | failed:  0 | time: 0.0215
---------------------------------------------------------

[INFO] Tests run: 37, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 9.092 s - in cotraining.CoTrainingTests
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 37, Failures: 0, Errors: 0, Skipped: 0

```

## References

Here's some references that i've used to build tests in this module :

Github : https://github.com/intuit/karate

Hello example project : https://github.com/Sdaas/hello-karate

Naming conventions : https://github.com/intuit/karate#naming-conventions

Requests : https://github.com/intuit/karate#request

Query params : https://github.com/intuit/karate#param

Url : https://github.com/intuit/karate#url

Path : https://github.com/intuit/karate#path

Basic authentication : https://github.com/intuit/karate#http-basic-authentication-example

Invoke another feature : https://github.com/intuit/karate#calling-other-feature-files

Assertions : https://github.com/intuit/karate#assert

Matches : https://github.com/intuit/karate#fuzzy-matching

Config file : https://github.com/intuit/karate#karate-configjs

Reading files : https://github.com/intuit/karate#reading-files

Schema validation : https://github.com/intuit/karate/blob/master/karate-junit4/src/test/java/com/intuit/karate/junit4/demos/schema-like.feature

Demos : https://github.com/intuit/karate/blob/master/karate-junit4/src/test/java/com/intuit/karate/junit4/demos

Cheat sheet : https://devqa.io/karate-api-testing-tool-cheat-sheet/

karate demo : https://github.com/intuit/karate/tree/master/karate-demo