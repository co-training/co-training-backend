package com.co.training.rest.controller;

import com.co.training.common.service.TagService;
import com.co.training.rest.test.OauthToken;
import com.co.training.rest.test.RequestsParamsConstants;
import com.co.training.rest.test.TagTestUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TagControllerTest {

    public static final String TAGS_API = "/v1/tags/search";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TagService tagService;

    @Autowired
    private OauthToken oauthToken;

    @Test
    void getAllTags_Tags_ReturnAllTagsByPage() throws Exception {

        Mockito.when(tagService.findTags(Mockito.anyInt(), Mockito.anyInt(), Mockito.any(String.class))).thenReturn(TagTestUtils.buildTag());
        this.mockMvc.perform(
                get(TAGS_API).contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                // use one of these methods to test length
                .andExpect(jsonPath("$.length()", is(1)))
                .andExpect(jsonPath("$[0].value", is(TagTestUtils.TAG_NAME)));
    }

}
