package com.co.training.rest.test;

import com.co.training.rest.entities.UserAccount;
import org.openapitools.client.model.Account;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

public class UserAccountTestUtils {

    public static final Long ACCOUNT_ID = 1L;
    public static final LocalDateTime CREATION_DATE = LocalDateTime.of(2019, Month.APRIL, 5, 14, 30, 15);
    public static final Boolean IS_ACTIVE = true;

    public static Account buildUserAccount() {

        Account account = new Account();
        account.setCreationDate(CREATION_DATE);
        account.setEmail(RequestsParamsConstants.EMAIL_VALUE);
        account.setId(ACCOUNT_ID);
        account.setIsActive(IS_ACTIVE);
        account.setUser(UserTestUtils.buildUser());
        return account;
    }

    public static UserAccount buildEntityUserAccount() {

        UserAccount account = new UserAccount();
        account.setCreationDate(CREATION_DATE);
        account.setEmail(RequestsParamsConstants.EMAIL_VALUE);
        account.setId(ACCOUNT_ID);
        account.setIsActive(IS_ACTIVE);
        account.setUser(UserTestUtils.buildUserEntity());
        return account;
    }

    public static List<Account> buildListUserAccounts() {

        return Arrays.asList(
                buildUserAccount()
        );
    }

    public static UserAccount buildUserAccountEntity() {

        UserAccount account = new UserAccount();
        account.setCreationDate(CREATION_DATE);
        account.setEmail(RequestsParamsConstants.EMAIL_VALUE);
        account.setId(ACCOUNT_ID);
        account.setIsActive(IS_ACTIVE);
        account.setUser(UserTestUtils.buildUserEntity());
        return account;
    }
}
