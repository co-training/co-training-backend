package com.co.training.rest.test;

import com.co.training.rest.config.security.OauthConfig;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * In this class we will build tokens for an admin an a user. these profiles are mentioned in the data-h2.sql file
 * using these methods we can pass the security layer provided by spring security
 */
@Service
public class OauthToken implements InitializingBean {

    public static final String AUTHORIZATION = "Authorization";
    private String adminToken;
    private String userToken;
    private final OauthConfig oauthConfig;
    private final MockMvc mockMvc;

    private OauthToken(OauthConfig oauthConfig, MockMvc mockMvc) {
        this.oauthConfig = oauthConfig;
        this.mockMvc = mockMvc;
    }

    private String buildToken(String username, String password) throws Exception {

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        //params.add("client_id", oauthConfig.getClientId());
        params.add("username", username);
        params.add("password", password);

        ResultActions result
                = mockMvc.perform(post("/oauth/token")
                .params(params)
                .with(SecurityMockMvcRequestPostProcessors.httpBasic(oauthConfig.getClientId(), oauthConfig.getClientPassword()))
                .accept("application/json;charset=UTF-8"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));

        String resultString = result.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }

    public String getAdminToken() {

        return "Bearer " + adminToken;
    }

    public String getUserToken() {

        return "Bearer " + userToken;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

        this.adminToken = buildToken("admin@mail.com", "password");
        this.userToken = buildToken("user@mail.com", "password");
    }
}
