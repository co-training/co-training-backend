package com.co.training.rest.test;

import org.openapitools.client.model.TrainingReview;

import java.util.Arrays;
import java.util.List;

public class ReviewTestUtils {

    public static final TrainingReview.StarsEnum STARS = TrainingReview.StarsEnum._5;
    public static final String COMMENT = "Very informative and well planned training";

    public static TrainingReview buildReview() {

        TrainingReview review = new TrainingReview();
        review.setComment(COMMENT);
        review.setDate(TrainingTestUtils.TODAY);
        review.setIsAnonymous(true);
        review.setStars(STARS);
        review.setAuthor(UserTestUtils.buildUser());
        review.setTraining(TrainingTestUtils.buildTraining());

        return review;
    }

    public static List<TrainingReview> buildReviews() {

        return Arrays.asList(buildReview());
    }
}
