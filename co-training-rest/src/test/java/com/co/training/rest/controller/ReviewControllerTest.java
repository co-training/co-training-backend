package com.co.training.rest.controller;

import com.co.training.common.service.ReviewService;
import com.co.training.dao.entities.User;
import com.co.training.rest.test.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.openapitools.client.model.TrainingReview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolationException;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ReviewControllerTest {

    static final String REVIEW_API = "/v1/reviews";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ReviewService reviewService;

    private ObjectMapper mapper = ObjectMapperUtils.getObjectMapper();

    @Autowired
    private OauthToken oauthToken;

    @Test
    void createNewReview_NotValidReview_ReviewCreated() throws Exception {

        Mockito.doThrow(ConstraintViolationException.class).when(reviewService).createNewReview(Mockito.any(TrainingReview.class), Mockito.any(User.class));
        this.mockMvc.perform(
                post(REVIEW_API)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(ReviewTestUtils.buildReview()))
        )
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    void createNewReview_ValidReview_ReviewCreated() throws Exception {

        Mockito.doNothing().when(reviewService).createNewReview(Mockito.any(TrainingReview.class), Mockito.any(User.class));
        this.mockMvc.perform(
                post(REVIEW_API)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(ReviewTestUtils.buildReview()))
        )
                .andDo(print())
                .andExpect(status().isCreated());
    }

    /*
     * getTrainingReviews Tests
     */
    @Test
    void getTrainingReviews_OneReview_ReturnOneReview() throws Exception {

        Mockito.doReturn(ReviewTestUtils.buildReviews()).when(reviewService).getTrainingReviews(Mockito.anyInt(), Mockito.anyInt(), Mockito.anyLong(), Mockito.anyLong(), Mockito.any(TrainingReview.StarsEnum.class));
        this.mockMvc.perform(
                get(REVIEW_API + "/search")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
                        .param(RequestsParamsConstants.UID_PARAM, RequestsParamsConstants.UID_VALUE)
                        .param(RequestsParamsConstants.TRAINING_PARAM, RequestsParamsConstants.TRAINING_VALUE)
                        .param(RequestsParamsConstants.STARS_PARAM, RequestsParamsConstants.STARS_VALUE)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].comment", is(ReviewTestUtils.COMMENT)))
                .andExpect(jsonPath("$[0].isAnonymous", is(Boolean.TRUE)))
                .andExpect(jsonPath("$[0].stars", is(ReviewTestUtils.STARS.toString())))
                .andExpect(jsonPath("$[0].date", is(TrainingTestUtils.TODAY.toString())))
                .andExpect(jsonPath("$[0].author.id", is(UserTestUtils.USER_ID.intValue())))
                .andExpect(jsonPath("$[0].author.firstName", is(UserTestUtils.FIRST_NAME)))
                .andExpect(jsonPath("$[0].author.lastName", is(UserTestUtils.LAST_NAME)))
                .andExpect(jsonPath("$[0].training.id", is(TrainingTestUtils.ID.intValue())));
    }
}
