package com.co.training.rest.test;

import org.openapitools.client.model.TrainingReview;

import java.time.LocalDate;

public final class RequestsParamsConstants {

    /*
     * Trainings params
     */
    public static final String START_DATE_PARAM = "start";
    public static final LocalDate START_DATE_VALUE = LocalDate.now();
    public static final String END_DATE_PARAM = "end";
    public static final LocalDate END_DATE_VALUE = LocalDate.now().plusDays(5);


    /*
     * Page and size params
     */
    public static final String PAGE_PARAM = "page";
    public static final String PAGE_VALUE = "0";
    public static final String SIZE_PARAM = "size";
    public static final String SIZE_VALUE = "50";

    /*
     * User accounts params
     */
    public static final String EMAIL_PARAM = "email";
    public static final String EMAIL_VALUE = "user@mail.com";
    public static final String PASSWORD_PARAM = "password";
    public static final String PASSWORD_VALUE = "p@ssword";

    /*
     * reviews params
     */
    public static final String UID_PARAM = "uid";
    public static final String UID_VALUE = UserTestUtils.USER_ID.toString();
    public static final String TRAINING_PARAM = "trainingId";
    public static final String TRAINING_VALUE = TrainingTestUtils.ID.toString();
    public static final String STARS_PARAM = "stars";
    public static final String STARS_VALUE = TrainingReview.StarsEnum._3.getValue();

    private RequestsParamsConstants() {
    }
}
