package com.co.training.rest.controller;

import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.rest.service.UserAccountService;
import com.co.training.rest.test.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.openapitools.client.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolationException;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserAccountControllerTest {

    static final String USER_ACCOUNT_API = "/v1/accounts";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserAccountService userAccountService;

    private ObjectMapper mapper = ObjectMapperUtils.getObjectMapper();

    @Autowired
    private OauthToken oauthToken;

    @BeforeEach
    void init() {

        Mockito.when(userAccountService.retrieveUserAccount(Mockito.anyLong()))
                .thenReturn(UserAccountTestUtils.buildUserAccount());

        Mockito.when(userAccountService.getAccountEntityById(Mockito.anyLong()))
                .thenReturn(UserAccountTestUtils.buildEntityUserAccount());
    }

    /*
     * createUserAccount Tests
     */

    @Test
    void createUserAccount_WrongEmail_ExceptionThrown() throws Exception {

        Mockito.doNothing().when(userAccountService).createUserAccount(Mockito.anyString(), Mockito.anyString(), Mockito.any(User.class));
        this.mockMvc.perform(
                post(USER_ACCOUNT_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param(RequestsParamsConstants.EMAIL_PARAM, "@wrong.email")
                        .param(RequestsParamsConstants.PASSWORD_PARAM, "p@ssword")
                        .content(mapper.writeValueAsString(UserTestUtils.buildUser()))
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> Assertions.assertThat(mvcResult.getResolvedException()).isInstanceOf(ConstraintViolationException.class));
    }

    @Test
    void createUserAccount_WrongPassword_ExceptionThrown() throws Exception {

        Mockito.doNothing().when(userAccountService).createUserAccount(Mockito.anyString(), Mockito.anyString(), Mockito.any(User.class));
        this.mockMvc.perform(
                post(USER_ACCOUNT_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param(RequestsParamsConstants.EMAIL_PARAM, "@wrong.email")
                        .param(RequestsParamsConstants.PASSWORD_PARAM, "p@ss")
                        .content(mapper.writeValueAsString(UserTestUtils.buildUser()))
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> Assertions.assertThat(mvcResult.getResolvedException()).isInstanceOf(ConstraintViolationException.class));
    }

    @Test
    void createUserAccount_NewUserAccount_ReturnCreated() throws Exception {

        Mockito.doNothing().when(userAccountService).createUserAccount(Mockito.anyString(), Mockito.anyString(), Mockito.any(User.class));
        this.mockMvc.perform(
                post(USER_ACCOUNT_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .param(RequestsParamsConstants.EMAIL_PARAM, RequestsParamsConstants.EMAIL_VALUE)
                        .param(RequestsParamsConstants.PASSWORD_PARAM, RequestsParamsConstants.PASSWORD_VALUE)
                        .content(mapper.writeValueAsString(UserTestUtils.buildUser()))
        )
                .andDo(print())
                .andExpect(status().isCreated());
    }

    /*
     * retrieveUsersAccounts Tests
     */

    @Test
    void retrieveUsersAccounts_WrongAuthorization_AccessDenied() throws Exception {

        this.mockMvc.perform(
                get(USER_ACCOUNT_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, "-1")
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    void retrieveUsersAccounts_PageNumberNegative_ExceptionThrown() throws Exception {

        Mockito.when(userAccountService.retrieveUsersAccounts(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserAccountTestUtils.buildListUserAccounts());
        this.mockMvc.perform(
                get(USER_ACCOUNT_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, "-1")
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> Assertions.assertThat(mvcResult.getResolvedException()).isInstanceOf(ConstraintViolationException.class));
    }

    @Test
    void retrieveUsersAccounts_PageSizeUnderMin_ExceptionThrown() throws Exception {

        Mockito.when(userAccountService.retrieveUsersAccounts(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserAccountTestUtils.buildListUserAccounts());
        this.mockMvc.perform(
                get(USER_ACCOUNT_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, "0")
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> Assertions.assertThat(mvcResult.getResolvedException()).isInstanceOf(ConstraintViolationException.class));
    }

    @Test
    void retrieveUsersAccounts_PageSizeOverMax_ExceptionThrown() throws Exception {

        Mockito.when(userAccountService.retrieveUsersAccounts(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserAccountTestUtils.buildListUserAccounts());
        this.mockMvc.perform(
                get(USER_ACCOUNT_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, "101")
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> Assertions.assertThat(mvcResult.getResolvedException()).isInstanceOf(ConstraintViolationException.class));
    }

    @Test
    void retrieveUsersAccounts_OneUserAccount_ReturnUserAccount() throws Exception {

        Mockito.when(userAccountService.retrieveUsersAccounts(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserAccountTestUtils.buildListUserAccounts());
        this.mockMvc.perform(
                get(USER_ACCOUNT_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(UserAccountTestUtils.ACCOUNT_ID.intValue())))
                // json response contains only numbers ( which is integer in our case ), that's why we have to remove L suffix from longs
                .andExpect(jsonPath("$[0].email", is(RequestsParamsConstants.EMAIL_VALUE)))
                .andExpect(jsonPath("$[0].user.id", is(UserTestUtils.USER_ID.intValue())))
                .andExpect(jsonPath("$[0].user.firstName", is(UserTestUtils.FIRST_NAME)))
                .andExpect(jsonPath("$[0].user.lastName", is(UserTestUtils.LAST_NAME)));
    }

    /*
     * retrieveUserAccount Tests
     */

    @Test
    @Disabled
        // TODO we are mocking the service layer so we cannot execute the security layer (which is defined in the service)
    void retrieveUserAccount_WrongAuthorization_ReturnUserAccount() throws Exception {

        this.mockMvc.perform(
                get(USER_ACCOUNT_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    void retrieveUserAccount_ExistUserAccount_ReturnUserAccount() throws Exception {

        this.mockMvc.perform(
                get(USER_ACCOUNT_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(UserAccountTestUtils.ACCOUNT_ID.intValue())))
                .andExpect(jsonPath("$.email", is(RequestsParamsConstants.EMAIL_VALUE)))
                .andExpect(jsonPath("$.creationDate", is(UserAccountTestUtils.CREATION_DATE.toString())))
                .andExpect(jsonPath("$.isActive", is(UserAccountTestUtils.IS_ACTIVE)))
                .andExpect(jsonPath("$.user.id", is(UserTestUtils.USER_ID.intValue())))
                .andExpect(jsonPath("$.user.firstName", is(UserTestUtils.FIRST_NAME)));
    }

    @Test
    void retrieveUserAccount_NotExistUserAccount_ExceptionThrown() throws Exception {

        Mockito.when(userAccountService.retrieveUserAccount(Mockito.anyLong())).thenThrow(ResourceNotFoundException.class);
        this.mockMvc.perform(
                get(USER_ACCOUNT_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    @Disabled
        // TODO check why this method throws an exception and block the test
    void retrieveUserAccount_stUserAccount_ExceptionThrown() throws Exception {

        // Exception thrown NestedServletException: Request processing failed; nested exception is java.lang.RuntimeException
        Mockito.when(userAccountService.retrieveUserAccount(Mockito.anyLong())).thenThrow(RuntimeException.class);
        this.mockMvc.perform(get(USER_ACCOUNT_API + "/1"))
                .andDo(print())
                .andExpect(status().isInternalServerError());
    }

    /*
     * retrieveUserAccount Tests
     */

    @Test
    void retrieveUser_OneUser_ReturnUser() throws Exception {

        Mockito.when(userAccountService.retrieveUser(Mockito.anyLong())).thenReturn(UserTestUtils.buildUser());
        this.mockMvc.perform(
                get(USER_ACCOUNT_API + "/1/user")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(UserTestUtils.USER_ID.intValue())))
                .andExpect(jsonPath("$.firstName", is(UserTestUtils.FIRST_NAME)))
                .andExpect(jsonPath("$.lastName", is(UserTestUtils.LAST_NAME)))
                .andExpect(jsonPath("$.birthday", is(UserTestUtils.BIRTHDAY.toString())))
                .andExpect(jsonPath("$.email", is(UserTestUtils.EMAIL)))
                .andExpect(jsonPath("$.team.id", is(UserTestUtils.TEAM_ID.intValue())))
                .andExpect(jsonPath("$.team.name", is(UserTestUtils.TEAM_NAME)))
                .andExpect(jsonPath("$.job", is(UserTestUtils.JOB)));
    }

    /*
     * deleteUserAccount Tests
     */

    @Test
    void deleteUserAccount_ExistUser_ReturnAccepted() throws Exception {

        Mockito.doNothing().when(userAccountService).deleteUserAccount(Mockito.anyLong());
        this.mockMvc.perform(
                delete(USER_ACCOUNT_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isAccepted());
        // we will not test not exist user because we will do the same call to get account by id in the service layer which was tested above
    }

    /*
     * cancelAccountDeletion Tests
     */

    @Test
    void cancelAccountDeletion_ExistAccount_ReturnOk() throws Exception {

        Mockito.doNothing().when(userAccountService).cancelAccountDeletion(Mockito.anyLong());
        this.mockMvc.perform(
                get(USER_ACCOUNT_API + "/1/cancel")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isOk());
    }
}
