package com.co.training.rest.test;

import org.openapitools.client.model.Team;
import org.openapitools.client.model.User;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

public class UserTestUtils {

    public static final Long USER_ID = 10L;
    public static final Long TEAM_ID = 150L;
    public static final String FIRST_NAME = "Jack";
    public static final String LAST_NAME = "BESOS";
    public static final String JOB = "Manager";
    public static final String TEAM_NAME = "supply";
    public static final String EMAIL = "email@gmail.com";
    public static final String DOCKER_HUB = "https://hub.docker.com/_/nginx";
    public static final LocalDate BIRTHDAY = LocalDate.of(1991, Month.APRIL, 14);

    public static User buildUser() {

        User user = new User();
        user.setId(USER_ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setJob(JOB);
        user.setEmail(EMAIL);
        user.setIsEmailEnabled(true);
        user.setDockerHub(DOCKER_HUB);
        user.setIsDockerHubEnabled(false);
        user.setBirthday(BIRTHDAY);

        // user's team
        Team team = new Team();
        team.setId(TEAM_ID);
        team.setName(TEAM_NAME);
        user.setTeam(team);

        return user;
    }

    public static List<User> buildUsers() {

        return Arrays.asList(buildUser());
    }

    public static com.co.training.dao.entities.User buildUserEntity() {

        com.co.training.dao.entities.User user = new com.co.training.dao.entities.User();
        user.setId(USER_ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setJob(JOB);
        user.setEmail(EMAIL);
        user.setIsEmailEnabled(true);
        user.setDockerHub(DOCKER_HUB);
        user.setIsDockerHubEnabled(false);
        user.setBirthday(BIRTHDAY);

        // user's team
        com.co.training.dao.entities.Team team = new com.co.training.dao.entities.Team();
        team.setId(TEAM_ID);
        team.setName(TEAM_NAME);
        user.setTeam(team);

        return user;
    }
}
