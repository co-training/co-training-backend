package com.co.training.rest.test;

import org.openapitools.client.model.TrainingTag;

import java.util.Arrays;
import java.util.List;

public class TagTestUtils {

    public static final String TAG_NAME = "spring";
    public static final Long TAG_ID = 1L;

    public static List<TrainingTag> buildTag() {

        TrainingTag trainingTag = new TrainingTag();
        trainingTag.setId(TAG_ID);
        trainingTag.setValue(TAG_NAME);
        return Arrays.asList(trainingTag);
    }
}
