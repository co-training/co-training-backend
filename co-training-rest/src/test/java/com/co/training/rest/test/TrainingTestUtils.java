package com.co.training.rest.test;

import org.openapitools.client.model.Training;
import org.openapitools.client.model.TrainingCriteria;
import org.openapitools.client.model.TrainingRequirement;
import org.openapitools.client.model.TrainingTag;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class TrainingTestUtils {


    public static final Long ID = 99L;
    public static final String NAME = "Scrum introduction";
    public static final String DESCRIPTION = "An introduction to the scrum framework";
    public static final String REQUIREMENT_DESCRIPTION = "No experience is necessary";
    public static final String CRITERIA1_DESCRIPTION = "Those interested in working within an Agile team";
    public static final String CRITERIA2_DESCRIPTION = "Current Agile team members that want to deepen their understanding";
    public static final String TAG_VALUE = "scrum";
    public static final Integer DURATION = 2;
    public static final LocalDate TODAY = LocalDate.now();
    public static final Integer MIN_PARTICIPANTS = 5;
    public static final Integer MAX_PARTICIPANTS = 12;
    public static final Float PRICE = 50f;

    public static Training buildTraining() {

        Training training = new Training();

        training.setId(ID);
        training.setName(NAME);
        training.setDescription(DESCRIPTION);

        // add a training requirement
        TrainingRequirement requirement = new TrainingRequirement();
        requirement.setDescription(REQUIREMENT_DESCRIPTION);
        training.setRequirements(Arrays.asList(requirement));

        // add training criterias
        TrainingCriteria criteria1 = new TrainingCriteria();
        criteria1.setDescription(CRITERIA1_DESCRIPTION);
        TrainingCriteria criteria2 = new TrainingCriteria();
        criteria2.setDescription(CRITERIA2_DESCRIPTION);
        training.setCriterias(Arrays.asList(criteria1, criteria2));

        training.setLevel(Training.LevelEnum.BEGINNER);

        // add a tag to the training
        TrainingTag tag = new TrainingTag();
        tag.setValue(TAG_VALUE);
        training.setTags(Arrays.asList(tag));

        training.setStartingDate(TODAY);
        training.setDuration(DURATION);
        training.setMinParticipants(MIN_PARTICIPANTS);
        training.setMaxParticipants(MAX_PARTICIPANTS);
        training.setPrice(PRICE);

        // add training author
        training.setAuthor(UserTestUtils.buildUser());

        return training;
    }

    public static List<Training> buildTrainings() {

        return Arrays.asList(buildTraining());
    }
}
