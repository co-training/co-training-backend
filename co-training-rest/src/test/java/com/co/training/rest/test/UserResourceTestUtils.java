package com.co.training.rest.test;


import org.openapitools.client.model.UserResource;

import java.util.Arrays;
import java.util.List;

public class UserResourceTestUtils {

    public static final Float NB_DAYS = 5.0f;
    public static final Double BUDGET = 300.50;
    public static final Double JACKPOT = 1200d;

    public static UserResource buildUserResource() {

        UserResource resource = new UserResource();
        resource.setBudget(BUDGET);
        resource.setNbDays(NB_DAYS);
        resource.setJackpot(JACKPOT);
        return resource;
    }

    public static List<UserResource> buildUsers() {

        return Arrays.asList(buildUserResource());
    }
}
