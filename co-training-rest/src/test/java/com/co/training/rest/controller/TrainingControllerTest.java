package com.co.training.rest.controller;

import com.co.training.common.exceptions.BadRequestException;
import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.common.service.TrainingService;
import com.co.training.dao.entities.User;
import com.co.training.rest.service.UserAccountService;
import com.co.training.rest.test.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.openapitools.client.model.Training;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;
import java.util.ArrayList;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TrainingControllerTest {

    static final String TRAINING_API = "/v1/trainings";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TrainingService trainingService;

    private ObjectMapper mapper = ObjectMapperUtils.getObjectMapper();

    @Autowired
    private OauthToken oauthToken;

    @MockBean
    private UserAccountService userAccountService;

    @BeforeEach
    void init() {

        Mockito.when(userAccountService.getAccountEntityByEmail(Mockito.anyString()))
                .thenReturn(UserAccountTestUtils.buildUserAccountEntity());
    }

    /*
     * createNewTraining Tests
     */

    @Test
    void createNewTraining_NewValidTraining_ReturnCreated() throws Exception {

        Mockito.doNothing().when(trainingService).createNewTraining(Mockito.any(Training.class), Mockito.any(User.class));
        this.mockMvc.perform(
                post(TRAINING_API)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(TrainingTestUtils.buildTraining()))
        )
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    void createNewTraining_NotValidTraining_ExceptionThrown() throws Exception {

        Mockito.doThrow(ConstraintViolationException.class).when(trainingService).createNewTraining(Mockito.any(Training.class), Mockito.any(User.class));
        this.mockMvc.perform(
                post(TRAINING_API)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(TrainingTestUtils.buildTraining()))
        )
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    /*
     * getTrainings Tests
     */

    @Test
    void getTrainings_OneTrainingInApp_ReturnOneTraining() throws Exception {

        Mockito.when(trainingService.getTrainings(Mockito.any(LocalDate.class), Mockito.any(LocalDate.class), Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(TrainingTestUtils.buildTrainings());
        this.mockMvc.perform(
                get(TRAINING_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .param(RequestsParamsConstants.START_DATE_PARAM, RequestsParamsConstants.START_DATE_VALUE.toString())
                        .param(RequestsParamsConstants.END_DATE_PARAM, RequestsParamsConstants.END_DATE_VALUE.toString())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(TrainingTestUtils.ID.intValue())))
                .andExpect(jsonPath("$[0].name", is(TrainingTestUtils.NAME)))
                .andExpect(jsonPath("$[0].description", is(TrainingTestUtils.DESCRIPTION)))
                .andExpect(jsonPath("$[0].startingDate", is(TrainingTestUtils.TODAY.toString())))
                .andExpect(jsonPath("$[0].duration", is(TrainingTestUtils.DURATION)))
                .andExpect(jsonPath("$[0].minParticipants", is(TrainingTestUtils.MIN_PARTICIPANTS)))
                .andExpect(jsonPath("$[0].maxParticipants", is(TrainingTestUtils.MAX_PARTICIPANTS)))
                .andExpect(jsonPath("$[0].price", is(TrainingTestUtils.PRICE.doubleValue())));
        // i've changed the price from float to double because of format issues in jsonPath, it expected 50.0F to be equal to 50.0 ( api response )
        // we can workaround the issue by transforming the price to double ( to eliminate the suffix F )
    }

    @Test
    void getTrainings_ZeroTrainingInApp_ReturnEmptyList() throws Exception {

        Mockito.when(trainingService.getTrainings(Mockito.any(LocalDate.class), Mockito.any(LocalDate.class), Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(new ArrayList<>());
        this.mockMvc.perform(
                get(TRAINING_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .param(RequestsParamsConstants.START_DATE_PARAM, RequestsParamsConstants.START_DATE_VALUE.toString())
                        .param(RequestsParamsConstants.END_DATE_PARAM, RequestsParamsConstants.END_DATE_VALUE.toString())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    /*
     * getTraining Tests
     */

    @Test
    void getTrainings_ExistTraining_ReturnTraining() throws Exception {

        Mockito.when(trainingService.getTrainingApi(Mockito.anyLong())).thenReturn(TrainingTestUtils.buildTraining());
        this.mockMvc.perform(
                get(TRAINING_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(TrainingTestUtils.ID.intValue())))
                .andExpect(jsonPath("$.name", is(TrainingTestUtils.NAME)))
                .andExpect(jsonPath("$.level", is(Training.LevelEnum.BEGINNER.toString())))
                .andExpect(jsonPath("$.requirements", hasSize(1)))
                .andExpect(jsonPath("$.requirements[0].description", is(TrainingTestUtils.REQUIREMENT_DESCRIPTION)))
                .andExpect(jsonPath("$.criterias", hasSize(2)))
                .andExpect(jsonPath("$.criterias[0].description", is(TrainingTestUtils.CRITERIA1_DESCRIPTION)))
                .andExpect(jsonPath("$.criterias[1].description", is(TrainingTestUtils.CRITERIA2_DESCRIPTION)))
                .andExpect(jsonPath("$.tags", hasSize(1)))
                .andExpect(jsonPath("$.tags[0].value", is(TrainingTestUtils.TAG_VALUE)))
                .andExpect(jsonPath("$.author.id", is(UserTestUtils.USER_ID.intValue())))
                .andExpect(jsonPath("$.author.firstName", is(UserTestUtils.FIRST_NAME)))
                .andExpect(jsonPath("$.author.lastName", is(UserTestUtils.LAST_NAME)));
    }

    @Test
    void getTrainings_NotExistTraining_ExceptionThrown() throws Exception {

        Mockito.doThrow(ResourceNotFoundException.class).when(trainingService).getTrainingApi(Mockito.anyLong());
        this.mockMvc.perform(
                get(TRAINING_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    void getTrainings_WrongTrainingId_ExceptionThrown() throws Exception {

        Mockito.doThrow(BadRequestException.class).when(trainingService).getTrainingApi(Mockito.anyLong());
        this.mockMvc.perform(
                get(TRAINING_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    /*
     * getTrainingParticipants Tests
     */

    @Test
    void getTrainingParticipants_TrainingHasOneParticipant_ReturnOneParticipant() throws Exception {

        Mockito.when(trainingService.getTrainingParticipants(Mockito.anyLong(), Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(UserTestUtils.buildUsers());
        this.mockMvc.perform(
                get(TRAINING_API + "/1/participants")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(UserTestUtils.USER_ID.intValue())))
                .andExpect(jsonPath("$[0].firstName", is(UserTestUtils.FIRST_NAME)))
                .andExpect(jsonPath("$[0].lastName", is(UserTestUtils.LAST_NAME)));
    }
}
