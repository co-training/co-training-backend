package com.co.training.rest.controller;

import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.common.service.UserService;
import com.co.training.dao.entities.User;
import com.co.training.rest.entities.UserAccount;
import com.co.training.rest.service.UserAccountService;
import com.co.training.rest.test.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolationException;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    static final String USER_API = "/v1/users";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private UserAccountService userAccountService;

    private ObjectMapper mapper = ObjectMapperUtils.getObjectMapper();

    @Autowired
    private OauthToken oauthToken;

    @BeforeEach
    void init() {

        Mockito.when(userAccountService.getAccountEntityByUserId(Mockito.anyLong()))
                .thenReturn(UserAccountTestUtils.buildUserAccountEntity());

    }

    /*
     * retrieveUsers Tests
     */

    @Test
    void retrieveUsers_PageNumberNegative_ExceptionThrown() throws Exception {

        Mockito.when(userService.retrieveUsers(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserTestUtils.buildUsers());
        this.mockMvc.perform(
                get(USER_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, "-10")
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> Assertions.assertThat(mvcResult.getResolvedException()).isInstanceOf(ConstraintViolationException.class));
    }

    @Test
    void retrieveUsers_PageSizeUnderMin_ExceptionThrown() throws Exception {

        Mockito.when(userService.retrieveUsers(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserTestUtils.buildUsers());
        this.mockMvc.perform(
                get(USER_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, "0")
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> Assertions.assertThat(mvcResult.getResolvedException()).isInstanceOf(ConstraintViolationException.class));
    }

    @Test
    void retrieveUsers_PageSizeOverMax_ExceptionThrown() throws Exception {

        Mockito.when(userService.retrieveUsers(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserTestUtils.buildUsers());
        this.mockMvc.perform(
                get(USER_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, "101")
        )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> Assertions.assertThat(mvcResult.getResolvedException()).isInstanceOf(ConstraintViolationException.class));
    }

    @Test
    void retrieveUsers_OneUserAccount_ReturnUserAccount() throws Exception {

        Mockito.when(userService.retrieveUsers(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserTestUtils.buildUsers());
        this.mockMvc.perform(
                get(USER_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(UserTestUtils.USER_ID.intValue())))
                .andExpect(jsonPath("$[0].firstName", is(UserTestUtils.FIRST_NAME)))
                .andExpect(jsonPath("$[0].lastName", is(UserTestUtils.LAST_NAME)))
                .andExpect(jsonPath("$[0].job", is(UserTestUtils.JOB)))
                .andExpect(jsonPath("$[0].birthday", is(UserTestUtils.BIRTHDAY.toString())));
    }

    @Test
    void retrieveUsers_WrongAuthorization_AccessDenied() throws Exception {

        Mockito.when(userService.retrieveUsers(Mockito.anyInt(), Mockito.anyInt())).thenReturn(UserTestUtils.buildUsers());
        this.mockMvc.perform(
                get(USER_API)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
                        .param(RequestsParamsConstants.PAGE_PARAM, RequestsParamsConstants.PAGE_VALUE)
                        .param(RequestsParamsConstants.SIZE_PARAM, RequestsParamsConstants.SIZE_VALUE)
        )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    /*
     * retrieveUser Tests
     */

    @Test
    void retrieveUser_ExistUser_ReturnUser() throws Exception {

        Mockito.when(userService.retrieveUserById(Mockito.anyLong())).thenReturn(UserTestUtils.buildUser());
        this.mockMvc.perform(
                get(USER_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(UserTestUtils.USER_ID.intValue())))
                .andExpect(jsonPath("$.firstName", is(UserTestUtils.FIRST_NAME)))
                .andExpect(jsonPath("$.lastName", is(UserTestUtils.LAST_NAME)))
                .andExpect(jsonPath("$.job", is(UserTestUtils.JOB)))
                .andExpect(jsonPath("$.birthday", is(UserTestUtils.BIRTHDAY.toString())))
                .andExpect(jsonPath("$.email", is(UserTestUtils.EMAIL)))
                .andExpect(jsonPath("$.isEmailEnabled", is(Boolean.TRUE)))
                .andExpect(jsonPath("$.dockerHub", is(UserTestUtils.DOCKER_HUB)))
                .andExpect(jsonPath("$.isDockerHubEnabled", is(Boolean.FALSE)))
                .andExpect(jsonPath("$.team.id", is(UserTestUtils.TEAM_ID.intValue())))
                .andExpect(jsonPath("$.team.name", is(UserTestUtils.TEAM_NAME)));
    }

    @Test
    void retrieveUser_NotExistUser_ExceptionThrown() throws Exception {

        Mockito.when(userService.retrieveUserById(Mockito.anyLong())).thenThrow(ResourceNotFoundException.class);
        this.mockMvc.perform(
                get(USER_API + "/1")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    /*
     * retrieveGivenTrainings Tests
     */

    @Test
    void retrieveGivenTrainings_OneGivenTraining_ReturnTraining() throws Exception {

        Mockito.when(userService.retrieveGivenTrainings(Mockito.anyLong())).thenReturn(TrainingTestUtils.buildTrainings());
        this.mockMvc.perform(
                get(USER_API + "/1/trainings/given")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(TrainingTestUtils.ID.intValue())))
                .andExpect(jsonPath("$[0].name", is(TrainingTestUtils.NAME)))
                .andExpect(jsonPath("$[0].description", is(TrainingTestUtils.DESCRIPTION)));
    }

    /*
     * retrieveParticipatedTrainings Tests
     */

    @Test
    void retrieveParticipatedTrainings_OneFollowedTraining_ReturnTraining() throws Exception {

        Mockito.when(userAccountService.getAccountEntityByUserId(Mockito.anyLong())).thenReturn(UserAccountTestUtils.buildUserAccountEntity());
        Mockito.when(userService.retrieveParticipatedTrainings(Mockito.anyLong())).thenReturn(TrainingTestUtils.buildTrainings());
        this.mockMvc.perform(
                get(USER_API + "/1/trainings/followed")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(TrainingTestUtils.ID.intValue())))
                .andExpect(jsonPath("$[0].startingDate", is(TrainingTestUtils.TODAY.toString())))
                .andExpect(jsonPath("$[0].duration", is(TrainingTestUtils.DURATION)));
    }

    /*
     * retrieveWhishlistTrainings Tests
     */

    @Test
    void retrieveWhishlistTrainings_OneTrainingInWhishlist_ReturnTraining() throws Exception {

        Mockito.when(userService.retrieveWhishlist(Mockito.anyLong())).thenReturn(TrainingTestUtils.buildTrainings());
        this.mockMvc.perform(
                get(USER_API + "/1/whishlist")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(TrainingTestUtils.ID.intValue())))
                .andExpect(jsonPath("$[0].name", is(TrainingTestUtils.NAME)))
                .andExpect(jsonPath("$[0].duration", is(TrainingTestUtils.DURATION)));
    }

    /*
     * retrieveParticipatedTrainings Tests
     */

    @Test
    void retrieveUserResource_ExistUser_ReturnUserResource() throws Exception {

        Mockito.when(userService.retrieveUserResource(Mockito.anyLong())).thenReturn(UserResourceTestUtils.buildUserResource());
        this.mockMvc.perform(
                get(USER_API + "/1/resources")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getAdminToken())
        )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nbDays", is(UserResourceTestUtils.NB_DAYS.doubleValue())))
                .andExpect(jsonPath("$.budget", is(UserResourceTestUtils.BUDGET.doubleValue())))
                .andExpect(jsonPath("$.jackpot", is(UserResourceTestUtils.JACKPOT.doubleValue())));
    }


    /*
     * whishlist tests
     */

    @Test
    void addToWhishlist_ExistTraining_ReturnOk() throws Exception {

        // in the whishlist service only the authenticated user can modify the whishlist of the user passed in url path that's why we have to change the user
        // test object to match the authenticated user (auth user id : 88 ; auth user email : user@mail.com ; check data-h2.sql for details)
        renderAuthenticatedUser();

        Mockito.doNothing().when(userService).addToWhishlist(Mockito.anyLong(), Mockito.any(User.class));
        this.mockMvc.perform(
                get(USER_API + "/88/whishlist/1/add")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    /**
     * return the authenticated user (not the admin) which is user@email.com and configure mockito to return this one
     *
     * @return
     */
    private UserAccount renderAuthenticatedUser() {
        UserAccount account = UserAccountTestUtils.buildUserAccountEntity();
        account.setId(88L);
        account.setEmail("user@mail.com");
        Mockito.when(userAccountService.getAccountEntityByUserId(Mockito.anyLong())).thenReturn(account);
        return account;
    }

    @Test
    void removeFromWhishlist_ExistTraining_ReturnOk() throws Exception {

        renderAuthenticatedUser();

        Mockito.doNothing().when(userService).removeFromWhishlist(Mockito.anyLong(), Mockito.any(User.class));
        this.mockMvc.perform(
                get(USER_API + "/88/whishlist/1/remove")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void retrieveUser_DifferentUserThanAuthenticated_ExceptionThrown() throws Exception {

        // change the mocked user so we can do this test
        UserAccount mockedAccount = UserAccountTestUtils.buildUserAccountEntity();
        mockedAccount.setEmail("email@mail.com");
        Mockito.when(userAccountService.getAccountEntityByUserId(Mockito.anyLong()))
                .thenReturn(mockedAccount);

        Mockito.doNothing().when(userService).removeFromWhishlist(Mockito.anyLong(), Mockito.any(User.class));
        this.mockMvc.perform(
                get(USER_API + "/99/whishlist/1/remove")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isMethodNotAllowed());
    }


    /*
     * followed trainings tests
     */

    @Test
    void participateToTheTraining_ExistTraining_ReturnOk() throws Exception {

        renderAuthenticatedUser();
        Mockito.doNothing().when(userService).addToFollowed(Mockito.anyLong(), Mockito.any(User.class));
        this.mockMvc.perform(
                get(USER_API + "/88/trainings/followed/1/participate")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void leaveToTheTraining_ExistTraining_ReturnOk() throws Exception {

        renderAuthenticatedUser();
        Mockito.doNothing().when(userService).removeFromFollowed(Mockito.anyLong(), Mockito.any(User.class));
        this.mockMvc.perform(
                get(USER_API + "/88/trainings/followed/1/leave")
                        .header(OauthToken.AUTHORIZATION, oauthToken.getUserToken())
        )
                .andDo(print())
                .andExpect(status().isOk());
    }
}
