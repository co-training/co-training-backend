package com.co.training.rest.config.security.resourceserver;

import com.co.training.rest.config.filter.LogFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Profile("h2")
/*
 * the config of routes in dev is not the same in production ( in dev we use h2 db ) that's why we have a dev profile (h2) and a production profile
 * (postgres)
 */
@EnableResourceServer
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class H2ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/", "/oauth/token", "/oauth/authorize**", "/v1/teams", "/h2-console/**", "/actuator/**")
                .permitAll()
                .antMatchers("/v1/levels/**").fullyAuthenticated()
                .antMatchers("/v1/levels").fullyAuthenticated()
                .antMatchers("/v1/trainings/**").fullyAuthenticated()
                .antMatchers("/v1/trainings").fullyAuthenticated()
                .antMatchers("/v1/reviews/**").fullyAuthenticated()
                .antMatchers("/v1/reviews").fullyAuthenticated()
                .antMatchers("/v1/users/**").fullyAuthenticated()
                .antMatchers("/v1/users").fullyAuthenticated()
                .antMatchers("/v1/tags").fullyAuthenticated()
                .antMatchers("/v1/tags/**").fullyAuthenticated()
                .antMatchers(HttpMethod.POST, "/v1/accounts").permitAll()
                .antMatchers("/v1/accounts/**").fullyAuthenticated()
                .antMatchers("/v1/teams/**").fullyAuthenticated()
                .antMatchers(HttpMethod.POST, "/v1/teams").fullyAuthenticated()
                .anyRequest().denyAll()
                .and()
                .addFilterAfter(logFilter(), BasicAuthenticationFilter.class)
                /*
                 * for dev platform we have to change some security rules so we can have access the h2 console. as this will make the app vulnerable in the
                 * production we have to do it in a different profile which is h2 in ou case
                 * https://springframework.guru/using-the-h2-database-console-in-spring-boot-with-spring-security/
                 * https://stackoverflow.com/questions/41961270/h2-console-and-spring-security-permitall-not-working
                 */
                .headers().frameOptions().disable()
                .and()
                .csrf().disable()
                .cors().disable();
    }

    @Bean
    public LogFilter logFilter() {
        return new LogFilter();
    }
}