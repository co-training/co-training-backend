package com.co.training.rest.config.security.authserver;

import com.co.training.rest.config.security.OauthConfig;
import com.co.training.rest.util.security.AppUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

    private static final String GRANT_TYPE_PASSWORD = "password";
    private static final String AUTHORIZATION_CODE = "authorization_code";
    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String SCOPE_READ = "read";
    private static final String SCOPE_WRITE = "write";
    private static final String TRUST = "trust";

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    private OauthConfig oauthConfig;

    @Autowired
    private AppUserDetailsService appUserDetailsService;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

        clients.inMemory()
                .withClient(oauthConfig.getClientId())
                //the user with whom we will identify in the server

                .authorizedGrantTypes(GRANT_TYPE_PASSWORD, AUTHORIZATION_CODE, REFRESH_TOKEN)
                //we specify  services that configure for the defined user, for 'client.' In our example, we will only use the password service.

                .authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT", "USER")
                //specifies roles or groups contained by the service offered

                .scopes(SCOPE_READ, SCOPE_WRITE, TRUST)
                //is the scope of the service
                .autoApprove(true)

                .secret(passwordEncoder().encode(oauthConfig.getClientPassword()))
                //the password of the client

                .accessTokenValiditySeconds(oauthConfig.getTokenValidity())  //Access token is only valid for 2 minutes.
                .refreshTokenValiditySeconds(oauthConfig.getRefreshTokenValidity());//Refresh token is only valid for 10 minutes.
    }


    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {

        endpoints
                .authenticationManager(authenticationManager)
                .tokenStore(tokenStore())
                .userDetailsService(appUserDetailsService);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {

        //The encode function is annotated with the @Bean tag because Spring, when we supply the password in an HTTP request,
        // will look for a  PasswordEncoder object to check the validity of the delivered password.
        return new BCryptPasswordEncoder();
    }

    @Bean
    public TokenStore tokenStore() {

        //tokens will be stored in memory
        return new InMemoryTokenStore();
    }
}
