package com.co.training.rest.controller.handler;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.time.ZonedDateTime;

@Getter
@ToString
@EqualsAndHashCode
/**
 * This object is used to render a summary of the exception thrown by our application. It's gonna be the same
 * as the default spring boot error response
 */
class ApiExceptionResponse {

    private ZonedDateTime timestamp = ZonedDateTime.now();
    private int status;
    private String error;
    private String message;
    private String path;
    private String method;

    public ApiExceptionResponse setStatus(int status) {
        this.status = status;
        return this;
    }

    public ApiExceptionResponse setError(String error) {
        this.error = error;
        return this;
    }

    public ApiExceptionResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public ApiExceptionResponse setPath(String path) {
        this.path = path;
        return this;
    }

    public ApiExceptionResponse setMethod(String method) {
        this.method = method;
        return this;
    }
}
