package com.co.training.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = {"com.co.training.common", "com.co.training.rest"})
// we've added this annotation to be able to scan our spring beans
@EntityScan(basePackages = {"com.co.training.rest.entities", "com.co.training.dao.entities"})
// we've added this annotation to be able to scan our entities package
@EnableJpaRepositories(basePackages = {"com.co.training.dao.repository", "com.co.training.rest.repository"})
@EnableCaching
public class RestBootstrap {

	public static void main(String[] args) {
		SpringApplication.run(RestBootstrap.class, args);
	}
}
