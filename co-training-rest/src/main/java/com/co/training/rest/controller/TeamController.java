package com.co.training.rest.controller;


import com.co.training.common.service.TeamService;
import org.openapitools.client.model.Team;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/teams")
public class TeamController {

    private final TeamService teamService;

    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    /**
     * get all the platform teams
     *
     * @return
     */
    @GetMapping
    public List<Team> getAllTeams() {

        return teamService.findAll();
    }

    /**
     * Create a new team;
     * we need to return the entity after creating it because it will be used by the frontend to keep an image about what we have in the database
     * (to keep tracking ids to be more specific, theses ids will be used by other features to update and delete a team)
     *
     * @param team the team to add
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Team createNewTeam(@RequestBody Team team) {

        return teamService.createNewTeam(team);
    }

    /**
     * Update an existing team
     *
     * @param team team data to update
     * @param id   team id to update
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateTeam(@RequestBody Team team, @PathVariable Long id) {

        teamService.updateTeam(team, id);
    }

    /**
     * delete an existing team
     *
     * @param id the id of the team to delete
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteTeam(@PathVariable Long id) {

        teamService.deleteTeam(id);
    }
}
