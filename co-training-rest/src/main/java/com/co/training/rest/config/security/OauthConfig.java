package com.co.training.rest.config.security;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "app.oauth2")
public class OauthConfig {

    private String clientId;
    private String clientPassword;
    private Integer tokenValidity;
    private Integer refreshTokenValidity;
}
