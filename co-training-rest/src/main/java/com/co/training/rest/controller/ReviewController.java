package com.co.training.rest.controller;

import com.co.training.common.service.ReviewService;
import com.co.training.common.utils.PageableUtils;
import com.co.training.rest.util.security.SecurityUtils;
import org.openapitools.client.model.TrainingReview;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/v1/reviews")
@Validated  // validate request params and path variables
public class ReviewController {

    private final ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    /**
     * Create a new review
     *
     * @param review
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_TRAININGS_WRITER')")
    public void createNewReview(@RequestBody TrainingReview review) {

        reviewService.createNewReview(review, SecurityUtils.getPrincipal().getUserAccount().getUser());
    }

    /**
     * Update an existing review
     *
     * @param review
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_TRAININGS_WRITER')")
    public void updateReview(@RequestBody TrainingReview review, @PathVariable Long id) {

        reviewService.updateReview(review, id, SecurityUtils.getPrincipal().getUserAccount().getUser());
    }

    /**
     * Get a list of reviews of a training using pagination
     *
     * @param page
     * @param size
     * @param trainingId
     * @param uid
     * @param stars
     * @return
     */
    @GetMapping("/search")
    @PreAuthorize("hasRole('ROLE_TRAININGS_READER') or hasRole('ROLE_ADMIN')")
    public List<TrainingReview> getTrainingReviews(@Min(value = 0, message = PageableUtils.PAGE_NUMBER_MIN_VALUE)
                                                   @RequestParam Integer page,
                                                   @Max(value = 100, message = PageableUtils.PAGE_SIZE_MAX_VALUE)
                                                   @Min(value = 1, message = PageableUtils.PAGE_SIZE_MIN_VALUE)
                                                   @RequestParam Integer size,
                                                   @RequestParam(required = false) Long trainingId,
                                                   @RequestParam(required = false) Long uid,
                                                   @RequestParam(required = false) TrainingReview.StarsEnum stars) {

        return reviewService.getTrainingReviews(page, size, trainingId, uid, stars);
    }
}
