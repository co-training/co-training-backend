package com.co.training.rest.util.security;

import com.co.training.rest.entities.UserAccount;
import com.co.training.rest.entities.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This object will contain our user's data and roles, it will be used by spring security to get information about the
 * authenticated user
 */
public class AppUserPrincipal implements UserDetails {

    private final UserAccount userAccount;
    private final List<UserRole> userRoles;

    public AppUserPrincipal(@NotNull UserAccount userAccount) {
        this.userAccount = userAccount;
        userRoles = userAccount.getRoles();
        Assert.noNullElements(userRoles, "User roles cannot be empty");
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userRoles.stream().map(UserRole::getRole)
                .map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return userAccount.getPassword();
    }

    @Override
    public String getUsername() {
        return userAccount.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return userAccount.getIsActive();
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public List<UserRole> getUserRoles() {
        return userRoles;
    }
}
