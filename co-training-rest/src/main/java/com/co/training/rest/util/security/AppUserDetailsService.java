package com.co.training.rest.util.security;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.rest.entities.UserAccount;
import com.co.training.rest.repository.UserAccountRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * This service will be used by spring to retrieve the user principal from database
 */
@Service
public class AppUserDetailsService implements UserDetailsService {

    private final UserAccountRepository userAccountRepository;

    public AppUserDetailsService(UserAccountRepository userAccountRepository) {
        this.userAccountRepository = userAccountRepository;
    }

    @Transactional
    /*
     *  The transactional annotation here is important because the fetch of the user roles is lazy
     *  when we create the AppUserPrincipal instance ( in the constructor ) we will load the roles and we are in a transaction hibernate
     *  will execute a query to get roles
     *  If you don't make the call in the AppUserPrincipal ( or in this method globally ) and you'll have an error because the transaction
     *  will be closed and the method that calls our method ( it'll be a spring security method ) cannot retrieve roles without transaction
     */
    @Override
    public UserDetails loadUserByUsername(String email) {

        Optional<UserAccount> account = userAccountRepository.findByEmail(email);
        if (!account.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_ACCOUNT);
        return new AppUserPrincipal(account.get());
    }
}
