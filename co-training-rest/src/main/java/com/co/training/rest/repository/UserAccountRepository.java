package com.co.training.rest.repository;

import com.co.training.rest.entities.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {

    boolean existsByEmail(String email);

    Optional<UserAccount> findByEmail(String email);

    Optional<UserAccount> findByUserId(Long userId);

    List<UserAccount> findByIsNewTrueOrderByRequestDateDesc();
}
