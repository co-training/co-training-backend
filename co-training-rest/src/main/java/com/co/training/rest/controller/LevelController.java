package com.co.training.rest.controller;

import com.co.training.common.service.LevelService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/levels")
public class LevelController {

    private final LevelService levelService;

    public LevelController(LevelService levelService) {
        this.levelService = levelService;
    }

    /**
     * get all trainings levels, users will use them to indicate the level of a training
     *
     * @return
     */
    @GetMapping
    @PreAuthorize("hasRole('ROLE_TRAININGS_WRITER')")
    public List<String> getAllLevels() {

        return levelService.findAll();
    }
}
