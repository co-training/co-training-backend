package com.co.training.rest.config;

import org.openapitools.client.model.TrainingReview;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry) {

        // this converter will help us to user the enumeration in the controller
        registry.addConverter(new StringToTrainingReviewConverter());
    }

    class StringToTrainingReviewConverter implements Converter<String, TrainingReview.StarsEnum> {
        @Override
        public TrainingReview.StarsEnum convert(String source) {
            try {
                return TrainingReview.StarsEnum.fromValue(source);
            } catch (IllegalArgumentException e) {
                return null;
            }
        }
    }
}
