package com.co.training.rest.controller;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.ResourceNotAllowedException;
import com.co.training.common.service.UserService;
import com.co.training.common.utils.PageableUtils;
import com.co.training.rest.entities.UserAccount;
import com.co.training.rest.service.UserAccountService;
import com.co.training.rest.util.security.SecurityUtils;
import org.openapitools.client.model.*;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/v1/users")
@Validated  // validate request params and path variables
public class UserController {

    private final UserService userService;
    private final UserAccountService userAccountService;

    public UserController(UserService userService, UserAccountService userAccountService) {
        this.userService = userService;
        this.userAccountService = userAccountService;
    }

    /**
     * Retrieve all users using pagination
     *
     * @param page
     * @param size
     * @return
     */
    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> retrieveUsers(@Min(value = 0, message = PageableUtils.PAGE_NUMBER_MIN_VALUE)
                                    @RequestParam Integer page,
                                    @Max(value = 100, message = PageableUtils.PAGE_SIZE_MAX_VALUE)
                                    @Min(value = 1, message = PageableUtils.PAGE_SIZE_MIN_VALUE)
                                    @RequestParam Integer size) {
        return userService.retrieveUsers(page, size);
    }

    /**
     * Retrieve a user by its unique identifier
     *
     * @param uid
     * @return
     */
    @GetMapping("/{uid}")
    @PreAuthorize("hasRole('ROLE_USERS_READER') or hasRole('ROLE_ADMIN')")
    public User retrieveUser(@PathVariable Long uid) {

        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(uid);
        return userService.retrieveUserById(uid);
    }

    /**
     * Retrieve a user description by its unique identifier
     *
     * @param uid
     * @return
     */
    @GetMapping("/{uid}/describe")
    @PreAuthorize("isAuthenticated()")
    public User retrieveUserDescription(@PathVariable Long uid) {

        return userService.retrieveUserDescription(uid);
    }

    /**
     * Update an existing user
     *
     * @param user user data to update
     * @param uid  user id to update
     */
    @PutMapping("/{uid}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ROLE_USERS_WRITER')")
    public void updateUser(@RequestBody User user, @PathVariable Long uid) {

        this.checkTheWrightToManageTheAccount(uid);
        userService.updateUser(user, uid);
    }

    /**
     * update the user team, this method is made for admins so they can update users teams
     *
     * @param uid
     */
    @PutMapping("/{uid}/team")
    @PreAuthorize("hasRole('ROLE_USERS_WRITER') or hasRole('ROLE_ADMIN')")
    public void updateUserTeam(@RequestBody Team team, @PathVariable Long uid) {

        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(uid);
        userService.updateUserTeam(team, uid);
    }

    /**
     * Update the user resources ( only admins can do that )
     *
     * @param userResource user resource data to update
     * @param uid          user id to update
     */
    @PutMapping("/{uid}/resources")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateUserResources(@RequestBody UserResource userResource, @PathVariable Long uid) {

        userService.updateUserResources(userResource, uid);
    }

    /**
     * Retrieve trainings in which this user is the author
     *
     * @param uid
     * @return
     */
    @GetMapping("/{uid}/trainings/given")
    @PreAuthorize("hasRole('ROLE_USERS_READER') or hasRole('ROLE_ADMIN')")
    public List<Training> retrieveGivenTrainings(@PathVariable Long uid) {

        return userService.retrieveGivenTrainings(uid);
    }


    /**
     * retrieve the user score : it will be based on the user's trainings scores
     *
     * @param uid
     * @return
     */
    @GetMapping("/{uid}/evaluate")
    @PreAuthorize("isFullyAuthenticated()")
    // accessible by any authenticated user
    public TrainingAuthorEvaluation evaluateUser(@PathVariable Long uid) {

        return userService.evaluate(uid);
    }

    /**
     * Retrieve trainings in which this user is a participant
     *
     * @param uid
     * @return
     */
    @GetMapping("/{uid}/trainings/followed")
    @PreAuthorize("hasRole('ROLE_USERS_READER') or hasRole('ROLE_ADMIN')")
    public List<Training> retrieveParticipatedTrainings(@PathVariable Long uid) {

        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(uid);
        return userService.retrieveParticipatedTrainings(uid);
    }


    /**
     * Remove the current user from the list of participants of the training
     *
     * @param uid
     * @param id
     */
    @GetMapping("/{uid}/trainings/followed/{id}/leave")
    @PreAuthorize("hasRole('ROLE_USERS_WRITER')")
    public void leaveTheTraining(@PathVariable Long uid, @PathVariable Long id) {

        // check if the uid passed in the path params is the same as the uid of the current user ( users can only manage theirs accounts not others accounts)
        this.checkTheWrightToManageTheAccount(uid);
        userService.removeFromFollowed(id, getAuthenticatedUser().getUser());
    }


    /**
     * Add the current user ( the authenticated user ) to the list of participants of the training
     *
     * @param uid
     * @param id
     */
    @GetMapping("/{uid}/trainings/followed/{id}/participate")
    @PreAuthorize("hasRole('ROLE_USERS_WRITER')")
    public void participateToTheTraining(@PathVariable Long uid, @PathVariable Long id) {

        this.checkTheWrightToManageTheAccount(uid);
        userService.addToFollowed(id, getAuthenticatedUser().getUser());
    }

    /**
     * Retrieve the whishlist of trainings of the user
     *
     * @param uid
     * @return
     */
    @GetMapping("/{uid}/whishlist")
    @PreAuthorize("hasRole('ROLE_USERS_READER') or hasRole('ROLE_ADMIN')")
    public List<Training> retrieveWhishlistTrainings(@PathVariable Long uid) {

        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(uid);
        return userService.retrieveWhishlist(uid);
    }

    /**
     * count the user whishlist number of elements
     *
     * @param uid
     * @return
     */
    @GetMapping("/{uid}/whishlist/count")
    @PreAuthorize("hasRole('ROLE_USERS_READER') or hasRole('ROLE_ADMIN')")
    public Long countWhishlistTrainings(@PathVariable Long uid) {

        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(uid);
        return userService.countWhishlist(uid);
    }


    /**
     * Add the passed training to the user whishlist
     *
     * @param uid
     * @param id
     */
    @GetMapping("/{uid}/whishlist/{id}/add")
    @PreAuthorize("hasRole('ROLE_USERS_WRITER')")
    public void addToWhishlist(@PathVariable Long uid, @PathVariable Long id) {

        this.checkTheWrightToManageTheAccount(uid);
        userService.addToWhishlist(id, getAuthenticatedUser().getUser());
    }

    /**
     * remove the passed training to the user whishlist
     *
     * @param uid
     * @param id
     */
    @GetMapping("/{uid}/whishlist/{id}/remove")
    @PreAuthorize("hasRole('ROLE_USERS_WRITER')")
    public void removeFromWhishlist(@PathVariable Long uid, @PathVariable Long id) {

        this.checkTheWrightToManageTheAccount(uid);
        userService.removeFromWhishlist(id, getAuthenticatedUser().getUser());
    }

    /**
     * Get the user resource
     *
     * @param uid
     * @return
     */
    @GetMapping("/{uid}/resources")
    @PreAuthorize("hasRole('ROLE_USERS_READER') or hasRole('ROLE_ADMIN') ")
    public UserResource retrieveUserResource(@PathVariable Long uid) {

        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(uid);
        return userService.retrieveUserResource(uid);
    }

    /**
     * This method will check if the principal user has the right to get the data of a user resource
     * He can read the data only if he is admin or if the data to check is his own data
     *
     * @param userId
     */
    private void checkTheWrightToManageTheAccount(Long userId) {

        if (!SecurityUtils.canManageUserResources(userAccountService.getAccountEntityByUserId(userId)))
            throw new ResourceNotAllowedException(ApiExceptionsMessages.RESOURCE_NOT_ALLOWED);
    }

    /**
     * This method return the current authenticated user
     *
     * @return
     */
    private UserAccount getAuthenticatedUser() {

        return SecurityUtils.getPrincipal().getUserAccount();
    }
}
