package com.co.training.rest.controller;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.ResourceNotAllowedException;
import com.co.training.common.service.ReviewService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.utils.PageableUtils;
import com.co.training.rest.entities.UserAccount;
import com.co.training.rest.service.UserAccountService;
import com.co.training.rest.util.security.SecurityUtils;
import org.openapitools.client.model.Training;
import org.openapitools.client.model.TrainingEvaluation;
import org.openapitools.client.model.User;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/v1/trainings")
@Validated  // validate request params and path variables
public class TrainingController {

    private final TrainingService trainingService;
    private final ReviewService reviewService;
    private final UserAccountService userAccountService;

    public TrainingController(TrainingService trainingService, ReviewService reviewService, UserAccountService userAccountService) {
        this.trainingService = trainingService;
        this.reviewService = reviewService;
        this.userAccountService = userAccountService;
    }

    /**
     * Create a new training
     *
     * @param training to add to the platform
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasRole('ROLE_TRAININGS_WRITER')")
    public void createNewTraining(@RequestBody Training training, Authentication authentication) {

        // get the current authenticated user, he will be the author
        com.co.training.dao.entities.User author = userAccountService.getAccountEntityByEmail(authentication.getName()).getUser();
        trainingService.createNewTraining(training, author);
    }

    /**
     * Update an existing training
     *
     * @param training to to be updated
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasRole('ROLE_TRAININGS_WRITER')")
    public void updateTraining(@RequestBody Training training, @PathVariable Long id, Authentication authentication) {

        UserAccount authUser = userAccountService.getAccountEntityByEmail(authentication.getName());
        this.allowModifyResourceAction(trainingService.getTrainingApi(id), authUser);
        trainingService.updateTraining(training, id, authUser.getUser());
        /*
         * TODO Hibernate cache issue
         *  it seems that we can the update training method does not need the author because we will update only the training the author remains the same
         * the issue is that when we get the training in the update method the author will be null. we did a first call to the training in the first instruction
         *  userAccountService.getAccountEntityByEmail and we get the author but if we do the same call in the update (or redo the getAccountEntityByEmail call) the
         * author is null (the author in the second call will be retrieved from hibernate cache)
         */
    }

    /**
     * delete an existing training
     *
     * @param trainingId the id of the training to delete
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasRole('ROLE_TRAININGS_WRITER')")
    public void deleteTraining(@PathVariable("id") Long trainingId, Authentication authentication) {

        UserAccount authUser = userAccountService.getAccountEntityByEmail(authentication.getName());
        this.allowModifyResourceAction(trainingService.getTrainingApi(trainingId), authUser);
        trainingService.deleteTraining(trainingId, authUser.getUser());
    }

    /**
     * Get a list of trainings using pagination and a creation date interval
     *
     * @param start
     * @param end
     * @param page
     * @param size
     * @return
     */
    @GetMapping
    @PreAuthorize("hasRole('ROLE_TRAININGS_READER') or hasRole('ROLE_ADMIN')")
    public List<Training> getTrainings(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate start,
                                       @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate end,
                                       @Min(value = 0, message = PageableUtils.PAGE_NUMBER_MIN_VALUE)
                                       @RequestParam Integer page,
                                       @Max(value = 100, message = PageableUtils.PAGE_SIZE_MAX_VALUE)
                                       @Min(value = 1, message = PageableUtils.PAGE_SIZE_MIN_VALUE)
                                       @RequestParam Integer size) {

        return trainingService.getTrainings(start, end, page, size);
    }

    /**
     * Get the total number of trainings of the last filter
     *
     * @param start
     * @param end
     * @param page
     * @param size
     * @return
     */
    // TODO this method and the last one are to replace with a unique method that returns a page which contains the result ( a list o trainings ) and the count (total number of trainings retrieved by the filter)
    @GetMapping("/count")
    @PreAuthorize("hasRole('ROLE_TRAININGS_READER') or hasRole('ROLE_ADMIN')")
    public Long countTrainings(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate start,
                               @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate end,
                               @Min(value = 0, message = PageableUtils.PAGE_NUMBER_MIN_VALUE)
                               @RequestParam Integer page,
                               @Max(value = 100, message = PageableUtils.PAGE_SIZE_MAX_VALUE)
                               @Min(value = 1, message = PageableUtils.PAGE_SIZE_MIN_VALUE)
                               @RequestParam Integer size) {

        return trainingService.countTrainings(start, end, page, size);
    }

    /**
     * Get a training by id
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_TRAININGS_READER') or hasRole('ROLE_ADMIN')")
    public Training getTraining(@PathVariable Long id) {

        return trainingService.getTrainingApi(id);
    }

    /**
     * Get a list of the participants of a training using pagination
     *
     * @param id   The training id
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/{id}/participants")
    @PreAuthorize("hasRole('ROLE_TRAININGS_READER') or hasRole('ROLE_ADMIN')")
    public List<User> getTrainingParticipants(@PathVariable Long id,
                                              @Min(value = 0, message = PageableUtils.PAGE_NUMBER_MIN_VALUE)
                                              @RequestParam Integer page,
                                              @Max(value = 100, message = PageableUtils.PAGE_SIZE_MAX_VALUE)
                                              @Min(value = 1, message = PageableUtils.PAGE_SIZE_MIN_VALUE)
                                              @RequestParam Integer size) {

        return trainingService.getTrainingParticipants(id, page, size);
    }

    /**
     * Get the number of participants
     *
     * @param id The training id
     * @return
     */
    @GetMapping("/{id}/participants/count")
    @PreAuthorize("hasRole('ROLE_TRAININGS_READER') or hasRole('ROLE_ADMIN')")
    public Long getTrainingParticipantsCount(@PathVariable Long id) {

        return trainingService.countTrainingParticipants(id);
    }

    /**
     * Evaluate the training and return an object which contains the rating, comments participants ...
     *
     * @param id
     */
    @GetMapping("/{id}/evaluate")
    @PreAuthorize("hasRole('ROLE_TRAININGS_READER') or hasRole('ROLE_ADMIN')")
    public TrainingEvaluation evaluateTraining(@PathVariable Long id) {

        return reviewService.evaluateTraining(id);
    }

    /**
     * count the number of reviews of a given training id
     *
     * @param id
     */
    @GetMapping("/{id}/reviews/count")
    @PreAuthorize("hasRole('ROLE_TRAININGS_READER') or hasRole('ROLE_ADMIN')")
    public Long countTrainingReviews(@PathVariable Long id) {

        return trainingService.countTrainingReviews(id);
    }

    /**
     * This method will check if the principal user has the right to write a training
     * He can add a training only if the he is its author
     *
     * @param training
     * @param user
     */
    private void allowModifyResourceAction(Training training, UserAccount user) {

        if (!SecurityUtils.canModifyTrainingResource(training, user))
            throw new ResourceNotAllowedException(ApiExceptionsMessages.RESOURCE_NOT_ALLOWED);
    }
}
