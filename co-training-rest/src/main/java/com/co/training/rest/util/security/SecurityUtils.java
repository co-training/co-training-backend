package com.co.training.rest.util.security;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.BadRequestException;
import com.co.training.rest.config.security.AppRoles;
import com.co.training.rest.entities.UserAccount;
import com.co.training.rest.entities.UserRole;
import org.openapitools.client.model.Training;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class SecurityUtils {

    private SecurityUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * This methods will return the authenticated user
     *
     * @return
     */
    public static AppUserPrincipal getPrincipal() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null)
            return null;

        if (!(authentication instanceof AnonymousAuthenticationToken)) {

            Object principal = authentication.getPrincipal();
            if (principal instanceof AppUserPrincipal)
                return (AppUserPrincipal) principal;

            return null;
        }
        return null;
    }

    // TODO refactor this and the last methods

    /**
     * returns true if the authenticated user can do write actions like delete, update ... on the account passed in parameters
     * a user can manage a resource if he owns its data ( even admins cannot update users data - some of them like personal data )
     *
     * @param user
     * @return
     */
    public static boolean canManageUserResources(@NotNull UserAccount user) {

        AppUserPrincipal principal = getPrincipal();
        if (principal == null)
            return false;

        return principal.getUsername().equals(user.getEmail());
    }

    /**
     * returns true if the current user id an admin, false otherwise
     *
     * @return
     */
    public static boolean isAdmin() {

        AppUserPrincipal principal = getPrincipal();
        if (principal == null)
            return false;

        if (principal.getUserRoles() != null) {

            List<String> roles = principal.getUserRoles().stream().map(UserRole::getRole).collect(Collectors.toList());
            if (roles.contains(AppRoles.ADMIN.role()))
                return true;
        }
        return false;
    }

    /**
     * Returns true if the current user can write a training resource
     *
     * @param training
     * @return
     */
    public static boolean canModifyTrainingResource(@NotNull Training training, @NotNull UserAccount account) {

        if (Objects.isNull(training.getAuthor()))
            throw new BadRequestException(ApiExceptionsMessages.TRAINING_AUTHOR_NULL);
        return (account.getUser().getId().equals(training.getAuthor().getId()));
    }
}
