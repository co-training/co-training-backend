package com.co.training.rest.service;

import com.co.training.rest.entities.UserAccount;
import org.openapitools.client.model.Account;
import org.openapitools.client.model.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public interface UserAccountService {

    @Transactional
    void createUserAccount(String email, String password, User user);

    List<Account> retrieveUsersAccounts(Integer page, Integer size);

    void updateUserPassword(Long id, String oldPassword, String newPassword);

    Account retrieveUserAccount(Long accountId);

    User retrieveUser(Long accountId);

    @Transactional
    void deleteUserAccount(Long accountId);

    @Transactional
    void cancelAccountDeletion(Long accountId);

    UserAccount getAccountEntityById(Long accountId);

    UserAccount getAccountEntityByEmail(String email);

    UserAccount getAccountEntityByUserId(Long userId);

    @Transactional
    void enableUserAccount(Long accountId);

    @Transactional
    /**
     * be careful to annotate only the transactional methods with @Transactional otherwise you'll have unexpected results for example
     * if you annotate the retrieveNewRequests method, users will be detached from users accounts after each execution
     */
    void disableUserAccount(Long accountId);

    List<Account> retrieveNewRequests();

    @Transactional
    void disableAccountAlert(Long id);

    List<String> retrieveRoles();

    void updateRoles(Long id, List<String> roles);

    Long countUsersAccounts();

    void updateUserEmail(Long id, String email);
}
