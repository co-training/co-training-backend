package com.co.training.rest.config.security;

public enum AppRoles {

    ADMIN("ROLE_ADMIN"),
    TRAININGS_READER("ROLE_TRAININGS_READER"),
    TRAININGS_WRITER("ROLE_TRAININGS_WRITER"),
    USERS_READER("ROLE_USERS_READER"),
    USERS_WRITER("ROLE_USERS_WRITER");

    private String role;

    AppRoles(String role) {
        this.role = role;
    }

    public String role() {
        return role;
    }
}
