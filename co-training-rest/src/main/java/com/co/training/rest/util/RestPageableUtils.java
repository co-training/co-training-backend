package com.co.training.rest.util;

import com.co.training.common.utils.PageableUtils;
import org.springframework.data.domain.Pageable;

public final class RestPageableUtils {

    private RestPageableUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static Pageable userAccountPageRequest(Integer page, Integer size) {

        return PageableUtils.getPageRequestDesc(page, size, "creationDate");
    }
}
