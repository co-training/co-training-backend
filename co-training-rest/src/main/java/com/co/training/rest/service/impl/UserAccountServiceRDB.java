package com.co.training.rest.service.impl;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.BadRequestException;
import com.co.training.common.exceptions.ResourceAlreadyExistException;
import com.co.training.common.exceptions.ResourceNotAllowedException;
import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.common.mapper.UserMapper;
import com.co.training.common.validators.UserValidator;
import com.co.training.dao.entities.Team;
import com.co.training.dao.entities.User;
import com.co.training.dao.entities.UserResource;
import com.co.training.dao.repository.TeamRepository;
import com.co.training.rest.config.security.AppRoles;
import com.co.training.rest.entities.UserAccount;
import com.co.training.rest.entities.UserRole;
import com.co.training.rest.mapper.UserAccountMapper;
import com.co.training.rest.repository.UserAccountRepository;
import com.co.training.rest.service.UserAccountService;
import com.co.training.rest.util.RestPageableUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openapitools.client.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
/**
 * This is one of the implementations ( and the unique for now ) of the UserAccountService interface
 * suffix RDB is Relational Database, this object will do all the operations against the relational database of the app
 */
public class UserAccountServiceRDB implements UserAccountService {

    private static final Logger LOGGER = LogManager.getLogger(UserAccountServiceRDB.class);
    private final UserAccountRepository userAccountRepository;
    private final TeamRepository teamRepository;
    private final UserValidator userValidator;
    private PasswordEncoder passwordEncoder;

    public UserAccountServiceRDB(UserAccountRepository userAccountRepository, TeamRepository teamRepository, UserValidator userValidator) {
        this.userAccountRepository = userAccountRepository;
        this.teamRepository = teamRepository;
        this.userValidator = userValidator;
    }

    /**
     * we inject the password encoder so we can hash the user password after registration
     * https://www.baeldung.com/spring-security-registration-password-encoding-bcrypt
     *
     * @param passwordEncoder
     * @Lazy was added to resolve a dependency circular issue when running the karate integration tests
     */
    @Autowired
    public void setPasswordEncoder(@Lazy PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * This method is used to add a new user to the platform
     *
     * @param user
     */
    @Override
    public void createUserAccount(String email, String password, org.openapitools.client.model.User user) {

        // map the user api to an entity
        User userToSave = UserMapper.toUserEntity(user);

        // we can validate the user request body from the controller (using @Valid annotation) but as this object (user) is generated with open-api we can't validate it so we gonna convert
        // it to a user entity and then validate it using spring boot validator bean
        // keep in mind that this object gonna be validated a second time by the repository
        userValidator.validateEntityResource(userToSave);

        // validate email
        validateEmail(email);
        //validate password
        validatePassword(password);

        // check the user team existence
        // The team could not be null (check the team validation)
        Optional<Team> team = teamRepository.getByName(user.getTeam().getName());
        if (!team.isPresent())
            throw new ResourceNotFoundException("The team name is not recognized.");
        userToSave.setTeam(team.get())
                // create a new user resource (as this is a new user)
                .setUserResource(new UserResource());

        // for user interests and skills (as they are new) we have to set ids to null otherwise hibernate will look for them
        // by id before persisting (which will cause problems, exception if he did not found them)
        // interests and skills can be null
        if (Objects.nonNull(userToSave.getInterestsSkills()) && !userToSave.getInterestsSkills().isEmpty())
            userToSave.getInterestsSkills().forEach(
                    interestsSkills -> interestsSkills.setId(null)
            );

        // hash the password
        password = passwordEncoder.encode(password);
        // create a new account
        UserAccount userAccount = new UserAccount()
                .setEmail(email)
                .setPassword(password)
                .setUser(userToSave);
        // add default role (by default the user has read write permissions on the user and trainings resources
        userAccount.addUserRoles(
                Arrays.asList(
                        new UserRole().setEmail(userAccount.getEmail()).setRole(AppRoles.USERS_READER.role()),
                        new UserRole().setEmail(userAccount.getEmail()).setRole(AppRoles.USERS_WRITER.role()),
                        new UserRole().setEmail(userAccount.getEmail()).setRole(AppRoles.TRAININGS_READER.role()),
                        new UserRole().setEmail(userAccount.getEmail()).setRole(AppRoles.TRAININGS_WRITER.role())
                )
        );

        LOGGER.info("Adding a new user to the platform , {}", userAccount.getEmail());
        // save user
        userAccountRepository.save(userAccount);
    }

    /**
     * check the email existence
     *
     * @param email
     */
    private void validateEmail(String email) {

        // check if the email is already used
        if (userAccountRepository.existsByEmail(email))
            throw new ResourceAlreadyExistException(ApiExceptionsMessages.ALREADY_EXISTS_EMAIL);
    }

    /**
     * Retrieve users
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public List<Account> retrieveUsersAccounts(Integer page, Integer size) {

        List<UserAccount> accounts = userAccountRepository.findAll(RestPageableUtils.userAccountPageRequest(page, size)).getContent();
        return toApi(accounts);
    }

    /**
     * transform a list of entities to a list of api models
     *
     * @param accounts
     * @return
     */
    private List<Account> toApi(List<UserAccount> accounts) {
        List<Account> accountsApi = new ArrayList<>();
        if (accounts != null && !accounts.isEmpty())
            accounts.forEach(
                    account -> accountsApi.add(UserAccountMapper.toAccountApi(account))
            );
        return accountsApi;
    }


    /**
     * retrieve the list of new requests to join the platform
     *
     * @return
     */
    @Override
    public List<Account> retrieveNewRequests() {

        return toApi(userAccountRepository.findByIsNewTrueOrderByRequestDateDesc());
    }

    /**
     * disable the account alerts so it will not be show again in the new requests
     *
     * @param id
     */
    @Override
    public void disableAccountAlert(Long id) {

        userAccountRepository.save(getById(id).setIsNew(Boolean.FALSE));
    }

    /**
     * get app roles
     *
     * @return
     */
    @Override
    public List<String> retrieveRoles() {

        return Arrays.stream(AppRoles.values()).map(AppRoles::role).collect(Collectors.toList());
    }

    /**
     * update the account roles
     *
     * @param id
     * @param roles
     */
    @Override
    public void updateRoles(Long id, List<String> roles) {

        UserAccount account = getById(id);
        this.preventUpdatingAdminAccount(account);

        LOGGER.debug("update roles : current roles {}", account.getRoles());
        LOGGER.debug("update roles : roles to update {}", roles);
        List<String> allRoles = retrieveRoles();
        if (!allRoles.containsAll(roles))
            throw new BadRequestException(ApiExceptionsMessages.ROLE_NOT_RECOGNIZED);

        // update only roles that needs to update
        account.getRoles().removeIf(role -> !roles.contains(role.getRole()));

        List<String> currentRoles = account.getRoles().stream().map(UserRole::getRole).collect(Collectors.toList());
        // get difference
        roles.removeIf(role -> currentRoles.contains(role));
        // update user roles
        roles.forEach(role -> account.addUserRole(new UserRole().setEmail(account.getEmail()).setRole(role)));

        userAccountRepository.save(account);
    }

    /**
     * count the number of users accounts
     *
     * @return
     */
    @Override
    public Long countUsersAccounts() {

        return userAccountRepository.count();
    }

    /**
     * update the user email
     *
     * @param id
     * @param email
     */
    @Override
    public void updateUserEmail(Long id, String email) {

        // check account
        UserAccount account = getById(id);
        // validate email
        validateEmail(email);
        account.setEmail(email);
        userAccountRepository.save(account);
    }

    /**
     * update the user password
     *
     * @param id
     * @param oldPassword
     */
    @Override
    public void updateUserPassword(Long id, String oldPassword, String newPassword) {

        // validate passwords
        validatePassword(oldPassword);
        validatePassword(newPassword);

        // hash password
        newPassword = passwordEncoder.encode(newPassword);
        // get account
        UserAccount account = getById(id);
        // compare passwords
        if (!passwordEncoder.matches(oldPassword, account.getPassword())) {
            LOGGER.info("The received password and the one in the database does not match");
            throw new BadRequestException(ApiExceptionsMessages.INVALID_PASSWORD);
        }

        account.setPassword(newPassword);
        userAccountRepository.save(account);
    }

    /**
     * apply the password policy to the user password
     *
     * @param password
     */
    private void validatePassword(String password) {
        // for passwords we will not provide specific details about the password validation
        if (StringUtils.isBlank(password) || StringUtils.containsWhitespace(password) || password.length() < 5)
            throw new BadRequestException(ApiExceptionsMessages.INVALID_PASSWORD);
    }

    /**
     * Retrieve the user account, the returned account is an api object generated from the open api yaml file
     *
     * @param id
     * @return
     */
    @Override
    public Account retrieveUserAccount(Long id) {

        return UserAccountMapper.toAccountApi(getById(id));
    }

    /**
     * Retrieve the user details using an account id
     *
     * @param id
     * @return
     */
    @Override
    public org.openapitools.client.model.User retrieveUser(Long id) {

        return retrieveUserAccount(id).getUser();
    }

    /**
     * Delete a user account
     *
     * @param id
     */
    @Override
    public void deleteUserAccount(Long id) {

        UserAccount account = getById(id);
        this.preventUpdatingAdminAccount(account);

        LOGGER.info("Deleting a user account , {}", account.getEmail());
        //TODO make a delay of three days before deleting the account so the user can cancel the deletion

        userAccountRepository.delete(account);
    }

    /**
     * Cancel the account deletion
     *
     * @param id
     */
    @Override
    public void cancelAccountDeletion(Long id) {

        // TODO cancel the account deletion : add feature
        throw new BadRequestException("This method is not yet implemented");
    }

    /**
     * Return the user account by its id after doing some checks
     * The returned object is not an api object (generated from the open api file) but and entity object
     *
     * @param id
     * @return
     */
    @Override
    public UserAccount getAccountEntityById(Long id) {

        return getById(id);
    }

    /**
     * Return the user account by its email ( not the user email but the user's account email ) after doing some checks
     *
     * @param email
     * @return
     */
    @Override
    public UserAccount getAccountEntityByEmail(String email) {

        Optional<UserAccount> account = userAccountRepository.findByEmail(email);
        if (!account.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_ACCOUNT);
        return account.get();
    }

    /**
     * Return the account of a user using its id
     *
     * @param userId
     * @return
     */
    @Override
    public UserAccount getAccountEntityByUserId(Long userId) {

        Optional<UserAccount> account = userAccountRepository.findByUserId(userId);
        if (!account.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_ACCOUNT);
        return getById(userId);
    }

    /**
     * Activate user account
     *
     * @param accountId
     */
    @Override
    public void enableUserAccount(Long accountId) {

        UserAccount account = getById(accountId);
        this.preventUpdatingAdminAccount(account);

        account.setIsActive(Boolean.TRUE)
                .setIsNew(Boolean.FALSE);
        // the first time we will log the creation date
        if (Objects.isNull(account.getCreationDate()))
            account.setCreationDate(LocalDateTime.now());

        LOGGER.info("Enabling the user account , {}", account.getEmail());
        userAccountRepository.save(account);
    }

    /**
     * Deactivate user account
     *
     * @param accountId
     */
    @Override
    public void disableUserAccount(Long accountId) {

        switchUserAccountStatus(accountId, Boolean.FALSE);
    }

    /**
     * Switch on and off the user account
     *
     * @param accountId
     * @param status
     */
    private void switchUserAccountStatus(Long accountId, boolean status) {

        UserAccount account = getById(accountId);
        this.preventUpdatingAdminAccount(account);

        LOGGER.info("Disabling the user account , {}", account.getEmail());
        userAccountRepository.save(account.setIsActive(status));
    }

    /**
     * get account entity by id
     *
     * @param accountId
     * @return
     */
    private UserAccount getById(Long accountId) {

        Optional<UserAccount> account = userAccountRepository.findById(accountId);
        if (!account.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_ACCOUNT);
        return account.get();
    }

    /**
     * throw error if we attempt to update an admin account
     *
     * @param account
     */
    void preventUpdatingAdminAccount(UserAccount account) {

        if (account.getRoles().stream().map(UserRole::getRole).collect(Collectors.toList()).contains(AppRoles.ADMIN.role()))
            throw new ResourceNotAllowedException(ApiExceptionsMessages.CANNOT_UPDATE_ADMIN_ACCOUNT);
    }

}
