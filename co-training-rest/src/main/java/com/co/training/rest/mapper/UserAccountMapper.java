package com.co.training.rest.mapper;

import com.co.training.common.mapper.UserMapper;
import com.co.training.common.mapper.utils.MapperUtils;
import com.co.training.rest.entities.UserAccount;
import com.co.training.rest.entities.UserRole;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.openapitools.client.model.Account;
import org.openapitools.client.model.User;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public final class UserAccountMapper {

    private UserAccountMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static org.openapitools.client.model.Account toAccountApi(UserAccount accountEntity) {

        // map the user entity
        User user = UserMapper.toUserApi(accountEntity.getUser());
        accountEntity.setUser(null);

        // map roles : roles in the api and in the entity are not the same so we will map them manually
        List<UserRole> roles = accountEntity.getRoles();
        accountEntity.setRoles(null);

        Account accountApi = MapperUtils.copyCustomProperties(Account.class, accountEntity);

        // map user
        accountApi.setUser(user);

        // map roles
        accountApi.setRoles(
                roles.stream().map(UserRole::getRole).collect(Collectors.toList())
        );

        return accountApi;
    }

    public static UserAccount toAccountEntity(org.openapitools.client.model.Account accountApi) {

        UserAccount accountEntity = new UserAccount();

        try {
            // we can use apache commons to copy properties from api as this object contains simple collections
            // like arraylist and not the hibernate collections ( proxies )
            BeanUtils.copyProperties(accountEntity, accountApi);
        } catch (Exception e) {
            log.warn("An error has occurred when trying to convert an account api object to entity.", e);
        }
        return accountEntity;
    }
}
