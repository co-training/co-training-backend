package com.co.training.rest.entities;

import com.co.training.dao.entities.User;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "user_account")
public class UserAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    // the date when the admin accepts the user request to join the platform
    private LocalDateTime creationDate;

    // by default all accounts will be disabled until approve the request by the admin
    private Boolean isActive = false;

    // the date of receiving the request to join the platform
    @Column(nullable = false)
    private LocalDateTime requestDate = LocalDateTime.now();

    // all new requests (to join the app) will have this flag as true to say that this is a new request, the admin can disable this alert by making this flag false
    private Boolean isNew = true;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "user_account_id")
    private List<UserRole> roles = new ArrayList<>();

    public UserAccount addUserRole(UserRole role) {
        this.getRoles().add(role);
        return this;
    }

    public UserAccount addUserRoles(List<UserRole> roles) {
        this.getRoles().addAll(roles);
        return this;
    }
}
