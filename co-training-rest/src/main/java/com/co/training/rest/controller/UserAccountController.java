package com.co.training.rest.controller;


import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.ResourceNotAllowedException;
import com.co.training.common.utils.PageableUtils;
import com.co.training.rest.mapper.UserAccountMapper;
import com.co.training.rest.service.UserAccountService;
import com.co.training.rest.util.JacksonUtils;
import com.co.training.rest.util.security.SecurityUtils;
import org.openapitools.client.model.Account;
import org.openapitools.client.model.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/v1/accounts")
@Validated  // validate request params and path variables
public class UserAccountController {

    private final UserAccountService userAccountService;

    public UserAccountController(UserAccountService userAccountService) {
        this.userAccountService = userAccountService;
    }

    /**
     * create a new account, this will create a request and it is up to the admin to accept or not
     *
     * @param email
     * @param password
     * @param user
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("permitAll()")    // we will make the subscription public
    public void createUserAccount(@Email @RequestParam String email, @Size(min = 5) @RequestParam String password, @RequestBody User user) {

        // TODO is it a safe way to pass a password in the params ?
        userAccountService.createUserAccount(email, password, user);
    }

    /**
     * only admins can retrieve the users accounts of the app
     *
     * @param page
     * @param size
     * @return
     */
    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Account> retrieveUsersAccounts(@Min(value = 0, message = PageableUtils.PAGE_NUMBER_MIN_VALUE)
                                               @RequestParam Integer page,
                                               @Max(value = 100, message = PageableUtils.PAGE_SIZE_MAX_VALUE)
                                               @Min(value = 1, message = PageableUtils.PAGE_SIZE_MIN_VALUE)
                                               @RequestParam Integer size) {
        return userAccountService.retrieveUsersAccounts(page, size);
    }

    /**
     * count the users accounts
     *
     * @return
     */
    @GetMapping("/count")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Long countUsersAccounts() {
        return userAccountService.countUsersAccounts();
    }

    /**
     * retrieve the new users requests to join the platform
     *
     * @return
     */
    @GetMapping("/new")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<Account> retrieveNewUsersAccounts() {
        return userAccountService.retrieveNewRequests();
    }

    /**
     * retrieve the application roles
     *
     * @return
     */
    @GetMapping("/roles")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<String> retrieveRoles() {
        return userAccountService.retrieveRoles();
    }

    /**
     * update the account roles
     *
     * @return
     */
    @PutMapping("/{id}/roles")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateRoles(@PathVariable Long id, @RequestParam List<String> roles) {

        userAccountService.updateRoles(id, roles);
    }

    /**
     * retrieve the user account data using its account id
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USERS_WRITER')")
    public Account retrieveUserAccount(@PathVariable Long id) {

        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(id);
        return userAccountService.retrieveUserAccount(id);
    }

    /**
     * update the user password
     * we will use a generic method to get the password to be updated from the request body ( instead of creating new objects to handle body )
     *
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}/password", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ROLE_USERS_WRITER')")
    public void updateUserPassword(@PathVariable Long id, HttpEntity<String> httpEntity) {

        this.checkTheWrightToManageTheAccount(id);
        Map<String, String> passwordMap = JacksonUtils.getProperties(httpEntity.getBody());
        userAccountService.updateUserPassword(id, passwordMap.get("oldPassword"), passwordMap.get("newPassword"));
        // TODO add unit and integration - Karate tests for this feature
    }

    /**
     * update the user email
     *
     * @param id
     * @return
     */
    @PutMapping(value = "/{id}/email")
    @PreAuthorize("hasRole('ROLE_USERS_WRITER')")
    public void updateUserEmail(@PathVariable Long id, @Email @RequestParam String email) {

        this.checkTheWrightToManageTheAccount(id);
        userAccountService.updateUserEmail(id, email);
        // TODO add unit and integration - Karate tests for this feature
    }

    /**
     * disable the user request alert so it will not be show again in the admin dashboard
     *
     * @param id
     */
    @GetMapping("/{id}/off")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void disableAccountAlert(@PathVariable Long id) {
        userAccountService.disableAccountAlert(id);
        // TODO add unit and integration - Karate tests for this feature
    }

    /**
     * enable a user account
     *
     * @param id
     */
    @GetMapping("/{id}/enable")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void enableUserAccount(@PathVariable Long id) {
        userAccountService.enableUserAccount(id);
        // TODO add unit and integration - Karate tests for this feature
    }

    /**
     * disable a user account
     *
     * @param id
     */
    @GetMapping("/{id}/disable")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void disableUserAccount(@PathVariable Long id) {
        userAccountService.disableUserAccount(id);
        // TODO add unit and integration - Karate tests for this feature
    }

    /**
     * retrieve the user data from its account
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}/user")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public User retrieveUser(@PathVariable Long id) {

        return userAccountService.retrieveUser(id);
    }


    /**
     * delete a user account
     *
     * @param id
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USERS_WRITER')")
    public void deleteUserAccount(@PathVariable Long id) {

        // TODO add two fields to the user account entity , toBeDeleted and deletionRequestDate
        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(id);
        userAccountService.deleteUserAccount(id);
    }

    /**
     * cancel the user account deletion
     *
     * @param id
     */
    @GetMapping("/{id}/cancel")
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USERS_WRITER')")
    public void cancelAccountDeletion(@PathVariable Long id) {

        if (!SecurityUtils.isAdmin())
            this.checkTheWrightToManageTheAccount(id);
        userAccountService.cancelAccountDeletion(id);
    }


    /**
     * Retrieve a current authenticated user
     *
     * @return
     */
    @GetMapping("/current")
    @PreAuthorize("isFullyAuthenticated()")
    public Account retrieveCurrentUser(Authentication authentication) {

        return UserAccountMapper.toAccountApi(userAccountService.getAccountEntityByEmail(authentication.getName()));
    }

    /**
     * This method will check if the principal user has the right to get the data of a user resource
     * He can read the data only if the data to check is his own data
     * or if the current user is an admin
     *
     * @param accountId
     */
    private void checkTheWrightToManageTheAccount(Long accountId) {

        if (!SecurityUtils.canManageUserResources(userAccountService.getAccountEntityById(accountId)))
            throw new ResourceNotAllowedException(ApiExceptionsMessages.RESOURCE_NOT_ALLOWED);
    }
}
