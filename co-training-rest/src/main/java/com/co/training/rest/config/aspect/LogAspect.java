package com.co.training.rest.config.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

/**
 * This aspect will be used to log the methods parameters and results of the controller package
 */
@Aspect
@Configuration
public class LogAspect {

    private static final Logger LOGGER = LogManager.getLogger(LogAspect.class);

    @Before("execution(* com.co.training.rest.controller.*.*(..))")
    public void loggingBefore(JoinPoint joinPoint) {

        LOGGER.debug("# Aspect : Calling the method _ {} _ using these parameters {}", joinPoint.getSignature(), joinPoint.getArgs());
    }

    @AfterReturning(value = "execution(* com.co.training.rest.controller.*.*(..))", returning = "result")
    // Executed only when a method executes successfully
    public void loggingAfterSuccess(JoinPoint joinPoint, Object result) {

        LOGGER.debug("# Aspect : Result of the method _ {} _ is {}", joinPoint.getSignature(), result);
    }

    @AfterThrowing(value = "execution(* com.co.training.rest.controller.*.*(..))", throwing = "ex")
    // Executed only when a method throws an exception
    public void loggingAfterFailing(JoinPoint joinPoint, Exception ex) {

        LOGGER.debug("# Aspect : Throw exception after executing the method _ {} _ exception was {}", joinPoint.getSignature(), ex.toString());
    }
}
