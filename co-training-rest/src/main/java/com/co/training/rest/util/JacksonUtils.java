package com.co.training.rest.util;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.Map;

public class JacksonUtils {

    public static final ObjectMapper mapper = new ObjectMapper();

    public static String getProperty(String json, String property) {

        Assert.hasLength(json, "json stream cannot be empty");
        Assert.hasLength(property, "property to find stream cannot be empty");

        try {
            return (String) mapper.readValue(json, Map.class).get(property);
        } catch (IOException e) {
            throw new RuntimeException(e.toString(), e.getCause());
        }
    }

    public static Map<String, String> getProperties(String json) {

        Assert.hasLength(json, "json stream cannot be empty");

        try {
            return mapper.readValue(json, Map.class);
        } catch (IOException e) {
            throw new RuntimeException(e.toString(), e.getCause());
        }
    }
}
