package com.co.training.rest.config.security.resourceserver;

import com.co.training.rest.config.filter.LogFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Profile("!h2")
/*
 * This configuration will be loaded in production. In a real app use dev and production profiles instead than h2 and postgres like i did
 */
@EnableResourceServer
/*
 * @EnableResourceServer annotation will cause Spring to activate the resource server.
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
/*
 * Enables Spring Security global method security similar to the <global-method-security> xml support
 */
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers("/oauth/token", "/oauth/authorize**", "/v1/teams", "/actuator/**")
                .permitAll()
                // only teams service will be opened to the public

                .antMatchers("/v1/levels/**").fullyAuthenticated()
                .antMatchers("/v1/levels").fullyAuthenticated()

                .antMatchers("/v1/trainings/**").fullyAuthenticated()
                .antMatchers("/v1/trainings").fullyAuthenticated()

                .antMatchers("/v1/reviews/**").fullyAuthenticated()
                .antMatchers("/v1/reviews").fullyAuthenticated()

                .antMatchers("/v1/users/**").fullyAuthenticated()
                .antMatchers("/v1/users").fullyAuthenticated()
                // difference between * and ** , https://stackoverflow.com/questions/24948651/spring-security-difference-between-and-url-pattern-in-spring-security

                .antMatchers("/v1/tags").fullyAuthenticated()
                .antMatchers("/v1/tags/**").fullyAuthenticated()

                .antMatchers(HttpMethod.POST, "/v1/accounts").permitAll()
                // permitAll because the post for /v1/accounts is public so users can subscribe

                .antMatchers("/v1/accounts/**").fullyAuthenticated()
                // for other resources we will enable them only for authenticated users, check the controller to see
                // if a resource is enabled to an ADMIN or a simple USER

                .antMatchers("/v1/teams/**").fullyAuthenticated()
                .antMatchers(HttpMethod.POST, "/v1/teams").fullyAuthenticated()

                .anyRequest().denyAll()
                .and()
                /*
                 * filter registration among the spring filter chain
                 */
                .addFilterAfter(logFilter(), BasicAuthenticationFilter.class);
    }

    @Bean
    public LogFilter logFilter() {
        return new LogFilter();
    }
}