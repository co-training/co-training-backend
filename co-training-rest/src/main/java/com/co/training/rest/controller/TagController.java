package com.co.training.rest.controller;

import com.co.training.common.service.TagService;
import com.co.training.common.utils.PageableUtils;
import org.openapitools.client.model.TrainingTag;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/v1/tags")
public class TagController {

    private final TagService tagService;

    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    /**
     * Get a list of tags that starts with startOfTag using pagination, only a trainings writer can get and add them to its trainings
     *
     * @param page
     * @param size
     * @param startOfTag
     * @return
     */
    @GetMapping("/search")
    @PreAuthorize("hasRole('ROLE_TRAININGS_WRITER')")
    public List<TrainingTag> getTags(@Min(value = 0, message = PageableUtils.PAGE_NUMBER_MIN_VALUE)
                                     @RequestParam Integer page,
                                     @Max(value = 100, message = PageableUtils.PAGE_SIZE_MAX_VALUE)
                                     @Min(value = 1, message = PageableUtils.PAGE_SIZE_MIN_VALUE)
                                     @RequestParam Integer size,
                                     @RequestParam(defaultValue = "") String startOfTag) {

        return tagService.findTags(page, size, startOfTag);
    }
}
