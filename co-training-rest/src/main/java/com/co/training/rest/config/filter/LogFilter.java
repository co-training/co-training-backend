package com.co.training.rest.config.filter;

import com.co.training.rest.util.security.AppUserPrincipal;
import com.co.training.rest.util.security.SecurityUtils;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * This filter will be used after the request authentication so we can add the user id and its ip address to mcd map.
 * These data will be displayed after in each line of the log so we can identify the user actions
 * https://logging.apache.org/log4j/2.x/manual/thread-context.html
 * https://www.baeldung.com/mdc-in-log4j-2-logback
 */
public class LogFilter extends GenericFilterBean {

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        try {

            AppUserPrincipal principal = SecurityUtils.getPrincipal();
            // this filter will be executed for every request.
            if (principal != null && principal.getUserAccount() != null) {

                ThreadContext.put("user.email", principal.getUserAccount().getEmail());
            }

            ThreadContext.put("user.ip", getClientIp((HttpServletRequest) request));
            chain.doFilter(request, response);
        } finally {
            ThreadContext.clearAll();
        }
    }

    private String getClientIp(HttpServletRequest request) {

        String remoteAddress = "";
        if (request != null) {
            remoteAddress = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddress == null || "".equals(remoteAddress)) {
                remoteAddress = request.getRemoteAddr();
            }
        }
        return remoteAddress;
    }
}
