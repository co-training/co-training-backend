-- it seems that spring will run this script for the postgres profile but it is not
-- explanation : spring will not run this script using the postgres profile as postgres is not an embedded database. to force spring to run it
-- use the property spring.datasource.initialization-mode=always

-- drop tables
drop table if exists whishlist cascade;
drop table if exists user_role cascade;
drop table if exists user_interests_skills cascade;
drop table if exists user_account cascade;
drop table if exists "user" cascade;
drop table if exists user_resource cascade;
drop table if exists training_tag cascade;
drop table if exists training_requirement cascade;
drop table if exists training_participants cascade;
drop table if exists training_criteria cascade;
drop table if exists review cascade;
drop table if exists team cascade;
drop table if exists tag cascade;
drop table if exists training cascade;

-- create tables
create table review (id  bigserial not null, comment varchar(255), date date not null, is_anonymous boolean not null, stars int4 not null, author_user_resource_id int8 not null, training_id int8 not null, primary key (id));
create table tag (id  bigserial not null, value varchar(255) not null, primary key (id));
create table team (id  bigserial not null, name varchar(255) not null, primary key (id));
create table training (id  bigserial not null, description varchar(1000) not null, duration int4 not null, image varchar(1500), level int4 not null, max_participants int4 not null check (max_participants>=1), min_participants int4 not null check (min_participants>=1), name varchar(200) not null, price float4 not null, starting_date date not null, author_id int8 not null, primary key (id));
create table training_criteria (id  bigserial not null, description varchar(255) not null, training_id int8, primary key (id));
create table training_participants (training_id int8 not null, user_id int8 not null, primary key (training_id, user_id));
create table training_requirement (id  bigserial not null, description varchar(255) not null, training_id int8, primary key (id));
create table training_tag (training_id int8 not null, tag_id int8 not null, primary key (training_id, tag_id));
create table "user" (about text, birthday date not null, docker_hub varchar(255), email varchar(255), facebook varchar(255), first_name varchar(255) not null, github varchar(255), gitlab varchar(255), is_docker_hub_enabled boolean, is_email_enabled boolean, is_facebook_enabled boolean, is_github_enabled boolean, is_gitlab_enabled boolean, is_linkedin_enabled boolean, is_phone_number_enabled boolean, is_site_enabled boolean, is_twitter_enabled boolean, is_youtube_enabled boolean, job varchar(255) not null, last_name varchar(255) not null, linkedin varchar(255), phone_number varchar(255), photo varchar(1500), site varchar(255), twitter varchar(255), youtube varchar(255), user_resource_id int8 not null, team_id int8 not null, primary key (user_resource_id));
create table user_account (id  bigserial not null, creation_date timestamp, request_date timestamp not null, email varchar(255) not null, is_active boolean, is_new boolean, password varchar(255) not null, user_id int8, primary key (id));
create table user_interests_skills (id  bigserial not null, description varchar(255) not null, user_id int8, primary key (id));
create table user_role (id  bigserial not null, email varchar(255) not null, role varchar(255) not null, user_account_id int8, primary key (id));
create table user_resource (id  bigserial not null, budget float8 not null, jackpot float8 not null, nb_days float4 not null, primary key (id));
create table whishlist (user_id int8 not null, training_id int8 not null, primary key (user_id, training_id));
alter table tag add constraint UK_tag_value unique (value);
alter table team add constraint UK_team_name unique (name);
alter table review add constraint FKl6n0q8ovrmwr47un4l04wbw9t foreign key (author_user_resource_id) references "user";
alter table review add constraint FKrypn36huj1ptpmnqbhri65ccl foreign key (training_id) references training;
alter table training add constraint FKe95hke6ulr0o51ieh3xo8auuj foreign key (author_id) references "user";
alter table training_criteria add constraint FK9pw664dn8g8gusouxjpyxj7rj foreign key (training_id) references training;
alter table training_participants add constraint FKqylwm9uyqgyupd16mw9syjptx foreign key (user_id) references "user";
alter table training_participants add constraint FK8qr0r8kfeuiacg5cwft0sygqh foreign key (training_id) references training;
alter table training_requirement add constraint FKflxr9phu0o5v7xdbtyuvi1go3 foreign key (training_id) references training;
alter table training_tag add constraint FK77s2d8wljask6dj3l2vjv5gmc foreign key (tag_id) references tag;
alter table training_tag add constraint FKjhvjilyd3ve8xt7mx8c2w7f4n foreign key (training_id) references training;
alter table "user" add constraint FKbmqm8c8m2aw1vgrij7h0od0ok foreign key (team_id) references team;
alter table "user" add constraint FK868gbmba588llm9h4byypucf7 foreign key (user_resource_id) references user_resource;
alter table user_account add constraint FK4qaqge5ewvmfuwsp5eddfr4r2 foreign key (user_id) references "user";
alter table user_interests_skills add constraint FKnewvafo0mvac0yblejbypkukc foreign key (user_id) references "user";
alter table user_role add constraint FKc5fbwe2wjv4qdrg5b5qxk7c0a foreign key (user_account_id) references user_account;
alter table whishlist add constraint FKmhv32b594df6n5h560bawu114 foreign key (training_id) references training;
alter table whishlist add constraint FK4id2mj07s001gsribs7o3c9rj foreign key (user_id) references "user";