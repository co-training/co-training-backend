-- This script will be launched if the data-source is h2
-- Team table
insert into team (id, name) values (1,'supply');
insert into team (id, name) values (2,'finance');
insert into team (id, name) values (3,'customs');
insert into team (id, name) values (4,'transport');
insert into team (id, name) values (5,'aws');
insert into team (id, name) values (6,'b&b');
insert into team (id, name) values (7,'g-team');
insert into team (id, name) values (8,'b-team');

-- create users
-- First User : Administrator
insert into user_resource (id, budget, jackpot, nb_days) values (99, 0.0, 0.0, 0);

insert into "user"
(birthday, docker_hub, is_docker_hub_enabled, first_name, job, last_name, team_id, user_resource_id, email, is_email_enabled, photo) values
('2004-12-27', 'https://hub.docker.com/java', TRUE, 'Michel', 'BUTCHER','DUBOIS', 8, 99, 'admin@mail.com', FALSE, 'https://cdn4.vectorstock.com/i/thumb-large/52/38/avatar-icon-vector-11835238.jpg');

insert into user_account (id, email, password, creation_date, is_active, request_date, is_new, user_id) values
(99, 'admin@mail.com', '$2a$10$0jI/67brDBfj2xxMnhi9V.1hPrtXZGEbxGZop.DEuGczGZyHLCWW.', CURRENT_DATE, TRUE, CURRENT_DATE, FALSE, 99);

insert into user_role (id, email, role, user_account_id) values (99, 'admin@mail.com', 'ROLE_ADMIN', 99);


-- Second user : user
insert into user_resource (id, budget, jackpot, nb_days) values (88, 89.01, 0.0, 4);

insert into "user"
(birthday, first_name, job, last_name, team_id, user_resource_id, phone_number, is_phone_number_enabled, facebook, is_facebook_enabled, about) values
('2004-12-27', 'Gerard', 'Scrum master','Lavoine', 2, 88, '0612485678', FALSE, 'https://facebook/accounts/#user', TRUE, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');

insert into user_account (id, email, password, creation_date, is_active, request_date, is_new, user_id) values
(88, 'user@mail.com', '$2a$10$0jI/67brDBfj2xxMnhi9V.1hPrtXZGEbxGZop.DEuGczGZyHLCWW.', CURRENT_DATE, TRUE, CURRENT_DATE, FALSE, 88);

insert into user_role (id, email, role, user_account_id) values (90, 'user@mail.com', 'ROLE_TRAININGS_READER', 88);
insert into user_role (id, email, role, user_account_id) values (91, 'user@mail.com', 'ROLE_TRAININGS_WRITER', 88);
insert into user_role (id, email, role, user_account_id) values (92, 'user@mail.com', 'ROLE_USERS_READER', 88);
insert into user_role (id, email, role, user_account_id) values (93, 'user@mail.com', 'ROLE_USERS_WRITER', 88);

insert into user_interests_skills(id,description,user_id) values (1, 'Software development', 88);
insert into user_interests_skills(id,description,user_id) values (2, 'Business Administration', 88);

-- the password is always password

-- Third user : Jane
insert into user_resource (id, budget, jackpot, nb_days) values (77, 99.0, 0.0, 7);

insert into "user"
(birthday, first_name, job, last_name, team_id, user_resource_id, youtube, is_youtube_enabled, docker_hub, is_docker_hub_enabled, gitlab, is_gitlab_enabled, twitter, is_twitter_enabled, email, is_email_enabled, github, is_github_enabled, phone_number, is_phone_number_enabled, facebook, is_facebook_enabled, linkedin, is_linkedin_enabled, site, is_site_enabled, about, photo) values
('2000-09-04', 'Jane', 'Developer','DUFF', 3, 77, 'https://www.youtube.com/user/mmumshad', TRUE, 'https://hub.docker.com/_/nginx', TRUE, 'https://gitlab.com/co-training/co-training-backend', TRUE, 'https://twitter.com/docker?lang=fr', TRUE, 'janett@yahoo.fr', TRUE, 'https://github.com/vuejs/vue', TRUE, '0789561248', TRUE,
'https://fr-fr.facebook.com/jane$$$.fr/', TRUE, 'https://www.linkedin.com/in/evanyou', TRUE, 'https://vuejs.org/', FALSE, 'Lorem ipsum dolor sit amet. Et sapiente aliquam qui similique assumenda est delectus assumenda cum eveniet consequatur aut ducimus iure. In aliquid eveniet et assumenda natus et error libero in provident quis et esse placeat aut optio ratione et error laborum. Eum animi sequi et architecto facilis ut quis alias. Et distinctio voluptatem et iste laudantium aut facere dolorem et facilis nobis. 33 tempore rerum qui sint obcaecati sed dolor architecto sed laborum ipsa. Cum assumenda dolorem aut totam molestiae in quam excepturi 33 dignissimos placeat aut rerum tempora et ipsum fuga?Hic doloribus pariatur in laboriosam quasi sit similique enim et aliquid voluptas. Qui enim commodi vel nisi voluptatem sit molestias quia aut distinctio vitae qui molestiae possimus. Non molestiae optio et velit perferendis qui voluptas velit est veritatis cupiditate ea nisi labore et iste modi inventore labore.',
'https://e7.pngegg.com/pngimages/423/768/png-clipart-computer-icons-user-profile-avatar-woman-business-woman-face-heroes-thumbnail.png');

insert into user_account (id, email, password, creation_date, is_active, request_date, is_new, user_id) values
(77, 'jane@mail.com', '$2a$10$0jI/67brDBfj2xxMnhi9V.1hPrtXZGEbxGZop.DEuGczGZyHLCWW.', CURRENT_DATE, TRUE, CURRENT_DATE, FALSE, 77);

insert into user_role (id, email, role, user_account_id) values (94, 'user@mail.com', 'ROLE_TRAININGS_READER', 77);
insert into user_role (id, email, role, user_account_id) values (95, 'user@mail.com', 'ROLE_TRAININGS_WRITER', 77);
insert into user_role (id, email, role, user_account_id) values (96, 'user@mail.com', 'ROLE_USERS_READER', 77);
insert into user_role (id, email, role, user_account_id) values (97, 'user@mail.com', 'ROLE_USERS_WRITER', 77);

insert into user_interests_skills(id,description,user_id) values (3, 'Software development', 77);
insert into user_interests_skills(id,description,user_id) values (4, 'New technologies', 77);
insert into user_interests_skills(id,description,user_id) values (5, 'Drawing', 77);
insert into user_interests_skills(id,description,user_id) values (6, 'Travelling', 77);

-- Add trainings
insert into training(id, description, duration, image, level, max_participants, min_participants, name, price, starting_date, author_id) values
(1,  STRINGDECODE('# Building single web applications \n \n If you''re looking to get started building full-stack applications with Vueand Firebase, then look no further. In this training you''ll get all what you need to create web applications using vuejs and firebase. \n\n ## Chapter 1 \n This is an introduction chapter, we will see what is a single page application and i will present the modern javascript. Then i will present what is a js framework and the difference between Angular, React and Vue. \n\n ## Chapter 2 \n In this chapter, i will cover the basics of vue and its core : \n * create a new application \n * templates \n * events and methods \n * v-bind & v-on \n * computed properties \n * watchers \n \nAnd that''s it for this chapter \n\n## Chapter 3 \n In this chapter, i will explain what vue `component`. How to create and use one. \n\n ## Chapter 4 \n In the chapter 4, we will see axios which is a great library to make http calls. This is not a vue library but it''s a great tool used in a large number of applications. \n\n ## Chapter 5 \n In this last chapter, we will see what is firebase, how to interact with such a system and how to create a simple app using vue.js and firebase. \n ``` \n So Let''s Go !!! \n ``` \n\nOfficial sites : \n- https://vuejs.org/ \n- https://firebase.google.com/ '),
3, 'https://miro.medium.com/max/308/1*lNX4JPcG9ZBzWcG9qLVPBQ.png', 1, 5, 1, 'Vuejs and Firebase Tutorial', 9.99, CURRENT_DATE + 1, 77);
-- criterias
insert into training_criteria(id, description, training_id) values(1, 'Anyone who wants to learn Vue and Firebase', 1);
insert into training_criteria(id, description, training_id) values(2, 'Anyone who wants to learn how to create websites with Vue & Firebase', 1);
-- requirements
insert into training_requirement(id, description, training_id) values(1, 'Simple JavaScript experience', 1);
insert into training_requirement(id, description, training_id) values(2, 'Some experience with other frontend frameworks like Angular or ReactJS.', 1);
-- tags
insert into tag(id, value) values(1, 'js');
insert into tag(id, value) values(2, 'css');
insert into tag(id, value) values(3, 'vue.js');
insert into tag(id, value) values(4, 'firebase');
-- training tags
insert into training_tag (training_id, tag_id) values(1,1);
insert into training_tag (training_id, tag_id) values(1,2);
insert into training_tag (training_id, tag_id) values(1,3);
insert into training_tag (training_id, tag_id) values(1,4);
-- participants
insert into training_participants(training_id, user_id) values(1,88);

-- Add reviews
insert into review (id, date, is_anonymous, stars, "author_user_resource_id", training_id, comment) values (1, CURRENT_DATE + 1, FALSE, 3, 88, 1, 'This is the best training for beginners like me in new js frameworks. Jane is a good teacher that explain things with simple examples. I encourage everyone to participate to this training.');

-- The table user causes pbs with postgres because it is a reserved table. The best way is to choose another name than user. Here i'm adapting the h2 script so the app can work with h2 and postgres.