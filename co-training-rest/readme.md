# Company training rest

## Description

This is the first solution to expose our application to the public. For a reminder, our business logic is in the business layer (co-training-common) 
and this application will expose it to the public as rest endpoints. This is one of the solutions as we can expose it via soap endpoints, jms, lambdas ...
This project will be flexible.

For the rest solution we will use the spring framework as you know and spring security to secure endpoints (oauth2 to be precise).

## Code structure

Here's the code structure.

```
co-training-rest
│
└─── config                                         : This package contains all the configuration beans of the app like persistence config, logs config, security config ...
│   │                                        
│   └─── aspect                                     : Aspects used by ou app. For this app we've configured only logging aspects
│   │    │   LogAspect                              : Contains aspects to log the input parameters and the result of each method in the controller package
│   │
│   └─── filter                                     : This package contains the filters of the application
│   │    │   LogFilter                              : This filter will be used to add the user name and its ip address to the log files
│   │
│   └─── security                                   : This package contains the oauth2 and security configuration
│        │
│        └─── authserver                            : This package contains the config of the authorization server of our application
│        │    │ AuthorizationServerConfiguration    : This class contains the oauth2 configuration
│        │   
│        └─── resourceserver                        : This package will secure the routes of the application
│        │    │ ResourceServerConfiguration         : This class will secure the access to the rest endpoints of the application in prod mode (restrictive)
│        │    │ H2ResourceServerConfiguration       : This class will secure the access to the rest endpoints of the application in dev mode (less restrictive)
│        │    
│        │ AppRoles                                 : Contains all the roles used in the project
│        │ OauthConfig                              : Retrieves the oauth2 config from the application.yaml properties file
│        │ WebSecurityConfiguration                 : In this class we will define the location of our users ( database in our case )
│
│
└─── controller                                     : Defines the rest controllers of the app
│   │
│   └─── handler                                    : Contains classes that catches exceptions of the app. These exceptions will be formated before sending them back to the user  
│   │   │   ApiExceptionResponse                    : Each catched exception will be transformed to an ApiExceptionResponse that will contain the exception details
│   │   │   ErrorHandler                            : This is the controller advice of the module
│   │
│   │ LevelController                               : This controller is used to retrieve the trainings levels of the app
│   │ TagController                                 : This controller is used to retrieve the trainings tags
│   │ TeamController                                : This is used to create and retrieve teams
│   │ TrainingController                            : This is used to manage the trainings of the platform
│   │ UserAccountController                         : This controller is used to manage the users accounts of the app
│   │ UserController                                : This is used to manage the users of the app
│
│
└─── entities
│   │ UserAccount                                   : This entity represents a user account
│   │ UserRole                                      : This entity binds a user to its roles. As you've noticed, we have only two entities in this project because the majority of entities are 
│                                                     in the dao module. In this example we have define and manage our users that's why we have these entities. In other solutions the users accounts
│                                                     management can be handled by another solutions like amazon cognito ...
│                  
└─── mapper                                         : This package mappers of the app
│   │ 
│   │ UserAccountMapper                             : This class contains methods to transform a user account entity to an api object and vice versa
│
│                  
└─── repository                                     : Contains the spring data jpa repositories
│   │ 
│   │ UserAccountRepository                         : The repository of the UserAccount entity
│     
│             
└─── service                                        : The business package of the module
│   │
│   └─── impl                                       : The implementation classes of the service interface
│   │ 
│   │ UserAccountService                            : This interface defines all the methods used to manage (CRUD operations) the user account ressource
│   
│               
└─── util                                           : The utility package of the module
│   │
│   └─── security                                   : The security utilities class
│   │   │ AppUserDetailsService                     : This class is used by spring to retrieve app users from the database
│   │   │ AppUserPrincipal                          : This class is used by spring as a UserPrincipal (from which we can get username, password and credentials)
│   │   │ SecurityUtils                             : Contains methods to check if a user has the authority to access or modify a resource
│   │ 
│   │ RestPageableUtils                             : Contains helpful methods to build a spring page request ( to retrieve data from database using pages )
│
│ RestBootstrap                                     : The bootstrap of our spring boot application
```

You can have more details in the api details :

- https://app.swaggerhub.com/apis-docs/mouhamedali/co-training/1.0.0

## Database schema

As we've added new entities classes to this module, the database schema has been changed. Here's the new one.

![db-diagram-rest](/uploads/06c38044d6645a1d6d7350168fff4ac5/db-diagram-rest.png)

## Application roles

As i explained, this is an api based application and the most of the endpoints are secured by tokens, tokens and roles
...

But, some of the endpoints are public like the subscription so new user can create accounts. The best way to know this
config is to check the controller package. The application roles are like below :

| Role name  |  Description |
|---|---|
| ROLE_ADMIN  | Using this role, you can accept new users, manage roles, manage accounts, manage teams, read/write sensitive users data ...  |
| ROLE_TRAININGS_READER  | Using this role, you can read trainings, get training evaluation/rating, get a training feedbacks ...  |
| ROLE_TRAININGS_WRITER  | Using this role, you can mange trainings (if you are the owner) and feedbacks (if you already gave one)  |
| ROLE_USERS_READER  | Using this role, you can get you account settings, personal data,   |
| ROLE_USERS_WRITER  | Using this role, you can update your profile, manage your account, participate/leave a training, get other users data (public data like given trainings, evaluation) ...  |

## Installing the project on your IDE

To install the project on your local machine, you have to follow these steps.

1. Build the project as mentioned in the main readme file

2. Configure your IDE to set up the livereload

As we are using spring boot devtools, you have to enable this feature in your ide. You can find below some references
that can help you to enable it on Intellij.

- https://medium.com/@bhanuchaddha/spring-boot-devtools-on-intellij-c0ab3f9afa63

- https://stackoverflow.com/questions/28415695/how-do-you-set-a-value-in-the-intellij-registry

3. Configure the spring application to use the h2 profile

By default, the app will use the h2 profile (you have to not set up the SPRING_PROFILES_ACTIVE environment variable). Check the `application.yaml` file for more details.

4. After running the project, you can access the app via :

- http://localhost:8090/co-training (co-training is the context path configured in the `application.yaml`)

You can access the h2 console via this link :

- http://localhost:8090/co-training/h2-console

You can find the configuration of the h2 database/console in the `application-h2.yaml`.

## Run the app on a docker container (light)

You can run this application in a docker image without installing a database or a sonar quality server. This is the
light mode, we will run the app using the `h2` profile which loads the app using an in memory database. check the steps
in the Dockerfile to have more details.

From the project main directory, run these commands :

```shell
$ mvn clean package -DskipTests

$ docker build -t mouhamedali/co-training-backend:0.0.2-DEV-SNAPSHOT .

$ docker container run --env SERVER_PORT=5000 --env LOG_LEVEL=info --rm --name co-training-backend -p 8090:5000 mouhamedali/co-training-backend:0.0.2-DEV-SNAPSHOT
```

We are running the app on the port 5000 (inside the container) and we are mapping it to the port 8090 in our local
machine. The log level is info and the container name is co-training-backend. You can now make a call to the api, for
example to get all teams via :

- http://localhost:8090/co-training/v1/teams

## Run the app on a docker environment

You can run the application via docker containers. You can use the docker-compose file from the project root directory.

The docker-compose file contains these images :

- sonarqube : The sonar image

- co-training-rest : The app image

- adminer : A database management tool written in PHP used to view our db data

- postgres-service : A postgres database server

Build the app :

```shell
$ mvn clean package -DskipTests
```

You can run our docker containers via this command :

```shell script
$ docker-compose up --build
```

The first time you run this command the spring app will not probably work, and you may have a database connection error.
To fix you have to wait for the database to start you can check the logs of the postgres server :

```shell script
$ docker-compose logs -f postgres-service
```

Wait for this line :

```log
postgres            | LOG:  database system is ready to accept connections
```

As we would like to keep the data in our database and not drop them each time we restart the application, we've chosen to use the validate hibernate persistence
strategy (`ddl-auto: validate` in `application-postgres.yaml`).

Using this approach, we have to run the schema script manually. To do so, you can access the adminer management too via this link :

- http://localhost:8000

![login_adminer](/uploads/022a65492dcd155118efcdaba1d6ac4f/login_adminer.png)

the password is : password

Now you can run the postgres schema script `schema-postgres.sql` ( you can find this file in the project resources folder ).

![execute-schema](/uploads/475c1f1d4355523707bd64a963061d43/execute-schema.png)

Now you can restart the app using this command : 

```shell script
$ docker-compose restart co-training-rest
```

To have more details about to build a docker image for the spring boot application you can see the Dockerfile in the root project and have a look at
these links :
 
- https://www.baeldung.com/spring-boot-docker-images
 
- https://spring.io/guides/gs/spring-boot-docker/

## Test the application

### Unit tests

junit tests can be launched using this command :

```shell script
$ mvn test
```

```log
[INFO] 
[INFO] Results:
[INFO] 
[WARNING] Tests run: 44, Failures: 0, Errors: 0, Skipped: 2
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  33.247 s
[INFO] Finished at: 2021-01-22T19:22:42+01:00
[INFO] ------------------------------------------------------------------------

```

### Integration tests

#### Postman

You can use the postman collection in the test/resources directory to test the app. The file name
is `Co-Training.postman_collection.json`. You can start tests by getting the user and the admin tokens and then you can
call other services.

Have a look at the Curl section to have an idea about tests scenarios.

#### Karate

If you have an experience using karate framework you can switch to the `co-training-rest-karate` module which contains a
list of scenarios to test the app.

#### Curl

If you are a fun of curl you can use it as well. Here's a scenario that you can try :

1. Get application teams (Not Secured Method)

```shell script
$ curl http://localhost:8090/co-training/v1/teams
```

```json
[{"id":1,"name":"supply"},{"id":2,"name":"finance"},{"id":3,"name":"customs"},{"id":4,"name":"transport"},{"id":5,"name":"aws"},{"id":6,"name":"b&b"},{"id":7,"name":"g-team"},{"id":8,"name":"b-team"}]
```

2. Create a new Account (Not Secured Method)

```shell script
$ curl -d '{"firstName":"first-name", "lastName":"last-name", "job":"Developer", "lastName":"last-name", "team": { "id": 3, "name": "supply"}, "birthday": "2018-01-17"}' \
  -H "Content-Type: application/json" -X POST "http://localhost:8090/co-training/v1/accounts?email=example@gmail.com&password=motdepasse"
```

3. Get admin account

```shell script
$ curl -u client-web:change_me -d "username=admin@mail.com&password=password&grant_type=password" -X POST http://localhost:8090/co-training/oauth/token
```

```json
{"access_token":"63cb3d70-728f-472a-92d2-c73a7b54b43d","token_type":"bearer","refresh_token":"7c05099b-e5dd-4b5a-9549-8055d726e450","expires_in":86399,"scope":"read write trust"}
```

Now you can define the access token as an environment variable :

```shell script
$ ADMIN_TOKEN="63cb3d70-728f-472a-92d2-c73a7b54b43d"
$ echo $ADMIN_TOKEN
63cb3d70-728f-472a-92d2-c73a7b54b43d
```

4. Enable last created account (using Admin Token)

```shell script
$ curl -H "Authorization: Bearer ${ADMIN_TOKEN}"  -H 'Accept: application/json' http://localhost:8090/co-training/v1/accounts/100/enable
```

5. Get all the app accounts (using Admin Token)

```shell script
$ curl -H "Authorization: Bearer ${ADMIN_TOKEN}"  -H 'Accept: application/json' "http://localhost:8090/co-training/v1/accounts?page=0&size=50"
```

6. Get Account user details

```shell script
$ curl -H "Authorization: Bearer ${ADMIN_TOKEN}"  -H 'Accept: application/json' http://localhost:8090/co-training/v1/accounts/100/user
```

```json
{"firstName":"first-name","lastName":"last-name","job":"Developer","team":{"id":1,"name":"supply"},"photo":null,"about":null,"interestsSkills":[],
"email":null,"isEmailEnabled":null,"site":null,"isSiteEnabled":null,"linkedin":null,"isLinkedinEnabled":null,"youtube":null,"isYoutubeEnabled":null,
"github":null,"isGithubEnabled":null,"gitlab":null,"isGitlabEnabled":null,"dockerHub":null,"isDockerHubEnabled":null,"twitter":null,"isTwitterEnabled":null,
"facebook":null,"isFacebookEnabled":null,"phoneNumber":null,"isPhoneNumberEnabled":null,"birthday":"2018-01-17","id":100}
```

Event if the user account is enabled, He has no roles, so he cannot use the api. You have to add roles to use it.
We will not describe all the api features, So to do more tests check the api description from the open api url, or the postman file which has more tests.

For more details about how using the curl check this link : https://gist.github.com/subfuzion/08c5d85437d5d4f00e58

## References

Here's some references that i've used to build the features of the application :

Spring beans validation : https://reflectoring.io/bean-validation-with-spring-boot/

Exception handler for rest api : https://www.baeldung.com/exception-handling-for-rest-with-spring

Spring boot 2 properties : https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html

Http conflict code : https://stackoverflow.com/questions/3825990/http-response-code-for-post-when-resource-already-exists

Junit 5 parameterized tests : https://www.baeldung.com/parameterized-tests-junit-5 , https://mkyong.com/junit5/junit-5-parameterized-tests/

Junit 5 extensions : https://www.baeldung.com/junit-5-extensions

Unmarshall objects and lists with jackson : https://www.baeldung.com/jackson-collection-array

Mocking void methods : https://www.baeldung.com/mockito-void-methods

Test the web layer : https://spring.io/guides/gs/testing-web/

Spring security oauth2 :

- https://github.com/amdouni-mohamed-ali/spring-boot-examples/tree/master/web/rest/rest-producer/2-secure-api-oauth2
- http://www.bubblecode.net/en/2016/01/22/understanding-oauth2/?source=post_page-----8dbc063bd5d4----------------------
- https://www.baeldung.com/spring-security-method-security
- https://docs.spring.io/spring-security/site/docs/4.2.x/reference/html/el-access.html

Spring security test oauth2 : https://www.baeldung.com/oauth-api-testing-with-spring-mvc

Put Http Status : https://stackoverflow.com/questions/2342579/http-status-code-for-update-and-delete

Registration with Spring Security – Password
Encoding : https://www.baeldung.com/spring-security-registration-password-encoding-bcrypt

Compare passwords : https://stackoverflow.com/questions/54597495/how-to-compare-a-password-text-with-the-bcrypt-hashes