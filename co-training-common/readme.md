# Company Training Common ( Business Layer of the App )

## Description

This is a maven module which will be used by other modules. It contains all the business layer of our application. We can easily after using it in other
modules or applications. Let's say that you'll deploy it on your own server, so it's gonna be easy to create a rest module and import this as a dependency and
if you'll use aws lambdas you have to do the same. 

## Open API generator

If you check the resources folder you'll find our api specification in a yaml file named `co-training-api.yaml`, this file has been generated
by the `apicurio` platform :

 - [Apicurio Studio](https://www.apicur.io/studio/)

It's an awesome platform to generate openapi files without doing it manually. You can see the generated documentation here :

- [Swagger HUB](https://app.swaggerhub.com/apis-docs/mouhamedali/co-training/1.0.0)

Yes it's swagger hub, using the free trial of apicurio you can't make your api public.

This file `co-training-api.yaml` is used to generate java classes that can be used in the rest layer for example. You'll not handle entities or return
them in your rest controller but the generated classes from openapi. 

Then you can send this file to other teams ( users of this app ) so they can use it to generate their own objects ( perhaps they are not using java ).

If you check the maven configuration of the parent pom, you'll find the `openapi-generator-maven-plugin` configuration to generate classes in a build management.
This build will be called by this module when you build it.

## code structure

Here's our code structure.

```
co-training-common
│
│
└───constants                           : java package that contains all the app constants
│   │
│   └───ApiExceptionsMessages           : java class that contains the content of the exceptions throws by the app
│
│
└───exceptions                          : java package that represents the app custom exceptions
│   │
│   └───BadRequestException             : java exception thrown when the app intercepts a bad request
│   │
│   └───ResourceAlreadyExistException   : java exception thrown when the user tries to create an alredy exist resource like trainings, accounts, users ...
│   │
│   └───ResourceNotFoundException       : java exception thrown when the user tries to retrieve a resource that does not exist like trainings, users ...
│
│
└───mapper                              : java package that contains all mappers of the app
│   │
│   └───utils                           : java package that contains the common methods used in the mapper package
│   │   │
│   │   └───MapperUtils                 : java class that contains methods to copy a java object to an another object
│   │
│   └───StarsMapper                     : java class that contains methods to transform an api stars enumeration to a stars entity enumeration and vice versa
│   │
│   └───TeamMapper                      : java class that contains methods to transform an api team object to a team entity object and vice versa
│   │
│   └───TrainingCriteriaMapper          : java class that contains methods to transform an api criteria object to a criteria entity object and vice versa
│   │
│   └───TrainingMapper                  : java class that contains methods to transform an api training object to a training entity object and vice versa
│   │
│   └───TrainingRequirementMapper       : java class that contains methods to transform an api training requirement object to a training requirement entity object and vice versa
│   │
│   └───TrainingReviewMapper            : java class that contains methods to transform an api review object to a review entity object and vice versa
│   │
│   └───TrainingTagMapper               : java class that contains methods to transform an api tag object to a tag entity object and vice versa
│   │
│   └───UserInterestsSkillsMapper       : java class that contains methods to transform an api interests/skills object to a interests/skills entity object and vice versa
│   │
│   └───UserMapper                      : java class that contains methods to transform an api user object to a team user object and vice versa
│   │
│   └───UserResourceMapper              : java class that contains methods to transform an api user resource object to a user resource entity object and vice versa
│
│
└───service                             : java package that contains buisiness classes of the app
│   │
│   └───impl                            : java package that contains implementations of the service package
│   │
│   └───LevelService                    : java interface that contains methods to manage the level of a training
│   │
│   └───ReviewService                   : java interface that contains methods to manage the review of a training
│   │
│   └───TagService                      : java interface that contains methods to manage the tag of a training
│   │
│   └───TeamService                     : java interface that contains methods to manage the company teams
│   │
│   └───TrainingService                 : java interface that contains methods to manage the trainings of the app
│   │
│   └───UserService                     : java interface that contains methods to manage the app's users
│
│
└───utils                               : java package that contains some utility classes to be used by other classes
│   │
│   └───PageableUtils                   : java package that contains methods to create Pageable objects to be used by spring data to retrieve data from database
│
│
└───validators                          : java package that contains validators used to validate the entities before persisting them
│   │
│   └───AbstractValidator               : java abstract generic class used to validate and object
│   │
│   └───TrainingValidator               : java class used to validate a training object programatically 
│   │
│   └───UserValidator                   : java class used to validate a user object programatically 
│
   
```

## Unit Tests

In the tests resources, you can find some tests to make sure the application behaves as expected :

![tests-common](/uploads/ec02187ead83c6443ef4de48ec8532ff/tests-common.png)

I've excluded junit 4 to make use of the version 5 of the junit framework. It's awesome to use it to create parameterized tests.

