package com.co.training.common.service.impl;

import com.co.training.common.mapper.TrainingTagMapper;
import com.co.training.common.service.TagService;
import com.co.training.common.utils.PageableUtils;
import com.co.training.dao.entities.Tag;
import com.co.training.dao.repository.TagRepository;
import org.apache.commons.lang3.StringUtils;
import org.openapitools.client.model.TrainingTag;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
/**
 * RDB for relational database, this is an implementation of TagService which will be used
 */
public class TagServiceRDB implements TagService {

    private final TagRepository tagRepository;

    public TagServiceRDB(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<TrainingTag> findAll() {

        List<Tag> tags = tagRepository.findAll();
        return toTrainingTagApi(tags);
    }

    private List<TrainingTag> toTrainingTagApi(List<Tag> tags) {
        List<TrainingTag> tagsApi = new ArrayList<>();
        if (!tags.isEmpty())
            tags.forEach(
                    tag -> tagsApi.add(TrainingTagMapper.toTrainingTagApi(tag))
            );
        return tagsApi;
    }

    @Override
    public List<TrainingTag> findTags(Integer page, Integer size, String startOfTag) {
        List<Tag> tags = null;
        if (StringUtils.isNotBlank(startOfTag))
            tags = tagRepository.findByValueStartsWith(startOfTag.trim().toLowerCase(), PageableUtils.tagsPageRequest(page, size));
        else
            tags = tagRepository.findAll(PageableUtils.tagsPageRequest(page, size)).getContent();
        return toTrainingTagApi(tags);
    }
}
