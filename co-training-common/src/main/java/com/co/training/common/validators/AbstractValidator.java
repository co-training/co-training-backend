package com.co.training.common.validators;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;

public abstract class AbstractValidator<T extends Object> {

    protected final Validator validator;

    protected AbstractValidator(Validator validator) {
        this.validator = validator;
    }

    /**
     * This method is used to validate the passed entity object
     *
     * @param resource
     */
    public void validateEntityResource(T resource) {
        Set<ConstraintViolation<T>> violations = validator.validate(resource);
        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
    }
}
