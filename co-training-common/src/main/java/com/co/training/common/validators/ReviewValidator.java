package com.co.training.common.validators;

import com.co.training.dao.entities.Review;
import org.springframework.stereotype.Service;

import javax.validation.Validator;

@Service
public class ReviewValidator extends AbstractValidator<Review> {

    protected ReviewValidator(Validator validator) {
        super(validator);
    }
}
