package com.co.training.common.validators;

import com.co.training.dao.entities.User;
import org.springframework.stereotype.Service;

import javax.validation.Validator;

@Service
public class UserValidator extends AbstractValidator<User> {

    protected UserValidator(Validator validator) {
        super(validator);
    }
}
