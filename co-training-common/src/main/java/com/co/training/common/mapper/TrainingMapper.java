package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import com.co.training.dao.entities.User;
import org.openapitools.client.model.Training;

import java.text.DecimalFormat;

public final class TrainingMapper {

    private TrainingMapper() {
        throw new IllegalStateException("Utility class");
    }

    private static DecimalFormat df = new DecimalFormat("0.00");

    public static Training toTrainingApi(com.co.training.dao.entities.Training trainingEntity) {

        if (trainingEntity == null) return null;

        // we will copy the author so it will not be copied with the training (the author has its own method to be transformed)
        User author = trainingEntity.getAuthor();
        trainingEntity.setAuthor(null);

        // As a first step here, we have to transform the entity object. If we keep it and then starts to to modify its collections we will have strange errors
        // so the tip is to not work with entities as they are proxied by hibernate
        Training trainingApi = MapperUtils.copyCustomProperties(Training.class, trainingEntity);

        // for collections and objects we will not use the copyCustomProperties because hibernate use his custom proxies which will throw an exception
        // so to render a training author we have to transform it
        trainingApi.setAuthor(UserMapper.toUserApiDescription(author));

        // format price
        trainingApi.setPrice(new Float(df.format(trainingApi.getPrice())));

        return trainingApi;
    }

    /**
     * In this method we will return a summary of the training and not all the data. It will be rendered in some apis like the review
     *
     * @param trainingEntity
     * @return
     */
    public static Training toTrainingApiSummary(com.co.training.dao.entities.Training trainingEntity) {

        trainingEntity.setCriterias(null);
        trainingEntity.setRequirements(null);
        trainingEntity.setTags(null);
        Training trainingApi = toTrainingApi(trainingEntity);
        return trainingApi;
    }

    public static com.co.training.dao.entities.Training toTrainingEntity(Training trainingApi) {

        if (trainingApi == null) return null;
        com.co.training.dao.entities.Training training = MapperUtils.copyCustomProperties(com.co.training.dao.entities.Training.class, trainingApi);
        // as the author will be automatically ignored we have to set manually
        User author = UserMapper.toUserEntity(trainingApi.getAuthor());
        training.setAuthor(author);
        return training;
    }

    public static com.co.training.dao.entities.Training toTrainingEntity(Training trainingApi, User authorEntity) {

        if (trainingApi == null) return null;
        com.co.training.dao.entities.Training training = MapperUtils.copyCustomProperties(com.co.training.dao.entities.Training.class, trainingApi);
        training.setAuthor(authorEntity);
        return training;
    }
}
