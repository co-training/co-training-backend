package com.co.training.common.constants;

public final class ApiExceptionsMessages {


    private ApiExceptionsMessages() {
    }

    // accounts
    public static final String NOT_FOUND_ACCOUNT = "The account you are looking for does not exist";
    public static final String CANNOT_UPDATE_ADMIN_ACCOUNT = "Admin accounts are not updatable";
    public static final String INVALID_PASSWORD = "Invalid password. Make sure that the password does not contains whitespaces and that its length is greater than 5";
    public static final String ALREADY_EXISTS_EMAIL = "This email is already registered, please choose another one.";

    // roles
    public static final String ROLE_NOT_RECOGNIZED = "unrecognized role";

    // users
    public static final String NOT_FOUND_USER = "The user you are looking for does not exist";
    public static final String MAX_PARTICIPANTS_TO_TRAINING_REACHED = "Sorry, you cannot participate. The maximum number of participants has been reached";

    // trainings
    public static final String NOT_FOUND_TRAINING = "The training you are looking for does not exist";
    public static final String NOT_FOUND_TRAINING_WHISHLIST = "The training you are looking for cannot be found in the user whishlist";
    public static final String TRAINING_ID_NULL = "The training id cannot be null";
    public static final String TRAINING_AUTHOR_NULL = "Training author cannot be null";
    public static final String CANNOT_PARTICIPATE_TO_TRAINING = "Sorry, you cannot participate to this training";
    public static final String CANNOT_LEAVE_THE_TRAINING = "Sorry, you are not one of the participants of this training";
    public static final String TRAINING_DATE_PASSED = "Sorry, this training is no more available";
    public static final String TRAINING_MIN_MAX_PARTICIPANTS = "Min number of participants must be lower than the max number of participants";

    // reviews
    public static final String NOT_FOUND_REVIEW = "The review you are looking for does not exist";
    public static final String ALREADY_RATED_REVIEW = "You have already evaluated this training";
    public static final String CANNOT_RATE_REVIEW = "You are not allowed to give a feedback on this training";

    // common
    public static final String RESOURCE_NOT_ALLOWED = "Sorry, you don't have the authority to manage this resource";

    // team
    public static final String NOT_FOUND_TEAM = "The team you are looking for does not exist";
    public static final String ALREADY_EXISTS_TEAM = "Team name already exist";

    // resource
    public static final String INSUFFICIENT_RESOURCES = "insufficient resources";

    // level
    public static final String LEVEL_NOT_RECOGNIZED = "The training level is not recognized.";
}
