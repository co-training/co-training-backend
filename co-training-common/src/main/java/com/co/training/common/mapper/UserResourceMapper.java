package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import org.openapitools.client.model.UserResource;

import java.text.DecimalFormat;

public final class UserResourceMapper {

    private UserResourceMapper() {
        throw new IllegalStateException("Utility class");
    }

    private static DecimalFormat df = new DecimalFormat("0.00");

    public static UserResource toUserResourceApi(com.co.training.dao.entities.UserResource userResourceEntity) {

        if (userResourceEntity == null) return null;
        UserResource userResourceApi = new UserResource();
        MapperUtils.copySimpleProperties(userResourceApi, userResourceEntity);
        // format budget to 2 decimal points
        userResourceApi.setBudget(new Double(df.format(userResourceApi.getBudget())));
        return userResourceApi;
    }

    public static com.co.training.dao.entities.UserResource toUserResourceEntity(UserResource userResourceApi) {

        if (userResourceApi == null) return null;
        com.co.training.dao.entities.UserResource userResourceEntity = new com.co.training.dao.entities.UserResource();
        MapperUtils.copySimpleProperties(userResourceEntity, userResourceApi);
        return userResourceEntity;
    }
}
