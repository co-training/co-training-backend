package com.co.training.common.mapper;

import com.co.training.dao.entities.Review;
import org.openapitools.client.model.TrainingReview;

import java.util.Arrays;
import java.util.Optional;

public final class StarsMapper {

    private StarsMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static TrainingReview.StarsEnum toStarsApi(Review.Stars stars) {

        Optional<TrainingReview.StarsEnum> star = Arrays.stream(TrainingReview.StarsEnum.values()).filter(
                starsEnum -> starsEnum.getValue().equals(stars.getValue())
        ).findFirst();
        if (star.isPresent())
            return star.get();
        return null;
    }

    public static Review.Stars toStarsEntity(TrainingReview.StarsEnum starsEnum) {

        Optional<Review.Stars> star = Arrays.stream(Review.Stars.values()).filter(
                stars -> stars.getValue().equals(starsEnum.getValue())
        ).findFirst();
        if (star.isPresent())
            return star.get();
        return null;
    }
}
