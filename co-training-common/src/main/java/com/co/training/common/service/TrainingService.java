package com.co.training.common.service;

import org.openapitools.client.model.Training;
import org.openapitools.client.model.User;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

public interface TrainingService {

    List<Training> getTrainings(LocalDate start, LocalDate end, Integer page, Integer size);

    @Transactional
    void createNewTraining(Training training, com.co.training.dao.entities.User author);

    Training getTrainingApi(Long trainingId);

    com.co.training.dao.entities.Training getTrainingEntity(Long trainingId);

    List<User> getTrainingParticipants(Long trainingId, Integer page, Integer size);

    void updateTraining(Training training, Long trainingId, com.co.training.dao.entities.User user);

    void deleteTraining(Long trainingId, com.co.training.dao.entities.User user);

    Long countTrainingParticipants(Long id);

    Long countTrainings(LocalDate start, LocalDate end, Integer page, Integer size);

    Long countTrainingReviews(Long id);

    com.co.training.dao.entities.Training getById(Long trainingId);
}
