package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import org.openapitools.client.model.TrainingCriteria;

public final class TrainingCriteriaMapper {

    private TrainingCriteriaMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static TrainingCriteria toTrainingCriteriaApi(com.co.training.dao.entities.TrainingCriteria criteriaEntity) {

        if (criteriaEntity == null) return null;
        TrainingCriteria criteriaApi = new TrainingCriteria();
        MapperUtils.copySimpleProperties(criteriaApi, criteriaEntity);
        return criteriaApi;
    }

    public static com.co.training.dao.entities.TrainingCriteria toTrainingCriteriaEntity(TrainingCriteria criteriaApi) {

        if (criteriaApi == null) return null;
        com.co.training.dao.entities.TrainingCriteria criteriaEntity = new com.co.training.dao.entities.TrainingCriteria();
        MapperUtils.copySimpleProperties(criteriaEntity, criteriaApi);
        return criteriaEntity;
    }
}
