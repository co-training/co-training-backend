package com.co.training.common.service;

import com.co.training.dao.entities.Review;
import com.co.training.dao.entities.User;
import org.openapitools.client.model.TrainingEvaluation;
import org.openapitools.client.model.TrainingReview;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ReviewService {

    List<TrainingReview> getTrainingReviews(Integer page, Integer size, Long trainingId, Long uid, TrainingReview.StarsEnum starsEnum);

    @Transactional
    void createNewReview(TrainingReview review, com.co.training.dao.entities.User authenticatedUser);

    void updateReview(TrainingReview review, Long reviewId, User user);

    TrainingEvaluation evaluateTraining(Long id);

    List<Review> getByTrainingId(Long trainingId);
}
