package com.co.training.common.exceptions;

public class ResourceNotAllowedException extends RuntimeException {

    public ResourceNotAllowedException(String message) {
        super(message);
    }

    public ResourceNotAllowedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotAllowedException(Throwable cause) {
        super(cause);
    }
}
