package com.co.training.common.mapper.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.apache.commons.beanutils.BeanUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class MapperUtils {

    private MapperUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Used to copy objects that contains only simple types (java types) like string, integers, dates ...
     *
     * @param destination
     * @param origin
     */
    public static void copySimpleProperties(Object destination, Object origin) {
        try {
            BeanUtils.copyProperties(destination, origin);
            // this method does not work for complex objects if our object A contains set(package.entity.B), we cannot copy it
            // ot an Object C that contains set(package.api.B) we  don't have the same package so we should pass via and advanced
            // copy method like below
        } catch (Exception e) {

            throw new RuntimeException("cannot copy properties.", e);
        }
    }

    /**
     * used to copy advanced properties ( your custom objects ) like User, Training ...
     * You have to be careful before using this method because it does not work for all kind of objects.
     * For example it does not work with objects controlled by hibernate ( Object$HibernateProxy ) also i found some issues after transforming
     * enums using this method
     *
     * @param destination
     * @param origin
     */
    public static <T extends Object> T copyCustomProperties(Class<T> destination, Object origin) {
        try {
            // TODO : make the object mapper a static field
            ObjectMapper objectMapper = new ObjectMapper();

            // add local date serializer/deserializer
            JavaTimeModule module = new JavaTimeModule();
            // serializer
            LocalDateTimeDeserializer deserializer = new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS"));
            module.addDeserializer(LocalDateTime.class, deserializer);
            // deserializer
            LocalDateTimeSerializer serializer = new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS"));
            module.addSerializer(LocalDateTime.class, serializer);
            // register module
            objectMapper.registerModule(module);
            objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

            // by default jackson fails on unrecognized fields, we gonna disable this feature
            objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

            objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

            // enable annotations so the object mapper can parse @JsonIgnore annotations in our entities classes
            objectMapper.configure(MapperFeature.USE_ANNOTATIONS, true);

            return objectMapper.readValue(objectMapper.writeValueAsString(origin), destination);
        } catch (Exception e) {

            throw new RuntimeException("cannot copy properties.", e);
        }
    }
}
