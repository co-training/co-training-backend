package com.co.training.common.service.impl;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.BadRequestException;
import com.co.training.common.exceptions.ResourceNotAllowedException;
import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.common.mapper.TrainingMapper;
import com.co.training.common.mapper.UserMapper;
import com.co.training.common.mapper.UserResourceMapper;
import com.co.training.common.service.ReviewService;
import com.co.training.common.service.TeamService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.service.UserService;
import com.co.training.common.utils.PageableUtils;
import com.co.training.common.validators.UserValidator;
import com.co.training.dao.entities.Team;
import com.co.training.dao.entities.UserInterestsSkills;
import com.co.training.dao.repository.TrainingRepository;
import com.co.training.dao.repository.UserRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.openapitools.client.model.*;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceRDB implements UserService {

    private final UserRepository userRepository;
    private final TrainingRepository trainingRepository;
    private final TrainingService trainingService;
    private final UserValidator userValidator;
    private final TeamService teamService;
    private final ReviewService reviewService;

    public UserServiceRDB(UserRepository userRepository, TrainingRepository trainingRepository, TrainingService trainingService, UserValidator userValidator, TeamService teamService, ReviewService reviewService) {
        this.userRepository = userRepository;
        this.trainingRepository = trainingRepository;
        this.trainingService = trainingService;
        this.userValidator = userValidator;
        this.teamService = teamService;
        this.reviewService = reviewService;
    }

    @Override
    public User retrieveUserById(Long id) {

        return UserMapper.toUserApi(getById(id));
    }

    private com.co.training.dao.entities.User getById(Long id) {

        Optional<com.co.training.dao.entities.User> userEntity = userRepository.findById(id);
        if (!userEntity.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_USER);
        return userEntity.get();
    }

    @Override
    public List<User> retrieveUsers(Integer page, Integer size) {

        List<com.co.training.dao.entities.User> users = userRepository.findAll(PageableUtils.userPageRequest(page, size)).getContent();
        List<User> userApi = new ArrayList<>();
        if (!users.isEmpty())
            users.forEach(
                    user -> userApi.add(UserMapper.toUserApi(user))
            );
        return userApi;
    }

    @Override
    public List<Training> retrieveGivenTrainings(Long userId) {

        List<com.co.training.dao.entities.Training> trainings = trainingRepository.findByAuthorId(userId);
        return renderTrainingsApi(trainings);
    }

    private List<Training> renderTrainingsApi(List<com.co.training.dao.entities.Training> trainings) {
        List<Training> trainingApi = new ArrayList<>();
        if (trainings != null && !trainings.isEmpty())
            trainings.forEach(
                    training -> trainingApi.add(TrainingMapper.toTrainingApi(training))
            );
        return trainingApi;
    }

    @Override
    public List<Training> retrieveParticipatedTrainings(Long userId) {

        com.co.training.dao.entities.User user = getById(userId);
        return renderTrainingsApi(new ArrayList<>(user.getFollowedTrainings()));
    }

    @Override
    public List<Training> retrieveWhishlist(Long userId) {

        com.co.training.dao.entities.User user = getById(userId);
        return renderTrainingsApi(new ArrayList<>(user.getWhishlist()));
    }

    @Override
    public UserResource retrieveUserResource(Long userId) {

        com.co.training.dao.entities.User user = getById(userId);
        return UserResourceMapper.toUserResourceApi(user.getUserResource());
    }

    @Override
    public User retrieveUserDescription(Long id) {

        return UserMapper.toUserApiDescription(getById(id));
    }

    @Override
    public void updateUser(User userApi, Long id) {

        com.co.training.dao.entities.User newUser = UserMapper.toUserEntity(userApi);
        // validate the user
        userValidator.validateEntityResource(newUser);

        // get the current user
        com.co.training.dao.entities.User oldUser = getById(id);

        // copy new properties to the old entity
        copy(newUser, oldUser);

        // get user team
        Optional<Team> team = teamService.getByName(newUser.getTeam().getName());
        oldUser.setTeam(team.get());

        // update interests skills
        if (CollectionUtils.isEmpty(newUser.getInterestsSkills())) {
            // don't set the interests and skills list in any case; hibernate transform it to a persistent bag. if you set it you loose the bag
            oldUser.getInterestsSkills().clear();
        } else if (CollectionUtils.isEmpty(oldUser.getInterestsSkills())) {
            // don't set the interests and skills directly because you'll have a remove entity exception ( oldUser interests list is proxied by hibernate)
            oldUser.getInterestsSkills().addAll(newUser.getInterestsSkills());
        } else {
            // in case we have interests and skills in the new and the old user resource we will compare them to update the difference between them
            // if the object is already exist in the old user we will not update it
            List<String> newInterestsSkills = newUser.getInterestsSkills().stream().map(UserInterestsSkills::getDescription).collect(Collectors.toList());
            List<String> oldInterestsSkills = oldUser.getInterestsSkills().stream().map(UserInterestsSkills::getDescription).collect(Collectors.toList());

            // remove elements that does not exist in the new interests and skills list
            oldUser.getInterestsSkills().removeIf(is -> !newInterestsSkills.contains(is.getDescription()));

            // get new elements to add to the user interests and skills
            List<String> subtraction = ListUtils.subtract(newInterestsSkills, oldInterestsSkills);
            // keep only the subtraction
            newUser.getInterestsSkills().removeIf(is -> !subtraction.contains(is.getDescription()));
            // make null ids
            newUser.getInterestsSkills().forEach(is -> is.setId(null));

            // add this list to the old user interests and skills
            oldUser.getInterestsSkills().addAll(newUser.getInterestsSkills());
        }

        userRepository.save(oldUser);
    }

    /**
     * update the user resources
     *
     * @param userResource
     * @param uid
     */
    @Override
    public void updateUserResources(UserResource userResource, Long uid) {

        // get the current user
        com.co.training.dao.entities.User oldUser = getById(uid);

        // update resources
        oldUser.getUserResource().setNbDays(userResource.getNbDays());
        oldUser.getUserResource().setBudget(userResource.getBudget());
        oldUser.getUserResource().setJackpot(userResource.getJackpot());
        userRepository.save(oldUser);
    }

    /**
     * count the number of given trainings of the passed user id
     *
     * @param id
     * @return
     */
    private Long countGivenTrainings(Long id) {
        return trainingRepository.countByAuthorId(id);
    }

    /**
     * count the number of participants of the trainings of the passed user id
     *
     * @param id
     * @return
     */
    private Long countParticipants(Long id) {

        return trainingRepository.findByAuthorId(id).stream()
                .mapToLong(training -> trainingRepository.countTrainingParticipants(training.getId()))
                .sum();
    }

    /**
     * count the number of students of the trainings of the passed user id
     *
     * @param id
     * @return
     */
    private Long countStudents(Long id) {

        return trainingRepository.findByAuthorId(id).stream()
                .flatMap(training -> trainingRepository.getTrainingParticipants(training.getId()).stream())
                .map(participant -> participant.getId())
                .distinct()
                .count();
    }

    /**
     * count the number of reviews of given training of the passed user id
     *
     * @param id
     * @return
     */
    private Long countGivenTrainingsReviewsCount(Long id) {

        return trainingRepository.findByAuthorId(id).stream()
                .flatMap(training -> reviewService.getByTrainingId(training.getId()).stream())
                .count();
    }

    /**
     * give a user a score using the rating of his trainings
     *
     * @param id
     * @return
     */
    @Override
    public TrainingAuthorEvaluation evaluate(Long id) {

        // calculate the user score
        Double score = trainingRepository.findByAuthorId(id).stream()
                .map(training -> reviewService.evaluateTraining(training.getId()))
                .map(TrainingEvaluation::getRating)
                // don't use mapToDouble below, if you do so you'll have a null pointer exception if the stream is empty
                .map(TrainingEvaluationRating::getScore)
                .filter(Objects::nonNull)
                // after filtering we can use mapToDouble
                .mapToDouble(s -> s)
                .average()
                .orElse(0.0);
        TrainingAuthorEvaluation evaluation = new TrainingAuthorEvaluation();
        evaluation.setRating(score);
        evaluation.setParticipants(countParticipants(id));
        evaluation.setReviews(countGivenTrainingsReviewsCount(id));
        evaluation.setStudents(countStudents(id));
        evaluation.setTrainings(countGivenTrainings(id));
        return evaluation;
    }

    /**
     * count the number of trainings in the user whishlist
     *
     * @param id
     * @return
     */
    @Override
    public Long countWhishlist(Long id) {

        return userRepository.countUserWhishlist(id);
    }


    /**
     * Remove a training from the authenticated user whishlist
     *
     * @param trainingId
     * @return
     * @throws ResourceNotFoundException if the training is not found
     */
    @Override
    public void removeFromWhishlist(Long trainingId, com.co.training.dao.entities.User user) {

        user = reattachUserEntity(user);
        Set<com.co.training.dao.entities.Training> whishlist = user.getWhishlist();

        Optional<com.co.training.dao.entities.Training> toRemoveTraining = whishlist.stream()
                .filter(training -> trainingId.equals(training.getId()))
                .findFirst();

        if (!toRemoveTraining.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_TRAINING_WHISHLIST);

        whishlist.remove(toRemoveTraining.get());
        userRepository.save(user);
    }


    /**
     * Add a training to the authenticated user whishlist
     *
     * @param trainingId
     * @return
     * @throws ResourceNotFoundException if the training is not found
     */
    @Override
    public void addToWhishlist(Long trainingId, com.co.training.dao.entities.User user) {

        // the user that we've received here is detached from the hibernate context so we have to retrieve it to work with
        com.co.training.dao.entities.Training training = trainingService.getById(trainingId);
        user = reattachUserEntity(user);
        user.addToWhishlist(training);
        userRepository.save(user);
    }

    /**
     * Reattach a user entity the persistence context
     *
     * @param user
     * @return
     */
    private com.co.training.dao.entities.User reattachUserEntity(com.co.training.dao.entities.User user) {
        Optional<com.co.training.dao.entities.User> attachedUser = userRepository.findById(user.getId());
        Assert.isTrue(attachedUser.isPresent(), "Cannot find the authenticated user");
        return attachedUser.get();
    }


    /**
     * Add the authenticated user to the the list of participants of the training
     *
     * @param trainingId
     * @return
     * @throws ResourceNotFoundException if the training is not found
     */
    @Override
    public void addToFollowed(Long trainingId, com.co.training.dao.entities.User user) {

        // to implement this feature we can add the training to the list of followed trainings of the user or we can add the user to the list
        // of participants of the training
        com.co.training.dao.entities.Training training = trainingService.getById(trainingId);
        user = reattachUserEntity(user);
        // throw an error if the author wants to participate to his training
        if (training.getAuthor().getId().equals(user.getId()))
            throw new BadRequestException(ApiExceptionsMessages.CANNOT_PARTICIPATE_TO_TRAINING);

        // verify training date
        if (training.getStartingDate().isBefore(LocalDate.now())) {
            throw new BadRequestException(ApiExceptionsMessages.TRAINING_DATE_PASSED);
        }
        // throw error if the max number of participants of the training is reached
        if (training.getMaxParticipants() <= trainingService.countTrainingParticipants(trainingId)) {
            throw new ResourceNotAllowedException(ApiExceptionsMessages.MAX_PARTICIPANTS_TO_TRAINING_REACHED);
        }

        // get the user resource
        com.co.training.dao.entities.UserResource userResource = user.getUserResource();
        if (userResource.getBudget() < training.getPrice() || userResource.getNbDays() < training.getDuration())
            throw new BadRequestException(ApiExceptionsMessages.INSUFFICIENT_RESOURCES);
        userResource.setBudget(userResource.getBudget() - training.getPrice());
        userResource.setNbDays(userResource.getNbDays() - training.getDuration());

        // now we can add the training and update
        user.addToFollowedTrainings(training);
        userRepository.save(user);
    }

    @Override
    public com.co.training.dao.entities.User findById(Long id) {
        return getById(id);
    }

    /**
     * update the user team
     *
     * @param teamApi
     * @param uid
     */
    @Override
    public void updateUserTeam(org.openapitools.client.model.Team teamApi, Long uid) {

        // get the current user
        com.co.training.dao.entities.User user = getById(uid);

        // get user team
        Optional<Team> team = teamService.getByName(teamApi.getName());
        user.setTeam(team.get());

        // update
        userRepository.save(user);
    }

    /**
     * Remove the current user from the the list of participants of the training
     *
     * @param trainingId
     * @return
     * @throws ResourceNotFoundException if the training is not found
     */
    @Override
    public void removeFromFollowed(Long trainingId, final com.co.training.dao.entities.User user) {

        com.co.training.dao.entities.Training training = trainingService.getById(trainingId);
        // The best way is to delete only the row in the training_user line but we should use a native sql to do it because jpql is object
        // oriented . check this https://stackoverflow.com/questions/1071940/how-do-i-use-jpql-to-delete-entries-from-a-join-table
        final com.co.training.dao.entities.User dbUser = reattachUserEntity(user);
        if (training.getParticipants().stream().noneMatch(u -> u.getId().equals(dbUser.getId())))
            throw new BadRequestException(ApiExceptionsMessages.CANNOT_LEAVE_THE_TRAINING);

        // verify training date
        if (training.getStartingDate().isBefore(LocalDate.now()) || training.getStartingDate().isEqual(LocalDate.now())) {
            throw new BadRequestException(ApiExceptionsMessages.TRAINING_DATE_PASSED);
        }

        // update the user resource
        com.co.training.dao.entities.UserResource userResource = dbUser.getUserResource();
        userResource.setBudget(userResource.getBudget() + training.getPrice());
        userResource.setNbDays(userResource.getNbDays() + training.getDuration());

        dbUser.removeFromFollowedTrainings(training);
        userRepository.save(dbUser);
    }


    /**
     * copy only simple properties from source to destination
     *
     * @param source
     * @param destination
     */
    private void copy(com.co.training.dao.entities.User source, com.co.training.dao.entities.User destination) {
        destination.setFirstName(source.getFirstName());
        destination.setLastName(source.getLastName());
        destination.setJob(source.getJob());
        destination.setPhoto(source.getPhoto());
        destination.setAbout(source.getAbout());
        destination.setEmail(source.getEmail());
        destination.setIsEmailEnabled(source.getIsEmailEnabled());
        destination.setSite(source.getSite());
        destination.setIsSiteEnabled(source.getIsSiteEnabled());
        destination.setLinkedin(source.getLinkedin());
        destination.setIsLinkedinEnabled(source.getIsLinkedinEnabled());
        destination.setYoutube(source.getYoutube());
        destination.setIsYoutubeEnabled(source.getIsYoutubeEnabled());
        destination.setGithub(source.getGithub());
        destination.setIsGithubEnabled(source.getIsGithubEnabled());
        destination.setGitlab(source.getGitlab());
        destination.setIsGitlabEnabled(source.getIsGitlabEnabled());
        destination.setDockerHub(source.getGithub());
        destination.setDockerHub(source.getDockerHub());
        destination.setIsDockerHubEnabled(source.getIsDockerHubEnabled());
        destination.setTwitter(source.getTwitter());
        destination.setIsTwitterEnabled(source.getIsTwitterEnabled());
        destination.setFacebook(source.getFacebook());
        destination.setIsFacebookEnabled(source.getIsFacebookEnabled());
        destination.setPhoneNumber(source.getPhoneNumber());
        destination.setIsPhoneNumberEnabled(source.getIsPhoneNumberEnabled());
        destination.setBirthday(source.getBirthday());
    }
}
