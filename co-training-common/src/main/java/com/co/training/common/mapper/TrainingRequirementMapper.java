package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import org.openapitools.client.model.TrainingRequirement;

public final class TrainingRequirementMapper {

    private TrainingRequirementMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static TrainingRequirement toTrainingRequirementApi(com.co.training.dao.entities.TrainingRequirement requirementEntity) {

        if (requirementEntity == null) return null;
        TrainingRequirement requirementApi = new TrainingRequirement();
        MapperUtils.copySimpleProperties(requirementApi, requirementEntity);
        return requirementApi;
    }

    public static com.co.training.dao.entities.TrainingRequirement toTrainingRequirementEntity(TrainingRequirement requirementApi) {

        if (requirementApi == null) return null;
        com.co.training.dao.entities.TrainingRequirement requirementEntity = new com.co.training.dao.entities.TrainingRequirement();
        MapperUtils.copySimpleProperties(requirementEntity, requirementApi);
        return requirementEntity;
    }
}
