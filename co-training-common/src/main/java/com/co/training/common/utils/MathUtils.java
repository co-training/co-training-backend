package com.co.training.common.utils;

import java.text.DecimalFormat;

public class MathUtils {

    private static long VERY_POOR_SCORE = 0;
    private static long POOR_SCORE = 25;
    private static long FAIR_SCORE = 50;
    private static long GOOD_SCORE = 75;
    private static long VERY_GOOD_SCORE = 100;

    /**
     * get ratio of obtained
     *
     * @param obtained
     * @param total
     * @return
     */
    public static int calculatePercentage(double obtained, double total) {
        if (total == 0)
            throw new IllegalArgumentException("The total number to calculate the percentage cannot be zero. Infinite number.");
        double percentage = obtained * 100 / total;
        return (int) Math.round(percentage);
    }

    /**
     * This method will calculate the score of a given training using the users ratings
     * you can find the algorithm here
     * https://www.pressganey.com/docs/default-source/default-document-library/star_calculation_methodology.pdf?sfvrsn=0
     *
     * @param veryPoorRatings number of reviews with ONE as rating
     * @param poorRatings     number of reviews with TWO as rating
     * @param fairRatings     number of reviews with THREE as rating
     * @param goodRatings     number of reviews with FOUR as rating
     * @param veryGoodRatings number of reviews with FIVE as rating
     * @param total
     * @return
     */
    public static double calculateStarRatings(long veryPoorRatings, long poorRatings, long fairRatings, long goodRatings, long veryGoodRatings, int total) {

        if (total == 0)
            throw new IllegalArgumentException("The total number to calculate the ratings cannot be zero. Infinite number.");

        long score = veryPoorRatings * VERY_POOR_SCORE;
        score += poorRatings * POOR_SCORE;
        score += fairRatings * FAIR_SCORE;
        score += goodRatings * GOOD_SCORE;
        score += veryGoodRatings * VERY_GOOD_SCORE;

        String result = new DecimalFormat("0.0").format((double) score / (20 * total));
        return Double.parseDouble(result);
    }
}
