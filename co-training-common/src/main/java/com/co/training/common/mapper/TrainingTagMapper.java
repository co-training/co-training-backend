package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import com.co.training.dao.entities.Tag;
import org.openapitools.client.model.TrainingTag;

public final class TrainingTagMapper {

    private TrainingTagMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static TrainingTag toTrainingTagApi(Tag tagEntity) {

        if (tagEntity == null) return null;
        TrainingTag trainingTag = new TrainingTag();
        MapperUtils.copySimpleProperties(trainingTag, tagEntity);
        return trainingTag;
    }

    public static Tag toTrainingTagEntity(TrainingTag tagApi) {

        if (tagApi == null) return null;
        Tag tag = new Tag();
        MapperUtils.copySimpleProperties(tag, tagApi);
        return tag;
    }
}
