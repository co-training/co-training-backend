package com.co.training.common.service;

import org.openapitools.client.model.TrainingTag;

import java.util.List;

public interface TagService {

    List<TrainingTag> findAll();

    List<TrainingTag> findTags(Integer page, Integer size, String startOfTag);
}
