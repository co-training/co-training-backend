package com.co.training.common.validators;

import com.co.training.dao.entities.Training;
import org.springframework.stereotype.Service;

import javax.validation.Validator;

@Service
public class TrainingValidator extends AbstractValidator<Training> {

    protected TrainingValidator(Validator validator) {
        super(validator);
    }
}
