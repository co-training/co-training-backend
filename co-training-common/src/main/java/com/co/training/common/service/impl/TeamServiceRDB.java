package com.co.training.common.service.impl;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.BadRequestException;
import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.common.mapper.TeamMapper;
import com.co.training.common.service.TeamService;
import com.co.training.dao.repository.TeamRepository;
import org.openapitools.client.model.Team;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeamServiceRDB implements TeamService {

    private final TeamRepository teamRepository;

    public TeamServiceRDB(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public List<Team> findAll() {

        List<com.co.training.dao.entities.Team> teams = teamRepository.findAllByOrderByNameAsc();
        List<Team> teamsApi = new ArrayList<>();
        if (!teams.isEmpty())
            teams.forEach(
                    team -> teamsApi.add(TeamMapper.toTeamApi(team))
            );
        return teamsApi;
    }

    @Override
    public Team createNewTeam(Team team) {

        validateTeam(team);

        if (this.findAll().stream().map(Team::getName).anyMatch(name -> name.equals(team.getName())))
            throw new BadRequestException(ApiExceptionsMessages.ALREADY_EXISTS_TEAM);

        com.co.training.dao.entities.Team teamEntity = TeamMapper.toTeamEntity(team);
        // for a new entity to create we have to make the id null, its the db role to choose one
        teamEntity.setId(null);

        return TeamMapper.toTeamApi(teamRepository.save(teamEntity));
    }

    private void validateTeam(Team team) {

        Assert.notNull(team, "The team to save must not be null");
        Assert.notNull(team.getName(), "The team name must not be null");
        if (StringUtils.trimAllWhitespace(team.getName()).isEmpty())
            throw new BadRequestException("The team name must not be blank");
    }

    @Override
    public Optional<com.co.training.dao.entities.Team> getByName(String name) {

        Optional<com.co.training.dao.entities.Team> team = teamRepository.getByName(name);
        if (!team.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_TEAM);
        return team;
    }

    @Override
    public void updateTeam(Team team, Long id) {

        // validate and transform team
        validateTeam(team);
        com.co.training.dao.entities.Team teamApi = TeamMapper.toTeamEntity(team);

        // find team entity
        com.co.training.dao.entities.Team teamEntity = getById(id);
        teamEntity.setName(teamApi.getName());
        teamRepository.save(teamEntity);
    }

    /**
     * get team by id
     *
     * @param id
     * @return
     */
    private com.co.training.dao.entities.Team getById(Long id) {
        Optional<com.co.training.dao.entities.Team> teamEntity = teamRepository.findById(id);
        if (!teamEntity.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_TEAM);
        return teamEntity.get();
    }

    /**
     * delete team by its id
     *
     * @param id
     */
    @Override
    public void deleteTeam(Long id) {

        teamRepository.delete(getById(id));
    }
}
