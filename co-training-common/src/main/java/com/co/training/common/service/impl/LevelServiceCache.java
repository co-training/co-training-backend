package com.co.training.common.service.impl;

import com.co.training.common.service.LevelService;
import org.openapitools.client.model.Training;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
/**
 * This is the cache service of the app trainings levels
 */
public class LevelServiceCache implements LevelService {

    /**
     * return the list of levels of our application. this method is using spring cache
     * we will not use the database to get levels because a level must be declared in the api specification
     * to the client can know it. Levels will be retrieved from the open api specification
     *
     * @return
     */
    @Cacheable("levels")
    @Override
    public List<String> findAll() {

        return Arrays.stream(Training.LevelEnum.values()).map(Training.LevelEnum::getValue).collect(Collectors.toList());
    }

    /**
     * check the existence of a level
     *
     * @param level
     * @return
     */
    @Override
    public boolean exists(Training.LevelEnum level) {

        Assert.notNull(level, "level must not be null");
        Assert.notNull(level.getValue(), "level value must not be null");
        Assert.hasLength(level.getValue().trim(), "level value must not be blank");

        List<String> levels = findAll();
        return levels.contains(level.getValue());
    }
}
