package com.co.training.common.service;

import org.openapitools.client.model.Team;

import java.util.List;
import java.util.Optional;

public interface TeamService {

    List<Team> findAll();

    Team createNewTeam(Team team);

    Optional<com.co.training.dao.entities.Team> getByName(String name);

    void updateTeam(Team team, Long id);

    void deleteTeam(Long id);
}
