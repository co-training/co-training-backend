package com.co.training.common.utils;

import com.co.training.common.exceptions.BadRequestException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

public final class PageableUtils {

    private PageableUtils() {
    }

    // pageable constants
    public static final String PAGE_NUMBER_MIN_VALUE = "The page number have to be at least 0";
    public static final String PAGE_SIZE_MAX_VALUE = "You can't retrieve more than 100 items per page";
    public static final String PAGE_SIZE_MIN_VALUE = "You have to retrieve at least one item per page";

    public static final String PAGE_NUMBER_MUST_NOT_BE_NULL = "Page number must not be null";
    public static final String PAGE_SIZE_MUST_NOT_BE_NULL = "Page size must not be null";

    public static final String FILTER_MUST_NOT_BE_NULL = "filter must not be null";
    public static final String FILTER_MUST_NOT_BE_EMPTY = "filter must not be empty";

    public static Pageable userPageRequest(Integer page, Integer size) {

        return getPageRequestDesc(page, size, "id");
    }

    public static Pageable trainingPageRequest(Integer page, Integer size) {

        return getPageRequestDesc(page, size, "startingDate");
    }

    public static Pageable reviewsPageRequest(Integer page, Integer size) {

        return getPageRequestDesc(page, size, "date");
    }

    public static Pageable tagsPageRequest(Integer page, Integer size) {

        return getPageRequestAsc(page, size, "value");
    }

    public static Pageable getPageRequestDesc(Integer page, Integer size, String filter) {

        validatePage(page, size, filter);
        return PageRequest.of(page, size, Sort.by(filter).descending());
    }

    public static Pageable getPageRequestAsc(Integer page, Integer size, String filter) {

        validatePage(page, size, filter);
        return PageRequest.of(page, size, Sort.by(filter).ascending());
    }

    private static void validatePage(Integer page, Integer size, String filter) {

        if (page == null)
            throw new BadRequestException(PAGE_NUMBER_MUST_NOT_BE_NULL);
        if (size == null)
            throw new BadRequestException(PAGE_SIZE_MUST_NOT_BE_NULL);
        if (page < 0)
            throw new BadRequestException(PAGE_NUMBER_MIN_VALUE);
        if (size < 1)
            throw new BadRequestException(PAGE_SIZE_MIN_VALUE);
        if (size > 100)
            throw new BadRequestException(PAGE_SIZE_MAX_VALUE);
        if (filter == null)
            throw new BadRequestException(FILTER_MUST_NOT_BE_NULL);
        if (!StringUtils.hasText(filter))
            throw new BadRequestException(FILTER_MUST_NOT_BE_EMPTY);
    }
}
