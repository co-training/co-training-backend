package com.co.training.common.service;

import org.openapitools.client.model.Training;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
// We've annotate this interface so we can inject it in the tests without mention the implementation class name
public interface LevelService {

    List<String> findAll();

    boolean exists(Training.LevelEnum level);
}
