package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import com.co.training.dao.entities.Review;
import com.co.training.dao.entities.Training;
import org.openapitools.client.model.TrainingReview;
import org.openapitools.client.model.User;

public final class TrainingReviewMapper {

    private TrainingReviewMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static TrainingReview toReviewApi(Review review) {

        if (review == null) return null;
        // Get the author and the training of the review
        com.co.training.dao.entities.User author = review.getAuthor();
        Training training = review.getTraining();
        review.setTraining(null);
        review.setAuthor(null);

        TrainingReview trainingReview = MapperUtils.copyCustomProperties(TrainingReview.class, review);
        // apply the user policy
        trainingReview.setAuthor(trainingReview.getIsAnonymous() ? getAnonymousUser() : UserMapper.toUserApiDescription(author));
        trainingReview.setTraining(TrainingMapper.toTrainingApiSummary(training));
        return trainingReview;
    }

    public static Review toReviewEntity(TrainingReview review) {

        if (review == null) return null;
        return MapperUtils.copyCustomProperties(Review.class, review);
    }

    private static User getAnonymousUser() {
        User anonymous = new User();
        anonymous.setFirstName("anonymous");
        anonymous.setLastName("Anonymous");
        return anonymous;
    }

    /**
     * In case of hibernate control issues we can always use the manual way to map objects field by field
     *
     * @param review
     * @return
     */
    public static TrainingReview toManualReviewApi(Review review) {

        if (review == null) return null;
        TrainingReview reviewApi = new TrainingReview();
        reviewApi.setComment(review.getComment());
        reviewApi.setDate(review.getDate());
        reviewApi.setId(review.getId());
        reviewApi.setIsAnonymous(review.getIsAnonymous());
        // apply anonymous policy
        reviewApi.setAuthor(reviewApi.getIsAnonymous() ? getAnonymousUser() : UserMapper.toUserApiDescription(review.getAuthor()));
        reviewApi.setStars(StarsMapper.toStarsApi(review.getStars()));
        return reviewApi;
    }

    /**
     * I faced an issue when using the toReviewEntity to transform the stars enums so i'll use the
     * manual way
     *
     * @return
     */
    public static Review toManualReviewEntity(TrainingReview review) {

        if (review == null) return null;
        return new Review()
                .setComment(review.getComment())
                .setDate(review.getDate())
                .setId(review.getId())
                .setIsAnonymous(review.getIsAnonymous())
                .setAuthor(UserMapper.toUserEntity(review.getAuthor()))
                .setStars(StarsMapper.toStarsEntity(review.getStars()));
    }

    public static Review toManualReviewEntity(TrainingReview review, com.co.training.dao.entities.User author, Training training) {

        if (review == null) return null;
        return new Review()
                .setComment(review.getComment())
                .setDate(review.getDate())
                .setId(review.getId())
                .setIsAnonymous(review.getIsAnonymous())
                .setStars(StarsMapper.toStarsEntity(review.getStars()))
                .setAuthor(author)
                .setTraining(training);
    }
}
