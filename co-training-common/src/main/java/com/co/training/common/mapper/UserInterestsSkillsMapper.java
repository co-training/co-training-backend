package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import org.openapitools.client.model.UserInterestsSkills;

public final class UserInterestsSkillsMapper {

    private UserInterestsSkillsMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static UserInterestsSkills toUserInterestsSkillsApi(com.co.training.dao.entities.UserInterestsSkills userInterestsSkillsEntity) {

        if (userInterestsSkillsEntity == null) return null;
        UserInterestsSkills userInterestsSkillsApi = new UserInterestsSkills();
        MapperUtils.copySimpleProperties(userInterestsSkillsApi, userInterestsSkillsEntity);
        return userInterestsSkillsApi;
    }

    public static com.co.training.dao.entities.UserInterestsSkills toUserInterestsSkillsEntity(UserInterestsSkills userInterestsSkillsApi) {

        if (userInterestsSkillsApi == null) return null;
        com.co.training.dao.entities.UserInterestsSkills userInterestsSkillsEntity = new com.co.training.dao.entities.UserInterestsSkills();
        MapperUtils.copySimpleProperties(userInterestsSkillsEntity, userInterestsSkillsApi);
        return userInterestsSkillsEntity;
    }
}
