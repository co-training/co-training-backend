package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import com.co.training.dao.entities.Team;
import org.apache.commons.lang3.StringUtils;

public final class TeamMapper {

    private TeamMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static org.openapitools.client.model.Team toTeamApi(Team teamEntity) {

        if (teamEntity == null) return null;
        // we don't have collections in the team so it's save to use bean utils
        org.openapitools.client.model.Team teamApi = new org.openapitools.client.model.Team();
        MapperUtils.copySimpleProperties(teamApi, teamEntity);
        return teamApi;
    }

    public static Team toTeamEntity(org.openapitools.client.model.Team teamApi) {

        if (teamApi == null) return null;
        Team teamEntity = new Team();
        teamEntity.setId(teamApi.getId());
        teamEntity.setName(StringUtils.trim(teamApi.getName()));
        return teamEntity;
    }
}
