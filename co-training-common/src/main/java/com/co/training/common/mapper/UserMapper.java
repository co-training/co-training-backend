package com.co.training.common.mapper;

import com.co.training.common.mapper.utils.MapperUtils;
import com.co.training.dao.entities.UserInterestsSkills;
import org.openapitools.client.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserMapper {

    private UserMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static User toUserApi(com.co.training.dao.entities.User userEntity) {

        if (userEntity == null) return null;
        // to render a user we will set the followed trainings to null
        userEntity.setFollowedTrainings(null)
                .setWhishlist(null);

        return toManualUserApi(userEntity);
    }

    private static void applyUserPolicy(com.co.training.dao.entities.User userEntity) {
        // hide user data to preserve his policy
        if (!isEnabled(userEntity.getIsEmailEnabled())) userEntity.setEmail(null);
        if (!isEnabled(userEntity.getIsSiteEnabled())) userEntity.setSite(null);
        if (!isEnabled(userEntity.getIsLinkedinEnabled())) userEntity.setLinkedin(null);
        if (!isEnabled(userEntity.getIsYoutubeEnabled())) userEntity.setYoutube(null);
        if (!isEnabled(userEntity.getIsGithubEnabled())) userEntity.setGithub(null);
        if (!isEnabled(userEntity.getIsGitlabEnabled())) userEntity.setGitlab(null);
        if (!isEnabled(userEntity.getIsDockerHubEnabled())) userEntity.setDockerHub(null);
        if (!isEnabled(userEntity.getIsTwitterEnabled())) userEntity.setTwitter(null);
        if (!isEnabled(userEntity.getIsFacebookEnabled())) userEntity.setFacebook(null);
        if (!isEnabled(userEntity.getIsPhoneNumberEnabled())) userEntity.setPhoneNumber(null);
    }

    private static boolean isEnabled(Boolean isEnabled) {
        return Boolean.TRUE.equals(isEnabled);
    }

    private static Boolean falseIfNotTrue(Boolean bool) {
        if (!Boolean.TRUE.equals(bool))
            return Boolean.FALSE;
        return Boolean.TRUE;
    }

    public static com.co.training.dao.entities.User toUserEntity(User userApi) {

        if (userApi == null) return null;
        // to copy an api object to entity , as we haven't a problem of hibernate collections, we will use a generic method
        return MapperUtils.copyCustomProperties(com.co.training.dao.entities.User.class, userApi);
    }

    /**
     * In case we cannot use one of the previous objects, we can use this to map the user entity field by field
     * This will be useful if we get a user entity controlled by hibernate User$HibernateProxy. In such a case we will
     * get an error if we try to use the last methods because the entity is not of User but a new type created by hibernate
     *
     * @param user
     * @return
     */
    public static User toManualUserApi(com.co.training.dao.entities.User user) {

        if (user == null) return null;
        User userApi = new User();
        userApi.setId(user.getId());
        userApi.setFirstName(user.getFirstName());
        userApi.setLastName(user.getLastName());
        userApi.setJob(user.getJob());
        userApi.setPhoto(user.getPhoto());
        userApi.setAbout(user.getAbout());
        userApi.setInterestsSkills(mapInterestsAndSkills(user.getInterestsSkills()));
        userApi.setTeam(TeamMapper.toTeamApi(user.getTeam()));
        userApi.setBirthday(user.getBirthday());
        userApi.setEmail(user.getEmail());
        userApi.setIsEmailEnabled(falseIfNotTrue(user.getIsEmailEnabled()));
        userApi.setSite(user.getSite());
        userApi.setIsSiteEnabled(falseIfNotTrue(user.getIsSiteEnabled()));
        userApi.setLinkedin(user.getLinkedin());
        userApi.setIsLinkedinEnabled(falseIfNotTrue(user.getIsLinkedinEnabled()));
        userApi.setYoutube(user.getYoutube());
        userApi.setIsYoutubeEnabled(falseIfNotTrue(user.getIsYoutubeEnabled()));
        userApi.setGithub(user.getGithub());
        userApi.setIsGithubEnabled(falseIfNotTrue(user.getIsGithubEnabled()));
        userApi.setGitlab(user.getGitlab());
        userApi.setIsGitlabEnabled(falseIfNotTrue(user.getIsGitlabEnabled()));
        userApi.setDockerHub(user.getDockerHub());
        userApi.setIsDockerHubEnabled(falseIfNotTrue(user.getIsDockerHubEnabled()));
        userApi.setTwitter(user.getTwitter());
        userApi.setIsTwitterEnabled(falseIfNotTrue(user.getIsTwitterEnabled()));
        userApi.setFacebook(user.getFacebook());
        userApi.setIsFacebookEnabled(falseIfNotTrue(user.getIsFacebookEnabled()));
        userApi.setPhoneNumber(user.getPhoneNumber());
        userApi.setIsPhoneNumberEnabled(falseIfNotTrue(user.getIsPhoneNumberEnabled()));
        return userApi;
    }

    private static List<org.openapitools.client.model.UserInterestsSkills> mapInterestsAndSkills(List<UserInterestsSkills> interestsSkills) {
        List<org.openapitools.client.model.UserInterestsSkills> interestsSkillsApi = new ArrayList<>();
        if (interestsSkills != null && !interestsSkills.isEmpty())
            interestsSkills.forEach(
                    is -> interestsSkillsApi.add(UserInterestsSkillsMapper.toUserInterestsSkillsApi(is))
            );
        return interestsSkillsApi;
    }

    /**
     * This method will be used to map a user entity to be rendered in an external resource like a training
     * for example, we should not in that case show the user personal data like birthday or email
     *
     * @param user
     * @return
     */
    public static User toUserApiDescription(com.co.training.dao.entities.User user) {

        if (user == null) return null;
        // apply the user policy
        applyUserPolicy(user);
        User userApi = toManualUserApi(user);
        // to describe a user we will not return his birthday. it's a sensitive data
        userApi.setBirthday(null);
        return userApi;
    }
}
