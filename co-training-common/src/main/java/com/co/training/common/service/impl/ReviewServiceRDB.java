package com.co.training.common.service.impl;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.BadRequestException;
import com.co.training.common.exceptions.ResourceAlreadyExistException;
import com.co.training.common.exceptions.ResourceNotAllowedException;
import com.co.training.common.mapper.StarsMapper;
import com.co.training.common.mapper.TrainingReviewMapper;
import com.co.training.common.service.ReviewService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.utils.MathUtils;
import com.co.training.common.utils.PageableUtils;
import com.co.training.common.validators.ReviewValidator;
import com.co.training.dao.entities.Review;
import com.co.training.dao.entities.Training;
import com.co.training.dao.entities.User;
import com.co.training.dao.repository.ReviewRepository;
import org.openapitools.client.model.TrainingEvaluation;
import org.openapitools.client.model.TrainingEvaluationRating;
import org.openapitools.client.model.TrainingEvaluationRatingDetails;
import org.openapitools.client.model.TrainingReview;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class ReviewServiceRDB implements ReviewService {

    private final ReviewRepository reviewRepository;
    private final TrainingService trainingService;
    private final ReviewValidator reviewValidator;

    public ReviewServiceRDB(ReviewRepository reviewRepository, TrainingService trainingService, ReviewValidator reviewValidator) {
        this.reviewRepository = reviewRepository;
        this.trainingService = trainingService;
        this.reviewValidator = reviewValidator;
    }

    /**
     * Get a list of a training reviews
     *
     * @param trainingId The training id to look for its reviews
     * @param page       The page number
     * @param size       The page size ( The number of elements per page )
     * @return
     */
    @Override
    public List<TrainingReview> getTrainingReviews(Integer page, Integer size, Long trainingId, Long uid, TrainingReview.StarsEnum starsEnum) {

        Pageable pageable = PageableUtils.reviewsPageRequest(page, size);
        Review.Stars stars = null;
        if (starsEnum != null)
            stars = StarsMapper.toStarsEntity(starsEnum);

        // the query to run depends on the parameters of this method
        if (trainingId == null && uid == null && starsEnum == null)
            return fromReviewEntitiesToApi(reviewRepository.findAll(pageable).getContent());

        if (trainingId == null && uid == null)
            return fromReviewEntitiesToApi(reviewRepository.getByStars(stars, pageable));

        if (trainingId == null && stars == null)
            return fromReviewEntitiesToApi(reviewRepository.getByAuthorId(uid, pageable));

        if (uid == null && stars == null)
            return fromReviewEntitiesToApi(reviewRepository.getByTrainingId(trainingId, pageable));

        if (trainingId == null)
            return fromReviewEntitiesToApi(reviewRepository.getByStarsAndAuthorId(stars, uid, pageable));

        if (uid == null)
            return fromReviewEntitiesToApi(reviewRepository.getByTrainingIdAndStars(trainingId, stars, pageable));

        if (stars == null)
            return fromReviewEntitiesToApi(reviewRepository.getByTrainingIdAndAuthorId(trainingId, uid, pageable));

        return fromReviewEntitiesToApi(reviewRepository.getByTrainingIdAndAuthorIdAndStars(trainingId, uid, stars, pageable));
    }

    private List<TrainingReview> fromReviewEntitiesToApi(List<Review> reviews) {
        List<TrainingReview> reviewsApi = new ArrayList<>();
        // in some cases spring may return a null instead of an empty list , https://stackoverflow.com/questions/35045661/spring-jpa-query-returns-null-instead-of-list
        if (reviews != null && !reviews.isEmpty()) {
            reviews.forEach(
                    review -> reviewsApi.add(TrainingReviewMapper.toReviewApi(review))
            );
        }
        return reviewsApi;
    }


    /**
     * Create a new review for a training
     *
     * @param review The training review to create
     * @param user   The current authenticated user ( the source of the request )
     */
    @Override
    public void createNewReview(TrainingReview review, User user) {

        Training training = trainingService.getTrainingEntity(review.getTraining().getId());
        // an author cannot rate his own training
        if (user.getId().equals(training.getAuthor().getId()))
            throw new ResourceNotAllowedException(ApiExceptionsMessages.CANNOT_RATE_REVIEW);
        // a user cannot add multiple reviews to the same training
        if (reviewRepository.countByTrainingIdAndAuthorId(training.getId(), user.getId()) > 0) {
            throw new ResourceAlreadyExistException(ApiExceptionsMessages.ALREADY_RATED_REVIEW);
        }
        Review reviewEntity = TrainingReviewMapper.toManualReviewEntity(review, user, training);
        // change the id and the creation date
        reviewEntity.setId(null).setDate(LocalDate.now());
        // validate the entity
        reviewValidator.validateEntityResource(reviewEntity);
        reviewRepository.save(reviewEntity);
    }

    /**
     * Update an existing review
     *
     * @param reviewApi review body
     * @param reviewId  review identifier
     * @param user      The current authenticated user
     */
    @Override
    public void updateReview(TrainingReview reviewApi, Long reviewId, User user) {

        Optional<Review> reviewOpt = reviewRepository.findById(reviewId);
        if (!reviewOpt.isPresent())
            throw new BadRequestException(ApiExceptionsMessages.NOT_FOUND_REVIEW);

        Review review = reviewOpt.get();
        // check user permissions to modify this resource
        if (!user.getId().equals(review.getAuthor().getId())) {
            throw new ResourceNotAllowedException(ApiExceptionsMessages.RESOURCE_NOT_ALLOWED);
        }
        review.setDate(LocalDate.now())
                .setComment(reviewApi.getComment())
                .setStars(StarsMapper.toStarsEntity(reviewApi.getStars()))
                .setIsAnonymous(reviewApi.getIsAnonymous())
        ;
        reviewValidator.validateEntityResource(review);
        reviewRepository.save(review);
    }

    /**
     * In this method we will evaluate a training by giving it a score based on the participants reviews
     *
     * @param id
     * @return
     */
    @Override
    public TrainingEvaluation evaluateTraining(Long id) {

        // check if the training exists
        trainingService.getTrainingEntity(id);

        // init rating object
        TrainingEvaluationRatingDetails details = new TrainingEvaluationRatingDetails();
        TrainingEvaluationRating rating = new TrainingEvaluationRating();
        rating.setDetails(details);
        TrainingEvaluation evaluation = new TrainingEvaluation();
        evaluation.setRating(rating);

        List<Review> reviews = reviewRepository.getByTrainingId(id);
        if (reviews.isEmpty())
            return evaluation;
        Map<Review.Stars, Long> counters = reviews.stream().collect(Collectors.groupingBy(Review::getStars, Collectors.counting()));
        int total = reviews.size();
        //rating
        Long oneRatingScore = counters.get(Review.Stars.ONE);
        oneRatingScore = oneRatingScore != null ? oneRatingScore : 0L;
        Long twoRatingScore = counters.get(Review.Stars.TWO);
        twoRatingScore = twoRatingScore != null ? twoRatingScore : 0L;
        Long threeRatingScore = counters.get(Review.Stars.THREE);
        threeRatingScore = threeRatingScore != null ? threeRatingScore : 0L;
        Long fourRatingScore = counters.get(Review.Stars.FOUR);
        fourRatingScore = fourRatingScore != null ? fourRatingScore : 0L;
        Long fiveRatingScore = counters.get(Review.Stars.FIVE);
        fiveRatingScore = fiveRatingScore != null ? fiveRatingScore : 0L;
        details.setOneStar(MathUtils.calculatePercentage(oneRatingScore, total));
        details.setTwoStars(MathUtils.calculatePercentage(twoRatingScore, total));
        details.setThreeStars(MathUtils.calculatePercentage(threeRatingScore, total));
        details.setFourStars(MathUtils.calculatePercentage(fourRatingScore, total));
        details.setFiveStars(MathUtils.calculatePercentage(fiveRatingScore, total));
        //score
        rating.setDetails(details);
        rating.setScore(MathUtils.calculateStarRatings(oneRatingScore, twoRatingScore, threeRatingScore, fourRatingScore, fiveRatingScore, total));
        // evaluate
        evaluation.setReviews(new Long(total));
        evaluation.setRating(rating);

        return evaluation;
    }

    @Override
    public List<Review> getByTrainingId(Long trainingId) {

        return reviewRepository.getByTrainingId(trainingId);
    }
}
