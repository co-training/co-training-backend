package com.co.training.common.service.impl;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.BadRequestException;
import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.common.mapper.TrainingMapper;
import com.co.training.common.mapper.UserMapper;
import com.co.training.common.service.LevelService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.utils.PageableUtils;
import com.co.training.common.validators.TrainingValidator;
import com.co.training.dao.entities.*;
import com.co.training.dao.repository.TagRepository;
import com.co.training.dao.repository.TrainingRepository;
import com.co.training.dao.repository.UserRepository;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class TrainingServiceRDB implements TrainingService {

    private final TrainingRepository trainingRepository;
    private final TrainingValidator trainingValidator;
    private final TagRepository tagRepository;
    private final UserRepository userRepository;
    private final LevelService levelService;

    public TrainingServiceRDB(TrainingRepository trainingRepository, TrainingValidator trainingValidator, TagRepository tagRepository, UserRepository userRepository, LevelService levelService) {
        this.trainingRepository = trainingRepository;
        this.trainingValidator = trainingValidator;
        this.tagRepository = tagRepository;
        this.userRepository = userRepository;
        this.levelService = levelService;
    }

    /**
     * Get a list of trainings
     *
     * @param start The start date of the training from which we will do the search
     * @param end   The end date of the training from which we will do the search
     * @param page  The page number
     * @param size  The size (number of elements) of the page
     * @return
     */
    @Override
    public List<org.openapitools.client.model.Training> getTrainings(LocalDate start, LocalDate end, Integer page, Integer size) {

        List<Training> trainings = trainingRepository.findByStartingDateBetween(start, end, PageableUtils.trainingPageRequest(page, size));
        List<org.openapitools.client.model.Training> trainingsApi = new ArrayList<>();
        if (trainings != null && !trainings.isEmpty()) {
            trainings.forEach(
                    training -> trainingsApi.add(TrainingMapper.toTrainingApi(training))
            );
        }
        return trainingsApi;
    }

    /**
     * Get a the total number of trainings using a filter
     *
     * @param start The start date of the training from which we will do the search
     * @param end   The end date of the training from which we will do the search
     * @param page  The page number
     * @param size  The size (number of elements) of the page
     * @return
     */
    @Override
    public Long countTrainings(LocalDate start, LocalDate end, Integer page, Integer size) {
        Page<Training> trainings = trainingRepository.getByStartingDateBetween(start, end, PageableUtils.trainingPageRequest(page, size));
        return trainings.getTotalElements();
    }

    /**
     * Create a new training
     *
     * @param training
     */
    @Override
    public void createNewTraining(org.openapitools.client.model.Training training, User author) {

        // transform the training to an entity
        Training trainingToSave = TrainingMapper.toTrainingEntity(training, author);

        // validate the training before going forward
        trainingValidator.validateEntityResource(trainingToSave);

        // check if the user is using one of the app tags or new one, in such case we gonna add them to the database
        if (trainingToSave.getTags() != null && !trainingToSave.getTags().isEmpty()) {
            Set<Tag> tags = getTrainingTags(trainingToSave.getTags());
            trainingToSave.setTags(tags);
        }

        // check the level existence
        if (!levelService.exists(training.getLevel()))
            throw new ResourceNotFoundException(ApiExceptionsMessages.LEVEL_NOT_RECOGNIZED);

        // check if the min participants > max participants
        validateNumberParticipants(training.getMinParticipants(), training.getMaxParticipants());

        // set id to null for criterias and requirements
        if (trainingToSave.getRequirements() != null && !trainingToSave.getRequirements().isEmpty()) {
            // id must be null to successfully persist the requirements
            trainingToSave.getRequirements().forEach(
                    requirement -> requirement.setId(null)
            );
        }
        // same for criterias
        if (trainingToSave.getCriterias() != null && !trainingToSave.getCriterias().isEmpty()) {
            // id must be null to successfully persist the requirements
            trainingToSave.getCriterias().forEach(
                    criteria -> criteria.setId(null)
            );
        }
        // set the id to null
        trainingToSave.setId(null)

                // In this stage, we cannot persist reviews so they have to be null
                .setReviews(null);

        trainingRepository.save(trainingToSave);
    }

    /**
     * thrown error if min number participants > max number participants
     *
     * @param minParticipants
     * @param maxParticipants
     */
    private void validateNumberParticipants(Integer minParticipants, Integer maxParticipants) {
        if (minParticipants > maxParticipants)
            throw new BadRequestException(ApiExceptionsMessages.TRAINING_MIN_MAX_PARTICIPANTS);
    }

    /**
     * Get a training details by its id
     *
     * @param trainingId
     * @return
     * @throws ResourceNotFoundException if the training is not found
     */
    @Override
    public org.openapitools.client.model.Training getTrainingApi(Long trainingId) {

        return TrainingMapper.toTrainingApi(getById(trainingId));
    }

    /**
     * Get a training entity by its id
     *
     * @param trainingId
     * @return
     */
    @Override
    public Training getTrainingEntity(Long trainingId) {
        return getById(trainingId);
    }

    /**
     * Get the list of participants of the training
     *
     * @param trainingId
     * @return
     * @throws ResourceNotFoundException if the training is not found
     */
    @Override
    public List<org.openapitools.client.model.User> getTrainingParticipants(Long trainingId, Integer page, Integer size) {

        List<User> users = trainingRepository.getTrainingParticipants(trainingId, PageableUtils.userPageRequest(page, size));
        List<org.openapitools.client.model.User> usersApi = new ArrayList<>();
        if (users != null && !users.isEmpty()) {
            users.forEach(
                    user -> usersApi.add(UserMapper.toUserApiDescription(user))
            );
        }
        return usersApi;
    }

    /**
     * Reattach a user entity the persistence context
     *
     * @param user
     * @return
     */
    private User reattachUserEntity(User user) {
        Optional<User> attachedUser = userRepository.findById(user.getId());
        Assert.isTrue(attachedUser.isPresent(), "Cannot find the authenticated user");
        return attachedUser.get();
    }

    /**
     * Get a training by its id
     *
     * @param trainingId
     * @return
     * @throws ResourceNotFoundException if the training is not found
     */
    public Training getById(Long trainingId) {

        if (trainingId == null)
            throw new BadRequestException(ApiExceptionsMessages.TRAINING_ID_NULL);

        Optional<Training> training = trainingRepository.findById(trainingId);
        if (!training.isPresent())
            throw new ResourceNotFoundException(ApiExceptionsMessages.NOT_FOUND_TRAINING);
        return training.get();
    }

    /**
     * validate the tags received from the controller
     * a user can create a new tag and another user can use the last one like that we will not store the same tag many times
     *
     * @param trainingTags
     * @return
     */
    private Set<Tag> getTrainingTags(Set<Tag> trainingTags) {

        Set<Tag> tagsTmp = new HashSet<>();
        trainingTags.forEach(
                tag -> {

                    Assert.notNull(tag.getValue(), "Tag value must not be null");
                    Assert.hasLength(tag.getValue().trim(), "Tag value must not be empty or blank");
                    // check the tag existence
                    String tagName = tag.getValue().trim().toLowerCase();
                    Optional<Tag> tagEntity = tagRepository.getByValue(tagName);
                    if (tagEntity.isPresent()) {
                        tagsTmp.add(tagEntity.get());
                    } else {
                        // this is a new tag, we have to set the id to null before persisting it
                        tag.setId(null);
                        tag.setValue(tagName);
                        tagsTmp.add(tag);
                    }
                }
        );
        return tagsTmp;
    }

    /**
     * update an existing training
     *
     * @param trainingApi the training to update
     * @param user
     */
    @Override
    public void updateTraining(org.openapitools.client.model.Training trainingApi, Long trainingId, User user) {

        Training newTraining = TrainingMapper.toTrainingEntity(trainingApi);

        // validate the training before going forward
        trainingValidator.validateEntityResource(newTraining);

        // check if the min participants > max participants
        validateNumberParticipants(newTraining.getMinParticipants(), newTraining.getMaxParticipants());

        // The author of the training is not updatable
        // check the level existence
        if (!levelService.exists(trainingApi.getLevel()))
            throw new ResourceNotFoundException(ApiExceptionsMessages.LEVEL_NOT_RECOGNIZED);

        Training oldTraining = getById(trainingId);
        oldTraining.setName(newTraining.getName());
        oldTraining.setDescription(newTraining.getDescription());
        oldTraining.setLevel(newTraining.getLevel());

        // update requirements
        if (CollectionUtils.isEmpty(newTraining.getRequirements())) {
            oldTraining.getRequirements().clear();
            // the orphan requirements will be deleted automatically
        } else if (CollectionUtils.isEmpty(oldTraining.getRequirements())) {
            oldTraining.getRequirements().addAll(newTraining.getRequirements());
        } else {
            // we will not delete all the requirements here, we will check if a requirement (from new list) already exists in the old list we will keep it
            // otherwise we have to pull it from the list
            // then we will add the missing items from the new list to the old list
            List<TrainingRequirement> newList = newTraining.getRequirements();
            List<TrainingRequirement> oldList = oldTraining.getRequirements();

            List<String> newListRequirements = newList.stream()
                    .map(TrainingRequirement::getDescription).collect(Collectors.toList());
            // get the intersection of the old and new requirements (to get the requirements that we will not update)
            List<TrainingRequirement> intersection = oldList.stream()
                    .filter(r -> newListRequirements.contains(r.getDescription())).collect(Collectors.toList());

            List<String> intersectionRequirements = intersection.stream()
                    .map(TrainingRequirement::getDescription).collect(Collectors.toList());
            // now we will subtract the intersection from the new list to get the remaining requirements
            List<TrainingRequirement> subtraction = newList.stream()
                    .filter(r -> !intersectionRequirements.contains(r.getDescription())).collect(Collectors.toList());
            subtraction.forEach(r -> r.setId(null));

            // remove the requirements that does not exist in the new list
            oldList.removeIf(((Predicate) intersection::contains).negate());
            // add the new requirements from the new list
            subtraction.forEach(oldList::add);
            // don't change the old training requirements collection otherwise you will an orphan child error
            // https://stackoverflow.com/questions/9430640/a-collection-with-cascade-all-delete-orphan-was-no-longer-referenced-by-the-ow
        }
        // we did not use CollectionUtils.intersection and CollectionUtils.subtract apache methods because it does not accept a comparator (ou comparator is the description here not the object)
        // update criterias
        if (CollectionUtils.isEmpty(newTraining.getCriterias())) {
            oldTraining.getCriterias().clear();
        } else if (CollectionUtils.isEmpty(oldTraining.getCriterias())) {
            oldTraining.getCriterias().addAll(newTraining.getCriterias());
        } else {
            List<TrainingCriteria> newList = newTraining.getCriterias();
            List<TrainingCriteria> oldList = oldTraining.getCriterias();

            List<String> newListCriterias = newList.stream().map(TrainingCriteria::getDescription).collect(Collectors.toList());
            // intersection
            List<TrainingCriteria> intersection = oldList.stream()
                    .filter(c -> newListCriterias.contains(c.getDescription())).collect(Collectors.toList());
            List<String> intersectionCriterias = intersection.stream().map(TrainingCriteria::getDescription).collect(Collectors.toList());
            // remaining
            List<TrainingCriteria> subtraction = newList.stream()
                    .filter(c -> !intersectionCriterias.contains(c.getDescription())).collect(Collectors.toList());
            subtraction.forEach(c -> c.setId(null));

            // remove the criterias that does not exist in the new list
            oldList.removeIf(((Predicate) intersection::contains).negate());
            // add the new criterias from the new list
            subtraction.forEach(oldList::add);
        }

        // check if the trainings tags has been changed, if so create and add the new ones
        oldTraining.getTags().clear();
        if (!CollectionUtils.isEmpty(newTraining.getTags())) {
            oldTraining.getTags().addAll(getTrainingTags(newTraining.getTags()));
        }

        oldTraining.setStartingDate(newTraining.getStartingDate())
                .setDuration(newTraining.getDuration())
                .setMinParticipants(newTraining.getMinParticipants())
                .setMaxParticipants(newTraining.getMaxParticipants())
                .setPrice(newTraining.getPrice())
                .setImage(newTraining.getImage());

        // when we get the training in the start of the method the author is null ( not clear why; it seems to be hibernate cache issue) we have to add it manually
        oldTraining.setAuthor(user);

        trainingRepository.save(oldTraining);
        // TODO add unit and integration - Karate tests for this feature
    }

    /**
     * deletes a training by its id
     *
     * @param trainingId
     * @param user
     */
    @Override
    public void deleteTraining(Long trainingId, User user) {

        Training training = getById(trainingId);
        // when we get the training in the start of the method the author is null ( not clear why; it seems to be hibernate cache issue) we have to add it manually
        training.setAuthor(user);
        trainingRepository.delete(training);
        // TODO add unit and integration - Karate tests for this feature
    }

    /**
     * count the training number of participants
     *
     * @param id
     * @return
     */
    @Override
    public Long countTrainingParticipants(Long id) {
        // check existence
        getById(id);
        return trainingRepository.countTrainingParticipants(id);
    }

    /**
     * count the training number of reviews
     *
     * @param id
     * @return
     */
    @Override
    public Long countTrainingReviews(Long id) {
        // check existence
        getById(id);
        return trainingRepository.countTrainingReviews(id);
    }
}
