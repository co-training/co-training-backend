package com.co.training.common.service;

import org.openapitools.client.model.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserService {

    User retrieveUserById(Long id);

    List<User> retrieveUsers(Integer page, Integer size);

    List<Training> retrieveGivenTrainings(Long userId);

    List<Training> retrieveParticipatedTrainings(Long userId);

    List<Training> retrieveWhishlist(Long userId);

    UserResource retrieveUserResource(Long userId);

    User retrieveUserDescription(Long id);

    void updateUser(User user, Long id);

    void updateUserResources(UserResource userResource, Long uid);

    TrainingAuthorEvaluation evaluate(Long id);

    Long countWhishlist(Long id);

    @Transactional
    void addToWhishlist(Long trainingId, com.co.training.dao.entities.User authenticatedUser);

    @Transactional
    void removeFromWhishlist(Long trainingId, com.co.training.dao.entities.User authenticatedUser);

    @Transactional
    void removeFromFollowed(Long trainingId, com.co.training.dao.entities.User user);

    @Transactional
    void addToFollowed(Long trainingId, com.co.training.dao.entities.User user);

    com.co.training.dao.entities.User findById(Long id);

    @Transactional
    void updateUserTeam(Team team, Long uid);
}
