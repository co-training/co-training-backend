package com.co.training.common.service.training;

import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.BadRequestException;
import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.common.service.LevelService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.service.impl.TrainingServiceRDB;
import com.co.training.common.validators.TrainingValidator;
import com.co.training.dao.entities.Training;
import com.co.training.dao.repository.TagRepository;
import com.co.training.dao.repository.TrainingRepository;
import com.co.training.dao.repository.UserRepository;
import com.co.training.test.utils.TestFilesUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class GetTrainingTest {

    private static final Long TRAINING_ID = 0L;


    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TagRepository tagRepository;

    @Autowired
    private LevelService levelService;

    @Autowired
    private TrainingValidator trainingValidator;

    private TrainingService trainingService;

    @BeforeEach
    void setUp() {
        trainingService = new TrainingServiceRDB(trainingRepository, trainingValidator, tagRepository, userRepository, levelService);
    }

    @Test
    void getTraining_NotExistTraining_ExceptionThrown() {

        Mockito.when(trainingRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
        Exception exception = org.junit.jupiter.api.Assertions.assertThrows(ResourceNotFoundException.class, () -> trainingService.getTrainingApi(TRAINING_ID));
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(ApiExceptionsMessages.NOT_FOUND_TRAINING);
    }

    @Test
    void getTraining_NullTrainingID_ExceptionThrown() {

        Exception exception = org.junit.jupiter.api.Assertions.assertThrows(BadRequestException.class, () -> trainingService.getTrainingApi(null));
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(ApiExceptionsMessages.TRAINING_ID_NULL);
    }

    @Test
    void getTrainings_ExistTraining_ReturnTraining() throws IOException {

        org.openapitools.client.model.Training expected = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/get-training/expected-training.yaml");
        Training input = TestFilesUtils.loadObjectFromFile(Training.class, "data/trainings/get-training/mocked-training.yaml");


        Mockito.when(trainingRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(input));
        org.openapitools.client.model.Training actual = trainingService.getTrainingApi(TRAINING_ID);
        Assertions.assertThat(actual).isEqualTo(expected);
    }
}
