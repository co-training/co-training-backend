package com.co.training.common.utils;

import com.co.training.common.exceptions.BadRequestException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PageableUtilsTest {

    static final String FILTER = "filter";

    @Test
    void getPageRequest_PageNull_ExceptionThrown() {

        Exception exception = Assertions.assertThrows(BadRequestException.class, () -> {
            PageableUtils.getPageRequestDesc(null, 1, FILTER);
        });
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(PageableUtils.PAGE_NUMBER_MUST_NOT_BE_NULL);
    }

    @Test
    void getPageRequest_PageNegative_ExceptionThrown() {

        Exception exception = Assertions.assertThrows(BadRequestException.class, () -> {
            PageableUtils.getPageRequestDesc(-1, 1, FILTER);
        });
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(PageableUtils.PAGE_NUMBER_MIN_VALUE);
    }

    @Test
    void getPageRequest_SizeNull_ExceptionThrown() {

        Exception exception = Assertions.assertThrows(BadRequestException.class, () -> {
            PageableUtils.getPageRequestDesc(0, null, FILTER);
        });
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(PageableUtils.PAGE_SIZE_MUST_NOT_BE_NULL);
    }

    @Test
    void getPageRequest_SizeLowerThanMin_ExceptionThrown() {

        Exception exception = Assertions.assertThrows(BadRequestException.class, () -> {
            PageableUtils.getPageRequestDesc(0, 0, FILTER);
        });
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(PageableUtils.PAGE_SIZE_MIN_VALUE);
    }

    @Test
    void getPageRequest_SizeGreaterThanMax_ExceptionThrown() {

        Exception exception = Assertions.assertThrows(BadRequestException.class, () -> {
            PageableUtils.getPageRequestDesc(0, 101, FILTER);
        });
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(PageableUtils.PAGE_SIZE_MAX_VALUE);
    }

    @Test
    void getPageRequest_FilterNull_ExceptionThrown() {

        Exception exception = Assertions.assertThrows(BadRequestException.class, () -> {
            PageableUtils.getPageRequestDesc(0, 50, null);
        });
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(PageableUtils.FILTER_MUST_NOT_BE_NULL);
    }

    @Test
    void getPageRequest_FilterEmpty_ExceptionThrown() {

        Exception exception = Assertions.assertThrows(BadRequestException.class, () -> {
            PageableUtils.getPageRequestDesc(0, 50, "      ");
        });
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(PageableUtils.FILTER_MUST_NOT_BE_EMPTY);
    }
}
