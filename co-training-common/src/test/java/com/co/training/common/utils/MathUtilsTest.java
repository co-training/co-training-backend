package com.co.training.common.utils;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MathUtilsTest {

    @Test
    void calculatePercentage_InfiniteNumber_ExceptionThrown() {

        org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class, () -> {
            MathUtils.calculatePercentage(5, 0);
        });
    }

    @ParameterizedTest
    @CsvSource({
            "0, 500, 0",
            "5, 10, 50",
            "2, 7, 29",
            "21, 27, 78",
            "1, 3, 33",
            "66, 167, 40",
            "7, 33, 21",
            "122, 166, 73",
            "500, 500, 100"
    })
    void calculatePercentage(int obtained, int total, int expected) {

        Assertions.assertThat(MathUtils.calculatePercentage(obtained, total)).isEqualTo(expected);
    }

    @ParameterizedTest
    @CsvSource({
            "0,     0,      0,      0,      0,      100,    0.0",
            "100,   0,      0,      0,      0,      100,    0.0",
            "50,    50,     0,      0,      0,      100,    0.6",
            "0,     100,    0,      0,      0,      100,    1.2",
            "0,     50,     50,     0,      0,      100,    1.9",
            "0,     0,      100,    0,      0,      100,    2.5",
            "10,    10,     50,     15,     15,     100,    2.7",
            "0,     10,     30,     40,     20,     100,    3.4",
            "0,     0,      0,      100,    0,      100,    3.8",
            "0,     0,      20,     20,     60,     100,    4.2",
            "0,     0,      0,      10,     90,     100,    4.9",
            "0,     0,      0,      0,      100,    100,    5.0",
    })
    void calculateStarRatings(long veryPoorRatings, long poorRatings, long fairRatings, long goodRatings, long veryGoodRatings, int total, double expected) {

        Assertions.assertThat(MathUtils.calculateStarRatings(veryPoorRatings, poorRatings, fairRatings, goodRatings, veryGoodRatings, total)).isEqualTo(expected);
    }
}
