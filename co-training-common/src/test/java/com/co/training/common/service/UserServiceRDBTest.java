package com.co.training.common.service;


import com.co.training.common.constants.ApiExceptionsMessages;
import com.co.training.common.exceptions.ResourceNotFoundException;
import com.co.training.common.service.impl.UserServiceRDB;
import com.co.training.common.validators.UserValidator;
import com.co.training.dao.entities.Training;
import com.co.training.dao.repository.TrainingRepository;
import com.co.training.dao.repository.UserRepository;
import com.co.training.test.utils.TestFilesUtils;
import com.co.training.test.utils.TrainingsComparator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.openapitools.client.model.User;
import org.openapitools.client.model.UserResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class UserServiceRDBTest {

    private static final Long USER_ID = 1L;
    private static final Long NOT_EXIST_TRAINING_ID = 99L;
    private static final Long EXIST_TRAINING_ID = 1L;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TeamService teamService;

    @Mock
    private ReviewService reviewService;

    @Mock
    TrainingService trainingService;

    @Autowired
    private UserValidator userValidator;

    private UserService userService;

    @BeforeEach
    void setUp() {
        userService = new UserServiceRDB(userRepository, trainingRepository, trainingService, userValidator, teamService, reviewService);
    }

    @Test
    void retrieveById_ExistUser_ReturnUser() throws IOException {

        User expected = TestFilesUtils.loadObjectFromFile(User.class, "data/users/retrieve-by-id/expected-user.yaml");
        com.co.training.dao.entities.User input =
                TestFilesUtils.loadObjectFromFile(com.co.training.dao.entities.User.class, "data/users/retrieve-by-id/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(input));
        User actual = userService.retrieveUserById(USER_ID);
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @Test
    void retrieveDescriptionById_ExistUser_ReturnUser() throws IOException {

        User expected = TestFilesUtils.loadObjectFromFile(User.class, "data/users/retrieve-description-by-id/expected-user.yaml");
        com.co.training.dao.entities.User input =
                TestFilesUtils.loadObjectFromFile(com.co.training.dao.entities.User.class, "data/users/retrieve-description-by-id/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(input));
        User actual = userService.retrieveUserDescription(USER_ID);
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @Test
    void retrieveById_NotExistUser_ExceptionThrown() {

        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
        Exception exception = org.junit.jupiter.api.Assertions.assertThrows(ResourceNotFoundException.class, () -> {
            userService.retrieveUserById(USER_ID);
        });
        Assertions.assertThat(exception.getMessage()).contains(ApiExceptionsMessages.NOT_FOUND_USER);
    }

    @Test
    void retrieveUsers_ExistUsers_ReturnUsers() throws IOException {

        List<User> expected = TestFilesUtils.loadListFromFile(User.class, "data/users/retrieve-users/expected-users.yaml");
        List<com.co.training.dao.entities.User> input =
                TestFilesUtils.loadListFromFile(com.co.training.dao.entities.User.class, "data/users/retrieve-users/mocked-users.yaml");

        // mock the spring page interface
        Page<com.co.training.dao.entities.User> inputPage = Mockito.mock(Page.class);
        Mockito.when(inputPage.getContent()).thenReturn(input);

        Mockito.when(userRepository.findAll(Mockito.any(Pageable.class))).thenReturn(inputPage);
        List<User> actual = userService.retrieveUsers(0, 5);
        Assertions.assertThat(actual).isEqualTo(expected);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void retrieveGivenTrainings_ZeroTrainings_ReturnEmptyList(List<Training> trainings) {

        Mockito.when(trainingRepository.findByAuthorId(Mockito.anyLong())).thenReturn(trainings);
        List<org.openapitools.client.model.Training> actual = userService.retrieveGivenTrainings(USER_ID);
        Assertions.assertThat(actual).isNotNull().isEmpty();
    }

    @Test
    void retrieveGivenTrainings_TwoTrainings_ReturnTwoTrainings() throws IOException {

        List<org.openapitools.client.model.Training> expected = TestFilesUtils.loadListFromFile(org.openapitools.client.model.Training.class, "data/users/retrieve-given-trainings/expected-trainings.yaml");
        List<Training> input = TestFilesUtils.loadListFromFile(Training.class, "data/users/retrieve-given-trainings/mocked-trainings.yaml");
        Mockito.when(trainingRepository.findByAuthorId(Mockito.anyLong())).thenReturn(input);

        List<org.openapitools.client.model.Training> actual = userService.retrieveGivenTrainings(USER_ID);
        Assertions.assertThat(actual).isNotNull().isNotEmpty();
        TrainingsComparator.compareTwoTrainingsLists(actual, expected);
    }

    @Test
    void retrieveParticipatedTrainings_ZeroParticipatedTraining_ReturnEmptyList() throws IOException {

        com.co.training.dao.entities.User input = TestFilesUtils.loadObjectFromFile(com.co.training.dao.entities.User.class, "data/users/retrieve-participated-trainings/empty-trainings/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(input));

        List<org.openapitools.client.model.Training> actual = userService.retrieveParticipatedTrainings(USER_ID);
        Assertions.assertThat(actual).isNotNull().isEmpty();
    }

    @Test
    void retrieveParticipatedTrainings_OneParticipatedTraining_ReturnOneTraining() throws IOException {

        org.openapitools.client.model.Training expected = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/users/retrieve-participated-trainings/one-participated-training/expected-training.yaml");
        com.co.training.dao.entities.User input = TestFilesUtils.loadObjectFromFile(com.co.training.dao.entities.User.class, "data/users/retrieve-participated-trainings/one-participated-training/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(input));

        List<org.openapitools.client.model.Training> actual = userService.retrieveParticipatedTrainings(USER_ID);
        TrainingsComparator.compareTwoTrainingsLists(actual, Arrays.asList(expected));
    }

    @Test
    void retrieveWhishlist_ZeroTraining_ReturnEmptyList() throws IOException {

        com.co.training.dao.entities.User input = TestFilesUtils.loadObjectFromFile(com.co.training.dao.entities.User.class, "data/users/retrieve-whishlist/empty-trainings/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(input));

        List<org.openapitools.client.model.Training> actual = userService.retrieveWhishlist(USER_ID);
        Assertions.assertThat(actual).isNotNull().isEmpty();
    }

    @Test
    void retrieveWhishlist_TwoTrainingsInWhishlist_ReturnTwoTrainings() throws IOException {

        List<org.openapitools.client.model.Training> expected = TestFilesUtils.loadListFromFile(org.openapitools.client.model.Training.class, "data/users/retrieve-whishlist/two-trainings-whishlist/expected-trainings.yaml");
        com.co.training.dao.entities.User input = TestFilesUtils.loadObjectFromFile(com.co.training.dao.entities.User.class, "data/users/retrieve-whishlist/two-trainings-whishlist/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(input));

        List<org.openapitools.client.model.Training> actual = userService.retrieveWhishlist(USER_ID);
        TrainingsComparator.compareTwoTrainingsLists(actual, expected);
    }

    @Test
    void retrieveUserResource_UserHasResource_ReturnUserResource() throws IOException {

        UserResource expect = TestFilesUtils.loadObjectFromFile(UserResource.class, "data/users/retrieve-user-resource/expected-resource.yaml");
        com.co.training.dao.entities.User input = TestFilesUtils.loadObjectFromFile(com.co.training.dao.entities.User.class, "data/users/retrieve-user-resource/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(input));

        UserResource actual = userService.retrieveUserResource(USER_ID);
        Assertions.assertThat(actual).isNotNull().isEqualTo(expect);
    }


    @Test
    void removeFromWhishlist_NotExistTrainingInWhishlist_ExceptionThrown() {

        com.co.training.dao.entities.User dummy = getDummyUser();
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(dummy));
        Exception exception = org.junit.jupiter.api.Assertions.assertThrows(ResourceNotFoundException.class, () -> userService.removeFromWhishlist(NOT_EXIST_TRAINING_ID, dummy));
        org.assertj.core.api.Assertions.assertThat(exception.getMessage()).contains(ApiExceptionsMessages.NOT_FOUND_TRAINING_WHISHLIST);

    }

    @Test
    void removeFromWhishlist_ExistTrainingInWhishlist_TrainingRemovedFromWhishlist() {

        Mockito.when(userRepository.save(Mockito.any(com.co.training.dao.entities.User.class))).thenReturn(null);
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(getDummyUser()));
        userService.removeFromWhishlist(EXIST_TRAINING_ID, getDummyUser());
    }

    private com.co.training.dao.entities.User getDummyUser() {
        com.co.training.dao.entities.User dummyUser = new com.co.training.dao.entities.User();
        dummyUser.setId(1L);

        Set<Training> set = new HashSet<>();
        set.add(new Training().setId(EXIST_TRAINING_ID));
        dummyUser.setWhishlist(set);
        return dummyUser;
    }
}
