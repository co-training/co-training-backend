package com.co.training.common.service.training;

import com.co.training.common.service.LevelService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.service.impl.TrainingServiceRDB;
import com.co.training.common.validators.TrainingValidator;
import com.co.training.dao.entities.Training;
import com.co.training.dao.repository.TagRepository;
import com.co.training.dao.repository.TrainingRepository;
import com.co.training.dao.repository.UserRepository;
import com.co.training.test.utils.TestFilesUtils;
import com.co.training.test.utils.TrainingsComparator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class GetTrainingsTest {

    private static final Integer PAGE = 0;
    private static final Integer SIZE = 1;
    private static final LocalDate START_DATE = LocalDate.now();
    private static final LocalDate END_DATE = LocalDate.now().plusDays(5);

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TagRepository tagRepository;

    @Autowired
    private LevelService levelService;

    @Autowired
    private TrainingValidator trainingValidator;

    private TrainingService trainingService;

    @BeforeEach
    void setUp() {
        trainingService = new TrainingServiceRDB(trainingRepository, trainingValidator, tagRepository, userRepository, levelService);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void getTrainings_ZeroTrainings_ReturnEmptyList(List<Training> trainings) {

        Mockito.when(trainingRepository.findByStartingDateBetween(Mockito.any(LocalDate.class), Mockito.any(LocalDate.class), Mockito.any(Pageable.class)))
                .thenReturn(trainings);

        List<org.openapitools.client.model.Training> actual = trainingService.getTrainings(START_DATE, END_DATE, PAGE, SIZE);
        Assertions.assertThat(actual).isNotNull();
    }

    @Test
    void getTrainings_ThreeTrainings_ReturnListOfThreeTrainings() throws IOException {

        List<org.openapitools.client.model.Training> expected = TestFilesUtils.loadListFromFile(org.openapitools.client.model.Training.class, "data/trainings/get-trainings/expected-trainings.yaml");
        List<Training> input = TestFilesUtils.loadListFromFile(Training.class, "data/trainings/get-trainings/mocked-trainings.yaml");

        Mockito.when(trainingRepository.findByStartingDateBetween(Mockito.any(LocalDate.class), Mockito.any(LocalDate.class), Mockito.any(Pageable.class)))
                .thenReturn(input);

        List<org.openapitools.client.model.Training> actual = trainingService.getTrainings(START_DATE, END_DATE, PAGE, SIZE);
        TrainingsComparator.compareTwoTrainingsLists(actual, expected);
    }
}
