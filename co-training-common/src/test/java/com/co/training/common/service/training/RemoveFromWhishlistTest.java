package com.co.training.common.service.training;

import com.co.training.common.service.LevelService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.service.impl.TrainingServiceRDB;
import com.co.training.common.validators.TrainingValidator;
import com.co.training.dao.entities.User;
import com.co.training.dao.repository.TagRepository;
import com.co.training.dao.repository.TrainingRepository;
import com.co.training.dao.repository.UserRepository;
import com.co.training.test.utils.TestFilesUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class RemoveFromWhishlistTest {

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TagRepository tagRepository;

    @Autowired
    private LevelService levelService;

    @Autowired
    private TrainingValidator trainingValidator;

    private TrainingService trainingService;

    @BeforeEach
    void setUp() throws IOException {
        trainingService = new TrainingServiceRDB(trainingRepository, trainingValidator, tagRepository, userRepository, levelService);
        User user = TestFilesUtils.loadObjectFromFile(User.class, "data/trainings/remove-from-whishlist/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(user));
    }
}
