package com.co.training.common.service.training;

import com.co.training.common.service.LevelService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.service.impl.TrainingServiceRDB;
import com.co.training.common.validators.TrainingValidator;
import com.co.training.dao.entities.User;
import com.co.training.dao.repository.TagRepository;
import com.co.training.dao.repository.TrainingRepository;
import com.co.training.dao.repository.UserRepository;
import com.co.training.test.utils.TestFilesUtils;
import com.co.training.test.utils.UsersComparator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class GetTrainingParticipantsTest {

    private static final Integer PAGE = 0;
    private static final Integer SIZE = 1;
    private static final LocalDate START_DATE = LocalDate.now();
    private static final LocalDate END_DATE = LocalDate.now().plusDays(5);
    private static final Long TRAINING_ID = 0L;


    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TagRepository tagRepository;

    @Autowired
    private LevelService levelService;

    @Autowired
    private TrainingValidator trainingValidator;

    private TrainingService trainingService;

    @BeforeEach
    void setUp() {
        trainingService = new TrainingServiceRDB(trainingRepository, trainingValidator, tagRepository, userRepository, levelService);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void getTrainingParticipants_ZeroParticipants_ReturnEmptyList(List<User> participants) {

        Mockito.when(trainingRepository.getTrainingParticipants(Mockito.anyLong(), Mockito.any(Pageable.class))).thenReturn(participants);
        List<org.openapitools.client.model.User> actual = trainingService.getTrainingParticipants(TRAINING_ID, PAGE, SIZE);
        Assertions.assertThat(actual).isNotNull().isEmpty();
    }

    @Test
    void getTrainingParticipants_TwoParticipants_ReturnListOfTwoParticipants() throws IOException {

        List<org.openapitools.client.model.User> expected = TestFilesUtils.loadListFromFile(org.openapitools.client.model.User.class, "data/trainings/get-training-participants/expected-users.yaml");
        List<User> input = TestFilesUtils.loadListFromFile(User.class, "data/trainings/get-training-participants/mocked-users.yaml");
        Mockito.when(trainingRepository.getTrainingParticipants(Mockito.anyLong(), Mockito.any(Pageable.class))).thenReturn(input);

        List<org.openapitools.client.model.User> actual = trainingService.getTrainingParticipants(TRAINING_ID, PAGE, SIZE);
        UsersComparator.compareTwoUsersLists(actual, expected);
    }
}
