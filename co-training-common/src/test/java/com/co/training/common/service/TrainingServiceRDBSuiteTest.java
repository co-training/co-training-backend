package com.co.training.common.service;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

// TODO : better solution to run this test suite without running trainings units tests
@RunWith(JUnitPlatform.class)
@SelectPackages("com.co.training.common.service.training")
public class TrainingServiceRDBSuiteTest {

    /*
        Because the training service contains a lot of methods of our application business i'll not test them in one file to make the code
        easy to read. That's why i've create this test suite to make call to all trainings tests which are in the training package
        Please Note that i'll not create a unit test per method and as i know its a good practise to test your methods but you have
        not to them all so if i think that the there is not much to test in a method i'll not test it ( if the method make call to others
        that are already tested or if the method contains simple operations like add an object to list for example. check the addToParticipants
        and removeFromParticipants methods)
     */
}
