package com.co.training.common.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class LevelServiceCacheTest {

    @Autowired
    private LevelService levelService;

    @Test
    void findAll_CallMethod_ReturnAppLevels() {

        List<String> actual = levelService.findAll();
        Assertions.assertThat(actual).isNotNull().isNotEmpty();
    }

    @Test
    void exists_LevelNull_ExceptionThrown() {

        org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class, () -> {
            levelService.exists(null);
        });
    }
}
