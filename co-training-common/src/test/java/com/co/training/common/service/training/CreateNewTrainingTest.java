package com.co.training.common.service.training;

import com.co.training.common.mapper.UserMapper;
import com.co.training.common.service.LevelService;
import com.co.training.common.service.TrainingService;
import com.co.training.common.service.impl.TrainingServiceRDB;
import com.co.training.common.validators.TrainingValidator;
import com.co.training.dao.entities.Training;
import com.co.training.dao.entities.User;
import com.co.training.dao.repository.TagRepository;
import com.co.training.dao.repository.TrainingRepository;
import com.co.training.dao.repository.UserRepository;
import com.co.training.test.utils.TestFilesUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class CreateNewTrainingTest {

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TagRepository tagRepository;

    @Autowired
    private LevelService levelService;

    @Autowired
    private TrainingValidator trainingValidator;

    private TrainingService trainingService;

    @BeforeEach
    void setUp() throws IOException {
        trainingService = new TrainingServiceRDB(trainingRepository, trainingValidator, tagRepository, userRepository, levelService);
        // set the author using mockito
        User author = TestFilesUtils.loadObjectFromFile(User.class, "data/trainings/create-new-training/mocked-user.yaml");
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(author));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n", "lower 10", "a test with a name grater than 200 characters Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,  when"})
    void createNewTraining_TrainingName_ExceptionThrown(String trainingName) throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setName(trainingName);
        // in the real app we affect the training to the owner of the http request (authenticated user) but for tests it doesn't matter we can use any user (a dummy if you want)
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ", "\t", "\n", "lower than 25", "a test with a name grater than 5000 characters Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, , comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words," +
            "characters Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, , comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, " +
            "characters Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, , comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words," +
            "characters Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, , comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words," +
            "characters Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, , comes from a line in section 1.10.32. Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words,"})
    void createNewTraining_TrainingDescription_ExceptionThrown(String description) throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setDescription(description);
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @Test
    void createNewTraining_TrainingLevelNull_ExceptionThrown() throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setLevel(null);
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @Test
    void createNewTraining_TrainingStartDateInPast_ExceptionThrown() throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setStartingDate(LocalDate.now().minusYears(10));
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, -1, -10, -100})
    void createNewTraining_TrainingDuration_ExceptionThrown(Integer duration) throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setDuration(duration);
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, -1, -10, -100})
    void createNewTraining_TrainingMinParticipants_ExceptionThrown(Integer participants) throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setMinParticipants(participants);
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @ParameterizedTest
    @ValueSource(ints = {0, -1, -10, -100})
    void createNewTraining_TrainingMaxParticipants_ExceptionThrown(Integer participants) throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setMaxParticipants(participants);
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @ParameterizedTest
    @ValueSource(floats = {-0.5f, -10f, -100.99f})
    void createNewTraining_TrainingPrice_ExceptionThrown(Float price) throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setPrice(price);
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @Test
    void createNewTraining_TrainingAuthorNull_ExceptionThrown() throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        input.setAuthor(null);
        org.junit.jupiter.api.Assertions.assertThrows(ConstraintViolationException.class, () -> trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor())));
    }

    @Test
    void createNewTraining_NewTraining_NewTrainingSaved() throws IOException {

        org.openapitools.client.model.Training input = TestFilesUtils.loadObjectFromFile(org.openapitools.client.model.Training.class, "data/trainings/create-new-training/input-training.yaml");
        Mockito.when(trainingRepository.save(Mockito.any(Training.class))).thenReturn(null);
        trainingService.createNewTraining(input, UserMapper.toUserEntity(input.getAuthor()));
    }
}
