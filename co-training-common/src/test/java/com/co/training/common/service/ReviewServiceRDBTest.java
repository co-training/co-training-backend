package com.co.training.common.service;

import com.co.training.common.service.impl.ReviewServiceRDB;
import com.co.training.common.validators.ReviewValidator;
import com.co.training.dao.entities.Review;
import com.co.training.dao.repository.ReviewRepository;
import com.co.training.test.utils.TestFilesUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.openapitools.client.model.TrainingReview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class ReviewServiceRDBTest {

    private static final Integer PAGE = 0;
    private static final Integer SIZE = 1;
    private static final Long USER_ID = 88L;
    private static final Long TRAINING_ID = 1L;
    private static final TrainingReview.StarsEnum STARS_API = TrainingReview.StarsEnum._3;
    private static final Review.Stars STARS_ENTITY = Review.Stars.THREE;

    @Mock
    private ReviewRepository reviewRepository;
    @Mock
    private TrainingService trainingService;
    @Autowired
    private ReviewValidator reviewValidator;

    private ReviewService reviewService;

    @BeforeEach
    void setUp() {
        reviewService = new ReviewServiceRDB(reviewRepository, trainingService, reviewValidator);
    }

    @ParameterizedTest
    @NullAndEmptySource
        // This method will invoke our method twice. First with null and then with an empty list
        // more details on parameterized tests https://www.baeldung.com/parameterized-tests-junit-5
    void getReviews_NoResultInDb_ReturnEmptyList(List<Review> reviewList) {
        Mockito.when(reviewRepository.getByTrainingId(Mockito.anyLong(), Mockito.any(Pageable.class))).thenReturn(reviewList);
        List<TrainingReview> reviews = reviewService.getTrainingReviews(PAGE, SIZE, TRAINING_ID, USER_ID, STARS_API);
        org.assertj.core.api.Assertions.assertThat(reviews).isNotNull().isEmpty();
    }

    @Test
    void getReviews_AnonymousReview_ReviewAuthorMustBeNull() throws IOException {

        TrainingReview expected = TestFilesUtils.loadObjectFromFile(TrainingReview.class, "data/reviews/anonymous-review/expected-reviews.yaml");
        List<Review> mockedReviews = new ArrayList<>();
        mockedReviews.add(TestFilesUtils.loadObjectFromFile(Review.class, "data/reviews/anonymous-review/mocked-reviews.yaml"));
        Mockito.doReturn(mockedReviews).when(reviewRepository).getByTrainingIdAndAuthorIdAndStars(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(Review.Stars.class), Mockito.any(Pageable.class));
        List<TrainingReview> reviews = reviewService.getTrainingReviews(PAGE, SIZE, TRAINING_ID, USER_ID, STARS_API);
        org.assertj.core.api.Assertions.assertThat(reviews).hasSize(1);
        org.assertj.core.api.Assertions.assertThat(reviews.get(0)).isEqualTo(expected);
    }

    @Test
    void getReviews_NonAnonymousReview_ReviewWithAllData() throws IOException {

        TrainingReview expected = TestFilesUtils.loadObjectFromFile(TrainingReview.class, "data/reviews/non-anonymous-review/expected-reviews.yaml");
        List<Review> mockedReviews = new ArrayList<>();
        mockedReviews.add(TestFilesUtils.loadObjectFromFile(Review.class, "data/reviews/non-anonymous-review/mocked-reviews.yaml"));
        Mockito.doReturn(mockedReviews).when(reviewRepository).getByTrainingIdAndAuthorIdAndStars(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(Review.Stars.class), Mockito.any(Pageable.class));
        List<TrainingReview> reviews = reviewService.getTrainingReviews(PAGE, SIZE, TRAINING_ID, USER_ID, STARS_API);
        org.assertj.core.api.Assertions.assertThat(reviews).hasSize(1);
        org.assertj.core.api.Assertions.assertThat(reviews.get(0)).isEqualTo(expected);
    }
}
