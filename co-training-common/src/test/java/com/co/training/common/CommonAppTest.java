package com.co.training.common;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * This class is used to bootstrap a spring boot context so we can use spring features (like autowired annotation, mocking
 * objects )
 */
@SpringBootApplication
// As the entities are in the dao module we have to add this configuration
@EntityScan(basePackages = {"com.co.training.dao.entities"})
// As the repositories are in the dao module we should use this annotation
@EnableJpaRepositories(basePackages = {"com.co.training.dao.repository"})
class CommonAppTest {

    @DisplayName("Load the spring context")
    @Test
    void contextLoads() {
    }
}
