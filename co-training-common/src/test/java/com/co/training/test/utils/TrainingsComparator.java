package com.co.training.test.utils;

import org.assertj.core.api.Assertions;
import org.openapitools.client.model.Training;
import org.openapitools.client.model.TrainingCriteria;
import org.openapitools.client.model.TrainingRequirement;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TrainingsComparator {

    /**
     * This method will compare two lists of trainings ( that contains other lists ) by ignoring the order of appearing of lists
     *
     * @param actual
     * @param expected
     */
    public static void compareTwoTrainingsLists(List<Training> actual, List<org.openapitools.client.model.Training> expected) {

        // assert that the two lists has the same size
        Assertions.assertThat(actual).hasSameSizeAs(expected);

        // sort the lists by id
        actual = actual.stream().sorted(Comparator.comparing(org.openapitools.client.model.Training::getId)).collect(Collectors.toList());
        expected = expected.stream().sorted(Comparator.comparing(org.openapitools.client.model.Training::getId)).collect(Collectors.toList());

        for (int i = 0; i < actual.size(); i++)
            compareTwoTrainingsObjects(actual.get(i), expected.get(i));
    }

    /**
     * This method will compare two trainings objects ( that contains other lists ) by ignoring the order of appearing of lists
     *
     * @param actual
     * @param expected
     */
    public static void compareTwoTrainingsObjects(org.openapitools.client.model.Training actual, org.openapitools.client.model.Training expected) {

        // assert that the actual has the same criterias as the expected
        if (actual.getCriterias() != null && expected.getCriterias() != null) {
            Assertions.assertThat(actual.getCriterias()).containsExactlyInAnyOrder(expected.getCriterias().toArray(new TrainingCriteria[expected.getCriterias().size()]));
            // after test , set criterias to null so we will not test them again when we'll test the trainings objects
            actual.setCriterias(null);
            expected.setCriterias(null);
        } else {
            Assertions.assertThat(actual.getCriterias()).isEqualTo(expected.getCriterias());
        }

        // test requirements list
        if (actual.getRequirements() != null && expected.getRequirements() != null) {
            Assertions.assertThat(actual.getRequirements()).containsExactlyInAnyOrder(expected.getRequirements().toArray(new TrainingRequirement[expected.getRequirements().size()]));
            actual.setRequirements(null);
            expected.setRequirements(null);
        } else {
            // if one of the arrays is null or the two are null we'll test them with equal method ( if the two are null it's ok )
            Assertions.assertThat(actual.getRequirements()).isEqualTo(expected.getRequirements());
        }

        // test tags , we'll not test tags because they are ignored in the equals method (check the training entity class)
        actual.setTags(null);
        expected.setTags(null);

        // now we can test trainings objects
        Assertions.assertThat(actual).isEqualTo(expected);
    }
}
