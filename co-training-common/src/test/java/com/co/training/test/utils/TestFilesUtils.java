package com.co.training.test.utils;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TestFilesUtils {

    private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    static {
        // we will load this object once to be used by all tests
        mapper.findAndRegisterModules();
        // well i know that it is a little strange to disable the annotations here and enable them in the source code. Here's the explanation :
        // in the app we have to enable them because of hibernate. Let's say that a user has a list of addresses and each address has a user. We will face
        // a stack overflow exception if you try to get the addresses because hibernate returns the list and each list return a user which returns a list
        // using json ignore annotation we can stop this chain
        // but in the tests we are not using hibernate but a plain text transformed to an object and used by mockito (to be returned each time wi call it)
        // which means if you try to get the addresses of a user you will get nothing if you didn't mention the addresses in the plain text
        // that's why we need to mention all the collections in these plain text files and to ignore the json ignore annotations otherwise they will not be mapped
        mapper.configure(MapperFeature.USE_ANNOTATIONS, false);
    }


    /**
     * Unmarshall a file to a java object
     *
     * @param type The type of object, it must not be a collection
     * @param file The file to unmarshall
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T extends Object> T loadObjectFromFile(Class<T> type, String file) throws IOException {

        InputStream inputStream = new ClassPathResource(file).getInputStream();
        T result = mapper.readValue(inputStream, type);
        return result;
    }

    /**
     * Unmarshall a file to a java list
     *
     * @param type The type of object, it must not be a collection
     * @param file The file to unmarshall
     * @param <T>
     * @return
     * @throws IOException
     */
    public static <T extends Object> List<T> loadListFromFile(Class<T> type, String file) throws IOException {

        InputStream inputStream = new ClassPathResource(file).getInputStream();
        CollectionType javaType = mapper.getTypeFactory().constructCollectionType(List.class, type);
        List<T> asList = mapper.readValue(inputStream, javaType);
        return asList;
    }

}
