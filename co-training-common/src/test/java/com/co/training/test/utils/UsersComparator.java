package com.co.training.test.utils;

import org.assertj.core.api.Assertions;
import org.openapitools.client.model.User;
import org.openapitools.client.model.UserInterestsSkills;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class UsersComparator {

    /**
     * This method will compare two lists of users ( that contains other lists ) by ignoring the order of appearing of lists
     *
     * @param actual
     * @param expected
     */
    public static void compareTwoUsersLists(List<User> actual, List<User> expected) {

        // assert that the two lists has the same size
        Assertions.assertThat(actual).hasSameSizeAs(expected);

        // sort the lists by id
        actual = actual.stream().sorted(Comparator.comparing(User::getId)).collect(Collectors.toList());
        expected = expected.stream().sorted(Comparator.comparing(User::getId)).collect(Collectors.toList());

        for (int i = 0; i < actual.size(); i++)
            compareTwoUsersObjects(actual.get(i), expected.get(i));
    }

    /**
     * This method will compare two users objects ( that contains other lists ) by ignoring the order of appearing of lists
     *
     * @param actual
     * @param expected
     */
    public static void compareTwoUsersObjects(User actual, User expected) {

        if (actual.getInterestsSkills() != null && expected.getInterestsSkills() != null) {
            Assertions.assertThat(actual.getInterestsSkills()).containsExactlyInAnyOrder(expected.getInterestsSkills().toArray(new UserInterestsSkills[expected.getInterestsSkills().size()]));
            // after test , set InterestsSkills to null so we will not test them again when we'll test the users objects
            actual.setInterestsSkills(null);
            expected.setInterestsSkills(null);
        } else {
            Assertions.assertThat(actual.getInterestsSkills()).isEqualTo(expected.getInterestsSkills());
        }
        // now we can test trainings objects
        Assertions.assertThat(actual).isEqualTo(expected);
    }
}
