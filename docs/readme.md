# Docs

This directory contains documents that explains the **ops** processes of our application like the gitlab jobs used to test and deploy the app and the yaml files used
to create the kubernetes objects of the project.

To understand the whole process of the deployment, start by checking the `k8s` directory. This directory contains a tutorial that explains how the set up a Google Cloud
project to hold the application. It will show you how also how to deploy a test version to the production.

You can then check the `sonarcloud.md`. It will show you how to set up a sonarcloud project to be used by the pipeline later to analyse the code.

After that, check the `gitlab-cicd.md`. It contains an explanation of the gitlab jobs used in our application.

Finally, have a look at `pipeline-example.md`. It contains an example of how to add a new feature to the project by following the git project workflow.
It shows also how the pipeline works and the job to launch after each action. It shows also the process launched by each job.