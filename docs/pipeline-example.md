# Pipeline Example

In this readme file we will explain the pipeline used by our project to deploy the application to the production platform. All the jobs has been explained in the
`gitlab-cicd.md` but in this file we will show you a practical case of how to add a new feature until deploy it to the production platform.

## Feature Branch

As explained in our git workflow. We start a new development by creating a new branch feature. The feature to do in this example is simple. We will just change our
entities to use a lombock annotation which is `@Accessors(chain = true)`. This will allow us to chain the setters calls of our objects. Check more details here :

- https://www.baeldung.com/lombok-accessors#chained

Let's start by creating a new branch from develop :

![1](/uploads/2a70012b69b369cb19d511c877e28a0d/1.png)

I used gitlab to do it but you can use your ide or git commands to do it. After that you can do your changes. Here's an example of the use of the chained setters :

![2](/uploads/7faca74e8c8f6644658d9c1061919a37/2.png)

After committing the changes, you can push them to the remote repository :

![3](/uploads/95257c93bc07261b79a8d3a3ff76eb22/3.png)

This will trigger the pipeline on gitlab, the jobs that will be executed on a feature branch are **dependencies** and **test:unit**. 

![4](/uploads/4c0f701f7ee7a0134a5e669968094a26/4.png)

The first one will compile the sources and second will execute unit tests.

![5](/uploads/80bfaec31eec34b549e4a5b67c17a504/5.png)

## Release Milestone

As this is a new release, we will create a new milestone with the release version. It will hold all the merge requests of the release and help us to group them in one
location.

Go to the milestones :

![6](/uploads/fa218718599b7e1c37a3dce3eb25ab80/6.png)

Create a new one with the release version :

![7](/uploads/77b532f3dd252829bb6d3e07baa4b51d/7.png)

## Merge the feature into develop

Now we can create a merge request to merge the feature into the develop branch.

![8](/uploads/ca10bd5838657ff4b39e784e49032b57/8.png)

Introduce the merge request title and description. Don't forget the milestone :

![9](/uploads/55ad29c8ec354d689bdb4ea863d4e8dc/9.png)

Now you can submit the merge request :

![10](/uploads/e205ff78a40f402eedfa9a8c7832f4a9/10.png)

You can choose to delete the source branch (feature) and to squash commits.

Double check the details of the merge request. If everything is good, you can click on the merge button.

![11](/uploads/c69bdf8f515f5ce120a41ccd1b4d49ea/11.png)

This will run the **dependencies** and the **test:integration** jobs. The last job will run integration tests.

![12](/uploads/6241217fbfc8b96d1bc49fd2df00adc7/12.png)

## Release Branch

After merging the feature into develop, we have to merge this last into a release branch. The release branch is used to test the application by the test team.

Checkout a new release branch from master :

![13](/uploads/5f83fb510908ffb0a48edf3c5cffde98/13.png)

As you can see, our release version is `1.0.2`. The release branch name must match the pattern **release/number.number.number**.

Create a merge request from develop :

![14](/uploads/b955bdaf7e69d034cdfcd52907f29231/14.png)

Don't forget the milestone :

![15](/uploads/8caaa338f2130be7b9e5be64ca25313d/15.png)

Merge :

![16](/uploads/0bd864242f7bdf8f45f788effe9959c8/16.png)

In the case you face merge conflicts :

![17](/uploads/51b0dc84b00774791427b2e5392f31fe/17.png)

1) from you local machine, Update the develop branch
2) checkout the release/1.0.2 branch
3) merge the develop into the release locally
4) resolve the conflicts
5) commit the changes
6) push the code
7) merge will be applied to the remote branch

![18](/uploads/9eef611fd0b92275ef8b7ae4af22460a/18.png)

The pipeline of release branches will be triggered :

![19](/uploads/df1dbc4be7fa26eb7d410e08f81d70e7/19.png)

After the **dependencies** job, the **sonarcloud:release** will be triggered :

![20](/uploads/72b10074f56162b69ca716557e53e1f2/20.png)

As I explained, the sonar jobs will fails always but that does not mean that we can't analyze the code. Go to sonarcloud and check for the release branch :

![21](/uploads/c4a41296f0f824af50b999690d7aa8e0/21.png)

You can see the new issues of the new release :

![22](/uploads/e15c238f9ee5967a290f76d949f9bbc6/22.png)

The **build:jar-snapshot** will build a new `1.0.2.SNAPSHOT` jar version release version and pass to the next job which is **publish:quality**.

![23](/uploads/68dc09a0d58d0fb04732e2983acffda1/23.png)

This job will build a docker image with the tag `1.0.2.SNAPSHOT` and push it to docker hub.

![24](/uploads/aabb0c84c092f137a7d94ef69c1deb0f/24.png)

The last job will be **deploy:quality** which will deploy the last created docker image to the quality platform.

![25](/uploads/7707bd79d7cb6a022b1a57d8ca3158af/25.png)

If you check the deployment from Google Cloud you'll find the last tag :

![26](/uploads/4e310b41781ef351459c3630fad66fd5/26.png)

## Master Branch

Well, after deploying the application to the quality platform, the testing team make all the functional tests and decide to go to production or not. If yes, we 
merge this release to the master.

Let's start by creating a merge request (as we can neither merge nor push) :

![27](/uploads/3964be1173d8e08dce794e3277cf4ff0/27.png)

Add details and don't forget the milestone :

![28](/uploads/bdf1285e41ea046f395fffd05ea27d1c/28.png)

submit :

![29](/uploads/61ca9b600589146875c077a36db1019d/29.png)

We didn't say it, the last commit has been created by the **build:jar-snapshot** job just after modyfing the project version by the release version.

If all is good, we can merge :

![30](/uploads/087056ee3b340b9e3e3ccc2611800088/30.png)

As I said the milestone holds all the merge requests :

![31](/uploads/743291854a9183eeb89b5855817bc230/31.png)

As always and after the **dependencies** job, the **sonarcloud:master** job will be launched :

![32](/uploads/96b6be1bffebeab849ff877f24f4c659/32.png)

Just after its execution, check the sonarcloud to have the analysis report :

![33](/uploads/0c7989147dc6a362f70c09a338024967/33.png)

The **build:jar-release** will be launched just after. It will change the project version by removing the snapshot. The new version will be `1.0.2`.

![34](/uploads/a4214b63799b97419d1c5783870a1457/34.png)

This job will also commit the changes to the remote repository after modifying the version number :

![35](/uploads/71d3144e8b453893c054f5e8705b8ca8/35.png)

Then it will tag it and push that tag :

![36](/uploads/73c3971ec37cfa95fc021ec8289ffdfd/36.png)

The **publish:production** will be triggered just after to push a new docker image with the new tag `1.0.2`.

![37](/uploads/7ff5a2fe5a1cc11873e844f7197d341b/37.png)

Check docker hub :

![38](/uploads/725b2177300b0c89773371a3910f4355/38.png)

Now we can deploy the production platform :

![39](/uploads/f4ce1ef3f2e0ee369f7642057edf0518/39.png)

Well this is a manual job, you have to execute it to by clicking the play button :

![40](/uploads/a9b19a198a910c4c11d1e33b470a7a1b/40.png)

Let's check if this image has been deployed to the production :

![41](/uploads/df43842a3c7622dd76dcde169744db6e/41.png)

## Tests

Check the ingress endpoints to do tests :

![42](/uploads/8a8915e0bf26a208923ec45d60ddbfb6/42.png)