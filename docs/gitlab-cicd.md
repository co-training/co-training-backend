# Gitlab Pipeline

In this section, I will explain the followed steps to create the ci/cd pipeline of the project using the gitlab jobs.
We will use the GitLab CI/CD to automatically build, test and deploy the application.
GitLab CI/CD uses a file in the root of the repository, named .gitlab-ci.yml, to read the definitions for jobs that will be executed by the configured runners. 
You can read more about this file in the GitLab Documentation.

- https://docs.gitlab.com/ee/ci/yaml/

gitlab runner here :

- https://docs.gitlab.com/ee/ci/quick_start/README.html

gitlab pipelines :

- https://docs.gitlab.com/ee/ci/pipelines/

First of all, remember to set up variables for your deployment. Navigate to your project’s Settings > CI/CD > Environment variables page and add the following ones (replace them with your current values, of course):

![variables_parent](/uploads/fd00901a11a2b6c950dc1b45990857a3/variables_parent.png)

We'll talk about these variables thereafter. You can define these variables in the group settings (co-training in my case) or in the project settings (co-training-backend in my case).

Now it’s time to define jobs in .gitlab-ci.yml and push it to the repository.

**Please Note** : that before running the pipeline and deploy the application you have to prepare a kubernetes cluster with multiple namespace. Please go to the `k8s` directory and
follow the steps to set up the cluster and build a new test version.

## Used git workflow

All along this project, we've used the GitFlow branching model. Our default is master and our development branch is
develop. Each time we need to release a new version we have to pull a feature branch from the develop branch. Feature
branches must start with the `feature/` + `name of the feature` for example feature/security.

After finishing the feature, we have to create a merge request to the develop branch. If approved, the branch can be
merged. For the release branch, we have to pull it from the master branch and not develop (difference between our
approach and GitFlow) and the release branch must have the next release version so it matches `release/` + `version`
for example release/1.0.9 . Now we can create a merge request to the release branch from the develop branch. If
approved, it can be merged. The release branch is used also to add the documentation of the branch and then you can
merge it back to the develop branch. This release branch will be also used by the testing team to validate the GO/NO-GO
to production. If GO, we'll create a merge request to the master branch and if approved it can be merged.

- https://backlog.com/git-tutorial/branching-workflows/

- https://datasift.github.io/gitflow/IntroducingGitFlow.html

## The pipeline of the co-training-rest

For the co-training-rest project, we will use the kubernetes technology to deploy it to the production environment.
Here's the pipeline used in this project to deploy this module to the Google Cloud Platform kubernetes engine.

![gitlab-cicd-workflow-2](/uploads/9420e1e95e892de472955e8d8e800a45/gitlab-cicd-workflow-2.jpg)

### pipeline default image

In our pipeline we will use the image `image: openjdk:8-alpine3.9` but you can use any dockerhub image. I'm choosing this image because it has jdk 8 installed obviously
but also because it is an alpine image so a small size. This is the image that will be used in all our jobs unless we define a different one (in each job you overwrite the image).

We don't need maven in this image as this job has been built from the [Spring Initializer](https://start.spring.io/) which contains a maven binary in the root of the project.

### environment variables

```yaml
variables:
  MAVEN_CLI_OPTS: "--batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -DLOG_LEVEL=ERROR -Dorg.slf4j.simpleLogger.defaultLogLevel=error"
  CI_DEBUG_TRACE: "false"
```
The batch-mode will automatically use default values instead of asking you via prompt for those values.

For all the jobs we will use these environment variables. We will set the logs to error, so the logs will not be huge.
CI_DEBUG_TRACE log level is false, but you can set it to true to run the gitlab runner log level to TRACE and have more details.

Gitlab will copy the project sources to a work directory `/builds/co-training/co-training-backend`, the maven repository `.me` will be in the current directory which 
is the same as the sources. This repository will be cached and passed from one job to another.

### dependencies job

```yaml
dependencies:
  stage: dependencies
  script:
    - ./mvnw compile $MAVEN_CLI_OPTS $MAVEN_OPTS
  only:
    - develop
```

This job will bo executed on all branches after any type of action ( push, merge, merge request ... ).
We will just compile the project and download the project dependencies then make them cached and used by the rest of the jobs.

### test:unit job

```yaml
test:unit:
  stage: test
  script:
    - ./mvnw clean test $MAVEN_CLI_OPTS $MAVEN_OPTS
  only:
    - /^feature/*/
```

This job is part of the test stage ( make sure to understand the difference between the job name and the stage name in gitlab ). This job will be triggered after
any push on any branch that its name starts with the word feature/ (like feature/ci-cd , feature/log4j2 , feature/sonar ...)

This job will not prevent the developer from pushing his code it will just run the unit tests on the project and inform him if something went wrong.

### test:integration job

```yaml
test:integration:
  stage: test
  script:
    - ./mvnw clean verify $MAVEN_CLI_OPTS $MAVEN_OPTS
  only:
    - develop
```

This job is part of the test stage, but it will be only triggered after merging a feature branch on the development branch.

This job will run the unit tests and the integration tests of the project. Integration tests are the karate tests in our example and they can be found in the
`co-training-rest-karate` module.

### sonarcloud:release job

```yaml
sonarcloud:release:
  stage: sonar
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  variables:
    CI_DEBUG_TRACE: "false"
  script:
    - >
      ./mvnw verify sonar:sonar $MAVEN_CLI_OPTS $MAVEN_OPTS
      -Dsonar.host.url=$SONAR_HOST_URL
      -Dsonar.login=$SONAR_TOKEN
      -Dsonar.projectKey="co-training_co-training-backend"
      -Dsonar.scanner.force-deprecated-java-version=true
      -Dsonar.jacoco.reportPath=target/jacoco.exec
      -Dsonar.organization="co-training"
      -Dsonar.qualitygate.wait=true
      -Dsonar.qualitygate.timeout=120
  allow_failure: true
  only:
    - /^release\/\d+\.\d+\.\d+$/
```

This job is part of the sonar stage which will analyse the quality of the code. This job will be executed after merging the develop branch on a release branch.
The release branch name must match this pattern `/^release\/\d+\.\d+\.\d+$/` which is the `release/` word + the version number like release/1.0.0 , release/36.0.23 .
You can make CI_DEBUG_TRACE variable true so you'll have a better look at the variables used in the job.
As we have issues with sonarcloud quality gate we will allow this to fail so we can continue with the next job if an error occur.
 
Have a look at the sonarcloud readme file to have more details of how to set up sonarcloud and integrate him to your gitlab pipeline.

To make multiline script, i followed this article : https://docs.gitlab.com/ee/ci/yaml/#multi-line-commands  

### build:jar-snapshot job

```yaml
build:jar-snapshot:
  stage: build
  variables:
    CI_DEBUG_TRACE: "false"
  before_script:
    - apk update && apk add git
    - echo 'Building the artifact jar ... '
    - version="$(echo $CI_COMMIT_REF_NAME | cut -d '/' -f 2)"-SNAPSHOT
    - echo "The new version to release is $version"
    - git remote set-url origin https://$GITLAB_USER_LOGIN:$GITLAB_CICD_ACCESS_TOKEN@gitlab.com/$CI_PROJECT_PATH.git
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    - ./mvnw versions:set -DnewVersion=$version -DartifactId=co-training-rest -DprocessParent=false -DallowSnapshots=true -DupdateMatchingVersions=false $MAVEN_CLI_OPTS $MAVEN_OPTS
    - ./mvnw versions:set-property -Dproperty=co-training-rest.version -DnewVersion=$version $MAVEN_CLI_OPTS $MAVEN_OPTS
    - ./mvnw versions:commit
    - ./mvnw clean package -DskipTests $MAVEN_CLI_OPTS $MAVEN_OPTS
    - git add .
    - git commit -m "[release][gitlab] create the release $version [ci skip]"
    - git push origin HEAD:$CI_COMMIT_REF_NAME
    - echo "BUILD_VERSION=$version" >> build.env
  artifacts:
    when: on_success
    paths:
      - co-training-rest/target/co-training-rest*.jar
    reports:
      dotenv: build.env
  only:
    - /^release\/\d+\.\d+\.\d+$/
```

This job will be triggered after merging the merge request to the release branch. As said before the release name must match `/^release\/\d+\.\d+\.\d+$/`.
As its name says the goal of this job is to build the fat jar of our spring boot application (located in the `co-training-rest` module). The version of our release
will be retrieved from the branch name. If for example the release branch name is `release/1.0.1` -> the release version will be 1.0.1-SNAPSHOT. Why SNAPSHOT ?

This version is SNAPSHOT as it is not yet validated by the testing team lets say, so it will be pushed to the quality platform to be tests and if validated it will
be a production candidate version and pushed to the production platform.

Please Note that the `co-training-rest` module version is always 1.0.0.DEV-SNAPSHOT. Developers does not need to change it. After each release the version will be changed automatically only on the master branch.

#### before_script section

Let's now explain the technical features used in this job. As the default image `alpine` does not contain `git` we have to install it in the first command ( in
before_script ). git will be used to commit the changes to the project repo after modifying the application version.

After that, we will retrieve the release version and store in a variable named `version` and configure git to point to our gitlab project repo.

To push changes from a gitlab job to a gitlab repo, you have to create a personal access token. Gitlab uses already a token to download the project sources but
it is a read only token and what we need here is write token. To create one you can follow these links :

- [create a gitlab personal token so you can push code from a gitlab-ci.yml](https://stackoverflow.com/questions/46472250/cannot-push-from-gitlab-ci-yml)

In my case i named the access token `GITLAB_CICD_TOKEN` :

![gitlab_access_token](/uploads/840c6f41fb6f08a25e28f999b9c66cf9/gitlab_access_token.png)


You have to register this token as an environment variable. Check this link if you don't know how to do it :

- https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui

In my case i named the access token `GITLAB_CICD_ACCESS_TOKEN` (its value if the value of the generated token `GITLAB_CICD_TOKEN`) :

![gitlab_access_token_variable](/uploads/f6e9adac2b5bf8598fee8c93f3218894/gitlab_access_token_variable.png)

$GITLAB_USER_LOGIN, $CI_PROJECT_PATH, $GITLAB_USER_NAME and $GITLAB_USER_EMAIL are variables provided by gitlab runner.

#### script section

In the script section, we have changed the release version to match the release branch name. In the first command, we've used the `versions:set` maven plugin to change the
`pom.xml` version of the `co-training-rest` module and only this module to **$version-SNAPSHOT** ($version is the variable created in the before_script).
As this module is used by other modules for example by the `co-training-rest-karate` module we've to change that to. If you check the root `pom.xml` you'll find a property
named `co-training-rest.version`. This property holds the `co-training-rest` version and that is the version number used by other modules so it has to be changed as well.

For this reason, we are using the `versions:set-property` maven plugin to set this property to **$version-SNAPSHOT** as you guess. After the change we've `versions:commit`
to commit these changes. Check the maven documentation for more details about this plugin :

- http://www.mojohaus.org/versions-maven-plugin/index.html

Everything is ready now to package the application with the new version number. A simple `./mvnw package` can do the stuff. We are skipping tests because they have been ran in the
previous merge_request job. The created release fat jar will be of course at this location `co-training-rest/target/co-training-rest-$version-SNAPSHOT.jar`.
Now we can commit the version changes to our gitlab repository using the commit name `[release][gitlab] create the release $version [ci skip]` and push it to the origin release branch.

#### artifacts

Last thing to do before passing to the next job is to store the generated artifact fat jar as a gitlab ci/cd artifact. Why doing this ?

As every job execution is isolated from other ones, each gitlab job must clone the repository before doing its work. Which means that the next job will not find
the release fat jar and to have it, the job must repackage the project which is already done in the previous one. 
Check more about the git strategy of the gitlab jobs here :

- https://docs.gitlab.com/ee/ci/yaml/#git-strategy

The artifacts can be transferred from one job to another after the job finishes. Check more here :

- https://docs.gitlab.com/ee/ci/yaml/#dependencies

Something is still missing here no ? it is the version of the release, the next job can determine the release version from the release branch name, but we can facilitate
the task by using the `Inheritence of environment variable`. We can write the variables to be inherited to the `build.env` file. These variables can be used by
other jobs now. Check how to do it here :

- [Inherit environment variables from another job](https://docs.gitlab.com/ee/ci/variables/README.html#inherit-environment-variables)

### publish:quality job

```yaml
publish:quality:
  # Official docker image. this image will behave as a docker client
  stage: publish
  image: docker:19.03.12
  # disable the cache for this job because we didn't need it
  cache: { }
  services:
    # this image will behave as a docker daemon
    - docker:19.03.12-dind
  variables:
    CI_REGISTRY_IMAGE: 'mouhamedali/co-training-backend'
  before_script:
    - echo " The build version is $BUILD_VERSION"
    - docker login --username mouhamedali --password-stdin < $GITLAB_DOCKER_HUB_TOKEN
  script:
    - docker pull $CI_REGISTRY_IMAGE || true
    - echo "Building the docker image $CI_REGISTRY_IMAGE:$BUILD_VERSION"
    - docker build --cache-from "$CI_REGISTRY_IMAGE" -t "$CI_REGISTRY_IMAGE:$BUILD_VERSION" .
    - docker push $CI_REGISTRY_IMAGE:$BUILD_VERSION
  dependencies:
    - build:jar
  only:
    - /^release\/\d+\.\d+\.\d+$/
```

This job will be triggered just after the build:jar finishes. Its main purpose is to create a docker image of the application. It will also tag the image with
the version that matches the release branch version.

As you can see, we are overwriting the default image by the docker image `docker:19.03.12` so we can use the docker commands. 
We are disabling the cache (which contains the m2 maven repository). Next, we are setting a service to run inside our docker image. This service is `docker:19.03.12-dind`
(dind : docker in docker) which is docker image and will behaves as a docker daemon in our docker image. Check this shot article if you don't know what's a docker daemon :

- https://nickjanetakis.com/blog/understanding-how-the-docker-daemon-and-docker-cli-work-together

These to know more details about gitlab services :

- [What is a service](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service)

- [Use Docker-in-Docker workflow with Docker executor](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor)

I've defined my docker hub repository as a variable `CI_REGISTRY_IMAGE`. As this job depends on the `build:jar`, it'll inherit the variables like **$BUILD_VERSION** and the fat jar artifact.

#### before_script section

The main purpose of this section is to login to my docker hub account, so i can push the docker image after building it. To do so, I've followed these steps :

- create a token from my docker hub account (From Account Settings -> Security -> New Access Token)

![4_co_training_token_dockerhub](/uploads/3c933f1a5c30f410db96ffe7497fc1f3/4_co_training_token_dockerhub.png)

- store the token as a file variable in gitlab

![4_co_training_token_dockerhub2](/uploads/ef9af7a6bab5c353407615b3d56bbfa4/4_co_training_token_dockerhub2.png)

- [gitlab custom file as variable](https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables-of-type-file)

This token file `$GITLAB_DOCKER_HUB_TOKEN` is used to login to my docker hub account.

#### script section

In this section we will pull the latest docker image from the repository `'mouhamedali/co-training-backend'`. This image behaves as a cache for our docker system
and will speed up the building of the release image. If the image does not exist, the job will not fails (that's why we are using `|| true` ). 
Then, we will build the release image using the cache mechanism. The release image tag will be retrieved from the $BUILD_VERSION variable.

Interesting links to build docker images :

- [Using Docker caching](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-docker-caching)
- [Best practices for building docker images with GitLab CI](https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/)

### deploy:quality job

```yaml
deploy:quality:
  stage: deploy
  image: "google/cloud-sdk:alpine"
  cache: { }
  before_script:
    - echo " The build version is $BUILD_VERSION"
    # Install envsubst
    - apk update && apk upgrade && apk add gettext
    - gcloud components install kubectl
  script:
    - gcloud auth activate-service-account --key-file=$GITLAB_GC_SERVICE_KEY
    - gcloud config set compute/zone europe-west1-d
    - gcloud config set container/cluster cluster-1
    - gcloud config set project co-training-293418
    - gcloud container clusters get-credentials cluster-1 --zone europe-west1-d
    - kubectl config set-context --current --namespace=co-training-quality
    - kubectl config current-context
    - tmp=$(mktemp)
    - envsubst < k8s/deployment.yaml  > "$tmp" &&  mv "$tmp" k8s/deployment.yaml
    - cat k8s/deployment.yaml
    - kubectl apply -f k8s
  dependencies:
    - build:jar
  only:
    - /^release\/\d+\.\d+\.\d+$/
```

This job will be triggered after the `publish:quality` and its main purpose is to deploy the docker image built by the publish job to the quality platform.

To deploy our application, we will use the kubernetes technology. You can find the deployment configuration files in the `k8s` directory. If you are not familiar
with this technology, check my repository dedicated to kubernetes beginners :

- https://gitlab.com/kubernetes-samples

There is so many cloud platforms that supports this technology. In this example we will use the GKE (Google Kubernetes Engine) solution which is the kubenetes solution
provided bo google, but you can use another provided if you want to.

- https://cloud.google.com/kubernetes-engine

The image used in this job is `google/cloud-sdk:alpine`. In contains a cli (command line interface) that helps you to interact with the google products and services.

#### before_script section

As this job depends on the `build:jar` job, it will have access to the **$BUILD_VERSION**.
As we will use the envsubst, we have to install the gettext package. This command will be used to substitute the environment variable used as docker tag in the
`k8s/deployment.yaml` file with the value of  **$BUILD_VERSION**. You can find more example about this command here :

- https://skofgar.ch/dev/2020/08/how-to-quickly-replace-environment-variables-in-a-file/

Last thing to do is to install the kubectl component on the gcloud image. As i'm using the alpine image, i should install it but if you want to have an image with several
components try another image from docker hub ( keep in mind that alpine is the best choice in most cases for example the latest image of the cloud-sdk weighs around 1 GB 
vs 138 MB for the alpine tag ).

- https://hub.docker.com/r/google/cloud-sdk/tags

#### script section

In this section, i suppose that you have a gcloud account and that you have already configured a project and activate the kubernetes service. We will use the same cluster
to deploy the quality and production applications and services but we will use two separate namespaces. The `co-training-quality` to deploy to quality and `co-training-production`
to deploy to production. You can create the namespaces using these commands :

```shell script
 kubectl create namespace co-training-quality
 kubectl create namespace co-training-production
```

To run commands on our cluster we have to generate a service token. This link will help you :

- https://cloud.google.com/container-registry/docs/advanced-authentication#token

Make sure that this token has these roles :

![gcloud_token_permissions](/uploads/1a4c082fd01ad7579bcd0851a29822dc/gcloud_token_permissions.png)

And add this file as a gitlab file variable :

![gcloud_token_variable](/uploads/866efe5248007cfab27359ff547dab8a/gcloud_token_variable.png)

Now we can authenticate on the gcloud server. We have to mention the project name, cluster name, the zone and the namespace (`co-training-quality` in our case).
After authentication, we will set the version of the release docker image using the `envsubst` command and a temporary file. Find more here :

- https://stackoverflow.com/questions/60546292/usage-of-envsubst-combined-with-tee-randomly-results-in-an-empty-file

Finally, we can deploy the yaml configuration files in the k8s directory using the `kubectl apply` command.

### sonarcloud:master job

Now we pass to the production config. After validating the release, we can continue ou process by merging the release branch to the master branche.

```yaml
sonarcloud:master:
  stage: sonar
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - >
      ./mvnw verify sonar:sonar $MAVEN_CLI_OPTS $MAVEN_OPTS
      -Dsonar.host.url=$SONAR_HOST_URL
      -Dsonar.login=$SONAR_TOKEN
      -Dsonar.projectKey="co-training_co-training-backend"
      -Dsonar.scanner.force-deprecated-java-version=true
      -Dsonar.jacoco.reportPath=target/jacoco.exec
      -Dsonar.organization="co-training"
      -Dsonar.qualitygate.wait=true
      -Dsonar.qualitygate.timeout=120
  allow_failure: true
  # we can continue the process even if the job fails because the sonar must be validated in the sonarcloud:release job
  only:
    - master
```

This job will be triggered after creating a merge request from a release branch to the master branch.

This job is the same as `sonarcloud:release`, the only difference is that we are tolerating the fail of it and such a case we can continue to the next jobs which will deploy
the release to the production platform.

### build:jar-release job

```yaml
build:jar-release:
  stage: build
  before_script:
    - apk update && apk add git
    - git remote set-url origin https://$GITLAB_USER_LOGIN:$GITLAB_CICD_ACCESS_TOKEN@gitlab.com/$CI_PROJECT_PATH.git
    - git config --global user.name "${GITLAB_USER_NAME}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
  script:
    - cd co-training-rest
    - FINAL_VERSION=$(../mvnw --non-recursive help:evaluate -Dexpression=project.version  -q -DforceStdout | cut -d '-' -f 1)
    - echo "The new version to release is $FINAL_VERSION"
    - cd ..
    - ./mvnw versions:set -DnewVersion=$FINAL_VERSION -DartifactId=co-training-rest -DprocessParent=false -DallowSnapshots=true -DupdateMatchingVersions=false $MAVEN_CLI_OPTS $MAVEN_OPTS
    - ./mvnw versions:set-property -Dproperty=co-training-rest.version -DnewVersion=$FINAL_VERSION $MAVEN_CLI_OPTS $MAVEN_OPTS
    - ./mvnw versions:commit
    - ./mvnw clean package -DskipTests $MAVEN_CLI_OPTS $MAVEN_OPTS
    - git add .
    - git commit -m "[master][gitlab] create the release $FINAL_VERSION [ci skip]"
    - git push origin HEAD:$CI_COMMIT_REF_NAME
    - git tag -a "v$FINAL_VERSION" -m "Create release version $FINAL_VERSION"
    - git push origin "v$FINAL_VERSION"
    - echo "BUILD_VERSION=$FINAL_VERSION" >> build.env
  artifacts:
    when: on_success
    paths:
      - co-training-rest/target/co-training-rest*.jar
    reports:
      dotenv: build.env
  only:
    - master
```

This job will be triggered after merging a release branch to the master branch. As its name says the goal of this job is to build the fat jar of our spring boot 
application (located in the `co-training-rest` module). The version of our release will be retrieved from the `co-training-rest` module version (by removing the snapshot). 
After changing the version number we will commit our changes like we did with the `build:jar-snapshot` job and the difference here is that we will create a tag with the version number.

#### script section

As in the `build:jar-release`, we've considered the version as a SNAPSHOT, in this job we will remove that and changed it to a release version.
In the first command, we've used the `versions:set` maven plugin to remove the SNAPSHOT from the `pom.xml` version of the `co-training-rest` module. To get the version of the release
from the version tag in the `pom.xml`, i've used the maven **help:evaluate** plugin :

- https://maven.apache.org/plugins/maven-help-plugin/evaluate-mojo.html

- [Get the version of your Maven project on command line](https://blog.soebes.de/blog/2018/06/09/help-plugin/)

The new release version will be held now by the `FINAL_VERSION` variable. As this module (co-training-rest) is used by other modules for example by the `co-training-rest-karate` module we've to change it too. That's why we updated the property
named `co-training-rest.version`. This property holds the `co-training-rest` version and that is the version number used by other modules.
After that, we can package the project to build the application fat jar. If everything is ok, we can commit our changes using this commit message `[master][gitlab] create the release $FINAL_VERSION [ci skip]` 
and create a tag with the version number `v$FINAL_VERSION`. The version number will be passed to next jobs to build a docker image.

For more details about artifacts and before_script, check the `build:jar-release` job definition.

### publish:production job

```yaml
publish:production:
  stage: publish
  image: docker:19.03.12
  cache: { }
  services:
    - docker:19.03.12-dind
  variables:
    CI_REGISTRY_IMAGE: 'mouhamedali/co-training-backend'
  before_script:
    - echo " The build version is $BUILD_VERSION"
    - docker login --username mouhamedali --password-stdin < $GITLAB_DOCKER_HUB_TOKEN
  script:
    # Build the image, pull the latest image so it can behave as a docker cache
    - docker pull $CI_REGISTRY_IMAGE || true
    - echo "Building the docker image $CI_REGISTRY_IMAGE:$BUILD_VERSION"
    - docker build --cache-from "$CI_REGISTRY_IMAGE" -t "$CI_REGISTRY_IMAGE:$BUILD_VERSION" .
    - docker push $CI_REGISTRY_IMAGE:$BUILD_VERSION
    # Create the latest tag and push it to docker hub
    - echo "Building the docker image $CI_REGISTRY_IMAGE:latest"
    - docker tag $CI_REGISTRY_IMAGE:$BUILD_VERSION $CI_REGISTRY_IMAGE
    - docker push $CI_REGISTRY_IMAGE
  dependencies:
    - build:jar-release
  only:
    - master
```

This job will be triggered after the `build:jar-release` and its main purpose is to build a docker image using the fat jar and the BUILD_VERSION variable generated by the previous job.

Same as the `publish:quality` job, the only difference is that in this job we will create two docker image or let's say the same docker image but with 2 tags. The first will be
the release version, and the second will be the `latest` tag.

### deploy:production job

```yaml
deploy:production:
  stage: deploy
  image: "google/cloud-sdk:alpine"
  when: manual
  cache: { }
  before_script:
    - echo " The version to deploy is $BUILD_VERSION"
    - apk update && apk upgrade && apk add gettext
    - gcloud components install kubectl
  script:
    # set the project config
    - gcloud auth activate-service-account --key-file=$GITLAB_GC_SERVICE_KEY
    - gcloud config set compute/zone europe-west1-d
    - gcloud config set container/cluster cluster-1
    # connect to the cluster
    - gcloud config set project co-training-293418
    - gcloud container clusters get-credentials cluster-1 --zone europe-west1-d
    - kubectl config set-context --current --namespace=co-training-production
    - kubectl config current-context
    # change the tag and deploy it
    - tmp=$(mktemp)
    - envsubst < k8s/deployment.yaml  > "$tmp" &&  mv "$tmp" k8s/deployment.yaml
    - cat k8s/deployment.yaml
    # deploy
    - kubectl apply -f k8s
  dependencies:
    - build:jar-release
  only:
    - master
```

This job will be triggered after the `publish:production` and its main purpose is to deploy the docker image built by the previous job to the production platform.

For more details about the process check the `deploy:quality` job. This job is manual which means that you have to start it from the gitlab web ui after the `publish:production`
job finishes otherwise the new version will not be available in the production platform.

## Protected environment variables

To create a custom variable using the gitlab ui, follow this steps :

- https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui

As you can see, here are the variables that i've used in my pipeline :

![variables_parent](/uploads/d904bac5dd0437808f2005423b80de88/variables_parent.png)

These variables will not work on the releases branches because they are protected variables which means you can't use them on a non protected branch. To make the releases
branches protected, follow these steps :

1) Navigate to your project's Settings ➔ Repository.
2) Scroll to find the Protected branches section.
3) From the Branch dropdown menu, select the branch you want to protect and click Protect. ...
4) Once done, the protected branch will appear in the “Protected branches” list.

As the releases branches name may change, you have to use a pattern like this one :

![protected_branches](/uploads/a5f99bbf5e6f6582dee4bce3a376850f/protected_branches.png)

A last tip is the use of the **CI Lint**, this tool will help you to validate your `.gitlab-ci.yml` file before committing your changes.

- https://docs.gitlab.com/ee/ci/lint.html

## References

- https://docs.gitlab.com/ee/ci/yaml/

- https://docs.gitlab.com/ee/ci/pipelines/

- https://about.gitlab.com/blog/2016/07/29/the-basics-of-gitlab-ci/

- [Automating Your Java Project Workflow with a Modified Gitflow Branching Model](https://www.infoq.com/articles/gitflow-java-project/)

- [How to deploy Maven projects to Artifactory with GitLab CI/CD](https://docs.gitlab.com/ee/ci/examples/artifactory_and_gitlab/index.html)

- [Cache dependencies in GitLab CI/CD ](https://docs.gitlab.com/ee/ci/caching/)

- [Updating version numbers of modules in a multi-module Maven project](https://stackoverflow.com/questions/5726291/updating-version-numbers-of-modules-in-a-multi-module-maven-project)

- https://docs.gitlab.com/ee/ci/yaml/#onlyvariablesexceptvariables

- https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-docker-caching

- [Best practices for building docker images with GitLab CI](https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/)

- https://docs.gitlab.com/ee/ci/yaml/#rules