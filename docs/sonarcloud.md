# Sonar Cloud

SonarCloud is a cloud-based code quality and security service. We've used sonarqube to analyse the sources on our local machine. It will convenient if we can share those
statistics with the team. That's why we will use a cloud based platform of sonar like sonar-cloud.

## Get started

### Create a new sonar cloud project

To get started, you have to create a new sonar-cloud project, it is free for open source projects. Try this link and connect with your git platform (gitlab in my case) :

- https://sonarcloud.io/welcome

You can start by creating a new organization :

![1](/uploads/8e79af701302f896e2ebc3ffbea1f34b/1.png)

Then you have to import your repository, follow this link to get it done :

- https://sonarcloud.io/documentation/integrations/gitlab/

generate gitlab token :

![1_token](/uploads/dd12e27546bfbf03c04a1639c7b6756c/1_token.png)

copy the token :

![2](/uploads/f5d383c96a19b304bdc82b801ffee3d7/2.png)
![3](/uploads/669fe20fcce6c8002759117a47833baa/3.png)

paste it :

![4](/uploads/0e381525d341f7375cd6479cbd85b3e4/4.png)

choose an organization name (or key) :

![5](/uploads/c6ab8e0a30c2b3bfc98ce4a789e4c019/5.png)

choose the free plan (only for open source projects) :

![6](/uploads/3dd08f0a4c08a86add0995644bb10774/6.png)

Click on **Analyse new project** and choose the projects to analyse :

![7](/uploads/c74f7cd302b54004e0e409336d30c163/7.png)

![8](/uploads/8c821268aff0e2d69fae3db067f23398/8.png)

### Generate sonar cloud tokens

Now we have to generate sonarcloud tokens to be used in the gitlab ci/cd. Follow this link to know more :

- https://sonarcloud.io/documentation/integrations/ci/gitlab-ci/#1-add-environment-variables

On SonarCloud go to My account > Security > Generate Tokens to generate a fresh token that GitLab CI can use. 

![9](/uploads/604d73b487eeb5e1f05602fd9269da1e/9.png)

In GitLab, go to Settings and then CI/CD Variables to add the following variables :

![10](/uploads/1c7eb34360e529550d513139f2a4355d/10.png)

### Configure the main branch

Go to the project (co-training-backend in my case) > Administration > Branches and Pull Requests and make sure that the main branch is `master`. 
You can change it if you want but in my case all the analysis will be based on the code in master.

- Main branch sonarcloud : https://docs.sonarqube.org/latest/branches/overview/

![11](/uploads/6a033f980df3d01365e53210956478e5/11.png)

## Gitlab CI/CD

We've declared two sonarcloud jobs in the `.gitlab-ci.yml` file. The first one will be executed after creating a merge request to a release branch. The second job will be 
executed after merging the code to the master branch. Here's the declaration :

```yaml
# Run sonarqube after merging on the master branch so we can have statistics between two releases
sonarcloud:master:
  stage: sonar
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - >
      ./mvnw verify sonar:sonar $MAVEN_CLI_OPTS $MAVEN_OPTS
      -Dsonar.host.url=$SONAR_HOST_URL
      -Dsonar.login=$SONAR_TOKEN
      -Dsonar.projectKey="co-training_co-training-backend"
      -Dsonar.scanner.force-deprecated-java-version=true
      -Dsonar.jacoco.reportPath=target/jacoco.exec
      -Dsonar.organization="co-training"
      -Dsonar.qualitygate.wait=true
      -Dsonar.qualitygate.timeout=120
  allow_failure: true
  # we can continue the process even if the job fails because the sonar must be validated in the sonarcloud:release job
  only:
    - master
```

The project organization is co-training as shown below :

![12](/uploads/3979334d3cb240d55e20f026390f5e1b/12.png)

**PN** : The current sonarcloud configuration of the project is `deprecated` that's why we are using `sonar.scanner.force-deprecated-java-version`. The test's coverage
is not configured too in this project and all of these issues will be resolved in a future release. We are using jacoco binary report (generated in target/jacoco.exec)
which is no longer supported by sonarqube. The new reports format to use is xml. The java version 8 is also no longer supported by sonarcloud and we have to change it in a
future release.

- https://community.sonarsource.com/t/sonarcloud-java-coverage-suddenly-zero-as-of-today/18410

The `sonar.jacoco.reportPath` is deprecated and will be changed in a next release.

## References

- https://docs.sonarqube.org/latest/analysis/coverage/