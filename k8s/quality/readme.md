# Kubernetes Yaml Files

In this readme file we will explain in details the different kubernetes features that we have used to set up our application on the cloud. We will see these files one 
by one and we will try to make the explanation simple. We will also provide used sources and examples from the kubernetes or google documentation.

## Yaml files

### Ingress

`ingress.yaml`

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: co-training-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  rules:
    - http:
        paths:
          - path: /backend(/|$)(.*)
            backend:
              serviceName: co-training-backend-service
              servicePort: 8080
```

Ingress is the component that will intercept the internet traffic and route it to the adequate service. All the traffic (http requests) that start with `/backend` will be redirected
to the backend service.

- https://kubernetes.io/docs/concepts/services-networking/ingress/#what-is-ingress

- https://kubernetes.github.io/ingress-nginx/

We have a problem here, our application context-path is `/co-training` which means that it handles http calls to `/co-training/*`. Any other request ,in this case `/backend/co-training/*`, will
be rejected ?

Nginx resolves this issue by rewriting the url and that's why we are using the annotation `nginx.ingress.kubernetes.io/rewrite-target`. It will remove the `/backend` before passing the request
to our app. You can check this example which explains in details this mechanism :

- https://kubernetes.github.io/ingress-nginx/examples/rewrite/#examples

- https://gitlab.com/kubernetes-samples/gke/-/tree/master/ingress-examples/2_multi-backends-nginx

### Service

`service.yaml`

```yaml
apiVersion: v1
kind: Service
metadata:
  name: co-training-backend-service
spec:
  selector:
    component: co-training
    type: backend
  type: ClusterIP
  ports:
    - port: 8080
      targetPort: 8090
```

The ClusterIP is the default service used by kubernetes. It exposes the Service on an internal IP in the cluster. 
This type makes the Service only reachable from within the cluster.

### Persistence volumes

`logs-pvc.yaml`

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: co-training-backend-logs-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```
As our application use log files to trace processes and provide errors (if there is some) to be analysed, we have to mount a volume to the logs location otherwise we will
lose them each time the container restarts.

To achieve this goal, we are using a persistence volume of one Gigabyte. The volume type is `PersistentVolumeClaim` which is a claim to allocate storage space. This volume named `co-training-backend-logs-pvc` will
be mounted as read write by only one pod at a time.

- [Configure a Pod to Use a PersistentVolume for Storage](https://kubernetes.io/docs/tasks/configure-pod-container/configure-persistent-volume-storage/)

### Deployment

`deployment.yaml`

I will split this file and explain each section apart to make it understandable.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: co-training-backend-deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      component: co-training
      type: backend
```

As mentioned in the kubernetes documentation, a Deployment is an object that provides declarative updates for Pods and ReplicaSets.
You describe a desired state in a Deployment, and the Deployment Controller changes the actual state to the desired state at a controlled rate.

Our desired state is to have only one pod without any replication. You can update the `replicas` to control the availability of your applications.

- https://kubernetes.io/docs/concepts/workloads/controllers/deployment/

```yaml
  template:
    metadata:
      labels:
        component: co-training
        type: backend
    spec:
```

This is the replica set definition used by our deployment to create and kill pods to reach the desired state. 
The ReplicaSet's purpose is to maintain a stable set of replica Pods running at any given time. 

- https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/

- https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/#deployment-recommended

```yaml
      volumes:
        # logs volume
        - name: logs-pvc-storage
          persistentVolumeClaim:
            claimName: co-training-backend-logs-pvc
        # google proxy sql service account volume
        - name: db-service-account
          secret:
            secretName: co-training-db-quality-pod
```

In this section we are declaring the volumes that we will use the container's definition later. `logs-pvc-storage` will be the name or the key of the persistence volume claim
created in the `logs-pvs.yaml` file. This volume will be used by our application container to store logs.
 
The second volume is `db-service-account` which is the key to the secret named `co-training-db-quality-pod`. So this secret will be mounted as a volume to the proxy container
 and will be used to establish a connection to postgres database instance of our project.
 
```yaml
      initContainers:
        - name: volume-permissions
          image: busybox
          command: [ 'sh', '-c', 'chown -R 100:101 /usr/app/logs' ]
          volumeMounts:
            - mountPath: "/usr/app/logs"
              name: logs-pvc-storage
```

Init containers are containers that run before app containers to set up something that the app containers cannot do. In our case, we are using this feature because of the `logs` volume.
Our application uses spring:spring as user:group and uses log4j2 to create log files in `/usr/app/logs`. So to do that without facing problems we have to make spring:spring the
owner of that directory before running the application.
This will be done by running the `busybox` image as an init container. The user of the container is root, so we can run a simple chown command to make the spring user the owner of the directory.
Then in the `containers` section, you have to tell kubernetes that the user running inside the container is not the root but another one (check securityContext of the first container, will be explained later).

If you check the command executed by the container you'll see the owner uid and gid which are 100 and 101. These are the first uid and gid created by alpine (as it is our base image). To make sure that you
are using the good ones, run the image on your local machine and execute the `id` command which will show the user and the group ids.

without running the init container, I had these permissions :

```log
/usr/app $ ls -ltr
total 16
drwxr-xr-x    3 spring   spring        4096 Nov  9 10:51 org
drwxr-xr-x    3 spring   spring        4096 Nov  9 10:51 META-INF
drwxr-xr-x    1 spring   spring        4096 Nov  9 10:51 BOOT-INF
drwxrwsr-x    3 root     spring        4096 Nov  9 19:14 logs
```
After adding the init container I had this result :

```log
/usr/app $ ls -ltr
total 16
drwxr-xr-x    3 spring   spring        4096 Nov  9 10:51 org
drwxr-xr-x    3 spring   spring        4096 Nov  9 10:51 META-INF
drwxr-xr-x    1 spring   spring        4096 Nov  9 10:51 BOOT-INF
drwxrwsr-x    4 spring   spring        4096 Nov  9 20:34 logs
```

- https://kubernetes.io/docs/concepts/workloads/pods/init-containers/

- https://github.com/kubernetes/kubernetes/issues/2630

- https://serverfault.com/questions/906083/how-to-mount-volume-with-specific-uid-in-kubernetes-pod

- [Get a Shell to a Running Container](https://kubernetes.io/docs/tasks/debug-application-cluster/get-shell-running-container/)

```yaml
containers:
        - name: co-training
          image: mouhamedali/co-training-backend:$BUILD_VERSION
          # We gonna set the image pull policy to always. So each time we restart the pod, it's gonna pull the last pushed image otherwise it will use the image already present locally (if exists).
          imagePullPolicy: Always
          securityContext:
            runAsUser: 100
            runAsGroup: 101
          ports:
            - containerPort: 8090
```

Now we can see the configuration of our `co-training-rest` application container. As you can see we are using the docker with a tag defined as an environment variable. The pipeline is the
responsible of changing this data to the version to be deployed to the quality or the production environment. The security context has been used to tell kubernetes that this container has to be runned
using the spring user and not the root.

- [UID (User Identifier) and GID (Group Identifier)](https://geek-university.com/linux/uid-user-identifier-gid-group-identifier/)

- [Configure a Security Context for a Pod or Container](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)

```yaml
          readinessProbe:
            httpGet:
              path: /co-training/actuator/health
              port: 8090
            initialDelaySeconds: 50
            periodSeconds: 5
          livenessProbe:
            httpGet:
              path: /co-training/actuator/health
              port: 8090
            initialDelaySeconds: 50
            periodSeconds: 5
```

Liveness and readiness probes used to check if the application is still alive to handle a new request and if it is ready to accept traffic.

- [Configure Liveness, Readiness and Startup Probes](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes)

```yaml
          volumeMounts:
            - mountPath: "/usr/app/logs"
              name: logs-pvc-storage
```

Here we are mounting the logs volume to the `/usr/app/logs` directory.

```yaml
          env:
            - name: SPRING_PROFILES_ACTIVE
              value: "postgres"
            - name: LOG_LEVEL
              value: "warn"
            # Define the environment variable from secrets
            - name: DB_NAME
              valueFrom:
                secretKeyRef:
                  name: backend-quality-props # The secret this value comes from.
                  key: db-name                # The key to fetch.
            - name: PG_USERNAME
              valueFrom:
                secretKeyRef:
                  name: backend-quality-props
                  key: username
            - name: PG_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: backend-quality-props
                  key: password
```

These are the environments variables used by our application. The first two variables are the spring profile and the log level. The rest of the variables are the database
parameters which are retrieved from the `backend-quality-props` secret. Check the readme file from `k8s` to know how to create that secret. Our application will talk directly
with the database, but it will send the jdbc request to the `cloud sql proxy container` (defined later) and this later will establish the connection to the database, execute the query
and retrieve the result. That's why we are not defining the `PG_HOST` environment variable here because it is `localhost` as the proxy and the app are in the same pod. The default
proxy port is the default postgres port (5432).

- https://gitlab.com/kubernetes-samples/minikube/-/blob/master/04-secrets/task-01.md

- https://kubernetes.io/docs/tasks/inject-data-application/define-environment-variable-container/

- [Using Secrets as environment variables](https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets-as-environment-variables)

```yaml
        - name: cloud-sql-proxy
          image: gcr.io/cloudsql-docker/gce-proxy:1.17
          command:
            - "/cloud_sql_proxy"
            # instance name must be in the form `project:region:instance-name`
            - "-instances=co-training-293418:europe-west1-d:co-training-db=tcp:5432"
            # This flag specifies where the service account key can be found
            - "-credential_file=/secrets/service_account.json"
          securityContext:
            # The default Cloud SQL proxy image runs as the "nonroot" user and group (uid: 65532) by default.
            runAsNonRoot: true
          volumeMounts:
            - name: db-service-account
              mountPath: /secrets/
              readOnly: true
```

This is the last container that we have used to set up our application. It is the recommended way suggested by Google to communicate with a database. This is a sidecar container which means a container to install
beside your containers to achieve a goal which is to secure the communication to our database in our case. The container mounts the sql client secret key as a volume (check the `k8s` readme file).

- [Using Secrets as files from a Pod](https://kubernetes.io/docs/concepts/configuration/secret/#using-secrets-as-files-from-a-pod)