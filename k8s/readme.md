# Kubernetes deployment

In this directory, you'll find all the yaml files (yes we are using the declarative way) used to deploy the co-training platform to the Google cloud kubernetes service. 
It is very simple to deploy the app using this method, you have to only the run a simple kubectl command which is `kubectl apply -f` and k8s will do all the work for us.
Before doing that, we have to set up the environment or let's say the environments because we have two. Like we said in other areas, we are using two platforms, the **quality**
which is used by the testing team to validate or not a release and the **production** platform which is used by the users of the app. In this tutorial we will not talk about the
differences between these platforms in term of performance nor about the security guideline as well. We will talk about how to set up the two platforms and some of the good practises 
to achieve that goal.

Let's begin with namespace, these two platforms will be installed on different namesapaces **co-training-quality** for the quality and **co-training-production** for the production.
Let's have a look about the architecture of each namespace.

If you are a beginner in kubernetes, please check the repo that I dedicated to beginners (like me). It contains simple examples about important kubernetes features :

- https://gitlab.com/kubernetes-samples

Start with the `minikube` tutorial and then switch to `gke` tutorial.

## Architecture

![kubernetes-architecture](/uploads/7741a0e2f5051cedd1ef1caae6ad4712/kubernetes-architecture.jpg)

As you can see, we are using nginx ingress to handle outside connections. Ingress is an api object that manages external (internet) access to the services of your cluster.
If you don't know what's an ingress or what's an nginx ingress, check these links :

- https://kubernetes.io/docs/concepts/services-networking/ingress/#what-is-ingress

- https://kubernetes.github.io/ingress-nginx/

Nginx handles all client calls and switch the call to the backend or the frontend based on the url. We are using a `ClusterIP` service and not a `NodePort` which is more secure. As our application 
does not need a high performance, we use one replication in the `Deployment`.
Our pod has been built using three containers, the `volume-permissions` is used to set up the logs persistence volume so our application `co-training` container can write logs to that volume.
The `cloud-sql-proxy` container has been used as a proxy to assure the communication between our app container and the postgres database.

We will talk more about this configuration later in this article.

## Yaml files

You can find the quality platform yaml files in the `k8s/quality` folder and production files in `k8s/production` folder. Each folder provides a readme file to explain in details
the used kubernetes objects.

## Set up the environment

Before beginning, you have to follow these requirements to create a Google cloud account, create your first project, enable billing and install google cloud sdk :

- https://gitlab.com/kubernetes-samples/gke/-/tree/master#requirements

If this is you first google cloud account, you can profit from 1 year trial and about 300$ in your account.

Set up you default zone if you didn't do it already. In my case, the default zone is **europe-west1-d**.

Check this link to get your zone :

- https://cloud.google.com/compute/docs/regions-zones

This one to set it up :

- https://cloud.google.com/compute/docs/regions-zones/changing-default-zone-region#changing_the_default_region_or_zone

To login to your project (in my case co-training-293418) use these commands (you need google sdk) :

```shell script
$ gcloud auth login
$ gcloud projects list
$ gcloud config set project co-training-293418
```

If you are a lazy one, you can create an alias of the kubectl command and activate the autocompletion like this :

```shell script
$ alias k=kubectl
$ source <(kubectl completion bash)
$ source <(kubectl completion bash | sed 's/kubectl/k/g' )
```

You can find more in my `ckad tips` repo here :

- https://gitlab.com/kubernetes-samples/ckad-tips

Or in k8s docs :

- https://kubernetes.io/docs/reference/kubectl/cheatsheet/

### Create clusters

As we have two platforms quality and production, we will create a cluster per platform. You can create it using the Google web ui, it is so simple to use. 

Let's start with the quality cluster :

![cluster_1](/uploads/2068a78fe8f24d6bf5e2a249d9e53122/cluster_1.png)

![cluster_2](/uploads/0e17e4af04a828f98cf1223dc3e6ca26/cluster_2.png)

I'm using the micro machine as I don't want to have to high performance and this is just to test the app not more.

If you check the clusters in your project you'll have something like this :

```shell script
$ gcloud container clusters list
```

```log
NAME             LOCATION        MASTER_VERSION   MASTER_IP      MACHINE_TYPE  NODE_VERSION     NUM_NODES  STATUS
quality-cluster  europe-west1-d  1.16.13-gke.401  104.199.9.151  e2-small      1.16.13-gke.401  3          RUNNING
```

We have 3 nodes in our cluster and that causes a quota problem in my case because I can't have more than 4 nodes in my projects. The node numbers doesn't show on the web ui
cluster configuration. So I will reduce it using the command line :

```shell script
$ gcloud container clusters resize quality-cluster --node-pool default-pool --num-nodes 2
```

Now we can create the production cluster, We will use the command line this time :

```shell script
$ gcloud container clusters create \
    --machine-type e2-small \
    --num-nodes 2 \
    --zone europe-west1-d \
    --cluster-version 1.16.13-gke.401 \
   production-cluster
```

```shell script
$ gcloud container clusters list
```

```log
NAME                LOCATION        MASTER_VERSION   MASTER_IP       MACHINE_TYPE  NODE_VERSION     NUM_NODES  STATUS
production-cluster  europe-west1-d  1.16.13-gke.401  35.187.176.197  e2-small      1.16.13-gke.401  2          RUNNING
quality-cluster     europe-west1-d  1.16.13-gke.401  34.76.255.16    e2-small      1.16.13-gke.401  2          RUNNING
```

![cluster_3](/uploads/c69ca8960f74a1ad3610cd734e805113/cluster_3.png)

### Create a postgres database

As our application uses a relational database, we have to have one accessible from the cluster. We can define our database as a docker container but this is the hardest
way because you have to manage it and maintain it. What I prefer is to use a ready to use database like the one provided by the Google cloud platform. So, let's create one :

- https://cloud.google.com/sql/docs/postgres/create-manage-databases#console

Make sure to save the generated password somewhere safe (this is the root password) :

![database](/uploads/8cd8a84bc5bfcfbc60051913d8daae26/database.png)

you have more options in the configuration options (in my case I reduced the machine performance because my app does not need high performance).

![database2](/uploads/b5cf35970ef1afda35490561f09753ac/database2.png)

Now you can create the production and quality databases and users. For the quality platform, add a new database named **quality** then add a new user named **quality-user**.

![database_user_quality](/uploads/c54a487de366dd6d3e0beb32d591159a/database_user_quality.png)

For production the database name is **production** and the user is **production-user**.

![database_names](/uploads/633706a778ee28c41271ba0f2a4c3cd8/database_names.png)

![database_users](/uploads/dcccee727ea03a5097af3eb7839b4462/database_users.png)

The database instance name is what identifies the database, it will be used later to establish a connection.

![db_instance](/uploads/4da75d1bcdb0a54fda043229303b45ca/db_instance.png)

### Install the app schema

Well, Now we have to connect to the created database and execute the app sql queries to build the schema. Please check the `readme` file of the `co-training-rest` module to know
more about the app schema. The schema full path is `co-training-rest/src/main/resources/schema-postgres.sql`.

To install this script, we will upload it to the Google bucket and import it using the web ui. Let's upload the script :

- https://cloud.google.com/storage/docs/creating-buckets

Keep the default options and upload only the script file from this path `co-training-rest/src/main/resources/schema-postgres.sql`.

- ![sql_bucket](/uploads/4eb41c8b09f49916a5bcf4d30de3e38a/sql_bucket.png)

Now you can import the script and execute it on your database. Check this link :

- https://cloud.google.com/sql/docs/mysql/import-export/importing#importing_data_from_a_sql_dump_file_in

Click on import :

![sql_bucket_0](/uploads/d1f04ab3a6a79f16abfc4f24f5ac2dcd/sql_bucket_0.png)

Select the created bucket and import the script :

![sql_bucket_2](/uploads/2e62f0c73391e52f718b8ebadb571cb5/sql_bucket_2.png)

Select the quality database :

![sql_bucket_3](/uploads/d71da396c9d8d0fca17cf4e3452fb682/sql_bucket_3.png)

Do the same for the production database :

![sql_bucket_4](/uploads/2e75ad4234eb3a836f1220717384a692/sql_bucket_4.png)

### Create the namespaces

We will isolate our pods by creating namespace to hold them. The quality namespace will be **co-training-quality** and the production namespace will be **co-training-production**.
Let's create the first one :

1) connect to the cluster and create the namespace :

```shell script
$ gcloud container clusters get-credentials quality-cluster
$ kubectl create namespace co-training-quality
```

```shell script
$ gcloud container clusters get-credentials production-cluster
$ kubectl create namespace co-training-production
```

If you are not sure about the cluster you are using, run this to check that :

```shell script
$ kubectl config current-context
```

### Install ingress

**This must be done on the different clusters**.

As we will use ingress to deploy all our applications, we have to install it on the cluster. You can consult this link for more details :

- https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke

In my case, the last version of nginx install script was :

```shell script
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.0/deploy/static/provider/cloud/deploy.yaml
```

Check if the ingress controller pods have started :

```shell script
$ k get pods --namespace ingress-nginx
```

```log
NAMESPACE       NAME                                       READY   STATUS      RESTARTS   AGE
ingress-nginx   ingress-nginx-admission-create-krnxz       0/1     Completed   0          42s
ingress-nginx   ingress-nginx-admission-patch-l9l5t        0/1     Completed   0          42s
ingress-nginx   ingress-nginx-controller-58459d46f-5l8fz   1/1     Running     0          43s
```

### Add kubernetes secrets

Go to the `quality` namespace :

```shell script
$ gcloud container clusters get-credentials quality-cluster
$ kubectl config set-context --current --namespace=co-training-quality
```

Add the secrets to be used by the app to connect the last created database :

```shell script
$ k create secret generic backend-quality-props --from-literal=db-name=quality --from-literal=username=quality-user --from-literal=password=${YOUR_DB_USER_PASSWORD}
```

Go to the `production` namespace :

```shell script
$ gcloud container clusters get-credentials production-cluster && k config set-context --current --namespace=co-training-production
```

Add the secrets to be used by the app :

```shell script
$ kubectl create secret generic backend-production-props --from-literal=db-name=production --from-literal=username=production-user --from-literal=password=${YOUR_DB_USER_PASSWORD}
```

### Add cloud sql proxy

Go to the `quality` namespace :

```shell script
$ gcloud container clusters get-credentials quality-cluster
```

To be able to communicate to my database, I will use the `Cloud SQL Proxy` technology which is a proxy between the application and the database that we will add to make
the communication more secure. The proxy is sidecar container which means that it will be attached as supporting container to your application’s container in order to make communication easier with other components in some way.

Have a look at this article, it explains the benefits of this proxy and how to install it :

- https://cloud.google.com/sql/docs/postgres/connect-kubernetes-engine#proxy

Follow the steps from the last link, in my case, I will use the service account way to install the proxy (I think that it is more simple and flexible in the case you want to migrate your app to
another cloud provider). Here are the steps that I've followed :

1) enable the Cloud SQL Admin API for your project as explained in the last link

2) create a Google service account

Follow these steps :

- https://cloud.google.com/sql/docs/postgres/sql-proxy#create-service-account

We will add a new service account for the quality platform.

![proxy_sa_quality_1](/uploads/8c6a7cfacf8548a2563cef1668ab33ce/proxy_sa_quality_1.png)

![proxy_sa_quality_2](/uploads/73b19cf7a7e6183a656379626b92f705/proxy_sa_quality_2.png)

Don't forget to generate a key :

![proxy_sa_quality_3](/uploads/d91ba9bf092240a25f61951f433239a5/proxy_sa_quality_3.png)

3) create a k8s service account

Go to the directory in which the last downloaded key is located :

```shell script
$ cd /tmp/mozilla_salto0/
$ ll
```

```log
total 32
drwx------  2 salto salto  4096 nov.  11 12:06 ./
drwxrwxrwt 69 root  root  20480 nov.  11 12:24 ../
-r--------  1 salto salto  2361 nov.  11 12:06 co-training-293418-96b8ac94e830.json
```

Run this command :

```shell script
$ kubectl create secret generic co-training-db-quality-pod --from-file=service_account.json=./co-training-293418-96b8ac94e830.json --namespace=co-training-quality
```

co-training-db-quality-pod : The secret name ( you can choose any name )

service_account.json : The key to get the secret value ( you can choose any name )

```log
secret/co-training-db-quality-pod created
```

We will see later how to use this secret in sidecar proxy container. Don't forget to apply the same steps to the production environment. Generate a new secret and add it the
production namespace.

```shell script
$ gcloud container clusters get-credentials production-cluster
$ kubectl create secret generic co-training-db-production-pod --from-file=service_account.json=./co-training-293418-424fe37a9a47.json --namespace=co-training-production
```

### Deploy

So for the deployment, it is actually the job of the gitlab pipeline to do that. You can check the `gitlab-cicd.md` file in the `docs` directory for more details, but it will
be good also if we can deploy a test version manually to validate and test our kubernetes yaml files.

Let's say that we want to deploy a new release to the quality environment, to do so you have to follow these steps :

1) build a docker image of the app

If you have run the gitlab pipeline at least one time, i suppose that the app docker image is in your container registry. If you did not do it yet, you can start by running
the application on your local environment using the docker compose tool. You can follow the steps provided in the `readme` file from the `co-training-rest` module. The only
thing that you have to change is the image name that you can find in the `docker-compose.yml` file which is for now **mouhamedali/co-training-backend:0.0.1-DEV-SNAPSHOT**.

If you want to do it from scratch, run these commands from the project main directory:

```shell script
$ mvn clean verify
$ docker build --tag mouhamedali/co-training-backend:0.0.1-DEV-SNAPSHOT .
```

Feel free to change the docker image with your docker hub identifier otherwise you'll have issues when pushing it.

```shell script
$ docker image ls | grep co-training
```

```log
mouhamedali/co-training-backend                           0.0.1-DEV-SNAPSHOT                         0b74e2a46444        About a minute ago   155MB
```

2) push the docker image

Before going further, you have to login to your container registry. In my case docker hub :

```shell script
$ docker login
```

If you faced this error ** toomanyrequests: too many failed login attempts for username or IP address ** , run these command to delete your config :

```shell script
$ rm ~/.docker/config.json
```

Then retry the authentication.

Now you can push your docker image using this command (change your identifier, or the registry if it is not the same) :

```shell script
$ docker push mouhamedali/co-training-backend:0.0.1-DEV-SNAPSHOT
```

3) deploy it on the quality platform :

Go to the `quality` platform :

```shell script
$ gcloud container clusters get-credentials quality-cluster
```

Replace the image version used in the `k8s/quality/deployment.yaml` file with this version `0.0.1-DEV-SNAPSHOT` (and the image name if different). The tag in this file is an
environment variable named `$BUILD_VERSION` which is used by the pipeline to set the version to deploy. After changing it, make sure to not commit your changes because as i said
it is used by the pipeline.

```shell script
$ kubectl apply -f k8s/quality
```

```log
deployment.apps/co-training-deployment created
ingress.extensions/co-training-ingress created
persistentvolumeclaim/co-training-backend-logs-pvc created
service/co-training-backend-service created
```

```shell script
$ k get all -n co-training-quality
```

```log
NAME                                                  READY   STATUS    RESTARTS   AGE
pod/co-training-backend-deployment-6d8ffdf74f-clq6t   2/2     Running   0          4m33s

NAME                                  TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
service/co-training-backend-service   ClusterIP   10.4.13.118   <none>        8080/TCP   4m31s

NAME                                             READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/co-training-backend-deployment   1/1     1            1           4m33s

NAME                                                        DESIRED   CURRENT   READY   AGE
replicaset.apps/co-training-backend-deployment-6d8ffdf74f   1         1         1       4m33s
```

4) Do the same thing on the production cluster

```shell script
$ gcloud container clusters get-credentials production-cluster && kubectl apply -f k8s/production
```

```shell script
$ k get all -n co-training-production
```

```log
NAME                                                  READY   STATUS    RESTARTS   AGE
pod/co-training-backend-deployment-54d47c97d7-7j92k   2/2     Running   0          81s

NAME                                  TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
service/co-training-backend-service   ClusterIP   10.11.249.145   <none>        8080/TCP   81s

NAME                                             READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/co-training-backend-deployment   1/1     1            1           81s

NAME                                                        DESIRED   CURRENT   READY   AGE
replicaset.apps/co-training-backend-deployment-54d47c97d7   1         1         1       81s
```

4) logs

You can always check the pod logs using this command :

```shell script
$ kubectl logs pod_name -c container_name
```
or this one to check status :
```shell script
$ kubectl describe pod/pod_name
```
To get a shell to a running container :

```shell script
$ kubectl exec -it pod_name -- sh
```

Last thing is to check the Google Cloud logs viewer.

- https://console.cloud.google.com/logs

5) test

Go to the ingress tab ( from google web ui ) and get the public ip address of the ingress controller:

![ingress_installation](/uploads/7ac8dd01059d3628956559f7c1fc3816/ingress_installation.png)

You have now the quality and the production endpoints. Use it to call the application teams endpoint :

- quality

```shell script
$ curl http://34.78.124.45/backend/co-training/v1/teams
```

- production

```shell script
$ curl http://35.195.197.139/backend/co-training/v1/teams
```

Check the `co-training-rest` module for more tests.

6) clean up

To clean your cluster run these commands :

```shell script
$ gcloud container clusters get-credentials quality-cluster && kubectl delete -f k8s/quality
$ gcloud container clusters get-credentials production-cluster && kubectl delete -f k8s/production
```

Keep the secrets and the ingress in the cluster, so they can be used later by the pipeline when it deploys the app from the gitlab ci-cd.

## References

- https://gitlab.com/kubernetes-samples/gke

- https://cloud.google.com/sdk/gcloud/reference/container/clusters/list

- https://cloud.google.com/kubernetes-engine/docs/how-to/resizing-a-cluster

- https://cloud.google.com/blog/products/gcp/kubernetes-best-practices-organizing-with-namespaces