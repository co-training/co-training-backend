# Kubernetes Yaml Files

This directory contains the yaml kubernetes files to be deployed to the production platform. A full explanation of these file has been provided in the `k8s/quality` directory.
The only differences between these files and the ones provided in the `quality` platform are :

`deployment.yaml`

- The secret used to store the database parameters : its name is `backend-production-props` for the production platform

- The secret used by the proxy to establish the db connection and mounted as a volume : its names is `co-training-db-production-pod` for production 