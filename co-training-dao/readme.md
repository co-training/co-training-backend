# Company Training DAO ( Persistence Layer of the App )

## Description

This is a maven module which will be used by other modules. It contains all the entities objects which forms the persistence layer of our application.
This module contains also the Spring Data JPA interfaces, which can be used to facilitate the CRUD interaction with the database. 

## code structure

Here's our data model.

```
co-training-dao
│
│
└───converters                          : java package that contains the jpa converters of the application
│   │
│   └───LevelConverter                  : java class used to persist a Level enumeration to the database and to load it after from the db to an enumeration
│   │
│   └───StarsConverter                  : java class used to persist a Stars enumeration to the database and to load it after from the db to an enumeration
│
│
└───entities                            : java package that represents the persistence layer of the application
│   │
│   └───Review                          : java class    , This class represents a training review
│   │   │   id                          : Long          , This is the unique identifier of a review
│   │   │   stars                       : Stars         , This enumeration represents the number of stars of the review ( from one to five )
│   │   │   comment                     : String        , This is the comment associated to the review
│   │   │   isAnonymous                 : Boolean       , If this boolean is true we will not show the user name in the comment
│   │   │   author                      : User          , The user of the review
│   │   │   date                        : LocalDate     , The review date of creation
│   │   │   training                    : Training      , The training associated to the review
│   │
│   │
│   └───Tag                             : java class    , This class represents a training tag
│   │   │   id                          : Long          , This is the unique identifier of a tag
│   │   │   value                       : String        , This is the value of the tag
│   │
│   │
│   └───Team                            : java class    , This class represents a team
│   │   │   id                          : Long          , This is the unique identifier of a team
│   │   │   name                        : String        , This is the team name
│   │
│   │
│   └───Training                        : java class                , This class represents a training
│   │   │   id                          : Long                      , This is the unique identifier of a training
│   │   │   name                        : String                    , The training's name
│   │   │   description                 : String                    , The training's description
│   │   │   requirements                : List<TrainingRequirement> , The training's requirements ( to mention if the a participant should has some knowledge to take the training )
│   │   │   participants                : Set<User>                 , The training's participants
│   │   │   criterias                   : List<TrainingCriteria>    , The training's criterias ( to represent to who this training is destinated )
│   │   │   level                       : Level                     , The training's level of complexity ( we have three levels BEGINNER, INTERMEDIATE or ADVANCED )
│   │   │   tags                        : Set<Tag>                  , The training's tags
│   │   │   startingDate                : LocalDate                 , The starting date of the training
│   │   │   duration                    : Integer                   , The duration of the training ( in days )
│   │   │   minParticipants             : Integer                   , The minimum number of particpants to do the training ( 1 participant at leaset by default )
│   │   │   maxParticipants             : Integer                   , The maximum number of particpants ( 1 participant at leaset by default )
│   │   │   image                       : String                    , The training's image url
│   │   │   price                       : Float                     , The training's price
│   │   │   author                      : User                      , The training's author
│   │   │   reviews                     : List<Review>              , The training's reviews
│   │
│   │
│   └───TrainingCriteria                : java class    , This class represents a criteria for users who wants to particpate to the training
│   │   │   id                          : Long          , This is the unique identifier of a training criteria
│   │   │   description                 : String        , This is the description of the criteria ( for example this training is detinated to anyone who wants to know how to use a skateboard )
│   │
│   │
│   └───TrainingRequirement             : java class    , This class represents a requirement for a training
│   │   │   id                          : Long          , This is the unique identifier of a requirement
│   │   │   description                 : String        , This is the description of the requirement ( for example you should know the basics of javascript to take this training )
│   │
│   │
│   └───User                            : java class                    , This class represents a user of the application
│   │   │   id                          : Long                          , This is the unique identifier of a user
│   │   │   firstName                   : String                        , The user's first name
│   │   │   lastName                    : String                        , The user's last name
│   │   │   job                         : String                        , The user's job in the company
│   │   │   photo                       : String                        , The user's avatar photo url
│   │   │   about                       : String                        , A simple introduction of the user ( interests, hobbies, job, passtime ... )
│   │   │   interestsSkills             : List<UserInterestsSkills>     , The user's interests and skills
│   │   │   userResource                : UserResource                  , This is the resources of the user ( each user has a quota per year that cannot exceed )
│   │   │   team                        : Team                          , The user's team
│   │   │   whishlist                   : Set<Training>                 , The user's whish list ( a list of potential trainings ) 
│   │   │   followedTrainings           : Set<Training>                 , The trainings taken by the user
│   │   │   email                       : String                        , The user's email
│   │   │   isEmailEnabled              : Boolean                       , If true, the email will be displayed for other users
│   │   │   site                        : String                        , The user's site
│   │   │   isSiteEnabled               : Boolean                       , If true, the user site will be displayed for other users
│   │   │   linkedin                    : String                        , TThe user's linkedin account
│   │   │   isLinkedinEnabled           : Boolean                       , If true, the linkedin account will be displayed for other users
│   │   │   youtube                     : String                        , The user's youtube account
│   │   │   isYoutubeEnabled            : Boolean                       , If true, the youtube account will be displayed for other users
│   │   │   github                      : String                        , The user's github account
│   │   │   isGithubEnabled             : Boolean                       , If true, the user's github account will be displayed for other users
│   │   │   gitlab                      : String                        , The user's gitlab account
│   │   │   isGitlabEnabled             : Boolean                       , If true, the gitlab account will be displayed for other users
│   │   │   dockerHub                   : String                        , The user's docker hub account
│   │   │   isDockerHubEnabled          : Boolean                       , If true, the docker hub account will be displayed for other users
│   │   │   twitter                     : String                        , The user's twitter account
│   │   │   isTwitterEnabled            : Boolean                       , If true, the twitter account will be displayed for other users
│   │   │   facebook                    : String                        , The user's facebook account
│   │   │   isFacebookEnabled           : Boolean                       , If true, the user's facebook account will be displayed for other users
│   │   │   phoneNumber                 : String                        , The user's phone number
│   │   │   isPhoneNumberEnabled        : Boolean                       , If true, the user's phone number will be displayed for other users
│   │   │   birthday                    : LocalDate                     , This is the user's birthday
│   │
│   │
│   └───UserInterestsSkills             : java class    , This class represents the user interests and/or skills
│   │   │   id                          : Long          , This is the unique identifier of the skill
│   │   │   description                 : String        , This is the description of the skill / interest
│   │
│   │
│   └───UserResource                    : java class    , This class represents the user's resouces until the end of the year
│       │   id                          : Long          , This is the unique identifier of the user resources
│       │   budget                      : Float         , remaining money to spent on trainings until the end of the year
│       │   nbDays                      : Integer       , remaining number of days to spent on trainings until the end of the year
│       │   jackpot                     : Double        , If the user didn't spent his money this year or he earn money by giving trainings, this money will be transferred to his jackpot
│   
│
└───repository                          : java package that contains all jpa repositories of the application
│   │
│   └───ReviewRepository                : java class that contains the CRUD operations of the Review entity
│   │
│   └───TagRepository                   : java class that contains the CRUD operations of the Tag entity
│   │
│   └───TeamRepository                  : java class that contains the CRUD operations of the Team entity
│   │
│   └───TrainingRepository              : java class that contains the CRUD operations of the Training entity
│   │
│   └───UserRepository                  : java class that contains the CRUD operations of the User entity
│
   
```

## Database schema

Here's our db schema.

![db-diagram-dao](/uploads/2d425d674d24217081970aa7449183c9/db-diagram-dao.png)

## Some examples

Here's some json examples of the entities :

### Training

```json
{
    "id": 15,
  "name": "Spring boot tutorial",
  "description": "An introduction to the spring boot framework. This may be a markdown description, the frontend will apply a style in this case.",
  "requirements": [
        {
            "id": 80,
            "description": "A simple understanding of the java language"
        },
        {
            "id": 72,
            "description": "A simple understanding of the rest api"
        }
    ],
    "criterias": [
        {
            "id": 30,
            "description": "Anyone wants to understand how to build a rest api using the spring boot framework"
        }
    ],
    "level": "BEGINNER",
    "tags": [
        {
            "id": 45,
            "value": "spring-boot"
        },
        {
            "id": 67,
          "value": "rest-api"
        }
    ],
  "startingDate": "2020-08-26",
  "duration": 5,
  "minParticipants": 5,
  "maxParticipants": 25,
  "image": "https://the_training_url.jpg",
  "price": 9.99,
  "author": {
  }
}
```

The author in the training is of type User.

### User

```json
{
  "firstName": "Maximilian",
  "lastName": "Schwarzmüller",
  "job": "Professional Web Developer and Instructor",
  "team": {
    "id": 79,
        "name": "Promise"
  },
  "photo": "http://the_user_avatar_url.jpg",
  "about": "Starting out at the age of 13 I never stopped learning new programming skills and languages. Early I started creating websites for friends and just for fun as well. Besides web development I also explored Python and other non-web-only languages. This passion has ...",
    "interestsSkills": [
        {
            "id": 42,
            "description": "Software development"
        },
        {
            "id": 12,
            "description": "Business Administration"
        }
    ],
    "email": "example@gmail.com",
    "isEmailEnabled": true,
    "site": "https://academind.com",
    "isSiteEnabled": true,
    "linkedin": "https://de.linkedin.com/in/maximilian-schwarzm%C3%BCller-66b152a5",
    "isLinkedinEnabled": false,
    "youtube": "https://www.youtube.com/c/academind/",
    "isYoutubeEnabled": true,
    "github": "https://github.com/maxschwarzmueller",
    "isGithubEnabled": true,
    "gitlab": "https://gitlab.com/theopract/udemy-vue-01-the-monster-slayer",
    "isGitlabEnabled": true,
    "dockerHub": "https://www.docker.com/captains/bret-fisher",
    "isDockerHubEnabled": false,
    "twitter": "https://twitter.com/maxedapps?lang=fr",
    "isTwitterEnabeld": true,
    "facebook": "https://www.facebook.com/maximilian.schwarzmuller.3",
    "isFacebookEnabled": false,
    "phoneNumber": "0033613295681",
    "isPhoneNumberEnabled": false,
    "birthday": "2018-01-17"
}
```

### Resource

```json
{
  "id": 88,
  "budget": 99.0,
  "nbDays": 47.0,
  "jackpot": 0.0
}
```

### Review

```json
{
  "id": 1,
  "stars": "3",
  "comment": "This is a great tutorial.",
  "isAnonymous": true,
  "author": {
    "firstName": "anonymous",
    "lastName": "Anonymous",
    "job": null,
    "team": null,
    "photo": null,
    "about": null,
    "interestsSkills": null,
    "email": null,
    "isEmailEnabled": null,
    "site": null,
    "isSiteEnabled": null,
    "linkedin": null,
    "isLinkedinEnabled": null,
    "youtube": null,
    "isYoutubeEnabled": null,
    "github": null,
    "isGithubEnabled": null,
    "gitlab": null,
    "isGitlabEnabled": null,
    "dockerHub": null,
    "isDockerHubEnabled": null,
    "twitter": null,
    "isTwitterEnabled": null,
    "facebook": null,
    "isFacebookEnabled": null,
    "phoneNumber": null,
    "isPhoneNumberEnabled": null,
    "birthday": null,
    "id": null
  },
  "date": "2021-01-22",
  "training": {
    "id": 1,
    "name": "Vuejs Tutorial",
    "description": "# Training title\npresent your training here\n\n## Introduction\n### Prerequisites\nWhat things you need to install the software and how to install them\n* [Maven](https://maven.apache.org/) - Dependency Management\n  * Nested Item\n* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds\n  * Another Nested Item\n\n### Installing\n1. First step. For some styling, use *This text will be italic* or **This text will be bold**\n2. Second step\n  1. Indented item\n  2. Indented item\n3. Third step\n\nTable of librairies to download for your os :\n\nFirst Header  |  Second Header |   Third Header\n--------------|-------------------------------------------\ncell 1        |  cell 2        |  cell 3\ncell 4        |  cell 5        |  cell 6\n\n## Architecture\nThe framework architecture\n        ",
    "requirements": null,
    "criterias": null,
    "level": "INTERMEDIATE",
    "tags": null,
    "startingDate": "2021-01-29",
    "duration": 1,
    "minParticipants": 1,
    "maxParticipants": 3,
    "image": null,
    "price": 9.99,
    "author": {
      "firstName": "Gerard",
      "lastName": "Lavoine",
      "job": "Scrum master",
      "team": {
        "id": 2,
        "name": "finance"
      },
      "photo": null,
      "about": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "interestsSkills": [
        {
          "id": 1,
          "description": "Software development"
        },
        {
          "id": 2,
          "description": "Business Administration"
        }
      ],
      "email": null,
      "isEmailEnabled": false,
      "site": null,
      "isSiteEnabled": false,
      "linkedin": null,
      "isLinkedinEnabled": false,
      "youtube": null,
      "isYoutubeEnabled": false,
      "github": null,
      "isGithubEnabled": false,
      "gitlab": null,
      "isGitlabEnabled": false,
      "dockerHub": null,
      "isDockerHubEnabled": false,
      "twitter": null,
      "isTwitterEnabled": false,
      "facebook": "https://facebook/accounts/#user",
      "isFacebookEnabled": true,
      "phoneNumber": null,
      "isPhoneNumberEnabled": false,
      "birthday": null,
      "id": 88
    }
  }
}
```

## Unit Tests

In the tests resources, you can find some tests to make sure that the application behaves as expected :

![tests-daopng](/uploads/bf5f78aa3ceddf800358af4de343f417/tests-daopng.png)

I've used junit 4 to make tests but i'll use the version 5 of the junit in the other modules.

## References

Spring data jpa : https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods

One to many : https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/