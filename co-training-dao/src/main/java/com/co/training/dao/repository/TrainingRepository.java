package com.co.training.dao.repository;

import com.co.training.dao.entities.Training;
import com.co.training.dao.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface TrainingRepository extends JpaRepository<Training, Long> {

    List<Training> findByStartingDateBetween(LocalDate start, LocalDate end, Pageable pageable);

    Page<Training> getByStartingDateBetween(LocalDate start, LocalDate end, Pageable pageable);

    @Query("SELECT t.participants FROM Training t WHERE t.id = :trainingId")
    List<User> getTrainingParticipants(@Param("trainingId") Long trainingId, Pageable pageable);

    @Query("SELECT t.participants FROM Training t WHERE t.id = :trainingId")
    List<User> getTrainingParticipants(@Param("trainingId") Long trainingId);

    @Query("SELECT COUNT(t) as total FROM Training t JOIN t.participants p WHERE t.id = :trainingId")
    Long countTrainingParticipants(@Param("trainingId") Long trainingId);

    @Query("SELECT COUNT(t) as total FROM Training t JOIN t.reviews r WHERE t.id = :trainingId")
    Long countTrainingReviews(@Param("trainingId") Long trainingId);

    List<Training> findByAuthorId(Long authorId);

    Long countByAuthorId(Long authorId);
}
