package com.co.training.dao.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Data
@Accessors(chain = true)
public class UserResource {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * remaining money to spent on trainings ( from now to the last of the year)
     */
    @Column(nullable = false)
    private Double budget = 0.0;

    /**
     * remaining days to spent on trainings ( from now to the last of the year)
     * nb days is not an integer because user can define define half a day
     */
    @Column(nullable = false)
    private Float nbDays = 0.0f;

    /**
     * user's jackpot
     * If the user didn't spent his money this year or he earn money by giving trainings, this money will be transferred
     * to his jackpot
     */
    @Column(nullable = false)
    private Double jackpot = 0.0;
}
