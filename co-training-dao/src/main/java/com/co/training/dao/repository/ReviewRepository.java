package com.co.training.dao.repository;

import com.co.training.dao.entities.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {

    Page<Review> findAll(Pageable pageable);

    List<Review> getByTrainingId(Long trainingId, Pageable pageable);

    List<Review> getByTrainingId(Long trainingId);

    List<Review> getByAuthorId(Long userId, Pageable pageable);

    List<Review> getByStars(Review.Stars stars, Pageable pageable);

    List<Review> getByTrainingIdAndAuthorId(Long trainingId, Long userId, Pageable pageable);

    Long countByTrainingIdAndAuthorId(Long trainingId, Long userId);

    List<Review> getByTrainingIdAndStars(Long trainingId, Review.Stars stars, Pageable pageable);

    List<Review> getByStarsAndAuthorId(Review.Stars stars, Long userId, Pageable pageable);

    List<Review> getByTrainingIdAndAuthorIdAndStars(Long trainingId, Long userId, Review.Stars stars, Pageable pageable);
}
