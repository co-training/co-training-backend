package com.co.training.dao.repository;

import com.co.training.dao.entities.Tag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Optional<Tag> getByValue(String value);

    List<Tag> findByValueStartsWith(String tag, Pageable pageable);
}
