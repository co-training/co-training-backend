package com.co.training.dao.repository;

import com.co.training.dao.entities.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TeamRepository extends JpaRepository<Team, Long> {

    Optional<Team> getByName(String name);

    List<Team> findAllByOrderByNameAsc();
}
