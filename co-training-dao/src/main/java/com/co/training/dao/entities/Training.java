package com.co.training.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "training")
public class Training {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "the training name is mandatory")
    @Size(min = 10, max = 200, message = "you have to introduce 10 to 200 characters for the training name field")
    @Column(nullable = false)
    private String name;

    @NotBlank(message = "the training description is mandatory")
    @Size(min = 25, max = 5000, message = "you have to introduce 25 to 5000 characters for the training description field")
    @Column(nullable = false)
    private String description;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "training_id")
    private List<TrainingRequirement> requirements = new ArrayList<>();
    // Hibernate will use PersistentBag as implementation for lists

    @ManyToMany(mappedBy = "followedTrainings")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private Set<User> participants = new HashSet<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "training_id")
    private List<TrainingCriteria> criterias = new ArrayList<>();

    @NotNull(message = "the training level is a mandatory field")
    @Column(nullable = false)
    private Level level;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    // save or update operations will propagate to tag entity
    @JoinTable(
            name = "training_tag",
            joinColumns = @JoinColumn(name = "training_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    @EqualsAndHashCode.Exclude
    private Set<Tag> tags = new HashSet<>();
    // Hibernate will use PersistentSet as an implementation for sets

    @FutureOrPresent(message = "starting date cannot be in the past")
    // validate that training date is today or in the future
    @Column(nullable = false)
    private LocalDate startingDate;

    @Positive(message = "duration must be a positive number")
    @Column(nullable = false)
    private Integer duration;

    @Min(value = 1, message = "minParticipants cannot be lower than one")
    @Column(nullable = false)
    private Integer minParticipants;

    @Min(value = 1, message = "maxParticipants cannot be lower than one")
    @Column(nullable = false)
    private Integer maxParticipants;

    private String image;

    @PositiveOrZero(message = "price cannot be negative")
    @Column(nullable = false)
    private Float price;

    @NotNull(message = "the author of the training must be introduced")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "author_id")
    private User author;

    @OneToMany(
            mappedBy = "training",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    // ignore this field otherwise it will cause a stackoverflow exception (because we already have the Training in the Review entity)
    // https://www.baeldung.com/jackson-bidirectional-relationships-and-infinite-recursion#json-ignore
    private List<Review> reviews = new ArrayList<>();

    public enum Level {
        BEGINNER, INTERMEDIATE, ADVANCED
    }

    public Training addRequirement(TrainingRequirement requirement) {
        this.getRequirements().add(requirement);
        return this;
    }

    public Training addCriteria(TrainingCriteria criteria) {
        this.getCriterias().add(criteria);
        return this;
    }

    public Training addTag(Tag tag) {
        this.getTags().add(tag);
        return this;
    }

    public Training addReview(Review review) {
        this.getReviews().add(review);
        review.setTraining(this);
        return this;
    }

    public Training removeReview(Review review) {
        this.getReviews().remove(review);
        review.setTraining(null);
        return this;
    }
}
