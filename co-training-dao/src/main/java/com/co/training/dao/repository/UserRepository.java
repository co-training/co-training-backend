package com.co.training.dao.repository;

import com.co.training.dao.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {


    @Query("SELECT COUNT(u) as total FROM User u JOIN u.whishlist WHERE u.id = :userId")
    Long countUserWhishlist(@Param("userId") Long userId);
}
