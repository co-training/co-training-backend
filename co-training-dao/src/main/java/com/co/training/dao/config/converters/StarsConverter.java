package com.co.training.dao.config.converters;

import com.co.training.dao.entities.Review;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

/**
 * This the stars converter that will be used to stores the stars enum to database and retrieve a stars instance from database
 * JPA 2.1 release introduced a new standardized API that can be used to convert an entity attribute to a database value and vice versa.
 */
@Converter(autoApply = true)
public class StarsConverter implements AttributeConverter<Review.Stars, String> {

    @Override
    public String convertToDatabaseColumn(Review.Stars stars) {
        if (stars == null) {
            return null;
        }
        return stars.getValue();
    }

    @Override
    public Review.Stars convertToEntityAttribute(String value) {
        if (value == null) {
            return null;
        }

        return Stream.of(Review.Stars.values())
                .filter(c -> c.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("%s is not recognized as a stars value", value)
                ));
    }
}
