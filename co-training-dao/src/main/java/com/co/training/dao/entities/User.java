package com.co.training.dao.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "`user`")
// issue with table user on postgres : https://stackoverflow.com/questions/4350874/unable-to-use-table-named-user-in-postgresql-hibernate
@Data
@Accessors(chain = true)
public class User {

    @Id
    private Long id;

    // we are using spring boot validation (which uses JSR 380) to validate a user dto
    @NotBlank(message = "first name is mandatory")
    @Size(min = 2, message = "you have to introduce at least 2 characters for the first name field")
    @Column(nullable = false)
    private String firstName;

    @NotBlank(message = "last name is mandatory")
    @Size(min = 2, message = "you have to introduce at least 2 characters for the last name field")
    @Column(nullable = false)
    private String lastName;

    @NotBlank(message = "job name is mandatory")
    @Size(min = 4, message = "you have to introduce at least 4 characters for the job field")
    @Column(nullable = false)
    private String job;

    private String photo;

    @Column(columnDefinition = "text")
    private String about;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "user_id")
    private List<UserInterestsSkills> interestsSkills = new ArrayList<>();

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @MapsId
    /*
     * check the usage of this annotation here : https://vladmihalcea.com/the-best-way-to-map-a-onetoone-relationship-with-jpa-and-hibernate/
     */
    private UserResource userResource;

    @NotNull(message = "You have to mention you team, this object must not be empty")
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "team_id")
    private Team team;

    /**
     * This is the list of trainings chosen by the user to be his whishlist
     */
    @ManyToMany
    @JoinTable(
            name = "whishlist",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "training_id"))
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    // User entity contains lists of trainings so we will ignore it here to avoid the stackoverflow
            Set<Training> whishlist = new HashSet<>();

    /**
     * This is the trainings that the user has participated to them
     */
    @ManyToMany
    @JoinTable(
            name = "training_participants",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "training_id"))
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    Set<Training> followedTrainings = new HashSet<>();

    /*
     * This annotation has been added at the end of the development of the app because the user deletion wasn't possible
     * that's why you'll notice that this collection is not used in the entities search or other features ...
     * check the issue and the solutions here (the hibernate OnDelete worked for reviews but not for trainings)
     * https://stackoverflow.com/questions/7197181/jpa-unidirectional-many-to-one-and-cascading-delete
     */
    @OneToMany(
            mappedBy = "author",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private List<Training> trainings = new ArrayList<>();

    /**
     * this is the email that will be show to other users in the platform. It will be the user's personal email
     * This is not the email used to log into the application
     */
    private String email;

    private Boolean isEmailEnabled;

    private String site;

    private Boolean isSiteEnabled;

    private String linkedin;

    private Boolean isLinkedinEnabled;

    private String youtube;

    private Boolean isYoutubeEnabled;

    private String github;

    private Boolean isGithubEnabled;

    private String gitlab;

    private Boolean isGitlabEnabled;

    private String dockerHub;

    private Boolean isDockerHubEnabled;

    private String twitter;

    private Boolean isTwitterEnabled;

    private String facebook;

    private Boolean isFacebookEnabled;

    private String phoneNumber;

    private Boolean isPhoneNumberEnabled;

    @NotNull(message = "You have to mention you birthday, this object must not be empty")
    @Past(message = "birthday must be in the past") // validate that a date value is in the past
    @Column(nullable = false)
    private LocalDate birthday;

    public User addUserInterestsSkills(UserInterestsSkills interestsSkills) {
        this.getInterestsSkills().add(interestsSkills);
        return this;
    }

    public User addToWhishlist(Training training) {
        this.getWhishlist().add(training);
        return this;
    }

    public User addToFollowedTrainings(Training training) {
        this.getFollowedTrainings().add(training);
        training.getParticipants().add(this);
        return this;
    }

    public User removeFromFollowedTrainings(Training training) {
        this.getFollowedTrainings().remove(training);
        training.getParticipants().remove(this);
        return this;
    }
}
