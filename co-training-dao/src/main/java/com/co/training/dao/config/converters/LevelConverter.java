package com.co.training.dao.config.converters;

import com.co.training.dao.entities.Training;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

/**
 * JPA 2.1 release introduced a new standardized API that can be used to convert an entity attribute to a database value and vice versa.
 */
@Converter(autoApply = true)
public class LevelConverter implements AttributeConverter<Training.Level, String> {

    @Override
    public String convertToDatabaseColumn(Training.Level level) {
        if (level == null) {
            return null;
        }
        return level.name();
    }

    @Override
    public Training.Level convertToEntityAttribute(String name) {
        if (name == null) {
            return null;
        }

        return Stream.of(Training.Level.values())
                .filter(c -> c.name().equals(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(
                        String.format("%s is not recognized as a level name", name)
                ));
    }
}
