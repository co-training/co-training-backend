package com.co.training.dao.entities;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Data
@Accessors(chain = true)
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "The evaluation is mandatory")
    @Column(nullable = false)
    private Stars stars;

    @Size(min = 25, max = 1000, message = "you have to introduce 25 to 1000 characters for the training description field")
    private String comment;

    // by default the comment review will be shown in the reviews area
    @NotNull(message = "You have to specify if the comment will anonymous or not")
    private Boolean isAnonymous = false;

    // unidirectional relation ship
    // as this is a unidirectional relation mapped with many to one, we cannot cascade the deletion of the review author. to do so
    // we have to make it bidirectional like we did with trainings or to user this hibernate annotation will remove child
    // https://stackoverflow.com/questions/7197181/jpa-unidirectional-many-to-one-and-cascading-delete
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotNull(message = "The author of the review must be present")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private User author;

    @PastOrPresent(message = "The review creation date must not be in the future")
    @Column(nullable = false)
    private LocalDate date;

    @NotNull(message = "The training of the review must be present")
    @ManyToOne(optional = false)
    @JoinColumn(name = "training_id")
    @ToString.Exclude
    private Training training;

    public enum Stars {

        ONE("1"), TWO("2"), THREE("3"), FOUR("4"), FIVE("5");

        @JsonValue
        // This field holds the enums values which means after deserialization the value of the enum will be 1, 2 ... and not ONE, TWO ...
        // https://www.baeldung.com/jackson-serialize-enums
        private String value;

        Stars(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
