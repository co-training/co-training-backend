package com.co.training.test.utils;

import com.co.training.dao.entities.*;
import com.co.training.dao.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class TrainingTestUtils {

    public static final String NAME = "Scrum introduction";
    public static final String DESCRIPTION = "An introduction to the scrum framework";
    public static final String REQUIREMENT_DESCRIPTION = "No experience is necessary";
    public static final String CRITERIA1_DESCRIPTION = "Those interested in working within an Agile team";
    public static final String CRITERIA2_DESCRIPTION = "Current Agile team members that want to deepen their understanding";
    public static final String TAG_VALUE = "scrum";
    public static final int DURATION = 2;
    public static final LocalDate TODAY = LocalDate.now();
    public static final int MIN_PARTICIPANTS = 5;
    public static final int MAX_PARTICIPANTS = 12;
    public static final float PRICE = 50f;
    public static final Review.Stars STARS = Review.Stars.FIVE;
    public static final String COMMENT = "Very informative and well planned training";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserTestUtils userTestUtils;

    /**
     * This method is used to build a training object with some values to do tests after
     */
    public Training buildTrainingObject() {

        Training training = new Training();

        training.setName(NAME);
        training.setDescription(DESCRIPTION);

        // add a training requirement
        TrainingRequirement requirement = new TrainingRequirement();
        requirement.setDescription(REQUIREMENT_DESCRIPTION);
        training.addRequirement(requirement);

        // add training criterias
        TrainingCriteria criteria1 = new TrainingCriteria();
        criteria1.setDescription(CRITERIA1_DESCRIPTION);
        TrainingCriteria criteria2 = new TrainingCriteria();
        criteria2.setDescription(CRITERIA2_DESCRIPTION);
        training.addCriteria(criteria1).addCriteria(criteria2);

        training.setLevel(Training.Level.BEGINNER);

        // add a tag to the training
        Tag tag = new Tag();
        tag.setValue(TAG_VALUE);
        training.addTag(tag);

        training.setStartingDate(TODAY);
        training.setDuration(DURATION);
        training.setMinParticipants(MIN_PARTICIPANTS);
        training.setMaxParticipants(MAX_PARTICIPANTS);
        training.setPrice(PRICE);

        // add training author
        User author = userRepository.save(userTestUtils.buildUserData());
        training.setAuthor(author);

        // add a review
        Review review = new Review();
        review.setComment(COMMENT);
        review.setAuthor(author);
        review.setDate(TODAY);
        review.setIsAnonymous(true);
        review.setStars(STARS);
        training.addReview(review);

        return training;
    }
}
