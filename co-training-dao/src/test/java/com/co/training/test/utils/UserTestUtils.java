package com.co.training.test.utils;

import com.co.training.dao.entities.Team;
import com.co.training.dao.entities.User;
import com.co.training.dao.entities.UserResource;
import com.co.training.dao.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;

@Service
public class UserTestUtils {

    public static final String FIRST_NAME = "Jack";
    public static final String LAST_NAME = "BESOS";
    public static final String JOB = "Manager";
    public static final String TEAM_NAME = "supply";
    public static final String EMAIL = "email@gmail.com";
    public static final String DOCKER_HUB = "https://hub.docker.com/_/nginx";
    public static final LocalDate BIRTHDAY = LocalDate.of(1991, Month.APRIL, 14);
    public static final float NB_DAYS = 5.0f;
    public static final double BUDGET = 300.50;

    @Autowired
    private TeamRepository teamRepository;

    /**
     * This method is used to build a user object with some values to do tests after
     */
    public User buildUserData() {

        User user = new User();
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setJob(JOB);
        user.setEmail(EMAIL);
        user.setIsEmailEnabled(true);
        user.setDockerHub(DOCKER_HUB);
        user.setIsDockerHubEnabled(false);
        user.setBirthday(BIRTHDAY);

        // create a new user resource, for a new user, user resources will be empty until the admin fill these values
        UserResource userResource = new UserResource();
        userResource.setNbDays(NB_DAYS);
        userResource.setBudget(BUDGET);
        user.setUserResource(userResource);

        // user's team
        Team team = new Team();
        if (teamRepository.getByName(TEAM_NAME).isPresent()) {

            team = teamRepository.getByName(TEAM_NAME).get();
        } else {

            team.setName(TEAM_NAME);
            team = teamRepository.save(team);
        }
        user.setTeam(team);

        return user;
    }
}
