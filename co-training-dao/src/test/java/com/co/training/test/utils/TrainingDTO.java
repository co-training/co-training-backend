package com.co.training.test.utils;

import com.co.training.dao.entities.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This object will be used to detach a Training object from the persistence context of hibernate
 * we can't for example compare a persisted object to an another built in our code because of implementations
 * used by hibernate. For lists, he used PersistentBag and not Arraylist for example.
 */
@EqualsAndHashCode
@Getter
@NoArgsConstructor  // we have to add this constructor because it will be used by jackson for serialize/deserialize
@ToString
// TODO to be deleted
public class TrainingDTO {

    @EqualsAndHashCode.Exclude
    private Training training;

    private Long id;
    private String name;
    private String description;
    private Training.Level level;
    private LocalDate startingDate;
    private Integer duration;
    private Integer minParticipants;
    private Integer maxParticipants;
    private String image;
    private Float price;
    private User author;
    private List<TrainingRequirement> requirements;

    @EqualsAndHashCode.Exclude
    // tags, participants and reviews are ignored from the comparison in the training entity
    private Set<User> participants;

    private List<TrainingCriteria> criterias;

    @EqualsAndHashCode.Exclude
    private Set<Tag> tags;

    @EqualsAndHashCode.Exclude
    private List<Review> reviews;

    public TrainingDTO(Training training) {
        this.training = training;
        Assert.notNull(training, "Training object must not be null");

        setId(training.getId());
        setName(training.getName());
        setDescription(training.getDescription());
        setLevel(training.getLevel());
        setStartingDate(training.getStartingDate());
        setDuration(training.getDuration());
        setMinParticipants(training.getMinParticipants());
        setMaxParticipants(training.getMaxParticipants());
        setImage(training.getImage());
        setPrice(training.getPrice());
        setAuthor(training.getAuthor());
        setRequirements(training.getRequirements());
        setCriterias(training.getCriterias());
        setParticipants(training.getParticipants());
        setTags(training.getTags());
        setReviews(training.getReviews());
    }

    private void setId(Long id) {
        this.id = id;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setDescription(String description) {
        this.description = description;
    }

    private void setLevel(Training.Level level) {
        this.level = level;
    }

    private void setStartingDate(LocalDate startingDate) {
        this.startingDate = startingDate;
    }

    private void setDuration(Integer duration) {
        this.duration = duration;
    }

    private void setMinParticipants(Integer minParticipants) {
        this.minParticipants = minParticipants;
    }

    private void setMaxParticipants(Integer maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    private void setImage(String image) {
        this.image = image;
    }

    private void setPrice(Float price) {
        this.price = price;
    }

    private void setAuthor(User author) {
        this.author = author;
    }

    private void setRequirements(List<TrainingRequirement> requirements) {
        this.requirements = new ArrayList<>();
        if (requirements != null) {
            requirements.forEach(
                    requirement -> this.requirements.add(requirement)
            );
        }
    }

    private void setParticipants(Set<User> participants) {
        this.participants = new HashSet<>();
        if (participants != null) {
            participants.forEach(
                    participant -> this.participants.add(participant)
            );
        }
    }

    private void setCriterias(List<TrainingCriteria> criterias) {
        this.criterias = new ArrayList<>();
        if (criterias != null) {
            criterias.forEach(
                    criteria -> this.criterias.add(criteria)
            );
        }
    }

    private void setTags(Set<Tag> tags) {
        this.tags = new HashSet<>();
        if (tags != null) {
            tags.forEach(
                    tag -> this.tags.add(tag)
            );
        }
    }

    private void setReviews(List<Review> reviews) {
        this.reviews = new ArrayList<>();
        if (reviews != null) {
            reviews.forEach(
                    review -> this.reviews.add(review)
            );
        }
    }
}
