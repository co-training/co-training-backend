package com.co.training.dao.repository;

import com.co.training.dao.DaoApplication;
import com.co.training.dao.entities.User;
import com.co.training.test.utils.UserTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContextManager;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

@RunWith(Parameterized.class)
@ContextConfiguration(classes = {DaoApplication.class})
/**
 * As i think that the junit 4 integration with the spring test is not well done, i will separate the two features.
 * I faced many issues in the integration like tests that are launched as parameterized but they are not, not expected behaviors and
 * limitations of using the spring features like @Sql which does not launch the script before the method start
 */
public class UserRepositoryParameterizedTest {

    @Autowired
    private UserTestUtils userTestUtils;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserRepository userRepository;

    private TestContextManager testContextManager;

    @Before
    public void init() throws Exception {

        /*
         * It's a little hard to integrate junit parameterized with spring, we have to load the spring context manually
         * https://www.baeldung.com/springjunit4classrunner-parameterized
         */
        this.testContextManager = new TestContextManager(getClass());
        this.testContextManager.prepareTestInstance(this);
    }

    /*
     * Init the test parameters using a junit 4 configuration
     */

    @Parameterized.Parameter(value = 0)
    public String firstName;

    @Parameterized.Parameter(value = 1)
    public String lastName;

    @Parameterized.Parameter(value = 2)
    public String job;

    @Parameterized.Parameter(value = 3)
    public LocalDate birthday;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Collection<Object[]> params = new ArrayList<>();
        // parameters : first name , last name , job name , birthday
        params.add(new Object[]{"", "", "", null});
        params.add(new Object[]{"b", "a", "abc", LocalDate.now().plusYears(5)});
        params.add(new Object[]{null, null, null, LocalDate.now().plusDays(5)});
        params.add(new Object[]{"     ", "\t          ", "   ", LocalDate.now().plusMonths(5)});
        return params;
    }

    @Test(expected = RuntimeException.class)
    public void saveUser_FirstName_ExceptionThrown() {

        // Test scenarios for this scenario are , first name is empty , null,  blank , equal to b ,
        // check the data method
        User user = userTestUtils.buildUserData();
        user.setFirstName(firstName);
        userRepository.save(user);
    }

    @Test(expected = RuntimeException.class)
    public void saveUser_LastName_ExceptionThrown() {

        User user = userTestUtils.buildUserData();
        user.setLastName(lastName);
        userRepository.save(user);
    }

    @Test(expected = RuntimeException.class)
    public void saveUser_JobName_ExceptionThrown() {

        User user = userTestUtils.buildUserData();
        user.setJob(job);
        userRepository.save(user);
    }

    @Test(expected = RuntimeException.class)
    public void saveUser_Birthday_ExceptionThrown() {

        User user = userTestUtils.buildUserData();
        user.setBirthday(birthday);
        userRepository.save(user);
    }

    // As i said, i didn't user parameters in this method but it's running 4 times
    @Test(expected = RuntimeException.class)
    public void saveUser_TeamNull_ExceptionThrown() {
        User user = userTestUtils.buildUserData();
        user.setTeam(null);
        userRepository.save(user);
    }
}
