package com.co.training.dao.repository;

import com.co.training.dao.DaoApplication;
import com.co.training.dao.entities.Training;
import com.co.training.test.utils.TrainingTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContextManager;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

@RunWith(Parameterized.class)
@ContextConfiguration(classes = {DaoApplication.class})
/**
 * This class is used to test some training repository features using the junit parameterized tests
 */
public class TrainingRepositoryParameterizedTest {

    @Autowired
    private TrainingTestUtils trainingTestUtils;

    @Autowired
    private TrainingRepository trainingRepository;

    private TestContextManager testContextManager;

    @Before
    public void init() throws Exception {

        this.testContextManager = new TestContextManager(getClass());
        this.testContextManager.prepareTestInstance(this);
    }

    /*
     * Init the test parameters using a junit 4 configuration
     */

    @Parameterized.Parameter(value = 0)
    public String name;

    @Parameterized.Parameter(value = 1)
    public String description;

    @Parameterized.Parameter(value = 2)
    public LocalDate startingDate;

    @Parameterized.Parameter(value = 3)
    public Integer minParticipants;

    @Parameterized.Parameter(value = 4)
    public Integer duration;

    @Parameterized.Parameter(value = 5)
    public Integer maxParticipants;

    @Parameterized.Parameter(value = 6)
    public Float price;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Collection<Object[]> params = new ArrayList<>();
        // parameters : name , description , startingDate , minParticipants
        params.add(new Object[]{"", "", null, null, null, null, null});
        params.add(new Object[]{"under 10 ", "under 25 chars", LocalDate.now().minusWeeks(5), 0, -1, 0, -0.5f});
        params.add(new Object[]{null, null, LocalDate.now().minusYears(5), -1, -10, -1, -0.55f});
        params.add(new Object[]{"     \t", "\t          ", LocalDate.now().minusMonths(5), -2, -100, -2, -10.100f});
        params.add(new Object[]{"More than 200 ipsum elementum ullamcorper ch'ai lotto-owe Chulien so merciipsumvielmols Pfourtz ! non réchime Carola pellentesque elit Miss Dahlias rossbolla id nullam semper Spätzle kartoffelsalad vulputate eget knack tristique  rucksack",
                "more than 5000 Lorem Elsass ipsum elementum ullamcorper ch'ai lotto-owe Chulien so merci vielmols Pfourtz ! non réchime Carola pellentesque elit Miss Dahlias rossbolla id nullam semper Spätzle Oberschaeffolsheim Salut bisamme wurscht mollis yeuh. geht's morbi et chambon Gal. munster leverwurscht ac quam, jetz gehts los aliquam varius gravida Huguette salu id, habitant bredele Heineken Hans schnaps kartoffelsalad vulputate eget knack tristique  rucksack sit wie eleifend tchao bissame blottkopf, dignissim hop amet in, adipiscing nüdle bissame libero, hoplageiss Wurschtsalad ante dolor auctor, porta sagittis Mauris hopla Richard Schirmeck Morbi libero, sed turpis, risus, knepfle kuglopf und tellus DNA, non flammekueche mänele hopla leo schneck lacus barapli Chulia Roberstau placerat ornare dui gewurztraminer vielmols, Gal ! quam. amet purus Salu bissame turpis suspendisse Christkindelsmärik sit rhoncus sed Strasbourg libero. ornare CoopéschnapsTruchtersheim Verdammi consectetur piconschnaps Oberschaeffolsheim"
                        + "Lorem Elsass ipsum elementum ullamcorper ch'ai lotto-owe Chulien so merci vielmols Pfourtz ! non réchime Carola pellentesque elit Miss Dahlias rossbolla id nullam semper Spätzle Oberschaeffolsheim Salut bisamme wurscht mollis yeuh. geht's morbi et chambon Gal. munster leverwurscht ac quam, jetz gehts los aliquam varius gravida Huguette salu id, habitant bredele Heineken Hans schnaps kartoffelsalad vulputate eget knack tristique  rucksack sit wie eleifend tchao bissame blottkopf, dignissim hop amet in, adipiscing nüdle bissame libero, hoplageiss Wurschtsalad ante dolor auctor, porta sagittis Mauris hopla Richard Schirmeck Morbi libero, sed turpis, risus, knepfle kuglopf und tellus DNA, non flammekueche mänele hopla leo schneck lacus barapli Chulia Roberstau placerat ornare dui gewurztraminer vielmols, Gal ! quam. amet purus Salu bissame turpis suspendisse Christkindelsmärik sit rhoncus sed Strasbourg libero. ornare CoopéschnapsTruchtersheim Verdammi consectetur piconschnaps Oberschaeffolsheim"
                        + "Lorem Elsass ipsum elementum ullamcorper ch'ai lotto-owe Chulien so merci vielmols Pfourtz ! non réchime Carola pellentesque elit Miss Dahlias rossbolla id nullam semper Spätzle Oberschaeffolsheim Salut bisamme wurscht mollis yeuh. geht's morbi et chambon Gal. munster leverwurscht ac quam, jetz gehts los aliquam varius gravida Huguette salu id, habitant bredele Heineken Hans schnaps kartoffelsalad vulputate eget knack tristique  rucksack sit wie eleifend tchao bissame blottkopf, dignissim hop amet in, adipiscing nüdle bissame libero, hoplageiss Wurschtsalad ante dolor auctor, porta sagittis Mauris hopla Richard Schirmeck Morbi libero, sed turpis, risus, knepfle kuglopf und tellus DNA, non flammekueche mänele hopla leo schneck lacus barapli Chulia Roberstau placerat ornare dui gewurztraminer vielmols, Gal ! quam. amet purus Salu bissame turpis suspendisse Christkindelsmärik sit rhoncus sed Strasbourg libero. ornare CoopéschnapsTruchtersheim Verdammi consectetur piconschnaps Oberschaeffolsheim"
                        + "Lorem Elsass ipsum elementum ullamcorper ch'ai lotto-owe Chulien so merci vielmols Pfourtz ! non réchime Carola pellentesque elit Miss Dahlias rossbolla id nullam semper Spätzle Oberschaeffolsheim Salut bisamme wurscht mollis yeuh. geht's morbi et chambon Gal. munster leverwurscht ac quam, jetz gehts los aliquam varius gravida Huguette salu id, habitant bredele Heineken Hans schnaps kartoffelsalad vulputate eget knack tristique  rucksack sit wie eleifend tchao bissame blottkopf, dignissim hop amet in, adipiscing nüdle bissame libero, hoplageiss Wurschtsalad ante dolor auctor, porta sagittis Mauris hopla Richard Schirmeck Morbi libero, sed turpis, risus, knepfle kuglopf und tellus DNA, non flammekueche mänele hopla leo schneck lacus barapli Chulia Roberstau placerat ornare dui gewurztraminer vielmols, Gal ! quam. amet purus Salu bissame turpis suspendisse Christkindelsmärik sit rhoncus sed Strasbourg libero. ornare CoopéschnapsTruchtersheim Verdammi consectetur piconschnaps Oberschaeffolsheim"
                        + "Lorem Elsass ipsum elementum ullamcorper ch'ai lotto-owe Chulien so merci vielmols Pfourtz ! non réchime Carola pellentesque elit Miss Dahlias rossbolla id nullam semper Spätzle Oberschaeffolsheim Salut bisamme wurscht mollis yeuh. geht's morbi et chambon Gal. munster leverwurscht ac quam, jetz gehts los aliquam varius gravida Huguette salu id, habitant bredele Heineken Hans schnaps kartoffelsalad vulputate eget knack tristique  rucksack sit wie eleifend tchao bissame blottkopf, dignissim hop amet in, adipiscing nüdle bissame libero, hoplageiss Wurschtsalad ante dolor auctor, porta sagittis Mauris hopla Richard Schirmeck Morbi libero, sed turpis, risus, knepfle kuglopf und tellus DNA, non flammekueche mänele hopla leo schneck lacus barapli Chulia Roberstau placerat ornare dui gewurztraminer vielmols, Gal ! quam. amet purus Salu bissame turpis suspendisse Christkindelsmärik sit rhoncus sed Strasbourg libero. ornare CoopéschnapsTruchtersheim Verdammi consectetur piconschnaps Oberschaeffolsheim",
                LocalDate.now().minusDays(5), -3, -1000, -3, -100.5f});
        return params;
    }

    @Test(expected = RuntimeException.class)
    public void saveTraining_Name_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setName(name);
        trainingRepository.save(training);
    }

    @Test(expected = RuntimeException.class)
    public void saveTraining_Description_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setDescription(description);
        trainingRepository.save(training);
    }

    // This test will be launched more than one time by junit, strange because it's not a parameterized test
    @Test(expected = RuntimeException.class)
    public void saveTraining_LevelNull_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setLevel(null);
        trainingRepository.save(training);
    }

    @Test(expected = RuntimeException.class)
    public void saveTraining_StartingDate_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setStartingDate(startingDate);
        trainingRepository.save(training);
    }

    @Test(expected = RuntimeException.class)
    public void saveTraining_Duration_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setDuration(duration);
        trainingRepository.save(training);
    }

    @Test(expected = RuntimeException.class)
    public void saveTraining_MinParticipants_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setMinParticipants(minParticipants);
        trainingRepository.save(training);
    }

    @Test(expected = RuntimeException.class)
    public void saveTraining_MaxParticipants_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setMaxParticipants(maxParticipants);
        trainingRepository.save(training);
    }

    @Test(expected = RuntimeException.class)
    public void saveTraining_Price_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setPrice(price);
        trainingRepository.save(training);
    }

    @Test(expected = RuntimeException.class)
    public void saveTraining_AuthorNull_ExceptionThrown() {

        Training training = trainingTestUtils.buildTrainingObject();
        training.setAuthor(null);
        trainingRepository.save(training);
    }
}
