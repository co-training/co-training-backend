package com.co.training.dao.repository;

import com.co.training.dao.entities.Tag;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql(scripts = "/sql/tag/delete.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class TagRepositoryTest {

    public static final String SPRING_BOOT = "spring-boot";

    /*
     * We a are using this test naming convention to name methods : [UnitOfWork_StateUnderTest_ExpectedBehavior]
     */

    @Autowired
    private TagRepository tagRepository;

    @Test
    public void saveTag_NewTag_SuccessfullySaved() {
        Tag tag = new Tag();
        tag.setValue(SPRING_BOOT);
        tagRepository.save(tag);

        Optional<Tag> dbTag = tagRepository.getByValue(SPRING_BOOT);
        Assertions.assertThat(dbTag).isPresent().get().isEqualTo(tag);
    }

    @Sql("/sql/tag/insert.sql")
    @Test
    public void saveTag_ExistTag_ExceptionThrown() {

        Tag tag = new Tag();
        tag.setValue(SPRING_BOOT);
        org.junit.jupiter.api.Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
            tagRepository.save(tag);
        });
    }

    @Test
    public void saveTag_NullTag_ExceptionThrown() {

        Tag tag = new Tag();
        tag.setValue(null);
        org.junit.jupiter.api.Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
            tagRepository.save(tag);
        });
    }

    @Sql("/sql/tag/insert.sql")
    @Test
    public void getTags_3TagsDataBase_Return3Tags() {

        Assertions.assertThat(tagRepository.findAll().size()).isEqualTo(3);
    }
}
