package com.co.training.dao;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
/**
 * as this project contains only model classes we have to add this annotation to bootstrap
 * a spring context only to run tests
 * https://www.baeldung.com/spring-boot-unable-to-find-springbootconfiguration-with-datajpatest
 */
@ComponentScan(basePackages = {"com.co.training.dao", "com.co.training.test"})
public class DaoApplication {
}
