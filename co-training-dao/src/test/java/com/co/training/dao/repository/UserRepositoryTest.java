package com.co.training.dao.repository;

import com.co.training.dao.entities.Team;
import com.co.training.dao.entities.User;
import com.co.training.dao.entities.UserInterestsSkills;
import com.co.training.dao.entities.UserResource;
import com.co.training.test.utils.UserTestUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql(scripts = "/sql/delete-data.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UserRepositoryTest {

    public static final String DESCRIPTION = "I love travel and discover new countries";

    @Autowired
    private UserTestUtils userTestUtils;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserRepository userRepository;

    @Test
    @Sql("/sql/team/insert.sql")
    public void saveUser_NewUser_SuccessfullySaved() {

        // create a new user
        User user = userTestUtils.buildUserData();

        // user's interests and skills
        user.setInterestsSkills(Arrays.asList(
                new UserInterestsSkills(null, DESCRIPTION)
        ));

        userRepository.save(user);

        // not a test but a method to validate the save method
        getUser_ExistUser_ValidateTheExistUser(user.getId());
    }

    /**
     * In this method we will get the user resource from the database ( using the passed id )
     * and test its properties values against what we have filled in the static constants
     *
     * @param id
     */
    private void getUser_ExistUser_ValidateTheExistUser(Long id) {

        Optional<User> userDB = userRepository.findById(id);
        Assertions.assertThat(userDB).isPresent();

        User user = userDB.get();
        Assertions.assertThat(user.getUserResource().getNbDays()).isEqualTo(userTestUtils.NB_DAYS);
        Assertions.assertThat(user.getUserResource().getBudget()).isEqualTo(userTestUtils.BUDGET);

        Assertions.assertThat(user.getFirstName()).isEqualTo(userTestUtils.FIRST_NAME);
        Assertions.assertThat(user.getLastName()).isEqualTo(userTestUtils.LAST_NAME);
        Assertions.assertThat(user.getJob()).isEqualTo(userTestUtils.JOB);

        Assertions.assertThat(user.getInterestsSkills()).isNotEmpty().hasSize(1);
        Assertions.assertThat(user.getInterestsSkills().get(0)).isNotNull();
        Assertions.assertThat(user.getInterestsSkills().get(0).getDescription()).isEqualTo(DESCRIPTION);

        Assertions.assertThat(user.getTeam()).isNotNull();
        Assertions.assertThat(user.getTeam().getName()).isEqualTo(userTestUtils.TEAM_NAME);

        Assertions.assertThat(user.getEmail()).isEqualTo(userTestUtils.EMAIL);
        Assertions.assertThat(user.getIsEmailEnabled()).isTrue();
        Assertions.assertThat(user.getDockerHub()).isEqualTo(userTestUtils.DOCKER_HUB);
        Assertions.assertThat(user.getIsDockerHubEnabled()).isFalse();
        Assertions.assertThat(user.getYoutube()).isNull();
        Assertions.assertThat(user.getIsYoutubeEnabled()).isNull();

    }

    @Test
    @Sql("/sql/team/insert.sql")
    // it does not work if you place this annotation in the class level to be run before each method, the user script
    // will be launched before the insert in that case
    @Sql("/sql/user/insert.sql")
    public void updateUser_UpdateSeveralProperties_SuccessfullyUpdated() {

        // check the insert sql script to get the used user id
        Optional<User> optionalUser = userRepository.findById(99L);
        Assertions.assertThat(optionalUser).isPresent();

        User user = optionalUser.get();
        // now we will change the user data
        user.setFirstName(userTestUtils.FIRST_NAME);
        user.setLastName(userTestUtils.LAST_NAME);
        user.setJob(userTestUtils.JOB);
        user.setEmail(userTestUtils.EMAIL);
        user.setIsEmailEnabled(true);
        user.setDockerHub(userTestUtils.DOCKER_HUB);
        user.setIsDockerHubEnabled(false);
        user.setBirthday(userTestUtils.BIRTHDAY);

        // update user resource
        UserResource userResource = user.getUserResource();
        Assertions.assertThat(userResource).isNotNull();
        userResource.setNbDays(userTestUtils.NB_DAYS);
        userResource.setBudget(userTestUtils.BUDGET);

        // update user's interests and skills
        Assertions.assertThat(user.getInterestsSkills()).isNotEmpty();
        user.getInterestsSkills().get(0).setDescription(DESCRIPTION);

        // update user's team
        Optional<Team> team = teamRepository.getByName(userTestUtils.TEAM_NAME);
        Assertions.assertThat(team).isPresent();
        user.setTeam(team.get());

        getUser_ExistUser_ValidateTheExistUser(99L);
    }

    @Test
    @Sql("/sql/team/insert.sql")
    @Sql("/sql/user/insert.sql")
    public void deleteUser_ExistUser_SuccessfullyDeleted() {

        userRepository.deleteById(99L);
        Assertions.assertThat(
                userRepository.findById(99L)
        ).isNotPresent();

        // we should not delete a team if the user has been deleted
        Optional<Team> team = teamRepository.getByName(userTestUtils.TEAM_NAME);
        Assertions.assertThat(team).isPresent();
    }
}
