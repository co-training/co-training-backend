package com.co.training.dao.repository;

import com.co.training.dao.entities.*;
import com.co.training.test.utils.TrainingDTO;
import com.co.training.test.utils.TrainingTestUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql(scripts = "/sql/delete-data.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class TrainingRepositoryTest {

    @Autowired
    private TrainingTestUtils trainingTestUtils;

    @Autowired
    private TrainingRepository trainingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Test
    @Sql("/sql/team/insert.sql")
    @Sql("/sql/user/insert.sql")
    public void saveTraining_NewTraining_SuccessfullySaved() {

        Training training = trainingTestUtils.buildTrainingObject();
        // save the training
        Training savedTraining = trainingRepository.save(training);
        Assertions.assertThat(training).isEqualTo(savedTraining);
    }

    @Test
    @Sql("/sql/team/insert.sql")
    @Sql("/sql/tag/insert.sql")
    @Sql("/sql/user/insert.sql")
    @Sql("/sql/training/insert.sql")
    public void getTraining_ExistTraining_ValidateTheExistTraining() {

        Optional<Training> trainingOptional = trainingRepository.findById(55L);
        Assertions.assertThat(trainingOptional).isNotEmpty();

        Training training = trainingOptional.get();
        Assertions.assertThat(training).isNotNull();
        Assertions.assertThat(training.getId()).isEqualTo(55L);

        // check mandatory fields
        Assertions.assertThat(training.getName()).isEqualTo(trainingTestUtils.NAME);
        Assertions.assertThat(training.getDescription()).isEqualTo(trainingTestUtils.DESCRIPTION);
        Assertions.assertThat(training.getDuration()).isEqualTo(trainingTestUtils.DURATION);
        Assertions.assertThat(training.getMinParticipants()).isEqualTo(trainingTestUtils.MIN_PARTICIPANTS);
        Assertions.assertThat(training.getMaxParticipants()).isEqualTo(trainingTestUtils.MAX_PARTICIPANTS);
        Assertions.assertThat(training.getPrice()).isEqualTo(trainingTestUtils.PRICE);
        Assertions.assertThat(training.getLevel()).isEqualTo(Training.Level.BEGINNER);

        // check requirements
        Assertions.assertThat(training.getRequirements()).hasSize(1);
        Assertions.assertThat(training.getRequirements().get(0).getDescription()).isEqualTo(trainingTestUtils.REQUIREMENT_DESCRIPTION);

        // check criterias
        Assertions.assertThat(training.getCriterias()).hasSize(2);
        Assertions.assertThat(training.getCriterias().get(0).getDescription()).isEqualTo(trainingTestUtils.CRITERIA1_DESCRIPTION);
        Assertions.assertThat(training.getCriterias().get(1).getDescription()).isEqualTo(trainingTestUtils.CRITERIA2_DESCRIPTION);

        // check tags
        Assertions.assertThat(training.getTags()).hasSize(1);
        training.getTags().forEach(tag -> Assertions.assertThat(tag.getValue()).isEqualTo(trainingTestUtils.TAG_VALUE));

        // check the author
        Assertions.assertThat(training.getAuthor()).isNotNull();
        Assertions.assertThat(training.getAuthor().getId()).isEqualTo(99L);

        // check reviews
        Assertions.assertThat(training.getReviews()).hasSize(1);
        Assertions.assertThat(training.getReviews().get(0).getStars()).isEqualTo(trainingTestUtils.STARS);
        Assertions.assertThat(training.getReviews().get(0).getComment()).isEqualTo(trainingTestUtils.COMMENT);

    }

    @Test
    @Sql("/sql/team/insert.sql")
    @Sql("/sql/user/insert.sql")
    @Sql("/sql/training/update.sql")
    public void updateTraining_UpdateSeveralProperties_SuccessfullyUpdated() {

        Optional<Training> persistedTraining = trainingRepository.findById(55L);
        Assertions.assertThat(persistedTraining).isPresent();

        Training trainingToUpdate = persistedTraining.get();
        fillTrainingObject(trainingToUpdate);
        trainingRepository.save(trainingToUpdate);
        // the training has been updated, we will create another object (from outside the persistence context) to perform the comparison

        Training training = new Training();
        fillTrainingObject(training);
        // This object contains only data and not ids, fill it with ids from ids. otherwise we can't perform the comparison
        fillIds(training, trainingToUpdate);

        // change the two objects to dto (otherwise the comparison fails because of the implementations of lists and sets)
        // as hibernate will proxy objects and lists, you can't compare objects from the hibernate context to others created in the app
        TrainingDTO actualDto = new TrainingDTO(trainingToUpdate);
        TrainingDTO expectedDto = new TrainingDTO(training);

        expectedDto.equals(actualDto);
        Assertions.assertThat(expectedDto).isEqualTo(actualDto);
    }


    /**
     * init a training object with some data
     *
     * @param training
     */
    private void fillTrainingObject(Training training) {
        training.setName(trainingTestUtils.NAME);
        training.setDescription(trainingTestUtils.DESCRIPTION);

        // add a training requirement
        TrainingRequirement requirement = new TrainingRequirement();
        requirement.setDescription(trainingTestUtils.REQUIREMENT_DESCRIPTION);
        training.addRequirement(requirement);

        // add training criterias
        TrainingCriteria criteria1 = new TrainingCriteria();
        criteria1.setDescription(trainingTestUtils.CRITERIA1_DESCRIPTION);
        TrainingCriteria criteria2 = new TrainingCriteria();
        criteria2.setDescription(trainingTestUtils.CRITERIA2_DESCRIPTION);
        training.addCriteria(criteria1).addCriteria(criteria2);

        training.setLevel(Training.Level.BEGINNER);

        // add a tag to the training
        Tag tag = new Tag();
        tag.setValue(trainingTestUtils.TAG_VALUE);
        training.addTag(tag);

        training.setStartingDate(trainingTestUtils.TODAY);
        training.setDuration(trainingTestUtils.DURATION);
        training.setMinParticipants(trainingTestUtils.MIN_PARTICIPANTS);
        training.setMaxParticipants(trainingTestUtils.MAX_PARTICIPANTS);
        training.setPrice(trainingTestUtils.PRICE);

        // add training author
        Optional<User> persistedUser = userRepository.findById(99L);
        Assertions.assertThat(persistedUser).isPresent();
        training.setAuthor(
                persistedUser.get()
        );

        // add a review
        Review review = new Review();
        review.setComment(trainingTestUtils.COMMENT);
        review.setAuthor(persistedUser.get());
        review.setDate(trainingTestUtils.TODAY);
        review.setIsAnonymous(true);
        review.setStars(trainingTestUtils.STARS);
        training.addReview(review);
    }

    private void fillIds(Training training, Training trainingToUpdate) {

        training.setId(trainingToUpdate.getId());
        training.getRequirements().get(0).setId(
                trainingToUpdate.getRequirements().get(0).getId()
        );
        training.getCriterias().get(0).setId(
                trainingToUpdate.getCriterias().get(0).getId()
        );
        training.getCriterias().get(1).setId(
                trainingToUpdate.getCriterias().get(1).getId()
        );
        training.getTags().forEach(
                tag -> tag.setId(trainingToUpdate.getTags().iterator().next().getId())
        );
        training.getReviews().get(0).setId(trainingToUpdate.getReviews().get(0).getId());
    }

    @Test
    @Sql("/sql/team/insert.sql")
    @Sql("/sql/tag/insert.sql")
    @Sql("/sql/user/insert.sql")
    @Sql("/sql/training/insert.sql")
    public void deleteTraining_ExistTraining_SuccessfullyDeleted() {

        trainingRepository.deleteById(55L);

        Optional<Training> persistedTraining = trainingRepository.findById(55L);
        Assertions.assertThat(persistedTraining).isNotPresent();

        // Trainings must not delete tags
        Optional<Tag> tag = tagRepository.getByValue(trainingTestUtils.TAG_VALUE);
        Assertions.assertThat(tag).isPresent();

        // reviews must be deleted after deleting the training
        Assertions.assertThat(reviewRepository.findAll()).isEmpty();

        // users must not be deleted (author or participants)
        Assertions.assertThat(userRepository.findAll()).hasSize(2);
    }
}
