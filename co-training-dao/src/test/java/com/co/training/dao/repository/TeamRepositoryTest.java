package com.co.training.dao.repository;

import com.co.training.dao.entities.Team;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@Sql(scripts = "/sql/team/delete.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class TeamRepositoryTest {

    public static final String BOYS_TEAM = "b-team";

    /*
     * We a are using this test naming convention to name methods : [UnitOfWork_StateUnderTest_ExpectedBehavior]
     */

    @Autowired
    private TeamRepository teamRepository;

    @Test
    public void saveTeam_NewTeam_SuccessfullySaved() {
        Team team = new Team();
        team.setName(BOYS_TEAM);
        teamRepository.save(team);

        Optional<Team> teamDB = teamRepository.getByName(BOYS_TEAM);
        Assertions.assertThat(teamDB).isPresent().get().isEqualTo(team);
    }

    @Sql("/sql/team/insert.sql")
    @Test
    public void saveTeam_ExistTeam_ExceptionThrown() {

        Team team = new Team();
        team.setName(BOYS_TEAM);
        org.junit.jupiter.api.Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
            teamRepository.save(team);
        });
    }

    @Test
    public void saveTeam_NullTeam_ExceptionThrown() {

        Team team = new Team();
        team.setName(null);
        org.junit.jupiter.api.Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
            teamRepository.save(team);
        });
    }

    @Sql("/sql/team/insert.sql")
    @Test
    public void getTeams_8TeamsDataBase_Return8Teams() {

        Assertions.assertThat(teamRepository.findAll().size()).isEqualTo(8);
    }
}
