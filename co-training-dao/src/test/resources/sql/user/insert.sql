-- add a new user resource
insert into user_resource (id, budget, jackpot, nb_days) values (99, 100, 990.0, 7);

-- add a new user
insert into "user"
(birthday, docker_hub, is_docker_hub_enabled, first_name, job, last_name, team_id, user_resource_id) values
('2004-12-27', 'https://hub.docker.com/java', TRUE, 'Michel', 'BUTCHER','DUBOIS', 8, 99);

-- add a new user_interests_skills
insert into user_interests_skills (id, description, user_id) values (5, 'Taking photos', 99);

-------------------------------------------------------------------------------------------

-- add a new user resource
insert into user_resource (id, budget, jackpot, nb_days) values (88, 0.0, 0.0, 0);

-- add a new user
insert into "user"
(birthday, first_name, job, last_name, team_id, user_resource_id) values
('2004-12-27', 'first_name', 'job','last_name', 2, 88);