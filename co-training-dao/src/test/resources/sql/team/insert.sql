insert into team (id, name) values (1,'supply');
insert into team (id, name) values (2,'finance');
insert into team (id, name) values (3,'customs');
insert into team (id, name) values (4,'transport');
insert into team (id, name) values (5,'aws');
insert into team (id, name) values (6,'b&b');
insert into team (id, name) values (7,'g-team');
insert into team (id, name) values (8,'b-team');