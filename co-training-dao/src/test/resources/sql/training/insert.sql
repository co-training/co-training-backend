-- add training details
-- add a training
insert into training
(id, author_id, description, duration, image, level, max_participants, min_participants, name, price, starting_date)
values
(55, 99, 'An introduction to the scrum framework', 2, null, 'BEGINNER', 12, 5, 'Scrum introduction', 50, CURRENT_DATE);

-- add a training requirement
insert into training_requirement (id, description, training_id) values (1, 'No experience is necessary', 55);

-- add the training criterias
insert into training_criteria (id, description, training_id) values (1, 'Those interested in working within an Agile team', 55);
insert into training_criteria (id, description, training_id) values (2, 'Current Agile team members that want to deepen their understanding', 55);

-- bind tag & training
insert into training_tag (training_id,tag_id) values (55,2);

-- add a review
insert into review (id, "author_user_resource_id", comment, date, is_anonymous, stars, training_id)
values
(1, 99, 'Very informative and well planned training', CURRENT_DATE, TRUE, '5', 55);